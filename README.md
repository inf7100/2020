# INF7100 - session d'été 2020
## Initiation à la science des données et à l'intelligence artificielle

* [Syllabus](syllabus.md)
* Partie 1 - **Initiation au langage python** (Jean-Hugues Roy)
    * Cours 1 - [Notions de base](https://gitlab.com/inf7100/2020/-/blob/master/1%20-%20python/INF7100-python1.md)
    * TP1 - [Corrigé](https://gitlab.com/inf7100/2020/-/blob/master/1%20-%20python/TP1-corrige.ipynb)
    * Cours 2 - [Lire des données dans des CSV et des API](https://gitlab.com/inf7100/2020/-/blob/master/1%20-%20python/INF7100-python2.ipynb)
    * Cours 3 - [Moissonner des données dans le ouaibe](https://gitlab.com/inf7100/2020/-/blob/master/1%20-%20python/INF7100-python3.ipynb)
    * Cours 4 (1) - [Gérer les dates (avec, en boni, la commande `try`)](https://gitlab.com/inf7100/2020/-/blob/master/1%20-%20python/INF7100-datetime+try.ipynb)
    * Cours 4 (2) - [Traitement du langage naturel (avec spaCy)](https://gitlab.com/inf7100/2020/-/blob/master/1%20-%20python/INF7100-python4-avec-spaCy.ipynb)
    * Cours 4 (3) - [Traitement du langage naturel (avec NLTK)](https://gitlab.com/inf7100/2020/-/blob/master/1%20-%20python/INF7100-python4-avec-NLTK.ipynb)
    * Cours 5 - [Pandas](https://gitlab.com/inf7100/2020/-/blob/master/1%20-%20python/INF7100-python5.ipynb)
    * TP3 - [Corrigé](https://gitlab.com/inf7100/2020/-/blob/master/1%20-%20python/TP3-corrige.ipynb)
* Partie 2 - **Initiation à la science des données** (Arthur Charpentier)
* Partie 3 - **Initiation au big data et à l'apprentissage automatique** (Marie-Jean Meurs)