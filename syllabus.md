# Syllabus

# UQAM – Faculté des sciences
### Plan de cours  -  Été 2020

# INF7100
## Initiation à la science des données et à l'intelligence artificielle

Les mardis et jeudis de 14h00 à 17h00 en ligne ([plateforme Zoom](https://uqam.zoom.us/))

Enseignants (en ordre de présence dans le syllabus) :
- Jean-Hugues Roy (roy.jean-hugues@uqam.ca) ou [jhroy](https://gitlab.com/jhroy) sur gitlab
- Arthur Charpentier (charpentier.arthur@uqam.ca) ou [mjmeurs](https://gitlab.com/mjmeurs) sur gitlab
- Marie-Jean Meurs (meurs.marie-jean@uqam.ca) ou [freakonometrics](https://gitlab.com/freakonometrics) sur gitlab

https://gitlab.com/inf7100

# Objectifs

L'objectif général est de former une prochaine génération de chercheuses et de chercheurs apte à créer ses propres outils numériques et à comprendre les avantages, mais aussi les limites, de l'informatique en général et de l'«intelligence artificielle» en particulier dans un contexte de recherche dans toutes les disciplines (sciences, sciences humaines et sociales, etc.).

# Contenu du cours

Le cours vise l'acquisition de compétences en science des données et IA :      

- L'initiation à la programmation au moyen d'un langage informatique d'usage général;
- La recherche d'information;
- Le traitement des données massives;
- La maîtrise de notions fondamentales d'apprentissage automatique.

Le cours comprend des exercices pratiques.

# Calendrier

### 01 - [Mardi, 5 mai 2020](https://uqam.zoom.us/j/95325866601) - Tous trois
- Description du syllabus
- Test [vidéo](https://uqam.hosted.panopto.com/Panopto/Pages/Viewer.aspx?id=8be157db-7d57-474c-8a2f-abac012cc7f1)
- Entente d'évaluation
- Attentes des étudiant.e.s
- Motivations et objectifs du cours
- Connexion à l'environnement : [Carnets Azure](https://notebooks.azure.com).

### 02 - Jeudi, 7 mai 2020 – JH Roy
- Python 1 : introduction générale, types de variables, boucles et conditions

### 03 - Mardi, 12 mai 2020 – JH Roy
- Python 2 : lire et créer des fichiers

### 04 - Jeudi, 14 mai 2020 – JH Roy
- Python 3 : interaction avec APIs; moissonnage de données dans le web

### 05 - Mardi, 19 mai 2020 – JH Roy
- Python 4 : analyse de données avec pandas

### 06 - Jeudi, 21 mai 2020 – JH Roy
- Python 5 : antichambre du traitement automatique du langage naturel (TAL) avec NLTK

### 07 - Mardi, 26 mai 2020 – A Charpentier
- Introduction à la science de données    
- Expérimentation vs. observation (courte introduction aux modèles causaux)    

### 08 - Jeudi, 28 mai 2020 – A Charpentier
- Introduction à la visualisation    
- Modélisation statistique    

### 09 - Mardi, 2 juin 2020 – A Charpentier
- Corrélation et régression    

### 10 - Jeudi, 4 juin 2020 – A Charpentier
- Algorithmes de classification    

### 11 - Mardi, 9 juin 2020 – MJ Meurs
- Données massives : définition, collecte, traitement     
- Applications industrielles, stratégie et régulation    

### 12 - Jeudi, 11 juin 2020 – MJ Meurs
- Recherche d'information et indexation

### 13 - Mardi, 16 juin 2020 – MJ Meurs
- Introduction à l'apprentissage automatique

### 14 - Jeudi, 18 juin 2020 – MJ Meurs
- Réglementation, données personnelles    

### 15 - Mardi, 23 juin 2020 - Tous trois
- Présentations des projets 

# Modalités d'évaluation

Description sommaire        |Date de remise / passation sur Moodle |Pondération
----------------------------|--------------------------------------|-----------:
Travaux de programmation    | Du 07 mai au 25 mai                  |25%
Travaux science des données | Du 27 mai au 09 juin                 |25%
Travaux IA                  | Du 10 juin au 22 juin                |25%
Projet de session           | Mardi 30 juin avant 23h59            |25%

Aucune remise n'est acceptée après la date et l'heure de remise.

L'utilisation de livres et de documentation personnelle est permise pour réaliser les travaux. 

Les travaux sont individuels.

## Détail des évaluations

### Python

- **TP1 – Premiers pas en python (5%)**<br>
Remettre votre carnet dans Moodle avant le **11 mai, 9&nbsp;h**. Inclure le plus de commentaires possibles (ou des blocs de texte en markdown) pour expliquer ce que vous faites. Donner à votre fichier le nom suivant&nbsp;: *TP1-<votre_code_permanent>.ipynb*

- **TP2 – Moissonnage (10%)**
Remettre votre carnet dans Moodle avant le **18 mai, 9&nbsp;h**. Inclure le plus de commentaires possibles (ou des blocs de texte en markdown) pour expliquer ce que vous faites. Donner à votre fichier le nom suivant&nbsp;: *TP2-<votre_code_permanent>.ipynb*

- **TP3 – Analyse de données dans Pandas (10%)**
Remettre votre carnet dans Moodle avant le **25 mai, 9&nbsp;h**. Inclure le plus de commentaires possibles (ou des blocs de texte en markdown) pour expliquer ce que vous faites. Donner à votre fichier le nom suivant&nbsp;: *TP3-<votre_code_permanent>.ipynb*

### Science des données

- **TP4 : Moissonnage (10%)**
- **TP5 : Prédiction (10%)** : prévoir une des variables (régression)
- **TP6 : Visualisation (5%)**
Les modalités de remise seront indiquées pendant la session.

### Données massives et intelligence artificielle
- Synthèse 1 : Données massives - outils et pratiques (10%)
- Synthèse 2 : Indexation - application (5%)
- Synthèse 3 : Apprentissage automatique - étude de cas (10%)

### Projet final

Le projet est un travail individuel.<br> 
Il pourra porter sur des sujets en lien avec les travaux et recherches de l'étudiant.e.<br>  
Le sujet de projet devra être validé par les enseignant.e.s au plus tard le **01 juin**.<br>
Le projet sera présenté oralement lors du dernier cours de la session, le mardi 23 juin.<br>
Le rapport de projet (5 à 10 pages, format **.pdf**) devra être remis au plus tard le mardi 30 juin.

La présentation orale vaut 5%, le rapport 20%.  

Toutes les remises seront effectuées sur [Moodle](https://www.moodle2.uqam.ca/).

# Politiques et règlements

Le cours a beau se dérouler en ligne, les politiques et règlements de l'UQAM demeurent en vigueur.

### [Règlement 18 sur l'intégrité académique](http://r18.uqam.ca/)

En cas de plagiat ou de fraude, la sanction peut aller de la note zéro pour le travail ou
l'examen, jusqu'à l'exclusion de l'université. Le règlement concernant l'intégrité académique sera strictement appliqué. Pour
plus de renseignements, consultez [ce site](http://www.sciences.uqam.ca/etudiants/integrite-academique.html)

### Politiques 16 et 42

Nous nous engageons à vous offrir un milieu d'apprentissage exempt de quelque forme de harcèlement que ce soit en vertu de la [Politique 16](https://instances.uqam.ca/wp-content/uploads/sites/47/2019/04/Politique_no_16_2.pdf) sur le harcèlement de nature sexuelle et de la [Politique 42](https://instances.uqam.ca/wp-content/uploads/sites/47/2018/05/Politique_no_42.pdf) sur le harcèlement psychologique.

# Médiagraphie

Cairo, A. (2016). *The Truthful Art : Data, Charts, and Maps for Communication*. San Francisco : New Riders. 523 pages.

Cardon, D. (2019). [*Culture numérique*](http://virtuolien.uqam.ca/tout/UQAM_BIB001398478). Paris : Presses de sciences po. ISBN: 978-2-7246-2367-3

Gray, J., Bounegru, L. (2019). *The Data Journalism Handbook 2: Towards a Critical Data Practice*. [En ligne](https://datajournalismhandbook.org)

Green, B. (2020). *The False Promise of Risk Assessments: Epistemic Reform and the Limits of Fairness*. [Proceedings of the ACM Conference on Fairness, Accountability, and Transparency (FAT*)](https://www.benzevgreen.com/20-fat-risk/)

Green, B. (2019). *'Good' isn't good enough*. [NeurIPS Joint Workshop on AI for Social Good](https://www.benzevgreen.com/19-ai4sg/)

Herzog, D. (2016). [*Data Literacy. A User's Guide*](http://virtuolien.uqam.ca/tout/UQAM_BIB001262176). Thousand Oaks : Sage Publications, Inc. 205 pages.

Wilke, Claus O. *Fundamentals of Data Visualization*. [En ligne](https://serialmentor.com/dataviz/)
