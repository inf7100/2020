<h1>Table des matières<span class="tocSkip"></span></h1>
<div class="toc"><ul class="toc-item"><li><span><a><span class="toc-item-num">1  </span>Calculs de base</a></span></li><li><span><a><span class="toc-item-num">2  </span>Sept grands types de variables</a></span><ul class="toc-item"><li><span><a><span class="toc-item-num">2.1  </span>Nombres entiers</a></span></li><li><span><a><span class="toc-item-num">2.2  </span>Nombres décimaux</a></span></li><li><span><a><span class="toc-item-num">2.3  </span>Booléens</a></span></li><li><span><a><span class="toc-item-num">2.4  </span>None</a></span></li><li><span><a><span class="toc-item-num">2.5  </span>Texte</a></span><ul class="toc-item"><li><span><a><span class="toc-item-num">2.5.1  </span>Transformations de types</a></span></li><li><span><a><span class="toc-item-num">2.5.2  </span>Fonctions et méthodes utiles pour traiter du texte</a></span></li><li><span><a><span class="toc-item-num">2.5.3  </span>La notion d'«index»</a></span></li></ul></li><li><span><a><span class="toc-item-num">2.6  </span>Listes</a></span><ul class="toc-item"><li><span><a><span class="toc-item-num">2.6.1  </span>Fonctions et méthodes relatives aux listes</a></span></li><li><span><a><span class="toc-item-num">2.6.2  </span>Notion d'index avec les listes</a></span></li></ul></li><li><span><a><span class="toc-item-num">2.7  </span>Dictionnaires</a></span></li></ul></li><li><span><a><span class="toc-item-num">3  </span>Boucles</a></span><ul class="toc-item"><li><span><a><span class="toc-item-num">3.1  </span>Boucles <code>for</code></a></span><ul class="toc-item"><li><span><a><span class="toc-item-num">3.1.1  </span>La notion de compteur</a></span></li></ul></li><li><span><a><span class="toc-item-num">3.2  </span>Boucles <code>while</code></a></span></li></ul></li><li><span><a><span class="toc-item-num">4  </span>Conditions</a></span></li></ul></div>

# Python 1 - Principes fondamentaux

<p style="font-size: 1.5em;">Carnet commenté des notions présentées lors du premier cours d'initiation au langage Python, le 7 mai 2020, par Zoom.</p>
<hr>

<h3 style="background: pink; text-align: center; padding: 10px;">Calculs de base</h3>

Python permet notamment d'effectuer toutes sortes de calculs. Des plus simples...


```python
2+2
```




    4



... aux plus complexes


```python
87656454345345354*876545678976546789765
```




    76834886290817418559209302118957501810



Pour exprimer des exposants, on utilise deux astérisques consécutifs.


```python
10**9
```




    1000000000



L'opérateur **modulo (```%```)** nous donne le nombre entier qu'il reste après la division d'un nombre par un autre.

Par exemple, **101%2** va donner 1 parce que si on divise 101 par 2, le nombre entier qu'on obtient est 50 et il nous reste 1.


```python
101%2
```




    1



Si on fait 100%2, il nous reste 0.

C'est ainsi que modulo peut être utile si on cherche à déterminer si un nombre est pair ou impair. Si le résultat d'un %2 est 0, on est en présence d'un nombre pair, si le résultat est 1, on est alors en présence d'un nombre impair.


```python
100%2
```




    0



<h3 style="background: pink; text-align: center; padding: 10px;">Sept grands types de variables</h3>

Les variables sont des contenants. Le contenu qu'on y place détermine le type que prendra cette variable.

Vous pouvez donnez à vos variables les noms que vous voulez. Choisissez les noms les plus descriptifs possibles. Par convention, tâchez de ne donner que des noms en minuscules. Si le nom que vous souhaitez donner comporte plusieurs mots, séparez-les par une barre de soulignement (```nom_de_variable```) ou en mettant en majuscules la première lettre de tous les mots, sauf le premier (```nomDeVariable```).

**Évitez aussi de mettre des accents dans vos noms de variables**.

Il y a plusieurs types de variables possibles en python. On va voir les sept plus usités, dans mon expérience.

<h4 style="background: cyan; text-align: center; padding: 10px;">Nombres entiers</h4>

Dans l'exemple ci-dessous, on place, à l'aide de l'opérateur ```=``` le nombre entier 55 dans la variable ```a```.


```python
a = 55
```


```python
a
```




    55



Ce faisant, la variable ```a``` est donc de type ```int``` pour *integer* (la traduction anglaise de **nombre entier**).

On peut afficher de quel type est une variable en utilisant la **fonction** ```type```. Vous remarquerez, au passage, que le mot ```type``` prend une coloration différente dans votre carnet. C'est parce qu'il s'agit d'un mot réservé. Évitez de donner à vos variables des noms réservés comme celui-ci.


```python
type(a)
```




    int



<h4 style="background: cyan; text-align: center; padding: 10px;">Nombres décimaux</h4>

Les nombres décimaux sont un type distinct de variable. Alors qu'en français, la décimale est marquée par une virgule, en python il faut utiliser un point.

Les variables ```b``` et ```c```, ci-dessous, contiennent respectivemnt les nombres 7,8 et 9,482758620689655. Elles sont de type ```float```, ce qui signifie *floating-point number*, ou nombre à [virgule flottante](https://fr.wikipedia.org/wiki/Virgule_flottante) en français.


```python
b = 5.8
type(b)
```




    float




```python
c = a/b
c
```




    9.482758620689655




```python
type(c)
```




    float



<img src="https://journalisme.uqam.ca/wp-content/uploads/sites/77/2020/05/bonus.png" style="text-align: center;">

Si vous avez besoin d'arrondir des nombres décimaux, utilisez la fonction ```round```.

Elle nécessite deux arguments: le nombre à arrondir et le nombre de chiffres à conserver après la virgule.


```python
round(c,3)
```




    9.483



<h4 style="background: cyan; text-align: center; padding: 10px;">Booléens</h4>

Vous connaissez certainement la [logique booléenne](https://fr.wikipedia.org/wiki/Alg%C3%A8bre_de_Boole_(logique)). Elle s'applique de différentes façons en programmation. L'une d'entre elles concerne directement les variables puisque peuvent être de type booléen. Dans ces cas, elle ne pourront prendre que deux valeurs: **```True```** (vrai) ou **```False```** (faux). Remarquez les majuscules dans ces valeurs.


```python
x = True
y = False
```


```python
type(x), type(y)
```




    (bool, bool)



<h4 style="background: cyan; text-align: center; padding: 10px;">None</h4>

Le néant existe-t-il? La question [déclenche des passions chez les philosophes](https://fr.wikipedia.org/wiki/L%27%C3%8Atre_et_le_N%C3%A9ant).

En informatique, on a réglé ça vite fait. Le néant existe.

C'est ainsi que des variables peuvent avoir une valeur vide, **```None```**, dont il faut que je vous parle parce que vous risquez de la croiser un peu plus tard dans le cours. Il arrive en effet, quand on fait du moissonnage de données, que ce qu'on avait prévu mettre comme valeur dans une variable ne se trouve pas, finalement, dans le site dans lequel on va chercher ces données. Notre variable va exister, mais ne contiendra rien. Elle sera de type ```NoneType```.


```python
sartre = None
```


```python
type(sartre)
```




    NoneType



<h4 style="background: cyan; text-align: center; padding: 10px;">Texte</h4>

On peut aussi mettre du texte dans une variable. Toute suite de caractères se trouvant entre deux guillemets anglais (```"``` ou *double quotes*; attention de ne pas utiliser les guillemets français) est considérée comme du texte en python. On peut aussi utiliser les apostrophes (```'``` ou *single quotes*), mais préférez généralement les guillemets anglais.

Par exemple, la variable ```d``` contient une expression contemporaine.


```python
d = "Ça va bien aller!"
d
```




    'Ça va bien aller!'



La variable ```e``` contient également du texte.


```python
e = " nous dit Horacio Arruda"
e
```




    ' nous dit Horacio Arruda'



Comme ce texte semble suivre celui de la variable ```d```, on peut le concaténer à cette dernière avec un simple ```+``` et mettre le résultat dans une nouvelle variable, qu'on peut appeler ```f```.


```python
f = d + e
f
```




    'Ça va bien aller! nous dit Horacio Arruda'



Maintenant, le contenu de certaines variables peut nous laisser perplexe.

La variable ```annee``` (vous avez remarqué, pas d'accent aigu) contient-elle du **texte** ou un **nombre entier**?


```python
annee = "2020"
```

La présence des guillemets anglais, ici, nous nonne la réponse, confirmée par la fonction ```type```.


```python
type(annee)
```




    str



Mais parfois, quand on code, on n'est pas toujours en mesure de voir que des variables dont on pense qu'elles sont des nombres sont en fait du texte.

On peut alors être tenté d'effectuer des calculs avec ces «faux nombres», ce qui génère le message d'erreur ci-dessous.


```python
tokyo = annee + 1
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-22-192376e146c4> in <module>
    ----> 1 tokyo = annee + 1
    

    TypeError: must be str, not int


Lisez bien ces messages d'erreur. Ils contiennent des indices sur ce qui a planté.

Ici, le message vous dit: ```TypeError: must be str, not int```.

On est en présence d'une erreur sur le type (```TypeError```). Python nous dit que ça doit être en raison d'une variable de type texte (```str```) alors qu'il s'attend à un nombre entier (```int```).

Hmmm... Quoi faire?

<h5 style="background: beige; text-align: center; padding: 10px;">Transformations de types</h5>

Dans un cas comme celui-ci, on peut changer le type de la variable ```annee```. Les types de variables sont aussi des fonctions. Ainsi, il suffit d'utiliser la fonction ```int``` pour transformer le *texte 2020* en *nombre 2020*.


```python
annee = int(annee)
type(annee)
```




    int



Attention, ça ne fonctionne que si on est en présence de nombres. Les transformations ci-dessous vont échouer car les textes qu'on tente de transformer contiennent des caractères qui ne sont pas des nombres.


```python
int("en 2020")
```


    ---------------------------------------------------------------------------

    ValueError                                Traceback (most recent call last)

    <ipython-input-24-c43b6f3dacb6> in <module>
    ----> 1 int("en 2020")
    

    ValueError: invalid literal for int() with base 10: 'en 2020'



```python
int("deux mille vingt")
```


    ---------------------------------------------------------------------------

    ValueError                                Traceback (most recent call last)

    <ipython-input-25-40e5b81d266d> in <module>
    ----> 1 int("deux mille vingt")
    

    ValueError: invalid literal for int() with base 10: 'deux mille vingt'



```python
int("twenty twenty")
```


    ---------------------------------------------------------------------------

    ValueError                                Traceback (most recent call last)

    <ipython-input-26-315d4d22a6f3> in <module>
    ----> 1 int("twenty twenty")
    

    ValueError: invalid literal for int() with base 10: 'twenty twenty'


<img src="https://journalisme.uqam.ca/wp-content/uploads/sites/77/2020/05/bonus.png" style="text-align: center;">


```python
int("7E4",16)
```




    2020



Ah, tiens! La fonction ci-dessus fonctionne alors qu'on a transformé une chaîne de caractères contenant un «E». Il faut simplement savoir que la fonction ```int``` peut contenir un 2e argument qui précise la base dans laquelle se trouve le «nombre de type texte» qu'on souhaite transformer. Par défaut, il s'agit de la base 10. Mais dans le cas ci-dessus, «7E4» est un nombre hexadécimal (base 16) et ```int``` le transforme en un nombre en base 10, à savoir 2020.

<h5 style="background: beige; text-align: center; padding: 10px;">Fonctions et méthodes utiles pour traiter du texte</h5>

Il y a [des myriades de fonctions et de méthodes qui vous permettent de traiter des variables de type texte](https://www.w3schools.com/python/python_ref_string.asp). Voici quelques-unes de celles dont je me sers le plus souvent.

<p style="text-align: center;">***</p>

La fonction ```len()``` est pratique si on a besoin de connaître le nombre de caractères dans un texte. Dans le bloc de code ci-dessous, je vais mettre le texte des objectifs du cours dans la variable ```objectifs``` et je vais en compter la longueur.


```python
objectifs = "L'objectif général est de former une prochaine génération de chercheurs apte à créer ses propres outils numériques et à comprendre les avantages, mais aussi les limites, de l'informatique en général et de l'«intelligence artificielle» (IA) en particulier dans un contexte de recherche dans toutes les disciplines (sciences, sciences humaines et sociales, etc.)."
len(objectifs)
```




    361



Ce texte a donc 361 caractères, espaces incluses.

<img src="https://journalisme.uqam.ca/wp-content/uploads/sites/77/2020/05/bonus.png" style="text-align: center;">

Certaines des fonctions que je vais vous présenter ci-dessous sont en fait des **méthodes**. Méthodes et fonctions sont similaires. On reconnaît les méthodes parce qu'elles commencent par un point qui suit le nom de la variable à laquelle on les applique. Les fonctions prennent des variables comme arguments entre parenthèses. Comme les méthodes aussi peuvent inclure des variables parmi les arguments qu'on peut inclure entre leurs parenthèses, cela en fait aussi des fonctions... Il n'y aura aucune question d'examen sur ces notions. Je vous en parle simplement par souci de correctement nommer les choses. 🤓

<p style="text-align: center;">***</p>

Jusqu'ici, vous avez remarqué: le simple fait d'écrire le nom d'une variable suffit pour que votre carnet en affiche le contenu.

Habituellement, cependant, on utilise la fonction ```print()``` pour afficher des variables. Cette fonction marche avec tout type de variable, pas uniquement les variables de type texte. Mais je vous en parle maintenant pour vous présenter la méthode ```.format()```. Celle-ci permet de formater l'affichage d'une variable texte pour y insérer d'autres variables. Je vous donne un exemple.

Vous travaillez sur des publications dans les réseaux sociaux. Le nombre de partages de chaque publications est dans une variable appelée ```partages```. Pour insérer ce nombre dans un affichage plus intéressant vous pourriez écrire ce code:


```python
partages = 82
print("La publication a été partagée {} fois".format(partages))
```

    La publication a été partagée 82 fois


Vous remarquez que dans votre texte se trouve une paire d'accolades (```{}```). C'est un **espace réservé** (ou *placeholder*) qui indique où placer la variable que vous donnez comme argument à la méthode ```.format()```.

Vous pouvez insérer autant de variables que nécessaire. Voici un autre exemple.

Vous travaillez sur des données médicales. Vous avez un code de patient (car vos données sont anonymisées) et des information sur ses signes vitaux, ainsi que le moment où ceux-ci ont été pris.


```python
code_patient = "j875af&3865DVx33"
age = "28"
date = "2020-05-06"
heure = "11:43"
pression = "122/41"
temp = "37.2"
rythme = "64"

print("Le {}, à {}, le patient {} ({} ans) avait un rythme cardiaque de {} battements par minute, une température de {}° et une pression de {}.".format(date,heure,code_patient,age,rythme,temp,pression))
```

    Le 2020-05-06, à 11:43, le patient j875af&3865DVx33 (28 ans) avait un rythme cardiaque de 64 battements par minute, une température de 37.2° et une pression de 122/41.


La méthode ```.replace()``` vous permet de substituer du texte pour un autre texte. Je m'en sers le plus souvent pour retrancher du texte dans une chaîne de caractères.

Par exemple, admettons que vous ayez à traiter un grand nombre d'URL du *Devoir*, comme celui-ci:

https://www.ledevoir.com/monde/578461/mieux-vaut-prevenir-que-guerir-dit-l-oms
<a><img src="https://media2.ledevoir.com/images_galerie/nwd_809240_641278/image.jpg" width="25%"></a>

Vous pourriez placer vos URL dans une variable que vous pourriez baptiser ```url```.


```python
url = "https://www.ledevoir.com/monde/578461/mieux-vaut-prevenir-que-guerir-dit-l-oms"
```

Mais voilà. Tous ces URL commencent par la même chose: «https://www.ledevoir.com/». Vous souhaitez retrancher ces débuts répétitifs. La méthode ```.replace``` peut vous aider.

Elle demande deux arguments. Le premier est le texte qu'on veut remplacer. Le second est le texte qui remplacera le premier. Dans les cas où on veut retrancher du texte, on le remplace par du texte vide, c'est-à-dire deux guillemets anglais collés l'un sur l'autre: ```""```.

Cela donne ceci:


```python
url = url.replace("https://www.ledevoir.com/","")
```

Et vous voyez que la valeur de la variable ```url``` a été changée en conséquence:


```python
url
```




    'monde/578461/mieux-vaut-prevenir-que-guerir-dit-l-oms'



Il arrive souvent, quand on moissonne des données dans le web, qu'on se retrouve avec des variables de texte qui comportent plusieurs espaces blancs indésirables au début et à la fin. Voici un exemple:


```python
auteur = "           Agence France-Presse                   "
```

Utiliser la méthode ```.replace()``` pour remplacer des espaces (```" "```) par du vide (```""```) ne serait pas une solution puisque l'espace entre «Agence» et «France-Presse» disparaîtrait aussi pour donner «AgenceFrance-Presse».

La solution, ici, est la méthode ```.strip()```. Elle décape littéralement les textes des espaces indésirables au début et à la fin d'une variable.

Par défaut, elle vous débarrasse des espaces.


```python
auteur = auteur.strip()
auteur
```




    'Agence France-Presse'



Les méthodes ```.strip()``` et ```.replace()``` sont utiles pour traiter des textes qui représentent des montants ou des pourcentages et qu'on souhaiterait transformer en nombres.

Voici deux exemples. Dans le premier, on a un montant d'un demi-million de dollars (**$500,000**), dans le second, on a un pourcentage exprimé avec une décimale en français (**79,5%**).

On va transformer le premier texte en nombre entier (et le multiplier par 5 pour prouver qu'il s'agit bel et bien un nombre) et le second en nombre décimal (auquel on va retrancher 1,1 pour montrer que ç'a bien marché ici aussi).

Vous allez aussi remarquer qu'on va le faire en une seule opération, car il est possible d'***enchaîner les méthodes les unes après les autres*** et d'***insérer des méthodes dans des fonctions***.


```python
subvention = "$500,000"
subvention = int(subvention.strip("$").replace(",",""))
subvention * 5
```




    2500000




```python
appuis = "79,5%"
appuis = float(appuis.strip("%").replace(",","."))
appuis - 1.1
```




    78.4



<img src="https://journalisme.uqam.ca/wp-content/uploads/sites/77/2020/05/bonus.png" style="text-align: center;">

Il existe aussi différentes méthodes pour transformer la casse des textes. Pour vous montrer leur effet, nous allons créer une variable avec du texte.


```python
martino = "Comme Richard Martineau, j'aime parfois TOUT ÉCRIRE EN MAJUSCULES!!!"
```

La méthode ```.lower()``` transforme tous les caractères de votre texte en minuscules (*lowercase*).


```python
martino.lower()
```




    "comme richard martineau, j'aime parfois tout écrire en majuscules!!!"



La méthode ```.upper()``` fait l'inverse et transforme tous les caractères de votre texte en majuscules (*uppercase*).


```python
martino.upper()
```




    "COMME RICHARD MARTINEAU, J'AIME PARFOIS TOUT ÉCRIRE EN MAJUSCULES!!!"



La méthode ```.capitalize()```, pour sa part, ne met une majuscule qu'au tout premier caractère de la chaîne.


```python
martino.capitalize()
```




    "Comme richard martineau, j'aime parfois tout écrire en majuscules!!!"



Enfin, une dernière méthode juste pour vous: ```.count()``` permet de compter le nombre d'occurrences d'un caractère ou d'une chaîne de caractères dans une variable texte donnée.

Par exemple, créons la variable ```justin``` contenant le texte d'un [récent discours du premier ministre canadien](https://pm.gc.ca/fr/nouvelles/discours/2020/05/05/allocution-du-premier-ministre-annoncer-aide-supplementaire-au).


```python
justin = "Bonjour à tous. Avant de commencer, je tiens à souligner qu’aujourd’hui marque le 75e anniversaire de la libération des Pays-Bas. En ce jour, on se souvient des Canadiens et des Terre-Neuviens qui ont libéré les Pays-Bas de la tyrannie des nazis. On souligne les liens étroits qui ont été forgés entre le Canada et les Pays‑Bas. Et on rend hommage au courage incroyable et aux sacrifices de nos anciens combattants, à qui on est à tout jamais reconnaissant. En période de crise, les Canadiens s’entraident les uns les autres et aident nos amis. On l’a constaté il y a 75 ans, sur les champs de bataille de la Seconde Guerre mondiale, et on le constate encore une fois aujourd’hui au cours de cette pandémie. Ces jours-ci, tous ceux qui œuvrent dans le secteur de l’alimentation travaillent plus fort que jamais pour remplir les étagères de nos épiceries. Ils continuent de faire de longues heures pour nous nourrir, mais la pandémie complique la tâche. Les travailleurs doivent prendre des mesures supplémentaires pour se protéger. Ils doivent changer leurs façons de faire et respecter la distanciation physique. Et puisque les hôtels et les restaurants sont fermés, de nombreux producteurs se retrouvent avec des surplus importants de certains produits. Depuis le début, on a dit qu’on allait être là pour ceux qui ont besoin d’un coup de main. Donc aujourd’hui, on annonce un nouvel investissement de plus de 252 millions de dollars pour aider le secteur agroalimentaire à traverser cette crise. De cette somme, on va allouer plus de 77 millions de dollars aux transformateurs alimentaires pour protéger la sécurité de leurs travailleurs. C’est de l’argent qu’ils vont pouvoir utiliser pour acheter plus d’équipement de protection personnelle, adapter leurs protocoles sanitaires et appuyer d’autres mesures de distanciation sociale. Ça va aussi permettre aux transformateurs d’adapter leurs usines de façon à ce qu’on puisse produire plus de produits au Canada. Pour aider les éleveurs de bétail et de porc, on met sur pied des initiatives nationales sous Agri-relance, qui disposera d’une enveloppe pouvant atteindre 125 millions de dollars pour aider les producteurs à s’adapter aux changements du marché. Les fermes et les porcheries élèvent plus d’animaux que notre système peut transformer en produits de consommation comme du steak et du bacon à cause de la COVID-19. Donc on doit garder les animaux sur la ferme plus longtemps que prévu – et ça coûte cher. Les fonds qu’on annonce aujourd’hui vont aider les producteurs de bœuf et de porc à s’adapter à cette crise. C’est un premier investissement et si on doit en faire plus, on va en faire plus. Pour ce qui est des producteurs laitiers, on compte travailler avec tous les partis à la Chambre pour augmenter de 200 millions de dollars la ligne de crédit de la Commission canadienne du lait. La Commission a déjà un programme en place pour entreposer et conserver les produits laitiers. Elle entrepose du beurre et du fromage depuis le début de la crise, et elle prévoit atteindre sa capacité maximale bientôt. En augmentant la ligne de crédit de la Commission, on peut éviter de perdre des produits frais de chez nous et aider les producteurs laitiers à composer avec les conséquences de la crise. Puisque les hôtels et les restaurants sont fermés, certains types d’aliments – comme le lait, le beurre et les pommes de terre – sont produits en trop grande quantité. Bien que des dons aient été faits à des banques alimentaires, on n’a pas la capacité en ce moment de redistribuer une quantité aussi importante d’aliments. Et certains producteurs n’ont pas d’autre choix que de jeter leur produit. Cela entraîne un gaspillage de nourriture et une perte de revenus pour les gens qui ont travaillé si fort pour produire ces aliments. Pour éviter ça, on lance le programme d’achat des aliments excédentaires, qui sera doté initialement d’un budget de 50 millions de dollars. Le gouvernement achètera une quantité importante de certains produits qui risquent d’être gaspillés, par exemple des pommes de terre ou de la volaille, et les redistribuera à des organisations qui luttent contre l’insécurité alimentaire. Nos agriculteurs seront donc rémunérés pour leur important travail et des gens vulnérables au Canada auront accès à des aliments frais durant la crise. Ensemble, ces mesures représentent un investissement de 252 millions de dollars à l’appui des gens qui remplissent les étagères de nos épiceries et nourrissent nos familles. Je veux d’ailleurs prendre un moment pour souligner le travail de la ministre Bibeau dans ce dossier. Cela dit, on sait que les agriculteurs s’inquiètent toujours des effets à long terme qu’aura cette pandémie sur leur industrie. Ils s’inquiètent d’avoir assez de travailleurs pour la récolte plus tard dans la saison ou d’avoir suffisamment d’équipement de protection individuelle pour leurs travailleurs. Ce sont des préoccupations valides. Et je peux vous assurer qu’on travaille avec les agriculteurs et les intervenants ainsi qu’avec les provinces et les territoires pour trouver des solutions durables. J’aimerais conclure aujourd’hui en remerciant tous les travailleurs de notre industrie alimentaire. Les gens passent beaucoup de temps dans leur cuisine. Ils préparent des plats pour un voisin âgé ou pour eux-mêmes. Ils découvrent de nouvelles recettes ou essaient de faire du pain au levain. Cuisiner ne sert pas seulement à se nourrir, surtout ces jours-ci. Ça permet également de réduire le stress, de faire partie d’une communauté, de s’appuyer les uns les autres et de créer des souvenirs. Cela est possible grâce aux travailleurs de notre industrie. À tous ceux qui travaillent dans le domaine de l’alimentation – merci pour tout ce que vous faites pour nous. On est là et on va continuer d’être là pour vous. Merci beaucoup."
```

Si on était curieux de savoir combien de fois M. Trudeau parle de travail ou de travailleurs, on pourrait mettre la chaîne «travail» comme argument de la méthode ```.count()``` et appliquer cette dernière à la variable ```justin``` comme ceci. Et on voit que le PM a parlé 13 fois de travail, de travailler ou de travailleurs dans ce discours.


```python
justin.count("travail")
```




    13



<h5 style="background: beige; text-align: center; padding: 10px;">La notion d'«index»</h5>

Les ascenseurs européens ont toujours un étage zéro pour ce qu'on désigne, au Québec, comme le «premier étage».

<img src="http://bit.ly/jhroy0" width="25%">

Il en va de même en informatique. Le premier élément d'une série porte toujours le numéro zéro.

C'est ainsi que le premier caractère dans un texte est numéroté zéro. C'est ce qu'on appelle son numéro d'**index**. L'index, dans une variable de type texte, désigne la place d'un caractère dans ce texte.

Allons-y avec la variable ```url``` sur laquelle nous avons travaillé tout à l'heure et qui est toujours en mémoire dans notre carnet.


```python
url
```




    'monde/578461/mieux-vaut-prevenir-que-guerir-dit-l-oms'



Si on veut accéder au premier caractère dans cette variable (en l'occurrence, la lettre «m»), voici ce qu'on devra écrire comme code.


```python
url[0]
```




    'm'



Pour accéder au 2e caractère, on écrirait plutôt ceci:


```python
url[1]
```




    'o'



Il s'agit ainsi d'accompagner le nom de la variable de crochets (```[]```) entre lesquels on indiquera le numéro d'index auquel on souhaite accéder.

Pour accéder au dernier caractère, il n'est pas nécessaire de compter le nombre de caractères avec la fonction ```len()```. On peut demander le numéro d'index -1, et ça fonctionne.


```python
url[-1]
```




    's'



On peut aussi accéder à des **étendues**, des plages d'index.

Par exemple, pour extraire une étendue qui va du 10e au 20e caractère de la variable ```url```, on écrira:


```python
url[10:21]
```




    '61/mieux-va'



Vous avez remarqué que le début et la fin de l'étendue sont séparés par deux-points (```:```) et que la fin de l'étendue n'est pas 20, comme je l'ai demandé, mais 21. C'est que le dernier numéro d'index **n'est pas inclus** dans l'étendue qui nous sera retournée.

Si l'étendue qui nous intéresse commence au premier caractère d'un texte, il n'est pas nécessaire de commencer par zéro. C'est ainsi que les deux codes ci-dessous sont identiques.


```python
url[0:21], url[:21]
```




    ('monde/578461/mieux-va', 'monde/578461/mieux-va')



Dans le cas d'une étendue qui se terminerait par le dernier caractère d'un texte, écrire -1 comme fin de l'étendue vous priverait de ce dernier caractère. Ainsi, les deux codes ci-dessous ne produisent pas le même résultat.


```python
url[20:-1], url[20:]
```




    ('aut-prevenir-que-guerir-dit-l-om', 'aut-prevenir-que-guerir-dit-l-oms')



On peut faire des recherches dans une variable texte pour connaître le numéro d'index d'un caractère donné ou d'une suite de caractères donnée. Pour ce faire, on peut utiliser la méthode ```.find()```.

Par exemple, lorsqu'on travaille avec des URL, on peut avoir besoin de connaître l'emplacement des barres obliques.

Si on applique la méthode ```.find()``` à notre variable ```url``` pour trouver le caractère «/», par exemple, on écrira:


```python
url.find("/")
```




    5



Ce code nous retourne «5», ce qui est le numéro d'index de la première barre oblique qu'on rencontre dans la variable ```url```.

On pourrait utiliser la méthode ```.find()``` pour extraire le début de notre variable url, qui contient le nom de la section du *Devoir* dans laquelle est classée l'article dont on est allé chercher l'URL. 


```python
url[:url.find("/")]
```




    'monde'



Cette méthode peut être utile si on souhaite extraire le nom de la section de plusieurs URL.

Voyez avec cet autre URL tiré du *Devoir*, qu'on va appeler ```url2```:


```python
url2 = "https://www.ledevoir.com/opinion/chroniques/578376/un-message-trop-confus"
url2 = url2.replace("https://www.ledevoir.com/","") # Il faut lui appliquer la méthode .replace() pour qu'elle ait la même structure que la variable url
url2[:url2.find("/")]
```




    'opinion'



Voyez: même si «monde» et «opinion» n'ont pas le même nombre de caractères, on peut les repérer avec la méthode ```.find()```.


Appliquons-la à nos variables ```url``` et ```url2``` pour que vous en voyez le résultat:


```python
url[url.rfind("/")+1:]
```




    'mieux-vaut-prevenir-que-guerir-dit-l-oms'




```python
url2[url2.rfind("/")+1:]
```




    'un-message-trop-confus'



C'est ainsi qu'on peut extraire la fin d'un URL. Vous avez remarqué que j'ai ajouté «+1» à la méthode ```.rfind()```. On peut en effet faire cela puisque le résultat de cette méthode est un numéro d'index et qu'un numéro d'index est nombre entier sur lequel on peut faire des calculs.

<h4 style="background: cyan; text-align: center; padding: 10px;">Listes</h4>

Les listes sont le type de variable avec lequel vous allez le plus travailler.

Jusqu'à maintenant, on a vu des variables qui ne pouvaient contenir qu'une seule valeur à la fois. Les listes sont des variables qui peuvent contenir plusieurs valeurs. On peut même y ajouter ou retrancher des valeurs.

Les valeurs, dans ces listes, sont toujours incluses entre crochets (```[]```) séparées par des virgules. Voici un exemple:


```python
pouet = [34,78,345,99,0,1,353,25,525,25,66,22,678,1090]
```

La variable ```pouet``` contient une série de nombre entiers.


```python
pouet
```




    [34, 78, 345, 99, 0, 1, 353, 25, 525, 25, 66, 22, 678, 1090]



Son type est, vous l'aurez deviné:


```python
type(pouet)
```




    list



On peut mettre du texte dans des listes.


```python
fleurs = ["Tulipe","Rose","Orchidée"]
```


```python
fleurs
```




    ['Tulipe', 'Rose', 'Orchidée']



Tous les types de variables qu'on a vu peuvent être inclus dans des listes. Même des listes peuvent être incluses dans des listes! Et on peut également mêler différents types de variables dans une même liste.


```python
wtf = [99,5.5,"Bonjour toi!!!",True,None,[3,8,25,49]]
```


```python
wtf
```




    [99, 5.5, 'Bonjour toi!!!', True, None, [3, 8, 25, 49]]



<h5 style="background: beige; text-align: center; padding: 10px;">Fonctions et méthodes relatives aux listes</h5>

La fonction ```range()``` vous permet de générer des nombres dans une étendue donnée. Il faut cependant l'insérer dans une autre fonction, la fonction ```list()``` pour que cette étendue soit transformée en une liste.

Ainsi, la variable ```tintin```, ci-dessous, contient tous les nombres de 7 à 77. Vous aurez remarqué que, comme avec les étendues qu'on a vu ci-dessus pour extraire des portions de texte, la fin de l'étendue n'est pas incluse.


```python
tintin = list(range(7,78))
tintin
```




    [7,
     8,
     9,
     10,
     11,
     12,
     13,
     14,
     15,
     16,
     17,
     18,
     19,
     20,
     21,
     22,
     23,
     24,
     25,
     26,
     27,
     28,
     29,
     30,
     31,
     32,
     33,
     34,
     35,
     36,
     37,
     38,
     39,
     40,
     41,
     42,
     43,
     44,
     45,
     46,
     47,
     48,
     49,
     50,
     51,
     52,
     53,
     54,
     55,
     56,
     57,
     58,
     59,
     60,
     61,
     62,
     63,
     64,
     65,
     66,
     67,
     68,
     69,
     70,
     71,
     72,
     73,
     74,
     75,
     76,
     77]



Par défaut, la fonction ```range()``` génère des nombres qui se suivent. Mais elle peut contenir un troisième argument qui précise une valeur d'incrément différente. Ainsi, le code ci-dessous génère des nombres par quatre, ce qui fait que la variable ```olympiques``` contient en fait la liste de toutes les années où ont (ou auraient dû être) présentés des jeux olympiques de l'ère moderne.


```python
olympiques = list(range(1896,2024,4))
olympiques
```




    [1896,
     1900,
     1904,
     1908,
     1912,
     1916,
     1920,
     1924,
     1928,
     1932,
     1936,
     1940,
     1944,
     1948,
     1952,
     1956,
     1960,
     1964,
     1968,
     1972,
     1976,
     1980,
     1984,
     1988,
     1992,
     1996,
     2000,
     2004,
     2008,
     2012,
     2016,
     2020]



Les fonctions ```min()``` et ```max()``` vous retournent les plus petites et les plus grandes valeurs d'une liste donnée. Cela fonctionne aussi avec du texte (dans ce cas, c'est l'ordre alphabétique qui déterminera les valeurs minimale et maximale).


```python
min(olympiques), min(fleurs)
```




    (1896, 'Orchidée')




```python
max(tintin), max(fleurs)
```




    (77, 'Tulipe')



On peut ajouter des éléments à une liste grâce à la méthode ```.append()``` qui signifie adjoindre.

Par exemple, ci-dessous, on ajoute le nom d'une autre fleur à notre variables ```fleurs```:


```python
fleurs.append("Marguerite")
```

En affichant le contenu de la variable, on constate en effet qu'elle contient désormais quatre éléments.


```python
fleurs
```




    ['Tulipe', 'Rose', 'Orchidée', 'Marguerite']



Comme avec du texte, il est possible de demander la taille d'une liste avec la fonction ```len()```. Elle ne nous retournera pas le nombre de caractères contenus dans la liste, mais seulement le nombre d'éléments.


```python
len(fleurs)
```




    4



Je vous présente une dernière méthode. Elle s'applique aux variables de type texte. Mais je vous en parle ici parce que son résultat est une liste.

Il s'agit de la méthode ```.split()```. Elle permet de briser un texte en fonction d'un caractère donné, ou d'une chaîne de caractères donnée.

Par exemple, si on prend notre variable ```url```, on peut la briser en fonction de la barre oblique («/») pour produire une liste dont les éléments sont composés du texte se trouvant de part et d'autre de toutes les barres obliques contenues dans ```url```.

Pour rappel, voici le contenu de la variable:


```python
url
```




    'monde/578461/mieux-vaut-prevenir-que-guerir-dit-l-oms'



Y appliquer la méthode ```.split()``` avec l'argument ```"/"``` va briser notre variable en trois éléments distincts.


```python
url.split("/")
```




    ['monde', '578461', 'mieux-vaut-prevenir-que-guerir-dit-l-oms']



C'est ainsi que si on souhaite accéder seulement à la section («monde»), seulement au code d'article («578461») ou seulement au titre de l'article, on peut utiliser du code comme celui-ci:


```python
url.split("/")[0]
```




    'monde'




```python
url.split("/")[1]
```




    '578461'




```python
url.split("/")[2]
```




    'mieux-vaut-prevenir-que-guerir-dit-l-oms'



Ou encore, on peut mettre le résultat du ```.split()``` dans une autre variable et accéder à ses différents éléments de la façon suivante:


```python
detailsArticle = url.split("/")
detailsArticle[2]
```




    'mieux-vaut-prevenir-que-guerir-dit-l-oms'



<h5 style="background: beige; text-align: center; padding: 10px;">Notion d'index avec les listes</h5>

La notion d'**index** qu'on a vue ci-dessus avec les variables de type texte s'applique également aux variables de type liste.

Pour extraire le premier élément d'une liste, par exemple, c'est l'index ```0``` qu'il faut utiliser.


```python
fleurs[0]
```




    'Tulipe'



Les étendues fontionnent de la même façon.

Et, en passant, on peut mettre ces étendues dans d'autres variables (quelque chose qu'il est aussi possible de faire avec du texte).


```python
boomers = tintin[-17:]
boomers # La variable boomers est une autre liste, créée à partir de la liste tintin
```




    [61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77]



Il faut aussi savoir qu'on peut enchaîner ces appels d'index. Par exemple, pour accéder à un élément qui se trouve dans une liste qui est elle-même un élément inséré dans une autre liste. Vous souvenez de la variable ```wtf```?


```python
wtf
```




    [99, 5.5, 'Bonjour toi!!!', True, None, [3, 8, 25, 49]]



Son dernier élément est une liste. On y accéderait en utilisant l'index -1.
Pour y extraire le 3e élément de cette liste, on utiliserait ensuite l'index 2.
Mais pour y aller directement, on peut enchaîner ces deux appels d'index de la façon suivante, et ça nous retourne le nombre entier 25, qui est bel et bien le 3e élément de la liste qui est le dernier élément de la liste ```wtf```.


```python
wtf[-1][2]
```




    25



<h4 style="background: cyan; text-align: center; padding: 10px;">Dictionnaires</h4>

Le dernier type de variable dont je veux vous parler est peut-être le plus complexe. Il s'agit des **dictionnaires**.

Les dictionnaires sont comme des listes qui nous donnent plus d'information. On les reconnaît parce que tous les éléments y sont inclus entre des accolades (```{}```).

Tous les éléments, comme dans le cas des listes, y sont séparés par des virgules.

Chaque élément, cependant, est composé de deux parties:

- une clé, qui décrit l'élément
- une valeur, qui contient l'élément proprement dit

Les paires **clé:valeur** sont toujours séparées par deux-points (```:```)

Voici un exemple. La variable ```jo``` est un dictionnaire.


```python
jo = {"Année":1896,"Ville":"Athènes","Pays participants":["France","Grèce","Empire Ottoman","États-Unis"]}
```

Cela peut ressembler à du charabia, mais quand on s'y arrête et qu'on lit la variable ```jo```, on constate rapidement qu'elle contient trois éléments:

1. la clé du premier est «Année» et sa valeur est le nombre 1896.
* la clé du deuxième est «Ville» et sa valeur est le texte «Athènes».
* la clé du troisième est «Pays participants» et sa valeur est une liste (incomplète) des pays qui ont participé à ces jeux.

En passant, le nom du type de ces variable est abrégé à ```dict```.


```python
type(jo)
```




    dict



Si, dans le cadre de vos recherches, vous avez à recueillir des données dans le web, dans des réseaux sociaux, ou ailleurs, il est fort probable que vous rencontriez des dictionnaires. Il est ainsi utile d'en reconnaître la structure pour mieux les lire et en extraire les données qui vous intéressent.

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Bixi_logo.svg/320px-Bixi_logo.svg.png">

Un exemple: l'état des stations du réseau Bixi est présenté dans [cette page de données ouvertes](https://api-core.bixi.com/gbfs/fr/station_status.json). Il s'agit d'un fichier [JSON](https://www.json.org/json-fr.html) dont la structure est identique à celle d'un dictionnaire en python.

Si vous allez la consulter, vous verrez qu'elle contient, à la base, elle aussi, trois éléments:

<ol>
    <li>la clé du premier est «last_updated» et sa valeur est un nombre qui est, en fait, une «époque» en format Unix que vous pouvez convertir en date grâce à [cet outil en ligne](https://www.epochconverter.com/).</li>
    <li>la clé du deuxième est «ttl» et sa valeur un nombre quelconque (j'ignore ce que ça veut dire)</li>
    <li>et la clé du troisième est «data» et sa valeur est, tiens donc, un autre dictionnaire qui ne contient qu'une paire clé:valeur:</li>
    <ul>
        <li>la clé est «stations» et la valeur est une grande liste des 617 stations Bixi à Montréal:</li>
        <ul>
            <li>chaque élément de cette liste est un dictionnaire qui contient 11 éléments ou paires de clé:valeur donnant des infos sur le nombre de vélos ou d'ancrages disponibles.</li>
        </ul>
    </ul>
</ol>

On va examiner au prochain cours comment extraire des données à partir d'un dictionnaire.

<h3 style="background: pink; text-align: center; padding: 10px;">Boucles</h3>

<h4 style="background: cyan; text-align: center; padding: 10px;">Boucles ```for```</h4>

Tout l'intérêt de programmer tient dans la possibilité de traiter un grand volume d'information et d'éviter de faire manuellement des tâches répétitives. La façon d'automatiser ces tâches répétitives est de les faire à l'aide d'une boucle.

En python, la syntaxe des boucles peut avoir la forme suivante:

<code>**for** variable **in** liste:
     *&lt;On fait quelque chose avec la variable&gt;
     &lt;On peut inclure d'autre code aussi&gt;*
</code>

Vous remarquerez que la première ligne, dans laquelle on déclare la boucle avec l'expression ```for```, se termine par un deux-points (```:```). Ce deux points est toujours requis.

Vous remarquerez aussi que les deux lignes qui suivent sont décalées de quelques caractères (ou d'un ```&lt;tab&gt;```) vers la droite. On appelle ce décalage l'**indentation**. Cette indentation est importante. Tout ce qui est indenté sera exécuté par la boucle. Si on souhaite quitter la boucle et écrire du code qui ne sera pas exécuté par celle-ci, il suffit de le «désintenter», de le décaler d'un cran vers la gauche.

C'est avec les boucles qu'on comprend tout l'intérêt de mettre des données dans des listes. Une boucle va vous permettre de traiter, l'un après l'autre, chacun des éléments de cette liste.

Dans l'exemple ci-dessous, on va écrire le même texte avec les éléments de notre liste ```fleurs```.&lt;/tab&gt;


```python
for fleur in fleurs:
    print("J'adore les {}s".format(fleur.lower()))
```

    J'adore les tulipes
    J'adore les roses
    J'adore les orchidées
    J'adore les marguerites


Dans l'exemple ci-dessus, la boucle ```for``` prend chacun des éléments de la variable ```fleurs```.

À chaque itération, la boucle place l'élément où elle est rendue dans la variable ```fleur``` (au singulier). J'aime bien donner des noms explicites à mes variables. C'est un bon exemple, ici. J'ai pris l'habitude de donner à mes variables de type liste, qui contiennent plusieurs éléments, des noms au pluriel, et aux variables qui suivent l'expression ```for``` des noms au singulier.

Au passage, elle la transforme en minuscules avec la méthode ```.lower()``` et l'insère dans un texte avec la méthode ```.format()```.

<p style="text-align: center;">***</p>

Nous allons maintenant travailler avec une liste qui était incluse dans un fichier texte que je vous ai transféré par Zoom.


```python
films = [["À la croisée des chemins","Paul Guèvremont","1943"],
["Le Père Chopin","Fedor Ozep","1945"],
["La Forteresse","Fedor Ozep","1947"],
["Whispering City","Fedor Ozep","1947"],
["On ne triche pas avec la vie","René Delacroix et Paul Vandenberghe","1949"],
["Le Curé de village","Paul Gury","1949"],
["Le Gros Bill","Jean-Yves Bigras et René Delacroix","1949"],
["Un homme et son péché","Paul Gury","1949"],
["Les Lumières de ma ville","Jean-Yves Bigras","1950"],
["L'Inconnue de Montréal","Jean Devaivre","1950"],
["Séraphin","Paul Gury","1950"],
["Étienne Brûlé gibier de potence","Melburn E. Turner","1952"],
["La Petite Aurore, l'enfant martyre","Jean-Yves Bigras","1952"],
["Le Rossignol et les Cloches","René Delacroix","1952"],
["Cœur de maman","René Delacroix","1953"],
["Tit-Coq","Gratien Gélinas et René Delacroix","1953"],
["L'Esprit du mal","Jean-Yves Bigras","1954"],
["Le Village enchanté","Marcel et Réal Racicot","1956"],
["Le Survenant","Un collectif de réalisateurs","1957"],
["Les Mains nettes","Claude Jutra","1958"],
["Les Brûlés","Bernard Devlin","1959"],
["Accade in Canada","Luigi Petrucci","1962"],
["Seul ou avec d'autres","Denys Arcand avec Denis Héroux et Stéphane Venne","1962"],
["À tout prendre","Claude Jutra","1963"],
["Amanita Pestilens","René Bonnière","1963"],
["Drylanders (Un autre pays)","Donald Haldane","1963"],
["Le Chat dans le sac","Gilles Groulx","1964"],
["La Fleur de l'âge, ou Les adolescentes","Un collectif de réalisateurs","1964"],
["Jusqu'au cou","Denis Héroux","1964"],
["Nobody Waved Good-Bye","Don Owen","1964"],
["La Terre à boire","Jean-Paul Bernier","1964"],
["Trouble-fête","Pierre Patry","1964"],
["Astataïon ou le Festin des morts","Fernand Dansereau","1965"],
["Caïn","Pierre Patry","1965"],
["La Corde au cou","Pierre Patry","1965"],
["Le Coup de grâce","Jean Cayrol et Claude Durand","1965"],
["Pas de vacances pour les idoles","Denis Héroux","1965"],
["Le Révolutionnaire","Jean Pierre Lefebvre","1965"],
["La Vie heureuse de Léopold Z.","Gilles Carle","1965"],
["Carnaval en chute libre","Guy Bouchard","1966"],
["La Douzième heure","Jean Martimbeau","1966"],
["Footsteps in the Snow (Des pas sur la neige)","Martin Green","1966"],
["Yul 871","Jacques Godbout","1966"],
["Entre la mer et l'eau douce","Michel Brault","1967"],
["Il ne faut pas mourir pour ça","Jean Pierre Lefebvre","1967"],
["Manette : la folle et les dieux de carton","Camil Adam","1967"],
["C'est pas la faute à Jacques Cartier","Georges Dufaux et Clément Perron","1968"],
["Façade","Larry Kent","1968"],
["Isabel","Paul Almond","1968"],
["Las Joyas del diablo (Le diable aime les bijoux)","José Maria Elorrieta","1968"],
["Kid Sentiment","Jacques Godbout","1968"],
["Patricia et Jean-Baptiste","Jean Pierre Lefebvre","1968"],
["Poussière sur la ville","Arthur Lamothe","1968"],
["T-bone Steak dans... les mangeuses d'hommes","Gilles Marchand et Hugues Tremblay","1968"],
["Le Viol d'une jeune fille douce","Gilles Carle","1968"],
["La Chambre blanche","Jean Pierre Lefebvre","1969"],
["Danger pour la société","Jean Martimbeau","1969"],
["Délivrez-nous du mal","Jean-Claude Lord","1969"],
["Don't Let the Angels Fall (Seuls les enfants étaient présents)","George Kaczender","1969"],
["Le Grand Rock","Raymond Garceau","1969"],
["Jusqu'au cœur","Jean Pierre Lefebvre","1969"],
["Mon amie Pierrette","Jean Pierre Lefebvre","1969"],
["Où êtes-vous donc ?","Gilles Groulx","1969"],
["Le Soleil des autres","Jean Faucher","1969"],
["Valérie","Denis Héroux","1969"],
["Act of the Heart","Paul Almond","1970"],
["Ainsi soient-ils","Yvan Patry","1970"],
["L'Amour humain","Denis Héroux","1970"],
["Deux femmes en or","Claude Fournier","1970"],
["Entre tu et vous","Gilles Groulx","1970"],
["L'Initiation","Denis Héroux","1970"],
["Love in a 4 Letter World","John Sone","1970"],
["On est loin du soleil","Jacques Leduc","1970"],
["Pile ou face","Roger Fournier","1970"],
["Q-bec my love","Jean Pierre Lefebvre","1970"],
["Quand hurlent les loups","André Lafferrere","1970"],
["Red","Gilles Carle","1970"],
["St-Denis dans le temps...","Marcel Carrière","1970"],
["Vive la France","Raymond Garceau","1970"],
["Wow","Claude Jutra","1970"],
["7 fois… (par jour)","Denis Héroux","1971"],
["Après-ski","Roger Cardinal","1971"],
["Les Chats bottés","Claude Fournier","1971"],
["Corps et âme","Michel Audy","1971"],
["L'Explosion","Marc Simenon","1971"],
["Finalement...","Richard Martin","1971"],
["Fleur bleue","Larry Kent","1971"],
["Le Grand film ordinaire","Roger Frappier","1971"],
["IXE-13","Jacques Godbout","1971"],
["Jean-François Xavier de...","Michel Audy","1971"],
["Les Mâles","Gilles Carle","1971"],
["Le Martien de Noël","Bernard Gosselin","1971"],
["Les Maudits sauvages","Jean Pierre Lefebvre","1971"],
["Mon enfance à Montréal","Jean Chabot","1971"],
["Mon œil","Jean Pierre Lefebvre","1971"],
["Mon oncle Antoine","Claude Jutra","1971"],
["Question de vie","André Théberge","1971"],
["Le Retour de l'Immaculée Conception","André Forcier","1971"],
["Stop","Jean Beaudin","1971"],
["Tiens-toi bien après les oreilles à Papa...","Jean Bissonnette","1971"],
["Ty-Peupe","Fernand Bélanger","1971"],
["L'Apparition","Roger Cardinal","1972"],
["Les Colombes","Jean-Claude Lord","1972"],
["Le diable est parmi nous","Jean Beaudin","1972"],
["Et du fils","Raymond Garceau","1972"],
["L'exil","Thomas Vamos","1972"],
["Le Grand Sabordage","Alain Périsson","1972"],
["Isis au 8","Alain Chartrand","1972"],
["Journey","Paul Almond","1972"],
["La Maudite Galette","Denys Arcand","1972"],
["Montréal blues","Pascal Gélinas","1972"],
["Le p'tit vient vite","Louis-Georges Carrier","1972"],
["Pas de jeu sans soleil","Claude Bérubé","1972"],
["Quelques arpents de neige","Denis Héroux","1972"],
["Les Smattes","Jean-Claude Labrecque","1972"],
["Le Temps d'une chasse","Francis Mankiewicz","1972"],
["La Tête au neutre","Jean Gagné","1972"],
["Un enfant comme les autres","Denis Héroux","1972"],
["La Vie rêvée","Mireille Dansereau","1972"],
["La Vraie nature de Bernadette","Gilles Carle","1972"],
["Ah ! Si mon moine voulait...","Claude Pierson","1973"],
["Les Allées de la terre","André Théberge","1973"],
["La Conquête","Jean Gagné","1973"],
["Les Corps célestes","Gilles Carle","1973"],
["Les Dernières fiançailles","Jean Pierre Lefebvre","1973"],
["Il était une fois dans l'Est","André Brassard","1973"],
["J'ai mon voyage!","Denis Héroux","1973"],
["Kamouraska","Claude Jutra","1973"],
["Keep it in the Family","Larry Kent","1973"],
["La Maîtresse","Anton Van de Water","1973"],
["La Mort d'un bûcheron","Gilles Carle","1973"],
["Mourir pour vivre","Franco Vigilante","1973"],
["The Neptune Factor: An Undersea Odyssey","Daniel Petrie","1973"],
["Noël et Juliette","Michel Bouchard","1973"],
["O.K.... Laliberté","Marcel Carrière","1973"],
["On n'engraisse pas les cochons à l'eau claire","Jean Pierre Lefebvre","1973"],
["The Rainbow Boys","Gerald Potterton","1973"],
["Réjeanne Padovani","Denys Arcand","1973"],
["Sensations","Robert Séguin","1973"],
["A Star is Lost!","John Howe","1973"],
["Taureau","Clément Perron","1973"],
["Tendresse ordinaire","Jacques Leduc","1973"],
["Tu brûles... tu brûles...","Jean-Guy Noël","1973"],
["U-Turn","George Kaczender","1973"],
["Ultimatum","Jean Pierre Lefebvre","1973"],
["Y a toujours moyen de moyenner!","Denis Héroux","1973"],
["Alien Thunder","Claude Fournier","1974"],
["The Apprenticeship of Duddy Kravitz","Ted Kotcheff","1974"],
["Au boutt'","Roger Laliberté","1974"],
["Les Aventures d'une jeune veuve","Roger Fournier","1974"],
["Bar Salon","André Forcier","1974"],
["Les Beaux dimanches","Richard Martin","1974"],
["Bingo","Jean-Claude Lord","1974"],
["Bulldozer","Pierre Harel","1974"],
["Les Deux pieds dans la même bottine","Pierre Rose","1974"],
["La Gammick","Jacques Godbout","1974"],
["Guitare","Richard Lavoie","1974"],
["Je t'aime","Pierre Duceppe","1974"],
["Montreal Main","Frank Vitale","1974"],
["Ô ou l'invisible enfant","Raôul Duguay","1974"],
["Les Ordres","Michel Brault","1974"],
["Le Plumard en folie","Jacques Lem","1974"],
["La Pomme, la queue... et les pépins!","Claude Fournier","1974"],
["Running Time","Mort Ransen","1974"],
["Why Rock the Boat?","John Howe","1974"],
["Y'a pas d'mal à se faire du bien","Claude Mulot","1974"],
["L'amour blessé","Jean Pierre Lefebvre","1975"],
["Candice Candy","Pierre Unia","1975"],
["Cold Journey","Martin Defalco","1975"],
["Eliza's Horoscope","Gordon Sheppard","1975"],
["Gina","Denys Arcand","1975"],
["The Heatwave Lasted Four Days","Douglas Jackson","1975"],
["L'île jaune","Jean Cousineau","1975"],
["Lies My Father Told Me","Ján Kádar","1975"],
["M'en revenant par les épinettes","François Brault","1975"],
["Mustang","Marcel Lefebvre","1975"],
["Partis pour la gloire","Clément Perron","1975"],
["Pour le meilleur et pour le pire","Claude Jutra","1975"],
["Pousse mais pousse égal","Denis Héroux","1975"],
["Shivers","David Cronenberg","1975"],
["Le temps de l'avant","Anne Claire Poirier","1975"],
["La tête de Normande St-Onge","Gilles Carle","1975"],
["Tout feu, tout femme","Gilles Richer","1975"],
["Une nuit en Amérique","Jean Chabot","1975"],
["Les vautours","Jean-Claude Labrecque","1975"],
["L'absence","Brigitte Sauriol","1976"],
["Beat","André Blanchard","1976"],
["Born for Hell","Denis Héroux","1976"],
["Chanson pour Julie","Jacques Vallée","1976"],
["Contebleu","Yves Angrignon","1976"],
["Death Weekend","William Fruet","1976"],
["La fleur aux dents","Thomas Vamos","1976"],
["J.A. Martin photographe","Jean Beaudin","1976"],
["Je suis loin de toi mignonne","Claude Fournier","1976"],
["Jos Carbone","Hugues Tremblay","1976"],
["The Little Girl Who Lives Down the Lane","Nicolas Gessner","1976"],
["Parlez-nous d'amour","Jean-Claude Lord","1976"],
["La piastre","Alain Chartrand","1976"],
["Simple histoire d'amours","Fernand Dansereau","1976"],
["Ti-Cul Tougas","Jean-Guy Noël","1976"],
["L'ange et la femme","Gilles Carle","1977"],
["Cathy's Curse","Eddy Matalon","1977"],
["L'eau chaude, l'eau frette","André Forcier","1977"],
["Full Circle","Richard Loncraine","1977"],
["Ilsa the Tigress of Siberia","Jean Lafleur","1977"],
["La menace","Alain Corneau","1977"],
["One Man","Robin Spry","1977"],
["La p'tite violence","Hélène Girard","1977"],
["Panique","Jean-Claude Lord","1977"],
["Rabid","David Cronenberg","1977"],
["Rituals","Peter Carter","1977"],
["Le soleil se lève en retard","André Brassard","1977"],
["Ti-Mine, Bernie pis la gang…","Marcel Carrière","1977"],
["Le vieux pays où Rimbaud est mort","Jean Pierre Lefebvre","1977"],
["L'ange gardien","Jacques Fournier","1978"],
["Blackout","Eddy Matalon","1978"],
["City on Fire","Alvin Rakoff","1978"],
["Comme les six doigts de la main","André Mélançon","1978"],
["Drying Up the Streets","Robin Spry","1978"],
["L'homme en colère","Claude Pinoteau","1978"],
["In Praise of Older Women","George Kaczender","1978"],
["Jacob Two-Two Meets the Hooded Fang","Theodore J. Flicker","1978"],
["Les liens de sang","Claude Chabrol","1978"],
["Tomorrow Never Comes","Peter Collinson","1978"],
["Un grand logement","Mario Bolduc et Marie-Ginette Guay","1978"],
["Une amie d'enfance","Francis Mankiewicz","1978"],
["Violette Nozière","Claude Chabrol","1978"],
["À nous deux","Claude Lelouch","1979"],
["L'affaire Coffin","Jean-Claude Labrecque","1979"],
["L'arrache-cœur","Mireille Dansereau","1979"],
["Au revoir… à lundi","Maurice Dugowson","1979"],
["Avoir 16 ans","Jean Pierre Lefebvre","1979"],
["La belle apparence","Denyse Benoît","1979"],
["The Brood","David Cronenberg","1979"],
["Caro papa","Dino Risi","1979"],
["Les célébrations","Yves Simoneau","1979"],
["Le château de cartes","François Labonté","1979"],
["Cordélia","Jean Beaudin","1979"],
["Éclair au chocolat","Jean-Claude Lord","1979"],
["Fuir","Hélène Girard","1979"],
["Girls","Just Jaeckin","1979"],
["L'hiver bleu (Abitibi 1978)","André Blanchard","1979"],
["La maladie c'est les compagnies","Richard Boutet","1979"],
["A Man Called Intrepid","Peter Carter","1979"],
["Mourir à tue-tête","Anne Claire Poirier","1979"],
["Vie d'ange","Pierre Harel","1979"],
["Agency","George Kaczender","1980"],
["Atlantic City, U.S.A.","Louis Malle","1980"],
["Les bons débarras","Francis Mankiewicz","1980"],
["Ça peut pas être l'hiver on n'a même pas eu d'été","Louise Carré","1980"],
["Le chef se déniaise","Jean Luret","1980"],
["Contrecœur","Jean-Guy Noël","1980"],
["La cuisine rouge","Paule Baillargeon","1980"],
["Fantastica","Gilles Carle","1980"],
["Final Assignment","Paul Almond","1980"],
["Les grands enfants","Paul Tana","1980"],
["Hog Wild","Les Rose","1980"],
["L'homme à tout faire","Micheline Lanctôt","1980"],
["Hot Dogs","Claude Fournier","1980"],
["The Lucky Star","Max Fischer","1980"],
["Pinball Summer","George Mihalka","1980"],
["Strass café","Léa Pool","1980"],
["Suzanne","Robin Spry","1980"],
["Terror Train","Roger Spottiswoode","1980"],
["Thetford au milieu de notre vie","Iolande Cadrin-Rossignol et Fernand Dansereau","1980"],
["Les beaux souvenirs","Francis Mankiewicz","1981"],
["Black Mirror","Pierre-Alain Jolivet","1981"],
["Deux super dingues","Claude Castravelli","1981"],
["The Funny Farm","Ron Clark","1981"],
["Gas","Les Rose","1981"],
["La guerre du feu","Jean-Jacques Annaud","1981"],
["Happy Birthday to Me","J. Lee Thompson","1981"],
["Hard Feelings","Daryl Duke","1981"],
["Heartaches","Donald Shebib","1981"],
["Hot Touch","Roger Vadim","1981"],
["My Bloody Valentine","George Mihalka","1981"],
["Les Plouffe","Gilles Carle","1981"],
["Salut! J.W.","Ian Ireland","1981"],
["Le shift de nuit","Mario Bolduc","1981"],
["Ups & Downs","Paul Almond","1981"],
["Yesterday","Larry Kent","1981"],
["Your Ticket is No Longer Valid","George Kaczender","1981"],
["Cross Country","Paul Lynch","1982"],
["Doux aveux","Fernand Dansereau","1982"],
["Les fleurs sauvages","Jean Pierre Lefebvre","1982"],
["Le futur intérieur","Jean Chabot","1982"],
["Killing 'em Softly","Max Fischer","1982"],
["Larose, Pierrot et la Luce","Claude Gagnon","1982"],
["Luc ou La part des choses","Michel Audy","1982"],
["Paradise","Stuart Gilliard","1982"],
["La quarantaine","Anne-Claire Poirier","1982"],
["Scandale","George Mihalka","1982"],
["Une journée en taxi","Robert Ménard","1982"],
["Visiting Hours","Jean-Claude Lord","1982"],
["Les yeux rouges ou Les vérités accidentelles","Yves Simoneau","1982"],
["Au clair de la lune","André Forcier","1983"],
["Au pays de Zom","Gilles Groulx","1983"],
["Bonheur d'occasion","Claude Fournier","1983"],
["Lucien Brouillard","Bruno Carrière","1983"],
["Maria Chapdelaine","Gilles Carle","1983"],
["Porky's 2: The Next Day","Bob Clark","1983"],
["Rien qu'un jeu","Brigitte Sauriol","1983"],
["Le ruffian","José Giovanni","1983"],
["Snapshot","Abderahmane Mazouz","1983"],
["Videodrome","David Cronenberg","1983"],
["The Wars","Robin Phillips","1983"],
["Les Années de rêves","Jean-Claude Labrecque","1984"],
["The Bay Boy","Daniel Petrie","1984"],
["La couleur encerclée","Jean Gagné, Serge Gagné","1984"],
["Le crime d'Ovide Plouffe","Denys Arcand","1984"],
["Evil Judgment","Claude Castravelli","1984"],
["La femme de l'hôtel","Léa Pool","1984"],
["La Guerre des tuques","André Melançon","1984"],
["Hey Babe!","Rafal Zielinski","1984"],
["The Hotel New Hampshire","Tony Richardson","1984"],
["Jacques et Novembre","Jean Beaudry","1984"],
["Le jour « S... »","Jean Pierre Lefebvre","1984"],
["Louisiana","Philippe de Broca","1984"],
["Mario","Jean Beaudin","1984"],
["The Masculine Mystique","John N. Smith, Giles Walker","1984"],
["Memoirs","Bashar Shbib","1984"],
["Mother's Meat Freuds Flesh","Demetri Demetrios","1984"],
["Paroles et musiques","Élie Chouraqui","1984"],
["Perfect Timing","René Bonnière","1984"],
["Le sang des autres","Claude Chabrol","1984"],
["Sonatine","Micheline Lanctôt","1984"],
["The Surrogate","Don Carmody","1984"],
["90 Days","Giles Walker","1985"],
["L'adolescente sucre d'amour","Jocelyne Saab","1985"],
["Adramélech","Pierre Grégoire","1985"],
["Bayo","Mort Ransen","1985"],
["The Blue Man","George Mihalka","1985"],
["Caffè Italia Montréal","Paul Tana","1985"],
["Celui qui voit les heures","Pierre Goupil","1985"],
["La dame en couleurs","Claude Jutra","1985"],
["Le dernier glacier","Roger Frappier, Jacques Leduc","1985"],
["Elvis Gratton : Le king des kings","Pierre Falardeau","1985"],
["The Gunrunner","Nardo Castillo","1985"],
["Instantanés","Michel Juliani","1985"],
["Joshua Then and Now","Ted Kotcheff","1985"],
["Junior","Jim Hanley","1985"],
["Lune de miel","Patrick Jamain","1985"],
["Le matou","Jean Beaudin","1985"],
["Medium blues","Michel Préfontaine","1985"],
["Night Magic","Lewis Furey","1985"],
["Opération beurre de pinottes","Michael Rubbo","1985"],
["Québec, opération Lambda","Marc Degryse","1985"],
["Visage pâle","Claude Gagnon","1985"],
["Anne Trister","Léa Pool","1986"],
["Bach et Bottine","André Melançon","1986"],
["The Boy in Blue","Charles Jarrott","1986"],
["Cat Squad","William Freidkin","1986"],
["Contes des mille et un jours","Iolande Cadrin-Rossignol","1986"],
["Deaf & Mute","Hunt Hoe","1986"],
["Le Déclin de l'empire américain","Denys Arcand","1986"],
["Le dernier havre","Denyse Benoit","1986"],
["Équinoxe","Arthur Lamothe","1986"],
["Evixion","Bashar Shbib","1986"],
["The First Killing Frost","Roland Hallé","1986"],
["Exit","Robert Ménard","1986"],
["Les fous de bassan","Yves Simoneau","1986"],
["La guêpe","Gilles Carle","1986"],
["Henri","François Labonté","1986"],
["L'homme renversé","Yves Dion","1986"],
["Meatballs 3","George Mendeluk","1986"],
["The Morning Man","Danièle J Suissa","1986"],
["Overnight","Jack Darcus","1986"],
["Pouvoir intime","Yves Simoneau","1986"],
["Qui a tiré sur nos histoires d'amour?","Louise Carré","1986"],
["Sauve-toi Lola","Michel Drach","1986"],
["Sitting in Limbo","John N. Smith","1986"],
["Toby McTeague","Jean-Claude Lord","1986"],
["À l'automne de la vie","Yvan Chouinard","1987"],
["The Great Land of Small","Vojtech Jasny","1987"],
["Le frère André","Jean-Claude Labrecque","1987"],
["Grelots rouges sanglots bleus","Pierre Harel","1987"],
["La Guerre oubliée","Richard Boutet","1987"],
["Hitting Home","Robin Spry","1987"],
["L'Île","François Leterrier","1987"],
["Le Jeune Magicien","Waldemar Dziki","1987"],
["Keeping Track","Robin Spry","1987"],
["The Kid Brother ou Kenny","Claude Gagnon","1987"],
["The Last Straw","Giles Walker","1987"],
["La ligne de chaleur","Hubert-Yves Rose","1987"],
["Marie s'en va-t-en ville","Marquise Lepage","1987"],
["Les roses de Matmata","José Pinheiro","1987"],
["Seductio","Bashar Shbib","1987"],
["Le sourd dans la ville","Mireille Dansereau","1987"],
["Tinamer","Jean-Guy Noël","1987"],
["Train of Dreams","John N. Smith","1987"],
["Tristesse, modèle réduit","Robert Morin","1987"],
["Un zoo la nuit","Jean-Claude Lauzon","1987"],
["À corps perdu","Léa Pool","1988"],
["La boîte à soleil","Jean Pierre Lefebvre","1988"],
["La bottega del orefice","Michael Anderson","1988"],
["Clair-obscur","Bashar Shbib","1988"],
["Eva Guerrillera","Jacqueline Levitin","1988"],
["Family Reunion","Dick Sarin","1988"],
["Gaspard et fils","François Labonté","1988"],
["La Grenouille et la Baleine","Jean-Claude Lord","1988"],
["Horses in Winter","Rick Raxlen","1988"],
["Kalamazoo","André Forcier","1988"],
["Moonlight Flight","Jim Kaufman","1988"],
["La nuit avec Hortense","Jean Chabot","1988"],
["Oh! Oh! Satan!","André Farwagi","1988"],
["La peau et les os","Johanne Prégent","1988"],
["Les Portes tournantes","Francis Mankiewicz","1988"],
["Salut Victor","Anne Claire Poirier","1988"],
["Something About Love","Tom Berry","1988"],
["Les Tisserands du pouvoir","Claude Fournier","1988"],
["Tommy Tricker and the Stamp Traveller","Michael Rubbo","1988"],
["L'Air de rien","Mary Jimenez","1989"],
["The Amityville Curse","Tom Berry","1989"],
["Bye bye Chaperon rouge","Márta Mészáros","1989"],
["Cher frangin","Gérard Mordillat","1989"],
["Comment faire l'amour avec un nègre sans se fatiguer","Jacques W. Benoît","1989"],
["Cruising Bar","Robert Ménard","1989"],
["Dans le ventre du dragon","Yves Simoneau","1989"],
["Fierro… l'été des secrets","André Melançon","1989"],
["Jésus de Montréal","Denys Arcand","1989"],
["Laura Laur","Brigitte Sauriol","1989"],
["Matinée","Richard Martin","1989"],
["Les matins infidèles","Jean Beaudry, François Bouvier","1989"],
["Mindfield","Jean-Claude Lord","1989"],
["Les Noces de papier","Michel Brault","1989"],
["Off Off Off","Jorge Fajardo","1989"],
["Portion d'éternité","Robert Favreau","1989"],
["La Réception","Robert Morin","1989"],
["La Révolution française : les années terribles","Richard T. Heffron","1989"],
["Short Change","Nicholas Kinsey","1989"],
["Sous les draps, les étoiles","Jean-Pierre Gariepy","1989"],
["T'es belle Jeanne","Robert Ménard","1989"],
["Trois pommes à côté du sommeil","Jacques Leduc","1989"],
["Vent de galerne","Bernard Favre","1989"],
["Welcome to Canada","John N. Smith","1989"],
["Babylone","Manu Bonmariage","1990"],
["Back Stab","Jim Kaufman","1990"],
["Bethune: The Making of a Hero","Phillip Borsos","1990"],
["Breadhead","Carlo Alacchi","1990"],
["A Bullet in the Head","Attila Bertalan","1990"],
["Cargo","François Girard","1990"],
["The Company of Strangers","Cynthia Scott","1990"],
["Cursed","Mychel Arsenault","1990"],
["Dames galantes","Jean-Charles Tacchella","1990"],
["Ding et Dong, le film","Alain Chartrand","1990"],
["Falling Over Backwards","Mort Ransen","1990"],
["La Fille du maquignon","Abderrahmane Mazouz","1990"],
["I'm Happy. You're Happy. We're All Happy. Happy, Happy, Happy.","Verlcrow Ripper","1990"],
["La Liberté d'une statue","Olivier Asselin","1990"],
["Lola Zipper","Ilan Duran Cohen","1990"],
["Manuel : le fils emprunté","François Labonté","1990"],
["Moody Beach","Richard Roy","1990"],
["New York doré","Suzanne Guy","1990"],
["Le Party","Pierre Falardeau","1990"],
["Pas de répit pour Mélanie","Jean Beaudry","1990"],
["Princes in Exile","Giles Walker","1990"],
["Rafales","André Melançon","1990"],
["Remous","Sylvie Van Brabant","1990"],
["Le Royaume ou l'asile","Jean Gagné","1990"],
["The Secret of Nandy","Danièle J Suissa","1990"],
["Simon les nuages","Roger Cantin","1990"],
["Un autre homme","Charles Binamé","1990"],
["Un été après l'autre","Anne-Marie Étienne","1990"],
["Une histoire inventée","André Forcier","1990"],
["Alisée","André Blanchard","1991"],
["Amoureux fou","Robert Ménard","1991"],
["L'annonce faite à Marie","Alain Cuny","1991"],
["L'assassin jouait du trombone","Roger Cantin","1991"],
["La Championne","Elisabeta Bostan","1991"],
["… comme un voleur","Michel Langlois","1991"],
["The Dance Goes On","Paul Almond","1991"],
["Les Danseurs du Mozambique","Philippe Lefebvre","1991"],
["Deadly Surveillance","Paul Ziller","1991"],
["La Demoiselle sauvage","Léa Pool","1991"],
["Le Fabuleux voyage de l'ange","Jean Pierre Lefebvre","1991"],
["The Final Heist","George Mihalka","1991"],
["Lana in Love","Bashar Shbib","1991"],
["Love-moi","Marcel Simard","1991"],
["Montréal vu par…","Un collectif de réalisateurs","1991"],
["Motyli Cas","Bretislav Pojar","1991"],
["Nelligan","Robert Favreau","1991"],
["Pablo qui court","Bernard Bergeron","1991"],
["The Pianist","Claude Gagnon","1991"],
["La Révolution française : les années lumières","Robert Enrico","1991"],
["Scanners 2: The New Order","Christian Duguay","1991"],
["Sous le signe du poisson","Serge Pénard","1991"],
["Un Léger vertige","Diane Poitras","1991"],
["Vincent et moi","Michael Rubbo","1991"],
["Aline","Carole Laganière","1992"],
["L'Automne sauvage","Gabriel Pelletier","1992"],
["Being at Home with Claude","Jean Beaudin","1992"],
["La Bête de foire","Isabelle Hayeur","1992"],
["Call of the Wild","Bjorn Kristen","1992"],
["Canvas","Alain Zaloum","1992"],
["Coyote","Richard Ciupka","1992"],
["A Cry in the Night","Robin Spry","1992"],
["Deadbolt","Douglas Jackson","1992"],
["La Fenêtre","Monique Champagne","1992"],
["Francœur: exit pour nomades","Pierre Bastien","1992"],
["L'Homme de ma vie","Jean-Charles Tacchella","1992"],
["Killer Image","David Winning","1992"],
["El lado oscuro del corazón","Eliseo Subiela","1992"],
["Léolo","Jean-Claude Lauzon","1992"],
["Le Mirage","Jean-Claude Guiguet","1992"],
["La Postière","Gilles Carle","1992"],
["Psychic","George Mihalka","1992"],
["Requiem pour un beau sans-cœur","Robert Morin","1992"],
["La Sarrasine","Paul Tana","1992"],
["Scanners 3: The Takeover","Christian Duguay","1992"],
["Shadow of the Wolf","Jacques Dorfmann, Pierre Magny","1992"],
["Tirelire Combines & Cie","Jean Beaudry","1992"],
["La Vie fantôme","Jacques Leduc","1992"],
["Le Voleur de caméra","Claude Fortin","1992"],
["Les Amoureuses","Johanne Prégent","1993"],
["Armen et Bullik","Allan Cook","1993"],
["Because Why","Arto Paragamian","1993"],
["Cap Tourmente","Michel Langlois","1993"],
["Deux actrices","Micheline Lanctôt","1993"],
["Doublures","Michel Murray","1993"],
["La Florida","George Mihalka","1993"],
["L'Homme sur les quais","Raoul Peck","1993"],
["Love & Human Remains","Denys Arcand","1993"],
["Matusalem","Roger Cantin","1993"],
["Les Mots perdus : un film en quatre saisons","Marcel Simard","1993"],
["The Myth of the Male Orgasm","John Hamilton","1993"],
["The Neighbor","Rodney Gibbons","1993"],
["Pamietnik znaleziony w garbie","Jan Kidawa-Blonski","1993"],
["Les Pots cassés","François Bouvier","1993"],
["Saint-Fortunat, les blues","Daniel St-Laurent","1993"],
["Le Sexe des étoiles","Paule Baillargeon","1993"],
["Tendre guerre","Daniel Morin","1993"],
["Thirty Two Short Films About Glenn Gould","François Girard","1993"],
["La Visite","Jorge Fajardo","1993"],
["C'était le 12 du 12 et Chili avait les blues","Charles Binamé","1994"],
["The Child","George Mihalka","1994"],
["Crack Me Up","Bashar Shbib","1994"],
["Craque la vie !","Jean Beaudin","1994"],
["Draghoula","Basha Shbib","1994"],
["La Fête des rois","Marquise Lepage","1994"],
["Gaïa","Pierre Lang","1994"],
["El jardín del edén","Maria Novaro","1994"],
["Kabloonak","Claude Massot","1994"],
["Le Lac de la lune","Michel Jetté","1994"],
["Louis 19, le roi des ondes","Michel Poulette","1994"],
["Ma sœur, mon amour","Suzy Cohen","1994"],
["Mon amie Max","Michel Brault","1994"],
["Mouvements du désir","Léa Pool","1994"],
["Los Naufragos","Miguel Littin","1994"],
["Octobre","Pierre Falardeau","1994"],
["The Paperboy","Douglas Jackson","1994"],
["Le Retour des aventuriers du timbre perdu","Michael Rubbo","1994"],
["Rêve aveugle","Diane Beaudry","1994"],
["Ride Me","Bashar Shbib","1994"],
["Ruth","François Delisle","1994"],
["Le Secret de Jérôme","Phil Comeau","1994"],
["Stalked","Douglas Jackson","1994"],
["V'la l'cinéma ou le roman de Charles Pathé","Jacques Rouffio","1994"],
["Le Vent du Wyoming","André Forcier","1994"],
["La Vie d'un héros","Micheline Lanctôt","1994"],
["Warriors","Shimon Dotan","1994"],
["Windigo","Robert Morin","1994"],
["Yes sir! Madame…","Robert Morin","1994"],
["Angelo, Frédo et Roméo","Pierre Plante","1995"],
["Le Confessionnal","Robert Lepage","1995"],
["Eldorado","Charles Binamé","1995"],
["L'Enfant d'eau","Robert Ménard","1995"],
["Erreur sur la personne","Gilles Noël","1995"],
["La folie des crinolines","Jean Gagné, Serge Gagné","1995"],
["Highlander 3: The Sorcerer","Andy Morahan","1995"],
["Hollow Point","Sidney J. Furie","1995"],
["J'aime j'aime pas","Sylvie Groulx","1995"],
["Le jour des cris","Éric Gignac","1995"],
["Kids of the Round Table","Robert Tinnell","1995"],
["Liste noire","Jean-Marc Vallée","1995"],
["Motel","Pascal Maeder (en)","1995"],
["La Mule et les Émeraudes","Bashar Shbib","1995"],
["Screamers","Christian Duguay","1995"],
["Silent Hunter","Fred Williamson","1995"],
["Le Sphinx","Louis Saïa","1995"],
["Suburban Legend","Jay Ferguson","1995"],
["Witchboard 3","Peter Svatek","1995"],
["Wrong Woman","Douglas Jackson","1995"],
["Zigrail","André Turpin","1995"],
["Aire libre","Luis Armando Roche","1996"],
["La Ballade de Titus : une fable tout en couleur","Vincent De Brus","1996"],
["La Beauté, fatale et féroce","Roger Boire","1996"],
["Bullet to Beijing","George Mihalka","1996"],
["Caboose","Richard Roy","1996"],
["Cosmos","Un collectif de réalisateurs et de réalisatrices","1996"],
["Le Cri de la nuit","Jean Beaudry","1996"],
["L'Escorte","Denis Langlois","1996"],
["La Fabrication d'un meurtrier","Isabelle Poissant","1996"],
["L'Homme idéal","George Mihalka","1996"],
["L'Homme perché","Stefan Pleszczynski","1996"],
["Hot Sauce","Bashar Shbib","1996"],
["J'en suis !","Claude Fournier","1996"],
["Joyeux Calvaire","Denys Arcand","1996"],
["Karmina","Gabriel Pelletier","1996"],
["Lilies","John Greyson","1996"],
["La Marche à l'amour","Jean Gagné, Serge Gagné","1996"],
["Never Too Late","Giles Walker","1996"],
["L'Oreille d'un sourd","Mario Bolduc","1996"],
["Panic","Bashar Shbib","1996"],
["Le Polygraphe","Robert Lepage","1996"],
["Pudding chômeur","Gilles Carle","1996"],
["Rowing Through","Masato Harada","1996"],
["Le Silence des fusils","Arthur Lamothe","1996"],
["Sous-sol","Pierre Gang","1996"],
["The Strange Blues of Cowboy Red","Rick Raxlen","1996"],
["La Vengeance de la femme en noir","Roger Cantin","1996"],
["The Windsor Protocol","George Mihalka","1996"],
["L'absent","Céline Baril","1997"],
["The Assignment","Christian Duguay","1997"],
["Les Boys","Louis Saïa","1997"],
["Burnt Eden","Eugene Garcia","1997"],
["Cabaret neiges noires","Raymond Saint-Jean","1997"],
["The Call of the Wild","Peter Svatek","1997"],
["Le Ciel est à nous","Graham Guit","1997"],
["Clandestins","Denis Chouinard, Nicolas Wadimoff","1997"],
["La Comtesse de Bâton Rouge","André Forcier","1997"],
["La Conciergerie","Michel Poulette","1997"],
["De l'autre côté du cœur","Suzy Cohen","1997"],
["Habitat","René Daalder","1997"],
["Hemoglobin","Peter Svatek","1997"],
["Kayla","Nicholas Kendall","1997"],
["Maîtres anciens","Olivier Asselin","1997"],
["Matusalem II : le dernier des Beauchesne","Roger Cantin","1997"],
["Les Mille merveilles de l'univers","Jean-Michel Roux","1997"],
["Moustaches","Jim Kaufman","1997"],
["Reaper","John Bradshaw","1997"],
["Sinon, oui","Claire Simon","1997"],
["Le Siège de l'âme","Olivier Asselin","1997"],
["Stranger in the House","Rodney Gibbons","1997"],
["Strip Search","Rod Hewitt","1997"],
["Treasure Island","Peter Rowe","1997"],
["2 secondes","Manon Briand","1998"],
["L'âge de braise","Jacques Leduc","1998"],
["Aujourd'hui ou jamais","Jean Pierre Lefebvre","1998"],
["Babel","Gérard Pullicino","1998"],
["Les Boys 2","Louis Saïa","1998"],
["C't'à ton tour, Laura Cadieux","Denise Filiatrault","1998"],
["Captive","Matt Dorff","1998"],
["Le Cœur au poing","Charles Binamé","1998"],
["Dancing on the Moon","Kit Hood","1998"],
["Dead End","Douglas Jackson","1998"],
["La Déroute","Paul Tana","1998"],
["Fatal Affair","Marc S. Grenier","1998"],
["Foreign Ghosts","Hunt Hoe","1998"],
["Going to Kansas City","Pekka Mandart","1998"],
["Le grand serpent du monde","Yves Dion","1998"],
["Hasards ou Coïncidences","Claude Lelouch","1998"],
["Hathi","Philippe Gautier","1998"],
["In Her Defense","Sidney J. Furie","1998"],
["N.I.P.","François Raymond","1998"],
["Nguol thùa","Dai Sijie","1998"],
["Nico the Unicorn","Graeme Campbell","1998"],
["Nô","Robert Lepage","1998"],
["La Position de l'escargot","Michka Saäl","1998"],
["The Press Run","Robert Ditchburn","1998"],
["Quelque chose d'organique","Bertrand Bonello","1998"],
["Quiconque meurt, meurt à douleur","Robert Morin","1998"],
["Random Encounter","Douglas Jackson","1998"],
["Requiem for Murder","Douglas Jackson","1998"],
["Revoir Julie","Jeanne Crépeau","1998"],
["Running Home","Marc F. Voizard","1998"],
["Snitch","Ted Demme","1998"],
["Sublet","John Hamilton","1998"],
["Thunder Point","George Mihalka","1998"],
["Un 32 août sur Terre","Denis Villeneuve","1998"],
["Une aventure de Papyrus : La vengeance de Seth","Michel Gauthier","1998"],
["Le Violon rouge","François Girard","1998"],
["Winter Lily","Roshell Bissett","1998"],
["You Can Thank Me Later","Shimon Dotan","1998"],
["4 Days","Curtis Wehrfritz","1999"],
["Artificial Lies","Rodney Gibbons","1999"],
["L'Autobiographe amateur","Claude Fortin","1999"],
["Autour de la maison rose","Joana Hadjithomas","1999"],
["Le Dernier Souffle","Richard Ciupka","1999"],
["Elvis Gratton 2 : Miracle à Memphis","Pierre Falardeau, Julien Poulin","1999"],
["Emporte-moi","Léa Pool","1999"],
["La Fabrication des mâles","Un collectif de réalisateurs et de réalisatrices","1999"],
["Fish Out of Water","Geoffrey Edwards","1999"],
["Full Blast","Rodrigue Jean","1999"],
["Grey Owl","Richard Attenborough","1999"],
["Histoires d'hiver","François Bouvier","1999"],
["Home Team","Allan A. Goldstein","1999"],
["Laura Cadieux… la suite","Denise Filiatrault","1999"],
["Matroni et moi","Jean-Philippe Duval","1999"],
["Le Petit ciel","Jean-Sébastien Lord","1999"],
["Pin-Pon, le film","Ghyslaine Côté","1999"],
["Post mortem","Louis Bélanger","1999"],
["Quand je serai parti… vous vivrez encore","Michel Brault","1999"],
["Les Siamoises","Isabelle Hayeur","1999"],
["Souffle d'ailleurs","Martin Leclerc, Thomas Schneider","1999"],
["Souvenirs intimes","Jean Beaudin","1999"],
["Task Force","Richard Ciupka","1999"],
["Taxman","Alain Zaloum","1999"],
["Who Gets the House?","Timothy J. Nelson","1999"],
["The Witness Files","Douglas Jackson","1999"],
["Alegria","Franco Dragone","2000"],
["The Art of War","Christian Duguay","2000"],
["La beauté de Pandore","Charles Binamé","2000"],
["Blind Terror","Giles Walker","2000"],
["La bouteille","Alain DesRochers","2000"],
["Café Olé","Richard Roy","2000"],
["Deception","Max Fischer","2000"],
["Desire","Colleen Murphy","2000"],
["Les fantômes des trois Madeleine","Guylaine Dionne","2000"],
["Heavy Metal 2000","Michael Coldewey, Michel Lemire","2000"],
["Hochelaga","Michel Jetté","2000"],
["L'île de sable","Johanne Prégent","2000"],
["L'invention de l'amour","Claude Demers","2000"],
["Island of the Dead","Tim Southam","2000"],
["Jack and Ella","Brenda Keesal","2000"],
["La journée avant","David Lussier","2000"],
["The List","Sylvain Guy","2000"],
["Maelström","Denis Villeneuve","2000"],
["Méchant party","Mario Chabot","2000"],
["La moitié gauche du frigo","Philippe Falardeau","2000"],
["Les muses orphelines","Robert Favreau","2000"],
["My Little Devil","Gopi Desai","2000"],
["Ne dis rien","Simon Lacombe","2000"],
["Possible Worlds","Robert Lepage","2000"],
["Rats and Rabbits","Lewis Furey","2000"],
["La répétition","Catherine Corsini","2000"],
["Saint Jude","John L'Écuyer","2000"],
["Stardom","Denys Arcand","2000"],
["Subconscious Cruelty","Karim Hussain","2000"],
["The Tracker","Jeff Schechter","2000"],
["Trick or Treat","Marc Cayer","2000"],
["Two Thousand and None","Arto Paragamian","2000"],
["Un petit vent de panique","Pierre Greco","2000"],
["Varian's War","Lionel Chetwynd","2000"],
["La veuve de Saint-Pierre","Patrice Leconte","2000"],
["La vie après l'amour","Gabriel Pelletier","2000"],
["Xchange","Allan Moyle","2000"],
["15 février 1839","Pierre Falardeau","2001"],
["L'ange de goudron","Denis Chouinard","2001"],
["Les Boys 3","Louis Saïa","2001"],
["Des chiens dans la neige","Michel Welterlin","2001"],
["Le ciel sur la tête","Geneviève Lefebvre, André Melançon","2001"],
["Crème glacée, chocolat et autres consolations","Julie Hivon","2001"],
["Danny in the Sky","Denis Langlois","2001"],
["Dead Awake","Marc S. Grenier","2001"],
["Du pic au cœur","Céline Baril","2001"],
["La femme qui boit","Bernard Émond","2001"],
["La forteresse suspendue","Roger Cantin","2001"],
["Hidden Agenda","Marc S. Grenier","2001"],
["Hôtel des horizons","Marc Cayer","2001"],
["Karmen Geï","Joseph Gaï Ramaka","2001"],
["Karmina 2","Gabriel Pelletier","2001"],
["La loi du cochon","Érik Canuel","2001"],
["Lost and Delirious","Léa Pool","2001"],
["Mariages","Catherine Martin","2001"],
["Nuit de noces","Émile Gaudreault","2001"],
["Opération Cobra","Dominic Gagnon, Richard Jutras, Robert Morin","2001"],
["Le pornographe","Bertrand Bonello","2001"],
["Protection","John Flynn","2001"],
["So Faraway and Blue","Roy Cross","2001"],
["Stéphanie, Nathalie, Caroline et Vincent","Carl Ulrich","2001"],
["Un crabe dans la tête","André Turpin","2001"],
["Une jeune fille à la fenêtre","Francis Leclerc","2001"],
["La vérité est un mensonge","Pierre Goupil","2001"],
["Au fil de l'eau","Jeannine Gagné","2002"],
["Barbaloune","Jean Gagné, Serge Gagné","2002"],
["The Baroness and the Pig","Michael MacKenzie","2002"],
["Between Strangers","Edoardo Ponti","2002"],
["Le collectionneur","Jean Beaudin","2002"],
["Les Dangereux","Louis Saïa","2002"],
["Les Fils de Marie","Carole Laure","2002"],
["Le gambit du fou","Bruno Dubuc","2002"],
["Histoire de pen","Michel Jetté","2002"],
["Home","Phyllis Katrapani","2002"],
["Iso","Dominic Gagnon","2002"],
["Katryn's Place","Bénédicte Ronfard","2002"],
["Leaving Metropolis","Brad Fraser","2002"],
["Madame Brouette","Moussa Sene Absa","2002"],
["Le marais","Kim Nguyen","2002"],
["La Mystérieuse Mademoiselle C.","Richard Ciupka","2002"],
["Le Nèg'","Robert Morin","2002"],
["L'Odyssée d'Alice Tremblay","Denise Filiatrault","2002"],
["One Way Out","Allan A. Goldstein","2002"],
["Québec-Montréal","Ricardo Trogi","2002"],
["Régina!","Maria Sigurdardottir","2002"],
["Royal Bonbon","Charles Najman","2002"],
["Savage Messiah","Mario Azzopardi","2002"],
["Secret de banlieue","Louis Choquette","2002"],
["Séraphin : un homme et son péché","Charles Binamé","2002"],
["Sex at the End of the Millenium","Sheldon Neuberger","2002"],
["Station Nord","Jean-Claude Lord","2002"],
["Summer","Phil Price","2002"],
["La turbulence des fluides","Manon Briand","2002"],
["Women Without Wings","Nicholas Kinsey","2002"],
["Yellowknife","Rodrigue Jean","2002"],
["100 % bio","Claude Fortin","2003"],
["20h17, rue Darling","Bernard Émond","2003"],
["Annie Brocoli dans les fonds marins","Claude Brie","2003"],
["The Book of Eve","Claude Fournier","2003"],
["Comment ma mère accoucha de moi durant sa ménopause","Sébastien Rose","2003"],
["La face cachée de la lune","Robert Lepage","2003"],
["The Favourite Game","Bernar Hébert","2003"],
["Gaz Bar Blues","Louis Bélanger","2003"],
["La grande séduction","Jean-François Pouliot","2003"],
["L'homme trop pressé prend son thé à la fourchette","Sylvie Groulx","2003"],
["Les immortels","Paul Thinel","2003"],
["Les invasions barbares","Denys Arcand","2003"],
["Jack Paradise","Gilles Noël","2003"],
["Ma voisine danse le ska","Nathalie St-Pierre","2003"],
["Mambo Italiano","Émile Gaudreault","2003"],
["Le manuscrit érotique","Jean Pierre Lefebvre","2003"],
["Nez rouge","Érik Canuel","2003"],
["Père et fils","Michel Boujenah","2003"],
["La petite Lili","Claude Miller","2003"],
["Le piège d'Issoudun","Micheline Lanctôt","2003"],
["Revival Blues","Claude Gagnon","2003"],
["Saved by the Belles","Ziad Touma","2003"],
["Le secret de Cyndia","Denyse Benoît","2003"],
["A Silent Love","Federico Hidalgo","2003"],
["Sur le seuil","Éric Tessier","2003"],
["Les triplettes de Belleville","Sylvain Chomet","2003"],
["Twist","Jacob Tierney","2003"],
["Acapulco Gold","André Forcier","2004"],
["Les aimants","Yves P. Pelletier","2004"],
["The Blue Butterfly","Léa Pool","2004"],
["Le bonheur c'est une chanson triste","François Delisle","2004"],
["Bonzaïon","Danny Gilmore, Clermont Jolicoeur","2004"],
["C'est pas moi… c'est l'autre","Alain Zaloum","2004"],
["Camping sauvage","Guy A. Lepage, Sylvain Roy","2004"],
["Comment conquérir l'Amérique en une nuit?","Dany Laferrière","2004"],
["Comment devenir un trou de cul et enfin plaire aux femmes","Roger Boire","2004"],
["CQ2 (Seek You Too)","Carole Laure","2004"],
["Dans l'œil du chat","Rudy Barichello","2004"],
["Dans une galaxie près de chez vous","Claude Desrosiers","2004"],
["Le dernier tunnel","Érik Canuel","2004"],
["Dorian","Allan A. Goldstein","2004"],
["Elles étaient cinq","Ghyslaine Côté","2004"],
["Elvis Gratton XXX : la vengeance d'Elvis Wong","Pierre Falardeau","2004"],
["L'espérance","Stefan Pleszczynski","2004"],
["Éternelle","Wilhelm Liebenberg","2004"],
["Folle embellie","Dominique Cabrera","2004"],
["Goldirocks","Paula Tiberius","2004"],
["Le golem de Montréal","Isabelle Hayeur","2004"],
["Le goût des jeunes filles","John L'Écuyer","2004"],
["L'incomparable mademoiselle C.","Richard Ciupka","2004"],
["Je n'aime que toi","Claude Fournier","2004"],
["Jeunesse dans l'ombre","Gervais Germain","2004"],
["Jimmywork","Simon Sauvé","2004"],
["Littoral","Wajdi Mouawad","2004"],
["La lune viendra d'elle-même","Marie-Jan Seille","2004"],
["Ma vie en cinémascope","Denise Filiatrault","2004"],
["Mémoires affectives","Francis Leclerc","2004"],
["Monica la mitraille","Pierre Houle","2004"],
["Les moscovites","Charles Barabé","2004"],
["Nouvelle-France","Jean Beaudin","2004"],
["Ordo","Laurence Ferreira Barbosa","2004"],
["La peau blanche","Daniel Roby","2004"],
["La pension des étranges","Stella Goulet","2004"],
["Pinocchio 3000","Daniel Robichaud","2004"],
["La planque","Alexandre Chartrand","2004"],
["Premier juillet - le film","Philippe Gagnon","2004"],
["Pure","Jim Donovan","2004"],
["Vendus","Éric Tessier","2004"],
["Amnésie - L'Énigme James Brighton","Denis Langlois","2005"],
["L'audition","Luc Picard","2005"],
["Aurore","Luc Dionne","2005"],
["Barmaids","Simon Boisvert","2005"],
["Les Boys 4","George Mihalka","2005"],
["C.R.A.Z.Y.","Jean-Marc Vallée","2005"],
["Le chalet","Jarrett Mann","2005"],
["Daniel et les Superdogs","André Melançon","2005"],
["La dernière incarnation","Demian Fuica","2005"],
["Les états nordiques","Denis Côté","2005"],
["Les États-Unis d'Albert","André Forcier","2005"],
["Familia","Louise Archambault","2005"],
["Greg & Gentillon","Matthiew Klinck","2005"],
["Horloge biologique","Ricardo Trogi","2005"],
["Idole instantanée","Yves Desgagnés","2005"],
["Kamataki","Claude Gagnon","2005"],
["Latex","Pierre Lapointe","2005"],
["Maman Last Call","François Bouvier","2005"],
["Manners of Dying","Jeremy Peter Allen","2005"],
["Maurice Richard","Charles Binamé","2005"],
["Les moutons de Jacob","Jean-François Pothier","2005"],
["La neuvaine","Bernard Émond","2005"],
["Niagara Motel","Gary Yates","2005"],
["Petit pow! pow! Noël","Robert Morin","2005"],
["La pharmacie de l'espoir","François Gourd","2005"],
["Saint-Ralph","Michael McGowan","2005"],
["Saints-Martyrs-des-Damnés","Robin Aubert","2005"],
["Stories of a Gravedigger","David Aubin","2005"],
["Summer with the Ghosts","Bernd Neuburger","2005"],
["Le survenant","Érik Canuel","2005"],
["Vers le sud","Laurent Cantet","2005"],
["La vie avec mon père","Sébastien Rose","2005"],
["1st bite","Hunt Hoe","2006"],
["The 4th Life","François Miron","2006"],
["L'anus Horribilis","Bruno Dubuc","2006"],
["La belle bête","Karim Hussain","2006"],
["Black Eyed Dog","Pierre Gang","2006"],
["Bluff sou bluff","Wilfort Estimable","2006"],
["Bobby","François Blouin","2006"],
["Bon cop bad cop","Érik Canuel","2006"],
["C'est beau une ville la nuit","Richard Bohringer","2006"],
["Cadavre exquis première édition","Un collectif de réalisateurs et de réalisatrices","2006"],
["Les cavaliers de la canette","Louis Champagne","2006"],
["Cercle vicieux","Patrick Hébert","2006"],
["Cheech","Patrice Sauvé","2006"],
["Comme tout le monde","Pierre-Paul Renders","2006"],
["Congorama","Philippe Falardeau","2006"],
["Convoitises","Jean Alix Holmand","2006"],
["La coupure","Jean Châteauvert","2006"],
["De ma fenêtre, sans maison…","Maryanne Zéhil","2006"],
["Délivrez-moi","Denis Chouinard","2006"],
["The Descendant","Philippe Spurrell","2006"],
["Duo","Richard Ciupka","2006"],
["End of the Line","Maurice Devereaux","2006"],
["Les filles du botaniste","Dai Sijie","2006"],
["Gason makoklen 1","Wilfort Estimable","2006"],
["Gason makoklen 2","Wilfort Estimable","2006"],
["Le génie du crime","Louis Bélanger","2006"],
["Le guide de la petite vengeance","Jean-François Pouliot","2006"],
["Heads of Control: The Gorul Baheu Brain Expedition","Pat Tremblay","2006"],
["Histoire de famille","Michel Poulette","2006"],
["Hiver","Charles Barabé","2006"],
["The Journals of Knud Rasmussen","Norman Cohn, Zacharias Kunuk","2006"],
["Lézaureil kilyve","Charles Barabé","2006"],
["Malléables journées","Charles Barabé","2006"],
["Manga Latina: Killer on the Loose","Henrique Vera-Villaneuva","2006"],
["Martin Clear: The Whole Story","Martin Leclerc","2006"],
["The Passenger","François Rotger","2006"],
["The Point","Joshua Dorsey","2006"],
["La porte de Fersein","Éric Campeau, Simon Tardif","2006"],
["Pourquoi le dire?","Hélène Duchesneau","2006"],
["Prenez vos places","Marc Thomas-Dupuis","2006"],
["Que Dieu bénisse l'Amérique","Robert Morin","2006"],
["La rage de l'ange","Dan Bigras","2006"],
["Rechercher Victor Pellerin","Sophie Deraspe","2006"],
["Roméo et Juliette","Yves Desgagnés","2006"],
["Sans elle","Jean Beaudin","2006"],
["Le secret de ma mère","Ghyslaine Côté","2006"],
["Steel Toes","Mark Adam, David Gow","2006"],
["Sur la trace d'Igor Rizzi","Noël Mitrani","2006"],
["These Girls","John Hazlett","2006"],
["Touche pas à mon homme","Jean Rony Lubin","2006"],
["Tous les autres, sauf moi","Ann Arson","2006"],
["Un dimanche à Kigali","Robert Favreau","2006"],
["Une rame à l'eau","Jean-François Boudreault","2006"],
["La vie secrète des gens heureux","Stéphane Lapointe","2006"],
["A Year in the Death of Jack Richards","Benjamin P. Paquette","2006"],
["Les 3 p'tits cochons","Patrick Huard","2007"],
["À vos marques… party!","Frédérik D'Amours","2007"],
["Adam's Wall","Michael MacKenzie","2007"],
["L'âge des ténèbres","Denys Arcand","2007"],
["Amour, mensonges et conséquences","Jean Alix Holmand","2007"],
["The Backup Man","Doug Sutherland","2007"],
["La belle empoisonneuse","Richard Jutras","2007"],
["Bluff","Simon-Olivier Fecteau, Marc-André Lavoie","2007"],
["Borderline","Lyne Charlebois","2007"],
["La brunante","Fernand Dansereau","2007"],
["La capture","Carole Laure","2007"],
["Le cèdre penché","Rafaël Ouellet","2007"],
["Continental, un film sans fusil","Stéphane Lafleur","2007"],
["Contre toute espérance","Bernard Émond","2007"],
["Dans les villes","Catherine Martin","2007"],
["De l'autre côté","Sean Marckos","2007"],
["Échangistes","Simon Boisvert","2007"],
["Emotional Arithmetic","Paolo Barzman","2007"],
["Imitation","Federico Hidalgo","2007"],
["La lâcheté","Marc Bisaillon","2007"],
["La logique du remords","Martin Laroche","2007"],
["Ma fille mon ange","Alexis Durand-Brault","2007"],
["Ma tante Aline","Gabriel Pelletier","2007"],
["Minushi","Tyler Gibb","2007"],
["Nitro","Alain DesRochers","2007"],
["Nos vies privées","Denis Côté","2007"],
["Nos voisins Dhantsu","Alain Chicoine","2007"],
["Où vas-tu, Moshé?","Hassan Benjelloun","2007"],
["Puffball","Nicolas Roeg","2007"],
["Rêves de poussière","Laurent Salgues","2007"],
["Le ring","Anaïs Barbeau-Lavalette","2007"],
["La rivière aux castors","Philippe Calderon","2007"],
["Shake Hands with the Devil","Roger Spottiswoode","2007"],
["Silk","François Girard","2007"],
["Steak","Quentin Dupieux","2007"],
["Surviving My Mother","Émile Gaudreault","2007"],
["Toi","François Delisle","2007"],
["Un cri au bonheur","Un collectif de réalisateurs et de réalisatrices","2007"],
["Voleurs de chevaux","Micha Wald","2007"],
["Young Triffie","Mary Walsh","2007"],
["24 mesures","Jalil Lespert","2008"],
["À l'ouest de Pluton","Henry Bernadet, Myriam Verreault","2008"],
["Adagio pour un gars de bicycle","Pascale Ferland","2008"],
["Amour, destin et rock'n'roll","Dario Gasbarro","2008"],
["Babine","Luc Picard","2008"],
["Le banquet","Sébastien Rose","2008"],
["Before Tomorrow","Marie-Hélène Cousineau, Madeline Ivalu","2008"],
["C'est pas moi, je le jure!","Philippe Falardeau","2008"],
["Le cas Roberge","Raphaël Malo","2008"],
["Ce qu'il faut pour vivre","Benoît Pilon","2008"],
["Cruising Bar 2","Michel Côté, Robert Ménard","2008"],
["Dans une galaxie près de chez vous 2","Philippe Gagnon","2008"],
["Demain","Maxime Giroux","2008"],
["Derrière moi","Rafaël Ouellet","2008"],
["Le déserteur","Simon Lavoie","2008"],
["Elle veut le chaos","Denis Côté","2008"],
["En plein cœur","Stéphane Géhami","2008"],
["Le grand départ","Claude Meunier","2008"],
["La ligne brisée","Louis Choquette","2008"],
["Magique!","Philippe Muyl","2008"],
["Maman est chez le coiffeur","Léa Pool","2008"],
["Papa à la chasse aux lagopèdes","Robert Morin","2008"],
["Le piège américain","Charles Binamé","2008"],
["Les plus beaux yeux du monde","Pascal Courchesne, Charlotte Laurier","2008"],
["Restless","Amos Kollek","2008"],
["Suivre Catherine","Jeanne Crépeau","2008"],
["Tout est parfait","Yves-Christian Fournier","2008"],
["Truffe","Kim Nguyen","2008"],
["Un capitalisme sentimental","Olivier Asselin","2008"],
["Un été sans point ni coup sûr","Francis Leclerc","2008"],
["Wushu Warrior","Alain DesRochers","2008"],
["1981","Ricardo Trogi","2009"],
["3 saisons","Jim Donovan","2009"],
["40 is the new 20","Simon Boisvert","2009"],
["5150, rue des Ormes","Éric Tessier","2009"],
["À quelle heure le train pour nulle part","Robin Aubert","2009"],
["À vos marques… party! 2","Frédérik D'Amours","2009"],
["Le bonheur de Pierre","Robert Ménard","2009"],
["Cadavres","Érik Canuel","2009"],
["Carcasses","Denis Côté","2009"],
["La chambre noire","François Aubry","2009"],
["De père en flic","Émile Gaudreault","2009"],
["Dédé à travers les brumes","Jean-Philippe Duval","2009"],
["Détour","Sylvain Guy","2009"],
["Les doigts croches","Ken Scott","2009"],
["La donation","Bernard Émond","2009"],
["Grande Ourse - La clé des possibles","Patrice Sauvé","2009"],
["Les grandes chaleurs","Sophie Lorain","2009"],
["Impasse","Joël Gauthier","2009"],
["J'ai tué ma mère","Xavier Dolan","2009"],
["Je me souviens","André Forcier","2009"],
["Lost Song","Rodrigue Jean","2009"],
["Love and Savagery","John N. Smith","2009"],
["Martyrs","Pascal Laugier","2009"],
["Modern Love","Stéphane Kazandjian","2009"],
["Noémie le secret","Frédérik D'Amours","2009"],
["Nuages sur la ville","Simon Galiero","2009"],
["Les pieds dans le vide","Mariloup Wolfe","2009"],
["Polytechnique","Denis Villeneuve","2009"],
["Pour toujours les Canadiens","Sylvain Archambault","2009"],
["Refrain","Tyler Gibb","2009"],
["Sans dessein","Caroline Labrèche, Steeve Léonard","2009"],
["Serveuses demandées","Guylaine Dionne","2009"],
["Suzie","Micheline Lanctôt","2009"],
["The Timekeeper","Louis Bélanger","2009"],
["Transit","Christian de la Cortina","2009"],
["Turbid","George Fok","2009"],
["Un ange à la mer","Frédéric Dumont","2009"],
["Un cargo pour l'Afrique","Roger Cantin","2009"],
["10 1/2","Podz","2010"],
["2 fois une femme","François Delisle","2010"],
["2 frogs dans l'Ouest","Dany Papineau","2010"],
["À l'origine d'un cri","Robin Aubert","2010"],
["Les amours imaginaires","Xavier Dolan","2010"],
["L'appât","Yves Simoneau","2010"],
["Le baiser du barbu","Yves P. Pelletier","2010"],
["Cabotins","Alain DesRochers","2010"],
["La cité","Kim Nguyen","2010"],
["Curling","Denis Côté","2010"],
["La dernière fugue","Léa Pool","2010"],
["L'enfant prodige","Luc Dionne","2010"],
["Everywhere","Alexis Durand-Brault","2010"],
["Filière 13","Patrick Huard","2010"],
["Incendies","Denis Villeneuve","2010"],
["Le journal d'Aurélie Laflamme","Christian Laurence","2010"],
["Journal d'un coopérant","Robert Morin","2010"],
["Lance et compte","Frédérik D'Amours","2010"],
["Lucidité passagère","Fabrice Barrilliet","2010"],
["Mesrine - L'instinct de mort","Jean-François Richet","2010"],
["Les mots gelés","Isabelle D'Amours","2010"],
["New Denmark","Rafaël Ouellet","2010"],
["Oscar et la dame rose","Éric-Emmanuel Schmitt","2010"],
["Piché : entre ciel et terre","Sylvain Archambault","2010"],
["Le poil de la bête","Philippe Gagnon","2010"],
["Reste avec moi","Robert Ménard","2010"],
["Romaine par moins 30","Agnès Obadia","2010"],
["Route 132","Louis Bélanger","2010"],
["Les sept jours du talion","Podz","2010"],
["Les signes vitaux","Sophie Deraspe","2010"],
["Snow & Ashes","Charles-Olivier Michaud","2010"],
["Sortie 67","Jephté Bastien","2010"],
["Trois temps après la mort d'Anna","Catherine Martin","2010"],
["Tromper le silence","Julie Hivon","2010"],
["The Trotsky","Jacob Tierney","2010"],
["The Wild Hunt","Alexandre Franchi","2010"],
["Y'en aura pas de facile","Marc-André Lavoie","2010"],
["À trois, Marie s'en va","Anne-Marie Ngô","2011"],
["Angle mort","Dominic James","2011"],
["Le bonheur des autres","Jean-Philippe Pearson","2011"],
["BumRush","Michel Jetté","2011"],
["Café de Flore","Jean-Marc Vallée","2011"],
["Le colis","Gaël D'Ynglemare","2011"],
["Coteau Rouge","André Forcier","2011"],
["Décharge","Benoît Pilon","2011"],
["Die","Dominic James","2011"],
["Le divan du monde","Dominic Desjardins","2011"],
["En terrains connus","Stéphane Lafleur","2011"],
["La fille de Montréal","Jeanne Crépeau","2011"],
["French Immersion","Kevin Tierney","2011"],
["French Kiss","Sylvain Archambault","2011"],
["Frisson des collines","Richard Roy","2011"],
["Funkytown","Daniel Roby","2011"],
["Gerry","Alain DesRochers","2011"],
["Good Neighbours","Jacob Tierney","2011"],
["The High Cost of Living","Deborah Chow","2011"],
["Jaloux","Patrick Demers","2011"],
["Jo pour Jonathan","Maxime Giroux","2011"],
["The Kate Logan Affair","Noël Mitrani","2011"],
["Laurentie","Mathieu Denis, Simon Lavoie","2011"],
["Marécages","Guy Édoin","2011"],
["Monsieur Lazhar","Philippe Falardeau","2011"],
["Nuit #1","Anne Émond","2011"],
["Pour l'amour de Dieu","Micheline Lanctôt","2011"],
["La run","Demian Fuica","2011"],
["Le sens de l'humour","Émile Gaudreault","2011"],
["Shadowboxing","Jesse Klein","2011"],
["Starbuck","Ken Scott","2011"],
["Sur le rythme","Charles-Olivier Michaud","2011"],
["Une vie qui commence","Michel Monty","2011"],
["Le vendeur","Sébastien Pilote","2011"],
["La vérité","Marc Bisaillon","2011"],
["The Year Dolly Parton Was My Mom","Tara Johns","2011"],
["L'affaire Dumont","Podz","2012"],
["Après la neige","Paul Barbeau","2012"],
["Avant que mon cœur bascule","Sébastien Rose","2012"],
["Bestiaire","Denis Côté","2012"],
["Camion","Rafaël Ouellet","2012"],
["Columbarium","Steve Kerr","2012"],
["L'empire Bo$$é","Claude Desrosiers","2012"],
["Ésimésac","Luc Picard","2012"],
["The Girl in the White Coat","Darrell Wasyk","2012"],
["The Hat Goes Wild","Guy Sprung","2012"],
["Hors les murs","David Lambert","2012"],
["Inch'Allah","Anaïs Barbeau-Lavalette","2012"],
["L'incrédule","Federico Hidalgo","2012"],
["J'espère que tu vas bien","David La Haye","2012"],
["Karakara","Claude Gagnon","2012"],
["Laurence Anyways","Xavier Dolan","2012"],
["Liverpool","Manon Briand","2012"],
["Mars et Avril","Martin Villeneuve","2012"],
["Mesnak","Yves Sioui Durand","2012"],
["La mise à l'aveugle","Simon Galiero","2012"],
["Omertà","Luc Dionne","2012"],
["Les pee-wee 3D","Éric Tessier","2012"],
["La peur de l'eau","Gabriel Pelletier","2012"],
["Rebelle","Kim Nguyen","2012"],
["Roméo Onze","Ivan Grbovic","2012"],
["Thanatomorphose","Éric Falardeau","2012"],
["Le torrent","Simon Lavoie","2012"],
["Tout ce que tu possèdes","Bernard Émond","2012"],
["Une bouteille dans la mer de Gaza","Thierry Binisti","2012"],
["La vallée des larmes","Maryanne Zéhil","2012"],
["1er amour","Guillaume Sylvestre","2013"],
["Les 4 soldats","Robert Morin","2013"],
["Amsterdam","Stefan Miljevic","2013"],
["L'autre maison","Mathieu Roy","2013"],
["Catimini","Nathalie Saint-Pierre","2013"],
["Chasse au Godard d'Abbittibbi","Éric Morin","2013"],
["La cicatrice","Jimmy Larouche","2013"],
["Cyanure","Séverine Cornamusaz","2013"],
["Le démantèlement","Sébastien Pilote","2013"],
["Diego Star","Frédérick Pelletier","2013"],
["Émilie","Guillaume Lonergan","2013"],
["Finissant(e)s","Rafaël Ouellet","2013"],
["Gabrielle","Louise Archambault","2013"],
["The Good Lie","Shawn Linden","2013"],
["Home Again","Sudz Sutherland","2013"],
["Hot Dog","Marc-André Lavoie","2013"],
["Il était une fois les Boys","Richard Goudreau","2013"],
["Jappeloup","Christian Duguay","2013"],
["Lac Mystère","Érik Canuel","2013"],
["La légende de Sarila","Nancy Florence Savard","2013"],
["Louis Cyr","Daniel Roby","2013"],
["La maison du pêcheur","Alain Chartrand","2013"],
["Les manèges humains","Martin Laroche","2013"],
["Memories Corner","Audrey Fouché","2013"],
["Le météore","François Delisle","2013"],
["Misogyny/Misandry","Erik Anderson","2013"],
["Moroccan Gigolos","Ismaël Saidi","2013"],
["Ressac","Pascale Ferland","2013"],
["Roche papier ciseaux","Yan Lanouette Turgeon","2013"],
["Rouge sang","Martin Doepner","2013"],
["Sarah préfère la course","Chloé Robichaud","2013"],
["Triptyque","Robert Lepage","2013"],
["Une jeune fille","Catherine Martin","2013"],
["Vic+Flo ont vu un ours","Denis Côté","2013"],
["1987","Ricardo Trogi","2014"],
["2 temps 3 mouvements","Christophe Cousin","2014"],
["3 histoires d'Indiens","Robert Morin","2014"],
["L'ange gardien","Jean-Sébastien Lord","2014"],
["Arwad","Dominique Chila","2014"],
["Bunker","Patrick Boivin","2014"],
["Ceci n'est pas un polar","Patrick Gazé","2014"],
["Le coq de St-Victor","Pierre Greco","2014"],
["Discopathe","Renaud Gauthier","2014"],
["Dr. Cabbie","Jean-François Pouliot","2014"],
["Exil","Charles-Olivier Michaud","2014"],
["La ferme des humains","Onur Karaman","2014"],
["Le fille du Martin","Samuel Thivierge","2014"],
["La gang des hors-la-loi","Jean Beaudry","2014"],
["La garde","Sylvain Archambault","2014"],
["Gerontophilia","Bruce LaBruce","2014"],
["The Grand Seduction","Don McKellar","2014"],
["Henri Henri","Martin Talbot","2014"],
["J'espère que tu vas bien 2","David La Haye","2014"],
["Limoilou","Edgar Fritz","2014"],
["Love Projet","Carole Laure","2014"],
["Maïna","Michel Poulette","2014"],
["Les maîtres du suspense","Stéphane Lapointe","2014"],
["Meetings with a Young Poet","Rudy Barichello","2014"],
["Miraculum","Podz","2014"],
["Mommy","Xavier Dolan","2014"],
["My Guys","Joseph Antaki","2014"],
["La petite reine","Alexis Durand-Brault","2014"],
["Pour que plus jamais","Marie Ange Barbancourt","2014"],
["Qu'est-ce qu'on fait ici?","Julie Hivon","2014"],
["Que ta joie demeure","Denis Côté","2014"],
["Rédemption","Joël Gauthier","2014"],
["Le règne de la beauté","Denys Arcand","2014"],
["Rhymes for Young Ghouls","Jeff Barnaby","2014"],
["Tom à la ferme","Xavier Dolan","2014"],
["Tu dors Nicole","Stéphane Lafleur","2014"],
["Un parallèle plus tard","Sébastien Landry","2014"],
["Une vie pour deux","Luc Bourdon","2014"],
["Uvanga","Marie-Hélène Cousineau, Madeline Piujuq Ivalu","2014"],
["La voix de l'ombre","Annie Molin Vasseur","2014"],
["Le vrai du faux","Émile Gaudreault","2014"],
["Whitewash","Emanuel Hoss-Desmarais","2014"],
["L'amour au temps de la guerre civile","Rodrigue Jean","2015"],
["Anna","Charles-Olivier Michaud","2015"],
["Antoine et Marie","Jimmy Larouche","2015"],
["Aurélie Laflamme - Les pieds sur terre","Nicolas Monette","2015"],
["Autrui","Micheline Lanctôt","2015"],
["Le bruit des arbres","François Péloquin","2015"],
["Ce qu'il ne faut pas dire","Marquise Lepage","2015"],
["Chorus","François Delisle","2015"],
["Le cœur de madame Sabali","Ryan McKenna","2015"],
["Corbo","Mathieu Denis","2015"],
["Les démons","Philippe Lesage","2015"],
["Le dep","Sonia Bonspille Boileau","2015"],
["Ego Trip","Benoit Pelletier","2015"],
["Elephant Song","Charles Binamé","2015"],
["L'énergie sombre","Leonardo Fuica","2015"],
["Les Êtres chers","Anne Émond","2015"],
["Félix et Meira","Maxime Giroux","2015"],
["The Forbidden Room","Guy Maddin, Evan Johnson","2015"],
["Le garagiste","Renée Beaulieu","2015"],
["La Guerre des tuques 3D","François Brisson, Jean-François Pouliot","2015"],
["Guibord s'en va-t-en guerre","Philippe Falardeau","2015"],
["Gurov & Anna","Rafaël Ouellet","2015"],
["Je suis à toi","David Lambert","2015"],
["Le journal d'un vieil homme","Bernard Émond","2015"],
["Les Loups","Sophie Deraspe","2015"],
["Le Mirage","Ricardo Trogi","2015"],
["Noir (NWA)","Yves-Christian Fournier","2015"],
["Nouvelles, nouvelles","Olivier Godin","2015"],
["La Passion d'Augustine","Léa Pool","2015"],
["Paul à Québec","François Bouvier","2015"],
["Preggoland","Jacob Tierney","2015"],
["Le scaphandrier","Alain Vézina","2015"],
["Scratch","Sébastien Godron","2015"],
["Standstill","Majdi El-Omari","2015"],
["Turbo Kid","François Simard, Anouk et Yoann-Karl Whissell","2015"],
["Ville-Marie","Guy Édoin","2015"],
["01:54","Yan England","2016"],
["Les 3 p'tits cochons 2","Jean-François Pouliot","2016"],
["9, le film","Un collectif de réalisateurs et de réalisatrices","2016"],
["Avant les rues","Chloé Leriche","2016"],
["Boris sans Béatrice","Denis Côté","2016"],
["Ceux qui font les révolutions à moitié n'ont fait que se creuser un tombeau","Simon Lavoie, Mathieu Denis","2016"],
["La Chasse au collet","Steve Kerr","2016"],
["Chasse-Galerie : La légende","Jean-Philippe Duval","2016"],
["Déserts","Charles-André Coderre, Yann-Manuel Hernandez","2016"],
["D’encre et de sang","Alexis Fortier Gauthier, Maxim Rheault, Francis Fortin","2016"],
["Early Winter","Michael Rowe","2016"],
["Ecartée","Lawrence Côté-Collins","2016"],
["Embrasse-moi comme tu m'aimes","André Forcier","2016"],
["Endorphine","André Turpin","2016"],
["Feuilles mortes","Thierry Bouffard, Steve Landry, Édouard A. Tremblay","2016"],
["Generation Wolf","Christian de la Cortina","2016"],
["Harry : Portrait d’un détective privé","Maxime Desruisseaux","2016"],
["Juste la fin du monde","Xavier Dolan","2016"],
["King Dave","Podz","2016"],
["Là où Attila passe","Onur Karaman","2016"],
["Les Mauvaises herbes","Louis Bélanger","2016"],
["Mes ennemis","Stéphane Géhami","2016"],
["Mon ami Dino","Jimmy Larouche","2016"],
["Montréal la blanche","Bachir Bensadekk","2016"],
["Nitro Rush","Alain Desrochers","2016"],
["L'Origine des espèces","Dominic Goyer","2016"],
["Le Pacte des anges","Richard Angers","2016"],
["Pays","Chloé Robichaud","2016"],
["Prank","Vincent Biron","2016"],
["Le Rang du lion","Stéphane Beaudoin","2016"],
["The Saver","Wiebke von Carolsfeld","2016"],
["Toujours encore","Jean-François Boisvenue","2016"],
["Two Lovers and a Bear","Kim Nguyen","2016"],
["Un paradis pour tous","Robert Morin","2016"],
["Vortex","Jephté Bastien","2016"],
["Votez Bougon","Jean-François Pouliot","2016"],
["Les Affamés","Robin Aubert","2017"],
["Après coup","Noël Mitrani","2017"],
["Les arts de la parole","Olivier Godin","2017"],
["L'Autre côté de novembre","Maryanne Zéhil","2017"],
["Ballerina","Éric Warrin, Éric Summer","2017"],
["Bon Cop Bad Cop 2","Alain Desrochers","2017"],
["Boost","Darren Curtis","2017"],
["Ça sent la coupe","Patrice Sauvé","2017"],
["C'est le coeur qui meurt en dernier","Alexis Durand-Brault","2017"],
["Le Cyclotron","Olivier Asselin","2017"],
["De père en flic 2","Émile Gaudreault","2017"],
["Et au pire, on se mariera","Léa Pool","2017"],
["Innocent","Marc-André Lavoie","2017"],
["Iqaluit","Benoît Pilon","2017"],
["Junior majeur","Éric Tessier","2017"],
["Maudite Poutine","Karl Lemieux","2017"],
["Mes nuits feront écho","Sophie Goyette","2017"],
["Nelly","Anne Émond","2017"],
["Nous sommes les autres","Jean-François Asselin","2017"],
["La Petite Fille qui aimait trop les allumettes","Simon Lavoie","2017"],
["Pieds nus dans l'aube","Francis Leclerc","2017"],
["Le problème d'infiltration","Robert Morin","2017"],
["Les Rois mongols","Luc Picard","2017"],
["Tadoussac","Martin Laroche","2017"],
["Ta peau si lisse","Denis Côté","2017"],
["Le trip à trois","Nicolas Monette","2017"],
["Tuktuq","Robin Aubert","2017"],
["1991","Ricardo Trogi","2018"],
["À tous ceux qui ne me lisent pas","Yan Giroux","2018"],
["Ailleurs","Samuel Matteau","2018"],
["Allure","Jason Sachez, Carlos Sanchez","2018"],
["All You Can Eat Buddha","Ian Lagarde","2018"],
["L’amour","Marc Bisaillon","2018"],
["Another Kind of Wedding","Pat Kiely","2018"],
["Birthmarked","Emanuel Hoss-Desmarais","2018"],
["La Bolduc","François Bouvier","2018"],
["Burn Out ou la servitude volontaire","Michel Jetté","2018"],
["Charlotte a du fun","Sophie Lorain","2018"],
["Chien de garde","Sophie Dupuis","2018"],
["La Chute de l'empire américain","Denys Arcand","2018"],
["La chute de Sparte","Tristan Dubois","2018"],
["Claire l’Hiver","Sophie Bédard-Marcotte","2018"],
["La course des Tuques","François Brisson, Benoît Godbout","2018"],
["La Disparition des lucioles","Sébastien Pilote","2018"],
["Dans la brume","Daniel Roby","2018"],
["Eye on Juliet","Kim Nguyen","2018"],
["Les faux tatouages","Pascal Plante","2018"],
["Hochelaga, terre des âmes","François Girard","2018"],
["Identités","Samuel Thivierge","2018"],
["Isla Blanca","Jeanne Leblanc","2018"],
["Lemonade","Ioana Uricaru","2018"],
["Napoléon en apparte","Jeff Denis","2018"],
["Nelly et Simon : Mission Yéti","Pierre Greco, Nancy Florence Savard","2018"],
["Le nid","David Paradis","2018"],
["Origami","Patrick Demers","2018"],
["Oscillations","Ky Nam Leduc","2018"],
["Papa est devenu un lutin","Dominique Adams","2018"],
["Pour vivre ici","Bernard Émond","2018"],
["Quand l'amour se creuse un trou","Ara Ball","2018"],
["Les salopes ou le sucre naturel de la peau","Renée Beaulieu","2018"],
["Ma vie avec John F. Donovan","Xavier Dolan","2018"],
["Kristina Wagenbauer","Carla Turcotte, Natalia Dontcheva, Emmanuel Schwartz","2018"],
["Les scènes fortuites","Guillaume Lambert","2018"],
["Touched","Karl R. Hearne","2018"],
["Un printemps d’ailleurs","Xiaodan He","2018"],
["Vénus","Eisha Marjara","2018"],
["Wolfe","Francis Bordeleau","2018"],
["Yolanda","Jeannine Gagné","2018"],
["Une colonie","Geneviève Dulude-De Celles","2019"],
["La grande noirceur","Maxime Giroux","2019"],
["Malek","Guy Édoin","2019"],
["Impetus","Jennifer Alleyn","2019"],
["Mon ami Walid","Adib Alkhalidey","2019"],
["Antigone","Sophie Deraspe","2019"],
["Attaque d'un autre monde","Adrien Lorion","2019"],
["Jeune Juliette","Anne Émond","2019"],
["Journal de Bolivie - 50 ans après la mort du Che","Jules Falardeau et Jean-Philippe Nadeau Marcoux","2019"],
["Kuessipan","Myriam Verreault","2019"],
["Matthias et Maxime","Xavier Dolan","2019"],
["Répertoire des villes disparues","Denis Côté","2019"],
["Sur les traces de Bochart","Pierre Saint-Yves et Yannick Gendron","2019"],
["Menteur","Émile Gaudreault","2019"],
["Apapacho: une caresse pour l'âme","Marquise Lepage","2019"],
["La femme de mon frère","Monia Chokri","2019"],
["Les Fleurs oubliées","André Forcier","2019"],
["Jouliks","Mariloup Wolfe","2019"],
["Vivre à 100 milles à l'heure","Louis Bélanger","2019"],
["Nous sommes Gold","Éric Morin","2019"],
["Wilcox","Denis Côté","2019"],
["Je finirai en prison","Alexandre Dostie","2019"],
["Cassy","Noël Mitrani","2019"],
["Les Barbares de La Malbaie","Vincent Biron","2019"],
["Avant qu'on explose","Rémi St-Michel","2019"],
["Le rire","Martin Laroche","2020"],
["Mafia Inc.","Podz","2020"],
["14 jours, 12 nuits","Jean-Philippe Duval","2020"],
["Les nôtres","Jeanne Leblanc","2020"],
["Tu te souviendras de moi","Éric Tessier","2020"],
["Le club Vinland","Benoît Pilon","2020"],
["Target Number One","Daniel Roby","2020"],
["Le guide de la famille parfaite","Ricardo Trogi","2020"],
["Gallant : Confessions d’un tueur à gages","Luc Picard","2020"],
["La déesse des mouches à feu","Anaïs Barbeau-Lavalette","2020"]]
```

Cette variable, appelée ```films```, est une liste de listes.
Comme nous en informe le code ci-dessous, elle contient 1412 éléments, des listes qui comptent trois éléments chacun.


```python
len(films), len(films[0]), len(films[1411])
```




    (1412, 3, 3)



Il est très facile d'y faire une boucle pour afficher les informations qu'elle contient.


```python
for film in films:
    print("Le film «{}» a été réalisé en {} par {}.".format(film[0],film[2],film[1]))
```

    Le film «À la croisée des chemins» a été réalisé en 1943 par Paul Guèvremont.
    Le film «Le Père Chopin» a été réalisé en 1945 par Fedor Ozep.
    Le film «La Forteresse» a été réalisé en 1947 par Fedor Ozep.
    Le film «Whispering City» a été réalisé en 1947 par Fedor Ozep.
    Le film «On ne triche pas avec la vie» a été réalisé en 1949 par René Delacroix et Paul Vandenberghe.
    Le film «Le Curé de village» a été réalisé en 1949 par Paul Gury.
    Le film «Le Gros Bill» a été réalisé en 1949 par Jean-Yves Bigras et René Delacroix.
    Le film «Un homme et son péché» a été réalisé en 1949 par Paul Gury.
    Le film «Les Lumières de ma ville» a été réalisé en 1950 par Jean-Yves Bigras.
    Le film «L'Inconnue de Montréal» a été réalisé en 1950 par Jean Devaivre.
    Le film «Séraphin» a été réalisé en 1950 par Paul Gury.
    Le film «Étienne Brûlé gibier de potence» a été réalisé en 1952 par Melburn E. Turner.
    Le film «La Petite Aurore, l'enfant martyre» a été réalisé en 1952 par Jean-Yves Bigras.
    Le film «Le Rossignol et les Cloches» a été réalisé en 1952 par René Delacroix.
    Le film «Cœur de maman» a été réalisé en 1953 par René Delacroix.
    Le film «Tit-Coq» a été réalisé en 1953 par Gratien Gélinas et René Delacroix.
    Le film «L'Esprit du mal» a été réalisé en 1954 par Jean-Yves Bigras.
    Le film «Le Village enchanté» a été réalisé en 1956 par Marcel et Réal Racicot.
    Le film «Le Survenant» a été réalisé en 1957 par Un collectif de réalisateurs.
    Le film «Les Mains nettes» a été réalisé en 1958 par Claude Jutra.
    Le film «Les Brûlés» a été réalisé en 1959 par Bernard Devlin.
    Le film «Accade in Canada» a été réalisé en 1962 par Luigi Petrucci.
    Le film «Seul ou avec d'autres» a été réalisé en 1962 par Denys Arcand avec Denis Héroux et Stéphane Venne.
    Le film «À tout prendre» a été réalisé en 1963 par Claude Jutra.
    Le film «Amanita Pestilens» a été réalisé en 1963 par René Bonnière.
    Le film «Drylanders (Un autre pays)» a été réalisé en 1963 par Donald Haldane.
    Le film «Le Chat dans le sac» a été réalisé en 1964 par Gilles Groulx.
    Le film «La Fleur de l'âge, ou Les adolescentes» a été réalisé en 1964 par Un collectif de réalisateurs.
    Le film «Jusqu'au cou» a été réalisé en 1964 par Denis Héroux.
    Le film «Nobody Waved Good-Bye» a été réalisé en 1964 par Don Owen.
    Le film «La Terre à boire» a été réalisé en 1964 par Jean-Paul Bernier.
    Le film «Trouble-fête» a été réalisé en 1964 par Pierre Patry.
    Le film «Astataïon ou le Festin des morts» a été réalisé en 1965 par Fernand Dansereau.
    Le film «Caïn» a été réalisé en 1965 par Pierre Patry.
    Le film «La Corde au cou» a été réalisé en 1965 par Pierre Patry.
    Le film «Le Coup de grâce» a été réalisé en 1965 par Jean Cayrol et Claude Durand.
    Le film «Pas de vacances pour les idoles» a été réalisé en 1965 par Denis Héroux.
    Le film «Le Révolutionnaire» a été réalisé en 1965 par Jean Pierre Lefebvre.
    Le film «La Vie heureuse de Léopold Z.» a été réalisé en 1965 par Gilles Carle.
    Le film «Carnaval en chute libre» a été réalisé en 1966 par Guy Bouchard.
    Le film «La Douzième heure» a été réalisé en 1966 par Jean Martimbeau.
    Le film «Footsteps in the Snow (Des pas sur la neige)» a été réalisé en 1966 par Martin Green.
    Le film «Yul 871» a été réalisé en 1966 par Jacques Godbout.
    Le film «Entre la mer et l'eau douce» a été réalisé en 1967 par Michel Brault.
    Le film «Il ne faut pas mourir pour ça» a été réalisé en 1967 par Jean Pierre Lefebvre.
    Le film «Manette : la folle et les dieux de carton» a été réalisé en 1967 par Camil Adam.
    Le film «C'est pas la faute à Jacques Cartier» a été réalisé en 1968 par Georges Dufaux et Clément Perron.
    Le film «Façade» a été réalisé en 1968 par Larry Kent.
    Le film «Isabel» a été réalisé en 1968 par Paul Almond.
    Le film «Las Joyas del diablo (Le diable aime les bijoux)» a été réalisé en 1968 par José Maria Elorrieta.
    Le film «Kid Sentiment» a été réalisé en 1968 par Jacques Godbout.
    Le film «Patricia et Jean-Baptiste» a été réalisé en 1968 par Jean Pierre Lefebvre.
    Le film «Poussière sur la ville» a été réalisé en 1968 par Arthur Lamothe.
    Le film «T-bone Steak dans... les mangeuses d'hommes» a été réalisé en 1968 par Gilles Marchand et Hugues Tremblay.
    Le film «Le Viol d'une jeune fille douce» a été réalisé en 1968 par Gilles Carle.
    Le film «La Chambre blanche» a été réalisé en 1969 par Jean Pierre Lefebvre.
    Le film «Danger pour la société» a été réalisé en 1969 par Jean Martimbeau.
    Le film «Délivrez-nous du mal» a été réalisé en 1969 par Jean-Claude Lord.
    Le film «Don't Let the Angels Fall (Seuls les enfants étaient présents)» a été réalisé en 1969 par George Kaczender.
    Le film «Le Grand Rock» a été réalisé en 1969 par Raymond Garceau.
    Le film «Jusqu'au cœur» a été réalisé en 1969 par Jean Pierre Lefebvre.
    Le film «Mon amie Pierrette» a été réalisé en 1969 par Jean Pierre Lefebvre.
    Le film «Où êtes-vous donc ?» a été réalisé en 1969 par Gilles Groulx.
    Le film «Le Soleil des autres» a été réalisé en 1969 par Jean Faucher.
    Le film «Valérie» a été réalisé en 1969 par Denis Héroux.
    Le film «Act of the Heart» a été réalisé en 1970 par Paul Almond.
    Le film «Ainsi soient-ils» a été réalisé en 1970 par Yvan Patry.
    Le film «L'Amour humain» a été réalisé en 1970 par Denis Héroux.
    Le film «Deux femmes en or» a été réalisé en 1970 par Claude Fournier.
    Le film «Entre tu et vous» a été réalisé en 1970 par Gilles Groulx.
    Le film «L'Initiation» a été réalisé en 1970 par Denis Héroux.
    Le film «Love in a 4 Letter World» a été réalisé en 1970 par John Sone.
    Le film «On est loin du soleil» a été réalisé en 1970 par Jacques Leduc.
    Le film «Pile ou face» a été réalisé en 1970 par Roger Fournier.
    Le film «Q-bec my love» a été réalisé en 1970 par Jean Pierre Lefebvre.
    Le film «Quand hurlent les loups» a été réalisé en 1970 par André Lafferrere.
    Le film «Red» a été réalisé en 1970 par Gilles Carle.
    Le film «St-Denis dans le temps...» a été réalisé en 1970 par Marcel Carrière.
    Le film «Vive la France» a été réalisé en 1970 par Raymond Garceau.
    Le film «Wow» a été réalisé en 1970 par Claude Jutra.
    Le film «7 fois… (par jour)» a été réalisé en 1971 par Denis Héroux.
    Le film «Après-ski» a été réalisé en 1971 par Roger Cardinal.
    Le film «Les Chats bottés» a été réalisé en 1971 par Claude Fournier.
    Le film «Corps et âme» a été réalisé en 1971 par Michel Audy.
    Le film «L'Explosion» a été réalisé en 1971 par Marc Simenon.
    Le film «Finalement...» a été réalisé en 1971 par Richard Martin.
    Le film «Fleur bleue» a été réalisé en 1971 par Larry Kent.
    Le film «Le Grand film ordinaire» a été réalisé en 1971 par Roger Frappier.
    Le film «IXE-13» a été réalisé en 1971 par Jacques Godbout.
    Le film «Jean-François Xavier de...» a été réalisé en 1971 par Michel Audy.
    Le film «Les Mâles» a été réalisé en 1971 par Gilles Carle.
    Le film «Le Martien de Noël» a été réalisé en 1971 par Bernard Gosselin.
    Le film «Les Maudits sauvages» a été réalisé en 1971 par Jean Pierre Lefebvre.
    Le film «Mon enfance à Montréal» a été réalisé en 1971 par Jean Chabot.
    Le film «Mon œil» a été réalisé en 1971 par Jean Pierre Lefebvre.
    Le film «Mon oncle Antoine» a été réalisé en 1971 par Claude Jutra.
    Le film «Question de vie» a été réalisé en 1971 par André Théberge.
    Le film «Le Retour de l'Immaculée Conception» a été réalisé en 1971 par André Forcier.
    Le film «Stop» a été réalisé en 1971 par Jean Beaudin.
    Le film «Tiens-toi bien après les oreilles à Papa...» a été réalisé en 1971 par Jean Bissonnette.
    Le film «Ty-Peupe» a été réalisé en 1971 par Fernand Bélanger.
    Le film «L'Apparition» a été réalisé en 1972 par Roger Cardinal.
    Le film «Les Colombes» a été réalisé en 1972 par Jean-Claude Lord.
    Le film «Le diable est parmi nous» a été réalisé en 1972 par Jean Beaudin.
    Le film «Et du fils» a été réalisé en 1972 par Raymond Garceau.
    Le film «L'exil» a été réalisé en 1972 par Thomas Vamos.
    Le film «Le Grand Sabordage» a été réalisé en 1972 par Alain Périsson.
    Le film «Isis au 8» a été réalisé en 1972 par Alain Chartrand.
    Le film «Journey» a été réalisé en 1972 par Paul Almond.
    Le film «La Maudite Galette» a été réalisé en 1972 par Denys Arcand.
    Le film «Montréal blues» a été réalisé en 1972 par Pascal Gélinas.
    Le film «Le p'tit vient vite» a été réalisé en 1972 par Louis-Georges Carrier.
    Le film «Pas de jeu sans soleil» a été réalisé en 1972 par Claude Bérubé.
    Le film «Quelques arpents de neige» a été réalisé en 1972 par Denis Héroux.
    Le film «Les Smattes» a été réalisé en 1972 par Jean-Claude Labrecque.
    Le film «Le Temps d'une chasse» a été réalisé en 1972 par Francis Mankiewicz.
    Le film «La Tête au neutre» a été réalisé en 1972 par Jean Gagné.
    Le film «Un enfant comme les autres» a été réalisé en 1972 par Denis Héroux.
    Le film «La Vie rêvée» a été réalisé en 1972 par Mireille Dansereau.
    Le film «La Vraie nature de Bernadette» a été réalisé en 1972 par Gilles Carle.
    Le film «Ah ! Si mon moine voulait...» a été réalisé en 1973 par Claude Pierson.
    Le film «Les Allées de la terre» a été réalisé en 1973 par André Théberge.
    Le film «La Conquête» a été réalisé en 1973 par Jean Gagné.
    Le film «Les Corps célestes» a été réalisé en 1973 par Gilles Carle.
    Le film «Les Dernières fiançailles» a été réalisé en 1973 par Jean Pierre Lefebvre.
    Le film «Il était une fois dans l'Est» a été réalisé en 1973 par André Brassard.
    Le film «J'ai mon voyage!» a été réalisé en 1973 par Denis Héroux.
    Le film «Kamouraska» a été réalisé en 1973 par Claude Jutra.
    Le film «Keep it in the Family» a été réalisé en 1973 par Larry Kent.
    Le film «La Maîtresse» a été réalisé en 1973 par Anton Van de Water.
    Le film «La Mort d'un bûcheron» a été réalisé en 1973 par Gilles Carle.
    Le film «Mourir pour vivre» a été réalisé en 1973 par Franco Vigilante.
    Le film «The Neptune Factor: An Undersea Odyssey» a été réalisé en 1973 par Daniel Petrie.
    Le film «Noël et Juliette» a été réalisé en 1973 par Michel Bouchard.
    Le film «O.K.... Laliberté» a été réalisé en 1973 par Marcel Carrière.
    Le film «On n'engraisse pas les cochons à l'eau claire» a été réalisé en 1973 par Jean Pierre Lefebvre.
    Le film «The Rainbow Boys» a été réalisé en 1973 par Gerald Potterton.
    Le film «Réjeanne Padovani» a été réalisé en 1973 par Denys Arcand.
    Le film «Sensations» a été réalisé en 1973 par Robert Séguin.
    Le film «A Star is Lost!» a été réalisé en 1973 par John Howe.
    Le film «Taureau» a été réalisé en 1973 par Clément Perron.
    Le film «Tendresse ordinaire» a été réalisé en 1973 par Jacques Leduc.
    Le film «Tu brûles... tu brûles...» a été réalisé en 1973 par Jean-Guy Noël.
    Le film «U-Turn» a été réalisé en 1973 par George Kaczender.
    Le film «Ultimatum» a été réalisé en 1973 par Jean Pierre Lefebvre.
    Le film «Y a toujours moyen de moyenner!» a été réalisé en 1973 par Denis Héroux.
    Le film «Alien Thunder» a été réalisé en 1974 par Claude Fournier.
    Le film «The Apprenticeship of Duddy Kravitz» a été réalisé en 1974 par Ted Kotcheff.
    Le film «Au boutt'» a été réalisé en 1974 par Roger Laliberté.
    Le film «Les Aventures d'une jeune veuve» a été réalisé en 1974 par Roger Fournier.
    Le film «Bar Salon» a été réalisé en 1974 par André Forcier.
    Le film «Les Beaux dimanches» a été réalisé en 1974 par Richard Martin.
    Le film «Bingo» a été réalisé en 1974 par Jean-Claude Lord.
    Le film «Bulldozer» a été réalisé en 1974 par Pierre Harel.
    Le film «Les Deux pieds dans la même bottine» a été réalisé en 1974 par Pierre Rose.
    Le film «La Gammick» a été réalisé en 1974 par Jacques Godbout.
    Le film «Guitare» a été réalisé en 1974 par Richard Lavoie.
    Le film «Je t'aime» a été réalisé en 1974 par Pierre Duceppe.
    Le film «Montreal Main» a été réalisé en 1974 par Frank Vitale.
    Le film «Ô ou l'invisible enfant» a été réalisé en 1974 par Raôul Duguay.
    Le film «Les Ordres» a été réalisé en 1974 par Michel Brault.
    Le film «Le Plumard en folie» a été réalisé en 1974 par Jacques Lem.
    Le film «La Pomme, la queue... et les pépins!» a été réalisé en 1974 par Claude Fournier.
    Le film «Running Time» a été réalisé en 1974 par Mort Ransen.
    Le film «Why Rock the Boat?» a été réalisé en 1974 par John Howe.
    Le film «Y'a pas d'mal à se faire du bien» a été réalisé en 1974 par Claude Mulot.
    Le film «L'amour blessé» a été réalisé en 1975 par Jean Pierre Lefebvre.
    Le film «Candice Candy» a été réalisé en 1975 par Pierre Unia.
    Le film «Cold Journey» a été réalisé en 1975 par Martin Defalco.
    Le film «Eliza's Horoscope» a été réalisé en 1975 par Gordon Sheppard.
    Le film «Gina» a été réalisé en 1975 par Denys Arcand.
    Le film «The Heatwave Lasted Four Days» a été réalisé en 1975 par Douglas Jackson.
    Le film «L'île jaune» a été réalisé en 1975 par Jean Cousineau.
    Le film «Lies My Father Told Me» a été réalisé en 1975 par Ján Kádar.
    Le film «M'en revenant par les épinettes» a été réalisé en 1975 par François Brault.
    Le film «Mustang» a été réalisé en 1975 par Marcel Lefebvre.
    Le film «Partis pour la gloire» a été réalisé en 1975 par Clément Perron.
    Le film «Pour le meilleur et pour le pire» a été réalisé en 1975 par Claude Jutra.
    Le film «Pousse mais pousse égal» a été réalisé en 1975 par Denis Héroux.
    Le film «Shivers» a été réalisé en 1975 par David Cronenberg.
    Le film «Le temps de l'avant» a été réalisé en 1975 par Anne Claire Poirier.
    Le film «La tête de Normande St-Onge» a été réalisé en 1975 par Gilles Carle.
    Le film «Tout feu, tout femme» a été réalisé en 1975 par Gilles Richer.
    Le film «Une nuit en Amérique» a été réalisé en 1975 par Jean Chabot.
    Le film «Les vautours» a été réalisé en 1975 par Jean-Claude Labrecque.
    Le film «L'absence» a été réalisé en 1976 par Brigitte Sauriol.
    Le film «Beat» a été réalisé en 1976 par André Blanchard.
    Le film «Born for Hell» a été réalisé en 1976 par Denis Héroux.
    Le film «Chanson pour Julie» a été réalisé en 1976 par Jacques Vallée.
    Le film «Contebleu» a été réalisé en 1976 par Yves Angrignon.
    Le film «Death Weekend» a été réalisé en 1976 par William Fruet.
    Le film «La fleur aux dents» a été réalisé en 1976 par Thomas Vamos.
    Le film «J.A. Martin photographe» a été réalisé en 1976 par Jean Beaudin.
    Le film «Je suis loin de toi mignonne» a été réalisé en 1976 par Claude Fournier.
    Le film «Jos Carbone» a été réalisé en 1976 par Hugues Tremblay.
    Le film «The Little Girl Who Lives Down the Lane» a été réalisé en 1976 par Nicolas Gessner.
    Le film «Parlez-nous d'amour» a été réalisé en 1976 par Jean-Claude Lord.
    Le film «La piastre» a été réalisé en 1976 par Alain Chartrand.
    Le film «Simple histoire d'amours» a été réalisé en 1976 par Fernand Dansereau.
    Le film «Ti-Cul Tougas» a été réalisé en 1976 par Jean-Guy Noël.
    Le film «L'ange et la femme» a été réalisé en 1977 par Gilles Carle.
    Le film «Cathy's Curse» a été réalisé en 1977 par Eddy Matalon.
    Le film «L'eau chaude, l'eau frette» a été réalisé en 1977 par André Forcier.
    Le film «Full Circle» a été réalisé en 1977 par Richard Loncraine.
    Le film «Ilsa the Tigress of Siberia» a été réalisé en 1977 par Jean Lafleur.
    Le film «La menace» a été réalisé en 1977 par Alain Corneau.
    Le film «One Man» a été réalisé en 1977 par Robin Spry.
    Le film «La p'tite violence» a été réalisé en 1977 par Hélène Girard.
    Le film «Panique» a été réalisé en 1977 par Jean-Claude Lord.
    Le film «Rabid» a été réalisé en 1977 par David Cronenberg.
    Le film «Rituals» a été réalisé en 1977 par Peter Carter.
    Le film «Le soleil se lève en retard» a été réalisé en 1977 par André Brassard.
    Le film «Ti-Mine, Bernie pis la gang…» a été réalisé en 1977 par Marcel Carrière.
    Le film «Le vieux pays où Rimbaud est mort» a été réalisé en 1977 par Jean Pierre Lefebvre.
    Le film «L'ange gardien» a été réalisé en 1978 par Jacques Fournier.
    Le film «Blackout» a été réalisé en 1978 par Eddy Matalon.
    Le film «City on Fire» a été réalisé en 1978 par Alvin Rakoff.
    Le film «Comme les six doigts de la main» a été réalisé en 1978 par André Mélançon.
    Le film «Drying Up the Streets» a été réalisé en 1978 par Robin Spry.
    Le film «L'homme en colère» a été réalisé en 1978 par Claude Pinoteau.
    Le film «In Praise of Older Women» a été réalisé en 1978 par George Kaczender.
    Le film «Jacob Two-Two Meets the Hooded Fang» a été réalisé en 1978 par Theodore J. Flicker.
    Le film «Les liens de sang» a été réalisé en 1978 par Claude Chabrol.
    Le film «Tomorrow Never Comes» a été réalisé en 1978 par Peter Collinson.
    Le film «Un grand logement» a été réalisé en 1978 par Mario Bolduc et Marie-Ginette Guay.
    Le film «Une amie d'enfance» a été réalisé en 1978 par Francis Mankiewicz.
    Le film «Violette Nozière» a été réalisé en 1978 par Claude Chabrol.
    Le film «À nous deux» a été réalisé en 1979 par Claude Lelouch.
    Le film «L'affaire Coffin» a été réalisé en 1979 par Jean-Claude Labrecque.
    Le film «L'arrache-cœur» a été réalisé en 1979 par Mireille Dansereau.
    Le film «Au revoir… à lundi» a été réalisé en 1979 par Maurice Dugowson.
    Le film «Avoir 16 ans» a été réalisé en 1979 par Jean Pierre Lefebvre.
    Le film «La belle apparence» a été réalisé en 1979 par Denyse Benoît.
    Le film «The Brood» a été réalisé en 1979 par David Cronenberg.
    Le film «Caro papa» a été réalisé en 1979 par Dino Risi.
    Le film «Les célébrations» a été réalisé en 1979 par Yves Simoneau.
    Le film «Le château de cartes» a été réalisé en 1979 par François Labonté.
    Le film «Cordélia» a été réalisé en 1979 par Jean Beaudin.
    Le film «Éclair au chocolat» a été réalisé en 1979 par Jean-Claude Lord.
    Le film «Fuir» a été réalisé en 1979 par Hélène Girard.
    Le film «Girls» a été réalisé en 1979 par Just Jaeckin.
    Le film «L'hiver bleu (Abitibi 1978)» a été réalisé en 1979 par André Blanchard.
    Le film «La maladie c'est les compagnies» a été réalisé en 1979 par Richard Boutet.
    Le film «A Man Called Intrepid» a été réalisé en 1979 par Peter Carter.
    Le film «Mourir à tue-tête» a été réalisé en 1979 par Anne Claire Poirier.
    Le film «Vie d'ange» a été réalisé en 1979 par Pierre Harel.
    Le film «Agency» a été réalisé en 1980 par George Kaczender.
    Le film «Atlantic City, U.S.A.» a été réalisé en 1980 par Louis Malle.
    Le film «Les bons débarras» a été réalisé en 1980 par Francis Mankiewicz.
    Le film «Ça peut pas être l'hiver on n'a même pas eu d'été» a été réalisé en 1980 par Louise Carré.
    Le film «Le chef se déniaise» a été réalisé en 1980 par Jean Luret.
    Le film «Contrecœur» a été réalisé en 1980 par Jean-Guy Noël.
    Le film «La cuisine rouge» a été réalisé en 1980 par Paule Baillargeon.
    Le film «Fantastica» a été réalisé en 1980 par Gilles Carle.
    Le film «Final Assignment» a été réalisé en 1980 par Paul Almond.
    Le film «Les grands enfants» a été réalisé en 1980 par Paul Tana.
    Le film «Hog Wild» a été réalisé en 1980 par Les Rose.
    Le film «L'homme à tout faire» a été réalisé en 1980 par Micheline Lanctôt.
    Le film «Hot Dogs» a été réalisé en 1980 par Claude Fournier.
    Le film «The Lucky Star» a été réalisé en 1980 par Max Fischer.
    Le film «Pinball Summer» a été réalisé en 1980 par George Mihalka.
    Le film «Strass café» a été réalisé en 1980 par Léa Pool.
    Le film «Suzanne» a été réalisé en 1980 par Robin Spry.
    Le film «Terror Train» a été réalisé en 1980 par Roger Spottiswoode.
    Le film «Thetford au milieu de notre vie» a été réalisé en 1980 par Iolande Cadrin-Rossignol et Fernand Dansereau.
    Le film «Les beaux souvenirs» a été réalisé en 1981 par Francis Mankiewicz.
    Le film «Black Mirror» a été réalisé en 1981 par Pierre-Alain Jolivet.
    Le film «Deux super dingues» a été réalisé en 1981 par Claude Castravelli.
    Le film «The Funny Farm» a été réalisé en 1981 par Ron Clark.
    Le film «Gas» a été réalisé en 1981 par Les Rose.
    Le film «La guerre du feu» a été réalisé en 1981 par Jean-Jacques Annaud.
    Le film «Happy Birthday to Me» a été réalisé en 1981 par J. Lee Thompson.
    Le film «Hard Feelings» a été réalisé en 1981 par Daryl Duke.
    Le film «Heartaches» a été réalisé en 1981 par Donald Shebib.
    Le film «Hot Touch» a été réalisé en 1981 par Roger Vadim.
    Le film «My Bloody Valentine» a été réalisé en 1981 par George Mihalka.
    Le film «Les Plouffe» a été réalisé en 1981 par Gilles Carle.
    Le film «Salut! J.W.» a été réalisé en 1981 par Ian Ireland.
    Le film «Le shift de nuit» a été réalisé en 1981 par Mario Bolduc.
    Le film «Ups & Downs» a été réalisé en 1981 par Paul Almond.
    Le film «Yesterday» a été réalisé en 1981 par Larry Kent.
    Le film «Your Ticket is No Longer Valid» a été réalisé en 1981 par George Kaczender.
    Le film «Cross Country» a été réalisé en 1982 par Paul Lynch.
    Le film «Doux aveux» a été réalisé en 1982 par Fernand Dansereau.
    Le film «Les fleurs sauvages» a été réalisé en 1982 par Jean Pierre Lefebvre.
    Le film «Le futur intérieur» a été réalisé en 1982 par Jean Chabot.
    Le film «Killing 'em Softly» a été réalisé en 1982 par Max Fischer.
    Le film «Larose, Pierrot et la Luce» a été réalisé en 1982 par Claude Gagnon.
    Le film «Luc ou La part des choses» a été réalisé en 1982 par Michel Audy.
    Le film «Paradise» a été réalisé en 1982 par Stuart Gilliard.
    Le film «La quarantaine» a été réalisé en 1982 par Anne-Claire Poirier.
    Le film «Scandale» a été réalisé en 1982 par George Mihalka.
    Le film «Une journée en taxi» a été réalisé en 1982 par Robert Ménard.
    Le film «Visiting Hours» a été réalisé en 1982 par Jean-Claude Lord.
    Le film «Les yeux rouges ou Les vérités accidentelles» a été réalisé en 1982 par Yves Simoneau.
    Le film «Au clair de la lune» a été réalisé en 1983 par André Forcier.
    Le film «Au pays de Zom» a été réalisé en 1983 par Gilles Groulx.
    Le film «Bonheur d'occasion» a été réalisé en 1983 par Claude Fournier.
    Le film «Lucien Brouillard» a été réalisé en 1983 par Bruno Carrière.
    Le film «Maria Chapdelaine» a été réalisé en 1983 par Gilles Carle.
    Le film «Porky's 2: The Next Day» a été réalisé en 1983 par Bob Clark.
    Le film «Rien qu'un jeu» a été réalisé en 1983 par Brigitte Sauriol.
    Le film «Le ruffian» a été réalisé en 1983 par José Giovanni.
    Le film «Snapshot» a été réalisé en 1983 par Abderahmane Mazouz.
    Le film «Videodrome» a été réalisé en 1983 par David Cronenberg.
    Le film «The Wars» a été réalisé en 1983 par Robin Phillips.
    Le film «Les Années de rêves» a été réalisé en 1984 par Jean-Claude Labrecque.
    Le film «The Bay Boy» a été réalisé en 1984 par Daniel Petrie.
    Le film «La couleur encerclée» a été réalisé en 1984 par Jean Gagné, Serge Gagné.
    Le film «Le crime d'Ovide Plouffe» a été réalisé en 1984 par Denys Arcand.
    Le film «Evil Judgment» a été réalisé en 1984 par Claude Castravelli.
    Le film «La femme de l'hôtel» a été réalisé en 1984 par Léa Pool.
    Le film «La Guerre des tuques» a été réalisé en 1984 par André Melançon.
    Le film «Hey Babe!» a été réalisé en 1984 par Rafal Zielinski.
    Le film «The Hotel New Hampshire» a été réalisé en 1984 par Tony Richardson.
    Le film «Jacques et Novembre» a été réalisé en 1984 par Jean Beaudry.
    Le film «Le jour « S... »» a été réalisé en 1984 par Jean Pierre Lefebvre.
    Le film «Louisiana» a été réalisé en 1984 par Philippe de Broca.
    Le film «Mario» a été réalisé en 1984 par Jean Beaudin.
    Le film «The Masculine Mystique» a été réalisé en 1984 par John N. Smith, Giles Walker.
    Le film «Memoirs» a été réalisé en 1984 par Bashar Shbib.
    Le film «Mother's Meat Freuds Flesh» a été réalisé en 1984 par Demetri Demetrios.
    Le film «Paroles et musiques» a été réalisé en 1984 par Élie Chouraqui.
    Le film «Perfect Timing» a été réalisé en 1984 par René Bonnière.
    Le film «Le sang des autres» a été réalisé en 1984 par Claude Chabrol.
    Le film «Sonatine» a été réalisé en 1984 par Micheline Lanctôt.
    Le film «The Surrogate» a été réalisé en 1984 par Don Carmody.
    Le film «90 Days» a été réalisé en 1985 par Giles Walker.
    Le film «L'adolescente sucre d'amour» a été réalisé en 1985 par Jocelyne Saab.
    Le film «Adramélech» a été réalisé en 1985 par Pierre Grégoire.
    Le film «Bayo» a été réalisé en 1985 par Mort Ransen.
    Le film «The Blue Man» a été réalisé en 1985 par George Mihalka.
    Le film «Caffè Italia Montréal» a été réalisé en 1985 par Paul Tana.
    Le film «Celui qui voit les heures» a été réalisé en 1985 par Pierre Goupil.
    Le film «La dame en couleurs» a été réalisé en 1985 par Claude Jutra.
    Le film «Le dernier glacier» a été réalisé en 1985 par Roger Frappier, Jacques Leduc.
    Le film «Elvis Gratton : Le king des kings» a été réalisé en 1985 par Pierre Falardeau.
    Le film «The Gunrunner» a été réalisé en 1985 par Nardo Castillo.
    Le film «Instantanés» a été réalisé en 1985 par Michel Juliani.
    Le film «Joshua Then and Now» a été réalisé en 1985 par Ted Kotcheff.
    Le film «Junior» a été réalisé en 1985 par Jim Hanley.
    Le film «Lune de miel» a été réalisé en 1985 par Patrick Jamain.
    Le film «Le matou» a été réalisé en 1985 par Jean Beaudin.
    Le film «Medium blues» a été réalisé en 1985 par Michel Préfontaine.
    Le film «Night Magic» a été réalisé en 1985 par Lewis Furey.
    Le film «Opération beurre de pinottes» a été réalisé en 1985 par Michael Rubbo.
    Le film «Québec, opération Lambda» a été réalisé en 1985 par Marc Degryse.
    Le film «Visage pâle» a été réalisé en 1985 par Claude Gagnon.
    Le film «Anne Trister» a été réalisé en 1986 par Léa Pool.
    Le film «Bach et Bottine» a été réalisé en 1986 par André Melançon.
    Le film «The Boy in Blue» a été réalisé en 1986 par Charles Jarrott.
    Le film «Cat Squad» a été réalisé en 1986 par William Freidkin.
    Le film «Contes des mille et un jours» a été réalisé en 1986 par Iolande Cadrin-Rossignol.
    Le film «Deaf & Mute» a été réalisé en 1986 par Hunt Hoe.
    Le film «Le Déclin de l'empire américain» a été réalisé en 1986 par Denys Arcand.
    Le film «Le dernier havre» a été réalisé en 1986 par Denyse Benoit.
    Le film «Équinoxe» a été réalisé en 1986 par Arthur Lamothe.
    Le film «Evixion» a été réalisé en 1986 par Bashar Shbib.
    Le film «The First Killing Frost» a été réalisé en 1986 par Roland Hallé.
    Le film «Exit» a été réalisé en 1986 par Robert Ménard.
    Le film «Les fous de bassan» a été réalisé en 1986 par Yves Simoneau.
    Le film «La guêpe» a été réalisé en 1986 par Gilles Carle.
    Le film «Henri» a été réalisé en 1986 par François Labonté.
    Le film «L'homme renversé» a été réalisé en 1986 par Yves Dion.
    Le film «Meatballs 3» a été réalisé en 1986 par George Mendeluk.
    Le film «The Morning Man» a été réalisé en 1986 par Danièle J Suissa.
    Le film «Overnight» a été réalisé en 1986 par Jack Darcus.
    Le film «Pouvoir intime» a été réalisé en 1986 par Yves Simoneau.
    Le film «Qui a tiré sur nos histoires d'amour?» a été réalisé en 1986 par Louise Carré.
    Le film «Sauve-toi Lola» a été réalisé en 1986 par Michel Drach.
    Le film «Sitting in Limbo» a été réalisé en 1986 par John N. Smith.
    Le film «Toby McTeague» a été réalisé en 1986 par Jean-Claude Lord.
    Le film «À l'automne de la vie» a été réalisé en 1987 par Yvan Chouinard.
    Le film «The Great Land of Small» a été réalisé en 1987 par Vojtech Jasny.
    Le film «Le frère André» a été réalisé en 1987 par Jean-Claude Labrecque.
    Le film «Grelots rouges sanglots bleus» a été réalisé en 1987 par Pierre Harel.
    Le film «La Guerre oubliée» a été réalisé en 1987 par Richard Boutet.
    Le film «Hitting Home» a été réalisé en 1987 par Robin Spry.
    Le film «L'Île» a été réalisé en 1987 par François Leterrier.
    Le film «Le Jeune Magicien» a été réalisé en 1987 par Waldemar Dziki.
    Le film «Keeping Track» a été réalisé en 1987 par Robin Spry.
    Le film «The Kid Brother ou Kenny» a été réalisé en 1987 par Claude Gagnon.
    Le film «The Last Straw» a été réalisé en 1987 par Giles Walker.
    Le film «La ligne de chaleur» a été réalisé en 1987 par Hubert-Yves Rose.
    Le film «Marie s'en va-t-en ville» a été réalisé en 1987 par Marquise Lepage.
    Le film «Les roses de Matmata» a été réalisé en 1987 par José Pinheiro.
    Le film «Seductio» a été réalisé en 1987 par Bashar Shbib.
    Le film «Le sourd dans la ville» a été réalisé en 1987 par Mireille Dansereau.
    Le film «Tinamer» a été réalisé en 1987 par Jean-Guy Noël.
    Le film «Train of Dreams» a été réalisé en 1987 par John N. Smith.
    Le film «Tristesse, modèle réduit» a été réalisé en 1987 par Robert Morin.
    Le film «Un zoo la nuit» a été réalisé en 1987 par Jean-Claude Lauzon.
    Le film «À corps perdu» a été réalisé en 1988 par Léa Pool.
    Le film «La boîte à soleil» a été réalisé en 1988 par Jean Pierre Lefebvre.
    Le film «La bottega del orefice» a été réalisé en 1988 par Michael Anderson.
    Le film «Clair-obscur» a été réalisé en 1988 par Bashar Shbib.
    Le film «Eva Guerrillera» a été réalisé en 1988 par Jacqueline Levitin.
    Le film «Family Reunion» a été réalisé en 1988 par Dick Sarin.
    Le film «Gaspard et fils» a été réalisé en 1988 par François Labonté.
    Le film «La Grenouille et la Baleine» a été réalisé en 1988 par Jean-Claude Lord.
    Le film «Horses in Winter» a été réalisé en 1988 par Rick Raxlen.
    Le film «Kalamazoo» a été réalisé en 1988 par André Forcier.
    Le film «Moonlight Flight» a été réalisé en 1988 par Jim Kaufman.
    Le film «La nuit avec Hortense» a été réalisé en 1988 par Jean Chabot.
    Le film «Oh! Oh! Satan!» a été réalisé en 1988 par André Farwagi.
    Le film «La peau et les os» a été réalisé en 1988 par Johanne Prégent.
    Le film «Les Portes tournantes» a été réalisé en 1988 par Francis Mankiewicz.
    Le film «Salut Victor» a été réalisé en 1988 par Anne Claire Poirier.
    Le film «Something About Love» a été réalisé en 1988 par Tom Berry.
    Le film «Les Tisserands du pouvoir» a été réalisé en 1988 par Claude Fournier.
    Le film «Tommy Tricker and the Stamp Traveller» a été réalisé en 1988 par Michael Rubbo.
    Le film «L'Air de rien» a été réalisé en 1989 par Mary Jimenez.
    Le film «The Amityville Curse» a été réalisé en 1989 par Tom Berry.
    Le film «Bye bye Chaperon rouge» a été réalisé en 1989 par Márta Mészáros.
    Le film «Cher frangin» a été réalisé en 1989 par Gérard Mordillat.
    Le film «Comment faire l'amour avec un nègre sans se fatiguer» a été réalisé en 1989 par Jacques W. Benoît.
    Le film «Cruising Bar» a été réalisé en 1989 par Robert Ménard.
    Le film «Dans le ventre du dragon» a été réalisé en 1989 par Yves Simoneau.
    Le film «Fierro… l'été des secrets» a été réalisé en 1989 par André Melançon.
    Le film «Jésus de Montréal» a été réalisé en 1989 par Denys Arcand.
    Le film «Laura Laur» a été réalisé en 1989 par Brigitte Sauriol.
    Le film «Matinée» a été réalisé en 1989 par Richard Martin.
    Le film «Les matins infidèles» a été réalisé en 1989 par Jean Beaudry, François Bouvier.
    Le film «Mindfield» a été réalisé en 1989 par Jean-Claude Lord.
    Le film «Les Noces de papier» a été réalisé en 1989 par Michel Brault.
    Le film «Off Off Off» a été réalisé en 1989 par Jorge Fajardo.
    Le film «Portion d'éternité» a été réalisé en 1989 par Robert Favreau.
    Le film «La Réception» a été réalisé en 1989 par Robert Morin.
    Le film «La Révolution française : les années terribles» a été réalisé en 1989 par Richard T. Heffron.
    Le film «Short Change» a été réalisé en 1989 par Nicholas Kinsey.
    Le film «Sous les draps, les étoiles» a été réalisé en 1989 par Jean-Pierre Gariepy.
    Le film «T'es belle Jeanne» a été réalisé en 1989 par Robert Ménard.
    Le film «Trois pommes à côté du sommeil» a été réalisé en 1989 par Jacques Leduc.
    Le film «Vent de galerne» a été réalisé en 1989 par Bernard Favre.
    Le film «Welcome to Canada» a été réalisé en 1989 par John N. Smith.
    Le film «Babylone» a été réalisé en 1990 par Manu Bonmariage.
    Le film «Back Stab» a été réalisé en 1990 par Jim Kaufman.
    Le film «Bethune: The Making of a Hero» a été réalisé en 1990 par Phillip Borsos.
    Le film «Breadhead» a été réalisé en 1990 par Carlo Alacchi.
    Le film «A Bullet in the Head» a été réalisé en 1990 par Attila Bertalan.
    Le film «Cargo» a été réalisé en 1990 par François Girard.
    Le film «The Company of Strangers» a été réalisé en 1990 par Cynthia Scott.
    Le film «Cursed» a été réalisé en 1990 par Mychel Arsenault.
    Le film «Dames galantes» a été réalisé en 1990 par Jean-Charles Tacchella.
    Le film «Ding et Dong, le film» a été réalisé en 1990 par Alain Chartrand.
    Le film «Falling Over Backwards» a été réalisé en 1990 par Mort Ransen.
    Le film «La Fille du maquignon» a été réalisé en 1990 par Abderrahmane Mazouz.
    Le film «I'm Happy. You're Happy. We're All Happy. Happy, Happy, Happy.» a été réalisé en 1990 par Verlcrow Ripper.
    Le film «La Liberté d'une statue» a été réalisé en 1990 par Olivier Asselin.
    Le film «Lola Zipper» a été réalisé en 1990 par Ilan Duran Cohen.
    Le film «Manuel : le fils emprunté» a été réalisé en 1990 par François Labonté.
    Le film «Moody Beach» a été réalisé en 1990 par Richard Roy.
    Le film «New York doré» a été réalisé en 1990 par Suzanne Guy.
    Le film «Le Party» a été réalisé en 1990 par Pierre Falardeau.
    Le film «Pas de répit pour Mélanie» a été réalisé en 1990 par Jean Beaudry.
    Le film «Princes in Exile» a été réalisé en 1990 par Giles Walker.
    Le film «Rafales» a été réalisé en 1990 par André Melançon.
    Le film «Remous» a été réalisé en 1990 par Sylvie Van Brabant.
    Le film «Le Royaume ou l'asile» a été réalisé en 1990 par Jean Gagné.
    Le film «The Secret of Nandy» a été réalisé en 1990 par Danièle J Suissa.
    Le film «Simon les nuages» a été réalisé en 1990 par Roger Cantin.
    Le film «Un autre homme» a été réalisé en 1990 par Charles Binamé.
    Le film «Un été après l'autre» a été réalisé en 1990 par Anne-Marie Étienne.
    Le film «Une histoire inventée» a été réalisé en 1990 par André Forcier.
    Le film «Alisée» a été réalisé en 1991 par André Blanchard.
    Le film «Amoureux fou» a été réalisé en 1991 par Robert Ménard.
    Le film «L'annonce faite à Marie» a été réalisé en 1991 par Alain Cuny.
    Le film «L'assassin jouait du trombone» a été réalisé en 1991 par Roger Cantin.
    Le film «La Championne» a été réalisé en 1991 par Elisabeta Bostan.
    Le film «… comme un voleur» a été réalisé en 1991 par Michel Langlois.
    Le film «The Dance Goes On» a été réalisé en 1991 par Paul Almond.
    Le film «Les Danseurs du Mozambique» a été réalisé en 1991 par Philippe Lefebvre.
    Le film «Deadly Surveillance» a été réalisé en 1991 par Paul Ziller.
    Le film «La Demoiselle sauvage» a été réalisé en 1991 par Léa Pool.
    Le film «Le Fabuleux voyage de l'ange» a été réalisé en 1991 par Jean Pierre Lefebvre.
    Le film «The Final Heist» a été réalisé en 1991 par George Mihalka.
    Le film «Lana in Love» a été réalisé en 1991 par Bashar Shbib.
    Le film «Love-moi» a été réalisé en 1991 par Marcel Simard.
    Le film «Montréal vu par…» a été réalisé en 1991 par Un collectif de réalisateurs.
    Le film «Motyli Cas» a été réalisé en 1991 par Bretislav Pojar.
    Le film «Nelligan» a été réalisé en 1991 par Robert Favreau.
    Le film «Pablo qui court» a été réalisé en 1991 par Bernard Bergeron.
    Le film «The Pianist» a été réalisé en 1991 par Claude Gagnon.
    Le film «La Révolution française : les années lumières» a été réalisé en 1991 par Robert Enrico.
    Le film «Scanners 2: The New Order» a été réalisé en 1991 par Christian Duguay.
    Le film «Sous le signe du poisson» a été réalisé en 1991 par Serge Pénard.
    Le film «Un Léger vertige» a été réalisé en 1991 par Diane Poitras.
    Le film «Vincent et moi» a été réalisé en 1991 par Michael Rubbo.
    Le film «Aline» a été réalisé en 1992 par Carole Laganière.
    Le film «L'Automne sauvage» a été réalisé en 1992 par Gabriel Pelletier.
    Le film «Being at Home with Claude» a été réalisé en 1992 par Jean Beaudin.
    Le film «La Bête de foire» a été réalisé en 1992 par Isabelle Hayeur.
    Le film «Call of the Wild» a été réalisé en 1992 par Bjorn Kristen.
    Le film «Canvas» a été réalisé en 1992 par Alain Zaloum.
    Le film «Coyote» a été réalisé en 1992 par Richard Ciupka.
    Le film «A Cry in the Night» a été réalisé en 1992 par Robin Spry.
    Le film «Deadbolt» a été réalisé en 1992 par Douglas Jackson.
    Le film «La Fenêtre» a été réalisé en 1992 par Monique Champagne.
    Le film «Francœur: exit pour nomades» a été réalisé en 1992 par Pierre Bastien.
    Le film «L'Homme de ma vie» a été réalisé en 1992 par Jean-Charles Tacchella.
    Le film «Killer Image» a été réalisé en 1992 par David Winning.
    Le film «El lado oscuro del corazón» a été réalisé en 1992 par Eliseo Subiela.
    Le film «Léolo» a été réalisé en 1992 par Jean-Claude Lauzon.
    Le film «Le Mirage» a été réalisé en 1992 par Jean-Claude Guiguet.
    Le film «La Postière» a été réalisé en 1992 par Gilles Carle.
    Le film «Psychic» a été réalisé en 1992 par George Mihalka.
    Le film «Requiem pour un beau sans-cœur» a été réalisé en 1992 par Robert Morin.
    Le film «La Sarrasine» a été réalisé en 1992 par Paul Tana.
    Le film «Scanners 3: The Takeover» a été réalisé en 1992 par Christian Duguay.
    Le film «Shadow of the Wolf» a été réalisé en 1992 par Jacques Dorfmann, Pierre Magny.
    Le film «Tirelire Combines & Cie» a été réalisé en 1992 par Jean Beaudry.
    Le film «La Vie fantôme» a été réalisé en 1992 par Jacques Leduc.
    Le film «Le Voleur de caméra» a été réalisé en 1992 par Claude Fortin.
    Le film «Les Amoureuses» a été réalisé en 1993 par Johanne Prégent.
    Le film «Armen et Bullik» a été réalisé en 1993 par Allan Cook.
    Le film «Because Why» a été réalisé en 1993 par Arto Paragamian.
    Le film «Cap Tourmente» a été réalisé en 1993 par Michel Langlois.
    Le film «Deux actrices» a été réalisé en 1993 par Micheline Lanctôt.
    Le film «Doublures» a été réalisé en 1993 par Michel Murray.
    Le film «La Florida» a été réalisé en 1993 par George Mihalka.
    Le film «L'Homme sur les quais» a été réalisé en 1993 par Raoul Peck.
    Le film «Love & Human Remains» a été réalisé en 1993 par Denys Arcand.
    Le film «Matusalem» a été réalisé en 1993 par Roger Cantin.
    Le film «Les Mots perdus : un film en quatre saisons» a été réalisé en 1993 par Marcel Simard.
    Le film «The Myth of the Male Orgasm» a été réalisé en 1993 par John Hamilton.
    Le film «The Neighbor» a été réalisé en 1993 par Rodney Gibbons.
    Le film «Pamietnik znaleziony w garbie» a été réalisé en 1993 par Jan Kidawa-Blonski.
    Le film «Les Pots cassés» a été réalisé en 1993 par François Bouvier.
    Le film «Saint-Fortunat, les blues» a été réalisé en 1993 par Daniel St-Laurent.
    Le film «Le Sexe des étoiles» a été réalisé en 1993 par Paule Baillargeon.
    Le film «Tendre guerre» a été réalisé en 1993 par Daniel Morin.
    Le film «Thirty Two Short Films About Glenn Gould» a été réalisé en 1993 par François Girard.
    Le film «La Visite» a été réalisé en 1993 par Jorge Fajardo.
    Le film «C'était le 12 du 12 et Chili avait les blues» a été réalisé en 1994 par Charles Binamé.
    Le film «The Child» a été réalisé en 1994 par George Mihalka.
    Le film «Crack Me Up» a été réalisé en 1994 par Bashar Shbib.
    Le film «Craque la vie !» a été réalisé en 1994 par Jean Beaudin.
    Le film «Draghoula» a été réalisé en 1994 par Basha Shbib.
    Le film «La Fête des rois» a été réalisé en 1994 par Marquise Lepage.
    Le film «Gaïa» a été réalisé en 1994 par Pierre Lang.
    Le film «El jardín del edén» a été réalisé en 1994 par Maria Novaro.
    Le film «Kabloonak» a été réalisé en 1994 par Claude Massot.
    Le film «Le Lac de la lune» a été réalisé en 1994 par Michel Jetté.
    Le film «Louis 19, le roi des ondes» a été réalisé en 1994 par Michel Poulette.
    Le film «Ma sœur, mon amour» a été réalisé en 1994 par Suzy Cohen.
    Le film «Mon amie Max» a été réalisé en 1994 par Michel Brault.
    Le film «Mouvements du désir» a été réalisé en 1994 par Léa Pool.
    Le film «Los Naufragos» a été réalisé en 1994 par Miguel Littin.
    Le film «Octobre» a été réalisé en 1994 par Pierre Falardeau.
    Le film «The Paperboy» a été réalisé en 1994 par Douglas Jackson.
    Le film «Le Retour des aventuriers du timbre perdu» a été réalisé en 1994 par Michael Rubbo.
    Le film «Rêve aveugle» a été réalisé en 1994 par Diane Beaudry.
    Le film «Ride Me» a été réalisé en 1994 par Bashar Shbib.
    Le film «Ruth» a été réalisé en 1994 par François Delisle.
    Le film «Le Secret de Jérôme» a été réalisé en 1994 par Phil Comeau.
    Le film «Stalked» a été réalisé en 1994 par Douglas Jackson.
    Le film «V'la l'cinéma ou le roman de Charles Pathé» a été réalisé en 1994 par Jacques Rouffio.
    Le film «Le Vent du Wyoming» a été réalisé en 1994 par André Forcier.
    Le film «La Vie d'un héros» a été réalisé en 1994 par Micheline Lanctôt.
    Le film «Warriors» a été réalisé en 1994 par Shimon Dotan.
    Le film «Windigo» a été réalisé en 1994 par Robert Morin.
    Le film «Yes sir! Madame…» a été réalisé en 1994 par Robert Morin.
    Le film «Angelo, Frédo et Roméo» a été réalisé en 1995 par Pierre Plante.
    Le film «Le Confessionnal» a été réalisé en 1995 par Robert Lepage.
    Le film «Eldorado» a été réalisé en 1995 par Charles Binamé.
    Le film «L'Enfant d'eau» a été réalisé en 1995 par Robert Ménard.
    Le film «Erreur sur la personne» a été réalisé en 1995 par Gilles Noël.
    Le film «La folie des crinolines» a été réalisé en 1995 par Jean Gagné, Serge Gagné.
    Le film «Highlander 3: The Sorcerer» a été réalisé en 1995 par Andy Morahan.
    Le film «Hollow Point» a été réalisé en 1995 par Sidney J. Furie.
    Le film «J'aime j'aime pas» a été réalisé en 1995 par Sylvie Groulx.
    Le film «Le jour des cris» a été réalisé en 1995 par Éric Gignac.
    Le film «Kids of the Round Table» a été réalisé en 1995 par Robert Tinnell.
    Le film «Liste noire» a été réalisé en 1995 par Jean-Marc Vallée.
    Le film «Motel» a été réalisé en 1995 par Pascal Maeder (en).
    Le film «La Mule et les Émeraudes» a été réalisé en 1995 par Bashar Shbib.
    Le film «Screamers» a été réalisé en 1995 par Christian Duguay.
    Le film «Silent Hunter» a été réalisé en 1995 par Fred Williamson.
    Le film «Le Sphinx» a été réalisé en 1995 par Louis Saïa.
    Le film «Suburban Legend» a été réalisé en 1995 par Jay Ferguson.
    Le film «Witchboard 3» a été réalisé en 1995 par Peter Svatek.
    Le film «Wrong Woman» a été réalisé en 1995 par Douglas Jackson.
    Le film «Zigrail» a été réalisé en 1995 par André Turpin.
    Le film «Aire libre» a été réalisé en 1996 par Luis Armando Roche.
    Le film «La Ballade de Titus : une fable tout en couleur» a été réalisé en 1996 par Vincent De Brus.
    Le film «La Beauté, fatale et féroce» a été réalisé en 1996 par Roger Boire.
    Le film «Bullet to Beijing» a été réalisé en 1996 par George Mihalka.
    Le film «Caboose» a été réalisé en 1996 par Richard Roy.
    Le film «Cosmos» a été réalisé en 1996 par Un collectif de réalisateurs et de réalisatrices.
    Le film «Le Cri de la nuit» a été réalisé en 1996 par Jean Beaudry.
    Le film «L'Escorte» a été réalisé en 1996 par Denis Langlois.
    Le film «La Fabrication d'un meurtrier» a été réalisé en 1996 par Isabelle Poissant.
    Le film «L'Homme idéal» a été réalisé en 1996 par George Mihalka.
    Le film «L'Homme perché» a été réalisé en 1996 par Stefan Pleszczynski.
    Le film «Hot Sauce» a été réalisé en 1996 par Bashar Shbib.
    Le film «J'en suis !» a été réalisé en 1996 par Claude Fournier.
    Le film «Joyeux Calvaire» a été réalisé en 1996 par Denys Arcand.
    Le film «Karmina» a été réalisé en 1996 par Gabriel Pelletier.
    Le film «Lilies» a été réalisé en 1996 par John Greyson.
    Le film «La Marche à l'amour» a été réalisé en 1996 par Jean Gagné, Serge Gagné.
    Le film «Never Too Late» a été réalisé en 1996 par Giles Walker.
    Le film «L'Oreille d'un sourd» a été réalisé en 1996 par Mario Bolduc.
    Le film «Panic» a été réalisé en 1996 par Bashar Shbib.
    Le film «Le Polygraphe» a été réalisé en 1996 par Robert Lepage.
    Le film «Pudding chômeur» a été réalisé en 1996 par Gilles Carle.
    Le film «Rowing Through» a été réalisé en 1996 par Masato Harada.
    Le film «Le Silence des fusils» a été réalisé en 1996 par Arthur Lamothe.
    Le film «Sous-sol» a été réalisé en 1996 par Pierre Gang.
    Le film «The Strange Blues of Cowboy Red» a été réalisé en 1996 par Rick Raxlen.
    Le film «La Vengeance de la femme en noir» a été réalisé en 1996 par Roger Cantin.
    Le film «The Windsor Protocol» a été réalisé en 1996 par George Mihalka.
    Le film «L'absent» a été réalisé en 1997 par Céline Baril.
    Le film «The Assignment» a été réalisé en 1997 par Christian Duguay.
    Le film «Les Boys» a été réalisé en 1997 par Louis Saïa.
    Le film «Burnt Eden» a été réalisé en 1997 par Eugene Garcia.
    Le film «Cabaret neiges noires» a été réalisé en 1997 par Raymond Saint-Jean.
    Le film «The Call of the Wild» a été réalisé en 1997 par Peter Svatek.
    Le film «Le Ciel est à nous» a été réalisé en 1997 par Graham Guit.
    Le film «Clandestins» a été réalisé en 1997 par Denis Chouinard, Nicolas Wadimoff.
    Le film «La Comtesse de Bâton Rouge» a été réalisé en 1997 par André Forcier.
    Le film «La Conciergerie» a été réalisé en 1997 par Michel Poulette.
    Le film «De l'autre côté du cœur» a été réalisé en 1997 par Suzy Cohen.
    Le film «Habitat» a été réalisé en 1997 par René Daalder.
    Le film «Hemoglobin» a été réalisé en 1997 par Peter Svatek.
    Le film «Kayla» a été réalisé en 1997 par Nicholas Kendall.
    Le film «Maîtres anciens» a été réalisé en 1997 par Olivier Asselin.
    Le film «Matusalem II : le dernier des Beauchesne» a été réalisé en 1997 par Roger Cantin.
    Le film «Les Mille merveilles de l'univers» a été réalisé en 1997 par Jean-Michel Roux.
    Le film «Moustaches» a été réalisé en 1997 par Jim Kaufman.
    Le film «Reaper» a été réalisé en 1997 par John Bradshaw.
    Le film «Sinon, oui» a été réalisé en 1997 par Claire Simon.
    Le film «Le Siège de l'âme» a été réalisé en 1997 par Olivier Asselin.
    Le film «Stranger in the House» a été réalisé en 1997 par Rodney Gibbons.
    Le film «Strip Search» a été réalisé en 1997 par Rod Hewitt.
    Le film «Treasure Island» a été réalisé en 1997 par Peter Rowe.
    Le film «2 secondes» a été réalisé en 1998 par Manon Briand.
    Le film «L'âge de braise» a été réalisé en 1998 par Jacques Leduc.
    Le film «Aujourd'hui ou jamais» a été réalisé en 1998 par Jean Pierre Lefebvre.
    Le film «Babel» a été réalisé en 1998 par Gérard Pullicino.
    Le film «Les Boys 2» a été réalisé en 1998 par Louis Saïa.
    Le film «C't'à ton tour, Laura Cadieux» a été réalisé en 1998 par Denise Filiatrault.
    Le film «Captive» a été réalisé en 1998 par Matt Dorff.
    Le film «Le Cœur au poing» a été réalisé en 1998 par Charles Binamé.
    Le film «Dancing on the Moon» a été réalisé en 1998 par Kit Hood.
    Le film «Dead End» a été réalisé en 1998 par Douglas Jackson.
    Le film «La Déroute» a été réalisé en 1998 par Paul Tana.
    Le film «Fatal Affair» a été réalisé en 1998 par Marc S. Grenier.
    Le film «Foreign Ghosts» a été réalisé en 1998 par Hunt Hoe.
    Le film «Going to Kansas City» a été réalisé en 1998 par Pekka Mandart.
    Le film «Le grand serpent du monde» a été réalisé en 1998 par Yves Dion.
    Le film «Hasards ou Coïncidences» a été réalisé en 1998 par Claude Lelouch.
    Le film «Hathi» a été réalisé en 1998 par Philippe Gautier.
    Le film «In Her Defense» a été réalisé en 1998 par Sidney J. Furie.
    Le film «N.I.P.» a été réalisé en 1998 par François Raymond.
    Le film «Nguol thùa» a été réalisé en 1998 par Dai Sijie.
    Le film «Nico the Unicorn» a été réalisé en 1998 par Graeme Campbell.
    Le film «Nô» a été réalisé en 1998 par Robert Lepage.
    Le film «La Position de l'escargot» a été réalisé en 1998 par Michka Saäl.
    Le film «The Press Run» a été réalisé en 1998 par Robert Ditchburn.
    Le film «Quelque chose d'organique» a été réalisé en 1998 par Bertrand Bonello.
    Le film «Quiconque meurt, meurt à douleur» a été réalisé en 1998 par Robert Morin.
    Le film «Random Encounter» a été réalisé en 1998 par Douglas Jackson.
    Le film «Requiem for Murder» a été réalisé en 1998 par Douglas Jackson.
    Le film «Revoir Julie» a été réalisé en 1998 par Jeanne Crépeau.
    Le film «Running Home» a été réalisé en 1998 par Marc F. Voizard.
    Le film «Snitch» a été réalisé en 1998 par Ted Demme.
    Le film «Sublet» a été réalisé en 1998 par John Hamilton.
    Le film «Thunder Point» a été réalisé en 1998 par George Mihalka.
    Le film «Un 32 août sur Terre» a été réalisé en 1998 par Denis Villeneuve.
    Le film «Une aventure de Papyrus : La vengeance de Seth» a été réalisé en 1998 par Michel Gauthier.
    Le film «Le Violon rouge» a été réalisé en 1998 par François Girard.
    Le film «Winter Lily» a été réalisé en 1998 par Roshell Bissett.
    Le film «You Can Thank Me Later» a été réalisé en 1998 par Shimon Dotan.
    Le film «4 Days» a été réalisé en 1999 par Curtis Wehrfritz.
    Le film «Artificial Lies» a été réalisé en 1999 par Rodney Gibbons.
    Le film «L'Autobiographe amateur» a été réalisé en 1999 par Claude Fortin.
    Le film «Autour de la maison rose» a été réalisé en 1999 par Joana Hadjithomas.
    Le film «Le Dernier Souffle» a été réalisé en 1999 par Richard Ciupka.
    Le film «Elvis Gratton 2 : Miracle à Memphis» a été réalisé en 1999 par Pierre Falardeau, Julien Poulin.
    Le film «Emporte-moi» a été réalisé en 1999 par Léa Pool.
    Le film «La Fabrication des mâles» a été réalisé en 1999 par Un collectif de réalisateurs et de réalisatrices.
    Le film «Fish Out of Water» a été réalisé en 1999 par Geoffrey Edwards.
    Le film «Full Blast» a été réalisé en 1999 par Rodrigue Jean.
    Le film «Grey Owl» a été réalisé en 1999 par Richard Attenborough.
    Le film «Histoires d'hiver» a été réalisé en 1999 par François Bouvier.
    Le film «Home Team» a été réalisé en 1999 par Allan A. Goldstein.
    Le film «Laura Cadieux… la suite» a été réalisé en 1999 par Denise Filiatrault.
    Le film «Matroni et moi» a été réalisé en 1999 par Jean-Philippe Duval.
    Le film «Le Petit ciel» a été réalisé en 1999 par Jean-Sébastien Lord.
    Le film «Pin-Pon, le film» a été réalisé en 1999 par Ghyslaine Côté.
    Le film «Post mortem» a été réalisé en 1999 par Louis Bélanger.
    Le film «Quand je serai parti… vous vivrez encore» a été réalisé en 1999 par Michel Brault.
    Le film «Les Siamoises» a été réalisé en 1999 par Isabelle Hayeur.
    Le film «Souffle d'ailleurs» a été réalisé en 1999 par Martin Leclerc, Thomas Schneider.
    Le film «Souvenirs intimes» a été réalisé en 1999 par Jean Beaudin.
    Le film «Task Force» a été réalisé en 1999 par Richard Ciupka.
    Le film «Taxman» a été réalisé en 1999 par Alain Zaloum.
    Le film «Who Gets the House?» a été réalisé en 1999 par Timothy J. Nelson.
    Le film «The Witness Files» a été réalisé en 1999 par Douglas Jackson.
    Le film «Alegria» a été réalisé en 2000 par Franco Dragone.
    Le film «The Art of War» a été réalisé en 2000 par Christian Duguay.
    Le film «La beauté de Pandore» a été réalisé en 2000 par Charles Binamé.
    Le film «Blind Terror» a été réalisé en 2000 par Giles Walker.
    Le film «La bouteille» a été réalisé en 2000 par Alain DesRochers.
    Le film «Café Olé» a été réalisé en 2000 par Richard Roy.
    Le film «Deception» a été réalisé en 2000 par Max Fischer.
    Le film «Desire» a été réalisé en 2000 par Colleen Murphy.
    Le film «Les fantômes des trois Madeleine» a été réalisé en 2000 par Guylaine Dionne.
    Le film «Heavy Metal 2000» a été réalisé en 2000 par Michael Coldewey, Michel Lemire.
    Le film «Hochelaga» a été réalisé en 2000 par Michel Jetté.
    Le film «L'île de sable» a été réalisé en 2000 par Johanne Prégent.
    Le film «L'invention de l'amour» a été réalisé en 2000 par Claude Demers.
    Le film «Island of the Dead» a été réalisé en 2000 par Tim Southam.
    Le film «Jack and Ella» a été réalisé en 2000 par Brenda Keesal.
    Le film «La journée avant» a été réalisé en 2000 par David Lussier.
    Le film «The List» a été réalisé en 2000 par Sylvain Guy.
    Le film «Maelström» a été réalisé en 2000 par Denis Villeneuve.
    Le film «Méchant party» a été réalisé en 2000 par Mario Chabot.
    Le film «La moitié gauche du frigo» a été réalisé en 2000 par Philippe Falardeau.
    Le film «Les muses orphelines» a été réalisé en 2000 par Robert Favreau.
    Le film «My Little Devil» a été réalisé en 2000 par Gopi Desai.
    Le film «Ne dis rien» a été réalisé en 2000 par Simon Lacombe.
    Le film «Possible Worlds» a été réalisé en 2000 par Robert Lepage.
    Le film «Rats and Rabbits» a été réalisé en 2000 par Lewis Furey.
    Le film «La répétition» a été réalisé en 2000 par Catherine Corsini.
    Le film «Saint Jude» a été réalisé en 2000 par John L'Écuyer.
    Le film «Stardom» a été réalisé en 2000 par Denys Arcand.
    Le film «Subconscious Cruelty» a été réalisé en 2000 par Karim Hussain.
    Le film «The Tracker» a été réalisé en 2000 par Jeff Schechter.
    Le film «Trick or Treat» a été réalisé en 2000 par Marc Cayer.
    Le film «Two Thousand and None» a été réalisé en 2000 par Arto Paragamian.
    Le film «Un petit vent de panique» a été réalisé en 2000 par Pierre Greco.
    Le film «Varian's War» a été réalisé en 2000 par Lionel Chetwynd.
    Le film «La veuve de Saint-Pierre» a été réalisé en 2000 par Patrice Leconte.
    Le film «La vie après l'amour» a été réalisé en 2000 par Gabriel Pelletier.
    Le film «Xchange» a été réalisé en 2000 par Allan Moyle.
    Le film «15 février 1839» a été réalisé en 2001 par Pierre Falardeau.
    Le film «L'ange de goudron» a été réalisé en 2001 par Denis Chouinard.
    Le film «Les Boys 3» a été réalisé en 2001 par Louis Saïa.
    Le film «Des chiens dans la neige» a été réalisé en 2001 par Michel Welterlin.
    Le film «Le ciel sur la tête» a été réalisé en 2001 par Geneviève Lefebvre, André Melançon.
    Le film «Crème glacée, chocolat et autres consolations» a été réalisé en 2001 par Julie Hivon.
    Le film «Danny in the Sky» a été réalisé en 2001 par Denis Langlois.
    Le film «Dead Awake» a été réalisé en 2001 par Marc S. Grenier.
    Le film «Du pic au cœur» a été réalisé en 2001 par Céline Baril.
    Le film «La femme qui boit» a été réalisé en 2001 par Bernard Émond.
    Le film «La forteresse suspendue» a été réalisé en 2001 par Roger Cantin.
    Le film «Hidden Agenda» a été réalisé en 2001 par Marc S. Grenier.
    Le film «Hôtel des horizons» a été réalisé en 2001 par Marc Cayer.
    Le film «Karmen Geï» a été réalisé en 2001 par Joseph Gaï Ramaka.
    Le film «Karmina 2» a été réalisé en 2001 par Gabriel Pelletier.
    Le film «La loi du cochon» a été réalisé en 2001 par Érik Canuel.
    Le film «Lost and Delirious» a été réalisé en 2001 par Léa Pool.
    Le film «Mariages» a été réalisé en 2001 par Catherine Martin.
    Le film «Nuit de noces» a été réalisé en 2001 par Émile Gaudreault.
    Le film «Opération Cobra» a été réalisé en 2001 par Dominic Gagnon, Richard Jutras, Robert Morin.
    Le film «Le pornographe» a été réalisé en 2001 par Bertrand Bonello.
    Le film «Protection» a été réalisé en 2001 par John Flynn.
    Le film «So Faraway and Blue» a été réalisé en 2001 par Roy Cross.
    Le film «Stéphanie, Nathalie, Caroline et Vincent» a été réalisé en 2001 par Carl Ulrich.
    Le film «Un crabe dans la tête» a été réalisé en 2001 par André Turpin.
    Le film «Une jeune fille à la fenêtre» a été réalisé en 2001 par Francis Leclerc.
    Le film «La vérité est un mensonge» a été réalisé en 2001 par Pierre Goupil.
    Le film «Au fil de l'eau» a été réalisé en 2002 par Jeannine Gagné.
    Le film «Barbaloune» a été réalisé en 2002 par Jean Gagné, Serge Gagné.
    Le film «The Baroness and the Pig» a été réalisé en 2002 par Michael MacKenzie.
    Le film «Between Strangers» a été réalisé en 2002 par Edoardo Ponti.
    Le film «Le collectionneur» a été réalisé en 2002 par Jean Beaudin.
    Le film «Les Dangereux» a été réalisé en 2002 par Louis Saïa.
    Le film «Les Fils de Marie» a été réalisé en 2002 par Carole Laure.
    Le film «Le gambit du fou» a été réalisé en 2002 par Bruno Dubuc.
    Le film «Histoire de pen» a été réalisé en 2002 par Michel Jetté.
    Le film «Home» a été réalisé en 2002 par Phyllis Katrapani.
    Le film «Iso» a été réalisé en 2002 par Dominic Gagnon.
    Le film «Katryn's Place» a été réalisé en 2002 par Bénédicte Ronfard.
    Le film «Leaving Metropolis» a été réalisé en 2002 par Brad Fraser.
    Le film «Madame Brouette» a été réalisé en 2002 par Moussa Sene Absa.
    Le film «Le marais» a été réalisé en 2002 par Kim Nguyen.
    Le film «La Mystérieuse Mademoiselle C.» a été réalisé en 2002 par Richard Ciupka.
    Le film «Le Nèg'» a été réalisé en 2002 par Robert Morin.
    Le film «L'Odyssée d'Alice Tremblay» a été réalisé en 2002 par Denise Filiatrault.
    Le film «One Way Out» a été réalisé en 2002 par Allan A. Goldstein.
    Le film «Québec-Montréal» a été réalisé en 2002 par Ricardo Trogi.
    Le film «Régina!» a été réalisé en 2002 par Maria Sigurdardottir.
    Le film «Royal Bonbon» a été réalisé en 2002 par Charles Najman.
    Le film «Savage Messiah» a été réalisé en 2002 par Mario Azzopardi.
    Le film «Secret de banlieue» a été réalisé en 2002 par Louis Choquette.
    Le film «Séraphin : un homme et son péché» a été réalisé en 2002 par Charles Binamé.
    Le film «Sex at the End of the Millenium» a été réalisé en 2002 par Sheldon Neuberger.
    Le film «Station Nord» a été réalisé en 2002 par Jean-Claude Lord.
    Le film «Summer» a été réalisé en 2002 par Phil Price.
    Le film «La turbulence des fluides» a été réalisé en 2002 par Manon Briand.
    Le film «Women Without Wings» a été réalisé en 2002 par Nicholas Kinsey.
    Le film «Yellowknife» a été réalisé en 2002 par Rodrigue Jean.
    Le film «100 % bio» a été réalisé en 2003 par Claude Fortin.
    Le film «20h17, rue Darling» a été réalisé en 2003 par Bernard Émond.
    Le film «Annie Brocoli dans les fonds marins» a été réalisé en 2003 par Claude Brie.
    Le film «The Book of Eve» a été réalisé en 2003 par Claude Fournier.
    Le film «Comment ma mère accoucha de moi durant sa ménopause» a été réalisé en 2003 par Sébastien Rose.
    Le film «La face cachée de la lune» a été réalisé en 2003 par Robert Lepage.
    Le film «The Favourite Game» a été réalisé en 2003 par Bernar Hébert.
    Le film «Gaz Bar Blues» a été réalisé en 2003 par Louis Bélanger.
    Le film «La grande séduction» a été réalisé en 2003 par Jean-François Pouliot.
    Le film «L'homme trop pressé prend son thé à la fourchette» a été réalisé en 2003 par Sylvie Groulx.
    Le film «Les immortels» a été réalisé en 2003 par Paul Thinel.
    Le film «Les invasions barbares» a été réalisé en 2003 par Denys Arcand.
    Le film «Jack Paradise» a été réalisé en 2003 par Gilles Noël.
    Le film «Ma voisine danse le ska» a été réalisé en 2003 par Nathalie St-Pierre.
    Le film «Mambo Italiano» a été réalisé en 2003 par Émile Gaudreault.
    Le film «Le manuscrit érotique» a été réalisé en 2003 par Jean Pierre Lefebvre.
    Le film «Nez rouge» a été réalisé en 2003 par Érik Canuel.
    Le film «Père et fils» a été réalisé en 2003 par Michel Boujenah.
    Le film «La petite Lili» a été réalisé en 2003 par Claude Miller.
    Le film «Le piège d'Issoudun» a été réalisé en 2003 par Micheline Lanctôt.
    Le film «Revival Blues» a été réalisé en 2003 par Claude Gagnon.
    Le film «Saved by the Belles» a été réalisé en 2003 par Ziad Touma.
    Le film «Le secret de Cyndia» a été réalisé en 2003 par Denyse Benoît.
    Le film «A Silent Love» a été réalisé en 2003 par Federico Hidalgo.
    Le film «Sur le seuil» a été réalisé en 2003 par Éric Tessier.
    Le film «Les triplettes de Belleville» a été réalisé en 2003 par Sylvain Chomet.
    Le film «Twist» a été réalisé en 2003 par Jacob Tierney.
    Le film «Acapulco Gold» a été réalisé en 2004 par André Forcier.
    Le film «Les aimants» a été réalisé en 2004 par Yves P. Pelletier.
    Le film «The Blue Butterfly» a été réalisé en 2004 par Léa Pool.
    Le film «Le bonheur c'est une chanson triste» a été réalisé en 2004 par François Delisle.
    Le film «Bonzaïon» a été réalisé en 2004 par Danny Gilmore, Clermont Jolicoeur.
    Le film «C'est pas moi… c'est l'autre» a été réalisé en 2004 par Alain Zaloum.
    Le film «Camping sauvage» a été réalisé en 2004 par Guy A. Lepage, Sylvain Roy.
    Le film «Comment conquérir l'Amérique en une nuit?» a été réalisé en 2004 par Dany Laferrière.
    Le film «Comment devenir un trou de cul et enfin plaire aux femmes» a été réalisé en 2004 par Roger Boire.
    Le film «CQ2 (Seek You Too)» a été réalisé en 2004 par Carole Laure.
    Le film «Dans l'œil du chat» a été réalisé en 2004 par Rudy Barichello.
    Le film «Dans une galaxie près de chez vous» a été réalisé en 2004 par Claude Desrosiers.
    Le film «Le dernier tunnel» a été réalisé en 2004 par Érik Canuel.
    Le film «Dorian» a été réalisé en 2004 par Allan A. Goldstein.
    Le film «Elles étaient cinq» a été réalisé en 2004 par Ghyslaine Côté.
    Le film «Elvis Gratton XXX : la vengeance d'Elvis Wong» a été réalisé en 2004 par Pierre Falardeau.
    Le film «L'espérance» a été réalisé en 2004 par Stefan Pleszczynski.
    Le film «Éternelle» a été réalisé en 2004 par Wilhelm Liebenberg.
    Le film «Folle embellie» a été réalisé en 2004 par Dominique Cabrera.
    Le film «Goldirocks» a été réalisé en 2004 par Paula Tiberius.
    Le film «Le golem de Montréal» a été réalisé en 2004 par Isabelle Hayeur.
    Le film «Le goût des jeunes filles» a été réalisé en 2004 par John L'Écuyer.
    Le film «L'incomparable mademoiselle C.» a été réalisé en 2004 par Richard Ciupka.
    Le film «Je n'aime que toi» a été réalisé en 2004 par Claude Fournier.
    Le film «Jeunesse dans l'ombre» a été réalisé en 2004 par Gervais Germain.
    Le film «Jimmywork» a été réalisé en 2004 par Simon Sauvé.
    Le film «Littoral» a été réalisé en 2004 par Wajdi Mouawad.
    Le film «La lune viendra d'elle-même» a été réalisé en 2004 par Marie-Jan Seille.
    Le film «Ma vie en cinémascope» a été réalisé en 2004 par Denise Filiatrault.
    Le film «Mémoires affectives» a été réalisé en 2004 par Francis Leclerc.
    Le film «Monica la mitraille» a été réalisé en 2004 par Pierre Houle.
    Le film «Les moscovites» a été réalisé en 2004 par Charles Barabé.
    Le film «Nouvelle-France» a été réalisé en 2004 par Jean Beaudin.
    Le film «Ordo» a été réalisé en 2004 par Laurence Ferreira Barbosa.
    Le film «La peau blanche» a été réalisé en 2004 par Daniel Roby.
    Le film «La pension des étranges» a été réalisé en 2004 par Stella Goulet.
    Le film «Pinocchio 3000» a été réalisé en 2004 par Daniel Robichaud.
    Le film «La planque» a été réalisé en 2004 par Alexandre Chartrand.
    Le film «Premier juillet - le film» a été réalisé en 2004 par Philippe Gagnon.
    Le film «Pure» a été réalisé en 2004 par Jim Donovan.
    Le film «Vendus» a été réalisé en 2004 par Éric Tessier.
    Le film «Amnésie - L'Énigme James Brighton» a été réalisé en 2005 par Denis Langlois.
    Le film «L'audition» a été réalisé en 2005 par Luc Picard.
    Le film «Aurore» a été réalisé en 2005 par Luc Dionne.
    Le film «Barmaids» a été réalisé en 2005 par Simon Boisvert.
    Le film «Les Boys 4» a été réalisé en 2005 par George Mihalka.
    Le film «C.R.A.Z.Y.» a été réalisé en 2005 par Jean-Marc Vallée.
    Le film «Le chalet» a été réalisé en 2005 par Jarrett Mann.
    Le film «Daniel et les Superdogs» a été réalisé en 2005 par André Melançon.
    Le film «La dernière incarnation» a été réalisé en 2005 par Demian Fuica.
    Le film «Les états nordiques» a été réalisé en 2005 par Denis Côté.
    Le film «Les États-Unis d'Albert» a été réalisé en 2005 par André Forcier.
    Le film «Familia» a été réalisé en 2005 par Louise Archambault.
    Le film «Greg & Gentillon» a été réalisé en 2005 par Matthiew Klinck.
    Le film «Horloge biologique» a été réalisé en 2005 par Ricardo Trogi.
    Le film «Idole instantanée» a été réalisé en 2005 par Yves Desgagnés.
    Le film «Kamataki» a été réalisé en 2005 par Claude Gagnon.
    Le film «Latex» a été réalisé en 2005 par Pierre Lapointe.
    Le film «Maman Last Call» a été réalisé en 2005 par François Bouvier.
    Le film «Manners of Dying» a été réalisé en 2005 par Jeremy Peter Allen.
    Le film «Maurice Richard» a été réalisé en 2005 par Charles Binamé.
    Le film «Les moutons de Jacob» a été réalisé en 2005 par Jean-François Pothier.
    Le film «La neuvaine» a été réalisé en 2005 par Bernard Émond.
    Le film «Niagara Motel» a été réalisé en 2005 par Gary Yates.
    Le film «Petit pow! pow! Noël» a été réalisé en 2005 par Robert Morin.
    Le film «La pharmacie de l'espoir» a été réalisé en 2005 par François Gourd.
    Le film «Saint-Ralph» a été réalisé en 2005 par Michael McGowan.
    Le film «Saints-Martyrs-des-Damnés» a été réalisé en 2005 par Robin Aubert.
    Le film «Stories of a Gravedigger» a été réalisé en 2005 par David Aubin.
    Le film «Summer with the Ghosts» a été réalisé en 2005 par Bernd Neuburger.
    Le film «Le survenant» a été réalisé en 2005 par Érik Canuel.
    Le film «Vers le sud» a été réalisé en 2005 par Laurent Cantet.
    Le film «La vie avec mon père» a été réalisé en 2005 par Sébastien Rose.
    Le film «1st bite» a été réalisé en 2006 par Hunt Hoe.
    Le film «The 4th Life» a été réalisé en 2006 par François Miron.
    Le film «L'anus Horribilis» a été réalisé en 2006 par Bruno Dubuc.
    Le film «La belle bête» a été réalisé en 2006 par Karim Hussain.
    Le film «Black Eyed Dog» a été réalisé en 2006 par Pierre Gang.
    Le film «Bluff sou bluff» a été réalisé en 2006 par Wilfort Estimable.
    Le film «Bobby» a été réalisé en 2006 par François Blouin.
    Le film «Bon cop bad cop» a été réalisé en 2006 par Érik Canuel.
    Le film «C'est beau une ville la nuit» a été réalisé en 2006 par Richard Bohringer.
    Le film «Cadavre exquis première édition» a été réalisé en 2006 par Un collectif de réalisateurs et de réalisatrices.
    Le film «Les cavaliers de la canette» a été réalisé en 2006 par Louis Champagne.
    Le film «Cercle vicieux» a été réalisé en 2006 par Patrick Hébert.
    Le film «Cheech» a été réalisé en 2006 par Patrice Sauvé.
    Le film «Comme tout le monde» a été réalisé en 2006 par Pierre-Paul Renders.
    Le film «Congorama» a été réalisé en 2006 par Philippe Falardeau.
    Le film «Convoitises» a été réalisé en 2006 par Jean Alix Holmand.
    Le film «La coupure» a été réalisé en 2006 par Jean Châteauvert.
    Le film «De ma fenêtre, sans maison…» a été réalisé en 2006 par Maryanne Zéhil.
    Le film «Délivrez-moi» a été réalisé en 2006 par Denis Chouinard.
    Le film «The Descendant» a été réalisé en 2006 par Philippe Spurrell.
    Le film «Duo» a été réalisé en 2006 par Richard Ciupka.
    Le film «End of the Line» a été réalisé en 2006 par Maurice Devereaux.
    Le film «Les filles du botaniste» a été réalisé en 2006 par Dai Sijie.
    Le film «Gason makoklen 1» a été réalisé en 2006 par Wilfort Estimable.
    Le film «Gason makoklen 2» a été réalisé en 2006 par Wilfort Estimable.
    Le film «Le génie du crime» a été réalisé en 2006 par Louis Bélanger.
    Le film «Le guide de la petite vengeance» a été réalisé en 2006 par Jean-François Pouliot.
    Le film «Heads of Control: The Gorul Baheu Brain Expedition» a été réalisé en 2006 par Pat Tremblay.
    Le film «Histoire de famille» a été réalisé en 2006 par Michel Poulette.
    Le film «Hiver» a été réalisé en 2006 par Charles Barabé.
    Le film «The Journals of Knud Rasmussen» a été réalisé en 2006 par Norman Cohn, Zacharias Kunuk.
    Le film «Lézaureil kilyve» a été réalisé en 2006 par Charles Barabé.
    Le film «Malléables journées» a été réalisé en 2006 par Charles Barabé.
    Le film «Manga Latina: Killer on the Loose» a été réalisé en 2006 par Henrique Vera-Villaneuva.
    Le film «Martin Clear: The Whole Story» a été réalisé en 2006 par Martin Leclerc.
    Le film «The Passenger» a été réalisé en 2006 par François Rotger.
    Le film «The Point» a été réalisé en 2006 par Joshua Dorsey.
    Le film «La porte de Fersein» a été réalisé en 2006 par Éric Campeau, Simon Tardif.
    Le film «Pourquoi le dire?» a été réalisé en 2006 par Hélène Duchesneau.
    Le film «Prenez vos places» a été réalisé en 2006 par Marc Thomas-Dupuis.
    Le film «Que Dieu bénisse l'Amérique» a été réalisé en 2006 par Robert Morin.
    Le film «La rage de l'ange» a été réalisé en 2006 par Dan Bigras.
    Le film «Rechercher Victor Pellerin» a été réalisé en 2006 par Sophie Deraspe.
    Le film «Roméo et Juliette» a été réalisé en 2006 par Yves Desgagnés.
    Le film «Sans elle» a été réalisé en 2006 par Jean Beaudin.
    Le film «Le secret de ma mère» a été réalisé en 2006 par Ghyslaine Côté.
    Le film «Steel Toes» a été réalisé en 2006 par Mark Adam, David Gow.
    Le film «Sur la trace d'Igor Rizzi» a été réalisé en 2006 par Noël Mitrani.
    Le film «These Girls» a été réalisé en 2006 par John Hazlett.
    Le film «Touche pas à mon homme» a été réalisé en 2006 par Jean Rony Lubin.
    Le film «Tous les autres, sauf moi» a été réalisé en 2006 par Ann Arson.
    Le film «Un dimanche à Kigali» a été réalisé en 2006 par Robert Favreau.
    Le film «Une rame à l'eau» a été réalisé en 2006 par Jean-François Boudreault.
    Le film «La vie secrète des gens heureux» a été réalisé en 2006 par Stéphane Lapointe.
    Le film «A Year in the Death of Jack Richards» a été réalisé en 2006 par Benjamin P. Paquette.
    Le film «Les 3 p'tits cochons» a été réalisé en 2007 par Patrick Huard.
    Le film «À vos marques… party!» a été réalisé en 2007 par Frédérik D'Amours.
    Le film «Adam's Wall» a été réalisé en 2007 par Michael MacKenzie.
    Le film «L'âge des ténèbres» a été réalisé en 2007 par Denys Arcand.
    Le film «Amour, mensonges et conséquences» a été réalisé en 2007 par Jean Alix Holmand.
    Le film «The Backup Man» a été réalisé en 2007 par Doug Sutherland.
    Le film «La belle empoisonneuse» a été réalisé en 2007 par Richard Jutras.
    Le film «Bluff» a été réalisé en 2007 par Simon-Olivier Fecteau, Marc-André Lavoie.
    Le film «Borderline» a été réalisé en 2007 par Lyne Charlebois.
    Le film «La brunante» a été réalisé en 2007 par Fernand Dansereau.
    Le film «La capture» a été réalisé en 2007 par Carole Laure.
    Le film «Le cèdre penché» a été réalisé en 2007 par Rafaël Ouellet.
    Le film «Continental, un film sans fusil» a été réalisé en 2007 par Stéphane Lafleur.
    Le film «Contre toute espérance» a été réalisé en 2007 par Bernard Émond.
    Le film «Dans les villes» a été réalisé en 2007 par Catherine Martin.
    Le film «De l'autre côté» a été réalisé en 2007 par Sean Marckos.
    Le film «Échangistes» a été réalisé en 2007 par Simon Boisvert.
    Le film «Emotional Arithmetic» a été réalisé en 2007 par Paolo Barzman.
    Le film «Imitation» a été réalisé en 2007 par Federico Hidalgo.
    Le film «La lâcheté» a été réalisé en 2007 par Marc Bisaillon.
    Le film «La logique du remords» a été réalisé en 2007 par Martin Laroche.
    Le film «Ma fille mon ange» a été réalisé en 2007 par Alexis Durand-Brault.
    Le film «Ma tante Aline» a été réalisé en 2007 par Gabriel Pelletier.
    Le film «Minushi» a été réalisé en 2007 par Tyler Gibb.
    Le film «Nitro» a été réalisé en 2007 par Alain DesRochers.
    Le film «Nos vies privées» a été réalisé en 2007 par Denis Côté.
    Le film «Nos voisins Dhantsu» a été réalisé en 2007 par Alain Chicoine.
    Le film «Où vas-tu, Moshé?» a été réalisé en 2007 par Hassan Benjelloun.
    Le film «Puffball» a été réalisé en 2007 par Nicolas Roeg.
    Le film «Rêves de poussière» a été réalisé en 2007 par Laurent Salgues.
    Le film «Le ring» a été réalisé en 2007 par Anaïs Barbeau-Lavalette.
    Le film «La rivière aux castors» a été réalisé en 2007 par Philippe Calderon.
    Le film «Shake Hands with the Devil» a été réalisé en 2007 par Roger Spottiswoode.
    Le film «Silk» a été réalisé en 2007 par François Girard.
    Le film «Steak» a été réalisé en 2007 par Quentin Dupieux.
    Le film «Surviving My Mother» a été réalisé en 2007 par Émile Gaudreault.
    Le film «Toi» a été réalisé en 2007 par François Delisle.
    Le film «Un cri au bonheur» a été réalisé en 2007 par Un collectif de réalisateurs et de réalisatrices.
    Le film «Voleurs de chevaux» a été réalisé en 2007 par Micha Wald.
    Le film «Young Triffie» a été réalisé en 2007 par Mary Walsh.
    Le film «24 mesures» a été réalisé en 2008 par Jalil Lespert.
    Le film «À l'ouest de Pluton» a été réalisé en 2008 par Henry Bernadet, Myriam Verreault.
    Le film «Adagio pour un gars de bicycle» a été réalisé en 2008 par Pascale Ferland.
    Le film «Amour, destin et rock'n'roll» a été réalisé en 2008 par Dario Gasbarro.
    Le film «Babine» a été réalisé en 2008 par Luc Picard.
    Le film «Le banquet» a été réalisé en 2008 par Sébastien Rose.
    Le film «Before Tomorrow» a été réalisé en 2008 par Marie-Hélène Cousineau, Madeline Ivalu.
    Le film «C'est pas moi, je le jure!» a été réalisé en 2008 par Philippe Falardeau.
    Le film «Le cas Roberge» a été réalisé en 2008 par Raphaël Malo.
    Le film «Ce qu'il faut pour vivre» a été réalisé en 2008 par Benoît Pilon.
    Le film «Cruising Bar 2» a été réalisé en 2008 par Michel Côté, Robert Ménard.
    Le film «Dans une galaxie près de chez vous 2» a été réalisé en 2008 par Philippe Gagnon.
    Le film «Demain» a été réalisé en 2008 par Maxime Giroux.
    Le film «Derrière moi» a été réalisé en 2008 par Rafaël Ouellet.
    Le film «Le déserteur» a été réalisé en 2008 par Simon Lavoie.
    Le film «Elle veut le chaos» a été réalisé en 2008 par Denis Côté.
    Le film «En plein cœur» a été réalisé en 2008 par Stéphane Géhami.
    Le film «Le grand départ» a été réalisé en 2008 par Claude Meunier.
    Le film «La ligne brisée» a été réalisé en 2008 par Louis Choquette.
    Le film «Magique!» a été réalisé en 2008 par Philippe Muyl.
    Le film «Maman est chez le coiffeur» a été réalisé en 2008 par Léa Pool.
    Le film «Papa à la chasse aux lagopèdes» a été réalisé en 2008 par Robert Morin.
    Le film «Le piège américain» a été réalisé en 2008 par Charles Binamé.
    Le film «Les plus beaux yeux du monde» a été réalisé en 2008 par Pascal Courchesne, Charlotte Laurier.
    Le film «Restless» a été réalisé en 2008 par Amos Kollek.
    Le film «Suivre Catherine» a été réalisé en 2008 par Jeanne Crépeau.
    Le film «Tout est parfait» a été réalisé en 2008 par Yves-Christian Fournier.
    Le film «Truffe» a été réalisé en 2008 par Kim Nguyen.
    Le film «Un capitalisme sentimental» a été réalisé en 2008 par Olivier Asselin.
    Le film «Un été sans point ni coup sûr» a été réalisé en 2008 par Francis Leclerc.
    Le film «Wushu Warrior» a été réalisé en 2008 par Alain DesRochers.
    Le film «1981» a été réalisé en 2009 par Ricardo Trogi.
    Le film «3 saisons» a été réalisé en 2009 par Jim Donovan.
    Le film «40 is the new 20» a été réalisé en 2009 par Simon Boisvert.
    Le film «5150, rue des Ormes» a été réalisé en 2009 par Éric Tessier.
    Le film «À quelle heure le train pour nulle part» a été réalisé en 2009 par Robin Aubert.
    Le film «À vos marques… party! 2» a été réalisé en 2009 par Frédérik D'Amours.
    Le film «Le bonheur de Pierre» a été réalisé en 2009 par Robert Ménard.
    Le film «Cadavres» a été réalisé en 2009 par Érik Canuel.
    Le film «Carcasses» a été réalisé en 2009 par Denis Côté.
    Le film «La chambre noire» a été réalisé en 2009 par François Aubry.
    Le film «De père en flic» a été réalisé en 2009 par Émile Gaudreault.
    Le film «Dédé à travers les brumes» a été réalisé en 2009 par Jean-Philippe Duval.
    Le film «Détour» a été réalisé en 2009 par Sylvain Guy.
    Le film «Les doigts croches» a été réalisé en 2009 par Ken Scott.
    Le film «La donation» a été réalisé en 2009 par Bernard Émond.
    Le film «Grande Ourse - La clé des possibles» a été réalisé en 2009 par Patrice Sauvé.
    Le film «Les grandes chaleurs» a été réalisé en 2009 par Sophie Lorain.
    Le film «Impasse» a été réalisé en 2009 par Joël Gauthier.
    Le film «J'ai tué ma mère» a été réalisé en 2009 par Xavier Dolan.
    Le film «Je me souviens» a été réalisé en 2009 par André Forcier.
    Le film «Lost Song» a été réalisé en 2009 par Rodrigue Jean.
    Le film «Love and Savagery» a été réalisé en 2009 par John N. Smith.
    Le film «Martyrs» a été réalisé en 2009 par Pascal Laugier.
    Le film «Modern Love» a été réalisé en 2009 par Stéphane Kazandjian.
    Le film «Noémie le secret» a été réalisé en 2009 par Frédérik D'Amours.
    Le film «Nuages sur la ville» a été réalisé en 2009 par Simon Galiero.
    Le film «Les pieds dans le vide» a été réalisé en 2009 par Mariloup Wolfe.
    Le film «Polytechnique» a été réalisé en 2009 par Denis Villeneuve.
    Le film «Pour toujours les Canadiens» a été réalisé en 2009 par Sylvain Archambault.
    Le film «Refrain» a été réalisé en 2009 par Tyler Gibb.
    Le film «Sans dessein» a été réalisé en 2009 par Caroline Labrèche, Steeve Léonard.
    Le film «Serveuses demandées» a été réalisé en 2009 par Guylaine Dionne.
    Le film «Suzie» a été réalisé en 2009 par Micheline Lanctôt.
    Le film «The Timekeeper» a été réalisé en 2009 par Louis Bélanger.
    Le film «Transit» a été réalisé en 2009 par Christian de la Cortina.
    Le film «Turbid» a été réalisé en 2009 par George Fok.
    Le film «Un ange à la mer» a été réalisé en 2009 par Frédéric Dumont.
    Le film «Un cargo pour l'Afrique» a été réalisé en 2009 par Roger Cantin.
    Le film «10 1/2» a été réalisé en 2010 par Podz.
    Le film «2 fois une femme» a été réalisé en 2010 par François Delisle.
    Le film «2 frogs dans l'Ouest» a été réalisé en 2010 par Dany Papineau.
    Le film «À l'origine d'un cri» a été réalisé en 2010 par Robin Aubert.
    Le film «Les amours imaginaires» a été réalisé en 2010 par Xavier Dolan.
    Le film «L'appât» a été réalisé en 2010 par Yves Simoneau.
    Le film «Le baiser du barbu» a été réalisé en 2010 par Yves P. Pelletier.
    Le film «Cabotins» a été réalisé en 2010 par Alain DesRochers.
    Le film «La cité» a été réalisé en 2010 par Kim Nguyen.
    Le film «Curling» a été réalisé en 2010 par Denis Côté.
    Le film «La dernière fugue» a été réalisé en 2010 par Léa Pool.
    Le film «L'enfant prodige» a été réalisé en 2010 par Luc Dionne.
    Le film «Everywhere» a été réalisé en 2010 par Alexis Durand-Brault.
    Le film «Filière 13» a été réalisé en 2010 par Patrick Huard.
    Le film «Incendies» a été réalisé en 2010 par Denis Villeneuve.
    Le film «Le journal d'Aurélie Laflamme» a été réalisé en 2010 par Christian Laurence.
    Le film «Journal d'un coopérant» a été réalisé en 2010 par Robert Morin.
    Le film «Lance et compte» a été réalisé en 2010 par Frédérik D'Amours.
    Le film «Lucidité passagère» a été réalisé en 2010 par Fabrice Barrilliet.
    Le film «Mesrine - L'instinct de mort» a été réalisé en 2010 par Jean-François Richet.
    Le film «Les mots gelés» a été réalisé en 2010 par Isabelle D'Amours.
    Le film «New Denmark» a été réalisé en 2010 par Rafaël Ouellet.
    Le film «Oscar et la dame rose» a été réalisé en 2010 par Éric-Emmanuel Schmitt.
    Le film «Piché : entre ciel et terre» a été réalisé en 2010 par Sylvain Archambault.
    Le film «Le poil de la bête» a été réalisé en 2010 par Philippe Gagnon.
    Le film «Reste avec moi» a été réalisé en 2010 par Robert Ménard.
    Le film «Romaine par moins 30» a été réalisé en 2010 par Agnès Obadia.
    Le film «Route 132» a été réalisé en 2010 par Louis Bélanger.
    Le film «Les sept jours du talion» a été réalisé en 2010 par Podz.
    Le film «Les signes vitaux» a été réalisé en 2010 par Sophie Deraspe.
    Le film «Snow & Ashes» a été réalisé en 2010 par Charles-Olivier Michaud.
    Le film «Sortie 67» a été réalisé en 2010 par Jephté Bastien.
    Le film «Trois temps après la mort d'Anna» a été réalisé en 2010 par Catherine Martin.
    Le film «Tromper le silence» a été réalisé en 2010 par Julie Hivon.
    Le film «The Trotsky» a été réalisé en 2010 par Jacob Tierney.
    Le film «The Wild Hunt» a été réalisé en 2010 par Alexandre Franchi.
    Le film «Y'en aura pas de facile» a été réalisé en 2010 par Marc-André Lavoie.
    Le film «À trois, Marie s'en va» a été réalisé en 2011 par Anne-Marie Ngô.
    Le film «Angle mort» a été réalisé en 2011 par Dominic James.
    Le film «Le bonheur des autres» a été réalisé en 2011 par Jean-Philippe Pearson.
    Le film «BumRush» a été réalisé en 2011 par Michel Jetté.
    Le film «Café de Flore» a été réalisé en 2011 par Jean-Marc Vallée.
    Le film «Le colis» a été réalisé en 2011 par Gaël D'Ynglemare.
    Le film «Coteau Rouge» a été réalisé en 2011 par André Forcier.
    Le film «Décharge» a été réalisé en 2011 par Benoît Pilon.
    Le film «Die» a été réalisé en 2011 par Dominic James.
    Le film «Le divan du monde» a été réalisé en 2011 par Dominic Desjardins.
    Le film «En terrains connus» a été réalisé en 2011 par Stéphane Lafleur.
    Le film «La fille de Montréal» a été réalisé en 2011 par Jeanne Crépeau.
    Le film «French Immersion» a été réalisé en 2011 par Kevin Tierney.
    Le film «French Kiss» a été réalisé en 2011 par Sylvain Archambault.
    Le film «Frisson des collines» a été réalisé en 2011 par Richard Roy.
    Le film «Funkytown» a été réalisé en 2011 par Daniel Roby.
    Le film «Gerry» a été réalisé en 2011 par Alain DesRochers.
    Le film «Good Neighbours» a été réalisé en 2011 par Jacob Tierney.
    Le film «The High Cost of Living» a été réalisé en 2011 par Deborah Chow.
    Le film «Jaloux» a été réalisé en 2011 par Patrick Demers.
    Le film «Jo pour Jonathan» a été réalisé en 2011 par Maxime Giroux.
    Le film «The Kate Logan Affair» a été réalisé en 2011 par Noël Mitrani.
    Le film «Laurentie» a été réalisé en 2011 par Mathieu Denis, Simon Lavoie.
    Le film «Marécages» a été réalisé en 2011 par Guy Édoin.
    Le film «Monsieur Lazhar» a été réalisé en 2011 par Philippe Falardeau.
    Le film «Nuit #1» a été réalisé en 2011 par Anne Émond.
    Le film «Pour l'amour de Dieu» a été réalisé en 2011 par Micheline Lanctôt.
    Le film «La run» a été réalisé en 2011 par Demian Fuica.
    Le film «Le sens de l'humour» a été réalisé en 2011 par Émile Gaudreault.
    Le film «Shadowboxing» a été réalisé en 2011 par Jesse Klein.
    Le film «Starbuck» a été réalisé en 2011 par Ken Scott.
    Le film «Sur le rythme» a été réalisé en 2011 par Charles-Olivier Michaud.
    Le film «Une vie qui commence» a été réalisé en 2011 par Michel Monty.
    Le film «Le vendeur» a été réalisé en 2011 par Sébastien Pilote.
    Le film «La vérité» a été réalisé en 2011 par Marc Bisaillon.
    Le film «The Year Dolly Parton Was My Mom» a été réalisé en 2011 par Tara Johns.
    Le film «L'affaire Dumont» a été réalisé en 2012 par Podz.
    Le film «Après la neige» a été réalisé en 2012 par Paul Barbeau.
    Le film «Avant que mon cœur bascule» a été réalisé en 2012 par Sébastien Rose.
    Le film «Bestiaire» a été réalisé en 2012 par Denis Côté.
    Le film «Camion» a été réalisé en 2012 par Rafaël Ouellet.
    Le film «Columbarium» a été réalisé en 2012 par Steve Kerr.
    Le film «L'empire Bo$$é» a été réalisé en 2012 par Claude Desrosiers.
    Le film «Ésimésac» a été réalisé en 2012 par Luc Picard.
    Le film «The Girl in the White Coat» a été réalisé en 2012 par Darrell Wasyk.
    Le film «The Hat Goes Wild» a été réalisé en 2012 par Guy Sprung.
    Le film «Hors les murs» a été réalisé en 2012 par David Lambert.
    Le film «Inch'Allah» a été réalisé en 2012 par Anaïs Barbeau-Lavalette.
    Le film «L'incrédule» a été réalisé en 2012 par Federico Hidalgo.
    Le film «J'espère que tu vas bien» a été réalisé en 2012 par David La Haye.
    Le film «Karakara» a été réalisé en 2012 par Claude Gagnon.
    Le film «Laurence Anyways» a été réalisé en 2012 par Xavier Dolan.
    Le film «Liverpool» a été réalisé en 2012 par Manon Briand.
    Le film «Mars et Avril» a été réalisé en 2012 par Martin Villeneuve.
    Le film «Mesnak» a été réalisé en 2012 par Yves Sioui Durand.
    Le film «La mise à l'aveugle» a été réalisé en 2012 par Simon Galiero.
    Le film «Omertà» a été réalisé en 2012 par Luc Dionne.
    Le film «Les pee-wee 3D» a été réalisé en 2012 par Éric Tessier.
    Le film «La peur de l'eau» a été réalisé en 2012 par Gabriel Pelletier.
    Le film «Rebelle» a été réalisé en 2012 par Kim Nguyen.
    Le film «Roméo Onze» a été réalisé en 2012 par Ivan Grbovic.
    Le film «Thanatomorphose» a été réalisé en 2012 par Éric Falardeau.
    Le film «Le torrent» a été réalisé en 2012 par Simon Lavoie.
    Le film «Tout ce que tu possèdes» a été réalisé en 2012 par Bernard Émond.
    Le film «Une bouteille dans la mer de Gaza» a été réalisé en 2012 par Thierry Binisti.
    Le film «La vallée des larmes» a été réalisé en 2012 par Maryanne Zéhil.
    Le film «1er amour» a été réalisé en 2013 par Guillaume Sylvestre.
    Le film «Les 4 soldats» a été réalisé en 2013 par Robert Morin.
    Le film «Amsterdam» a été réalisé en 2013 par Stefan Miljevic.
    Le film «L'autre maison» a été réalisé en 2013 par Mathieu Roy.
    Le film «Catimini» a été réalisé en 2013 par Nathalie Saint-Pierre.
    Le film «Chasse au Godard d'Abbittibbi» a été réalisé en 2013 par Éric Morin.
    Le film «La cicatrice» a été réalisé en 2013 par Jimmy Larouche.
    Le film «Cyanure» a été réalisé en 2013 par Séverine Cornamusaz.
    Le film «Le démantèlement» a été réalisé en 2013 par Sébastien Pilote.
    Le film «Diego Star» a été réalisé en 2013 par Frédérick Pelletier.
    Le film «Émilie» a été réalisé en 2013 par Guillaume Lonergan.
    Le film «Finissant(e)s» a été réalisé en 2013 par Rafaël Ouellet.
    Le film «Gabrielle» a été réalisé en 2013 par Louise Archambault.
    Le film «The Good Lie» a été réalisé en 2013 par Shawn Linden.
    Le film «Home Again» a été réalisé en 2013 par Sudz Sutherland.
    Le film «Hot Dog» a été réalisé en 2013 par Marc-André Lavoie.
    Le film «Il était une fois les Boys» a été réalisé en 2013 par Richard Goudreau.
    Le film «Jappeloup» a été réalisé en 2013 par Christian Duguay.
    Le film «Lac Mystère» a été réalisé en 2013 par Érik Canuel.
    Le film «La légende de Sarila» a été réalisé en 2013 par Nancy Florence Savard.
    Le film «Louis Cyr» a été réalisé en 2013 par Daniel Roby.
    Le film «La maison du pêcheur» a été réalisé en 2013 par Alain Chartrand.
    Le film «Les manèges humains» a été réalisé en 2013 par Martin Laroche.
    Le film «Memories Corner» a été réalisé en 2013 par Audrey Fouché.
    Le film «Le météore» a été réalisé en 2013 par François Delisle.
    Le film «Misogyny/Misandry» a été réalisé en 2013 par Erik Anderson.
    Le film «Moroccan Gigolos» a été réalisé en 2013 par Ismaël Saidi.
    Le film «Ressac» a été réalisé en 2013 par Pascale Ferland.
    Le film «Roche papier ciseaux» a été réalisé en 2013 par Yan Lanouette Turgeon.
    Le film «Rouge sang» a été réalisé en 2013 par Martin Doepner.
    Le film «Sarah préfère la course» a été réalisé en 2013 par Chloé Robichaud.
    Le film «Triptyque» a été réalisé en 2013 par Robert Lepage.
    Le film «Une jeune fille» a été réalisé en 2013 par Catherine Martin.
    Le film «Vic+Flo ont vu un ours» a été réalisé en 2013 par Denis Côté.
    Le film «1987» a été réalisé en 2014 par Ricardo Trogi.
    Le film «2 temps 3 mouvements» a été réalisé en 2014 par Christophe Cousin.
    Le film «3 histoires d'Indiens» a été réalisé en 2014 par Robert Morin.
    Le film «L'ange gardien» a été réalisé en 2014 par Jean-Sébastien Lord.
    Le film «Arwad» a été réalisé en 2014 par Dominique Chila.
    Le film «Bunker» a été réalisé en 2014 par Patrick Boivin.
    Le film «Ceci n'est pas un polar» a été réalisé en 2014 par Patrick Gazé.
    Le film «Le coq de St-Victor» a été réalisé en 2014 par Pierre Greco.
    Le film «Discopathe» a été réalisé en 2014 par Renaud Gauthier.
    Le film «Dr. Cabbie» a été réalisé en 2014 par Jean-François Pouliot.
    Le film «Exil» a été réalisé en 2014 par Charles-Olivier Michaud.
    Le film «La ferme des humains» a été réalisé en 2014 par Onur Karaman.
    Le film «Le fille du Martin» a été réalisé en 2014 par Samuel Thivierge.
    Le film «La gang des hors-la-loi» a été réalisé en 2014 par Jean Beaudry.
    Le film «La garde» a été réalisé en 2014 par Sylvain Archambault.
    Le film «Gerontophilia» a été réalisé en 2014 par Bruce LaBruce.
    Le film «The Grand Seduction» a été réalisé en 2014 par Don McKellar.
    Le film «Henri Henri» a été réalisé en 2014 par Martin Talbot.
    Le film «J'espère que tu vas bien 2» a été réalisé en 2014 par David La Haye.
    Le film «Limoilou» a été réalisé en 2014 par Edgar Fritz.
    Le film «Love Projet» a été réalisé en 2014 par Carole Laure.
    Le film «Maïna» a été réalisé en 2014 par Michel Poulette.
    Le film «Les maîtres du suspense» a été réalisé en 2014 par Stéphane Lapointe.
    Le film «Meetings with a Young Poet» a été réalisé en 2014 par Rudy Barichello.
    Le film «Miraculum» a été réalisé en 2014 par Podz.
    Le film «Mommy» a été réalisé en 2014 par Xavier Dolan.
    Le film «My Guys» a été réalisé en 2014 par Joseph Antaki.
    Le film «La petite reine» a été réalisé en 2014 par Alexis Durand-Brault.
    Le film «Pour que plus jamais» a été réalisé en 2014 par Marie Ange Barbancourt.
    Le film «Qu'est-ce qu'on fait ici?» a été réalisé en 2014 par Julie Hivon.
    Le film «Que ta joie demeure» a été réalisé en 2014 par Denis Côté.
    Le film «Rédemption» a été réalisé en 2014 par Joël Gauthier.
    Le film «Le règne de la beauté» a été réalisé en 2014 par Denys Arcand.
    Le film «Rhymes for Young Ghouls» a été réalisé en 2014 par Jeff Barnaby.
    Le film «Tom à la ferme» a été réalisé en 2014 par Xavier Dolan.
    Le film «Tu dors Nicole» a été réalisé en 2014 par Stéphane Lafleur.
    Le film «Un parallèle plus tard» a été réalisé en 2014 par Sébastien Landry.
    Le film «Une vie pour deux» a été réalisé en 2014 par Luc Bourdon.
    Le film «Uvanga» a été réalisé en 2014 par Marie-Hélène Cousineau, Madeline Piujuq Ivalu.
    Le film «La voix de l'ombre» a été réalisé en 2014 par Annie Molin Vasseur.
    Le film «Le vrai du faux» a été réalisé en 2014 par Émile Gaudreault.
    Le film «Whitewash» a été réalisé en 2014 par Emanuel Hoss-Desmarais.
    Le film «L'amour au temps de la guerre civile» a été réalisé en 2015 par Rodrigue Jean.
    Le film «Anna» a été réalisé en 2015 par Charles-Olivier Michaud.
    Le film «Antoine et Marie» a été réalisé en 2015 par Jimmy Larouche.
    Le film «Aurélie Laflamme - Les pieds sur terre» a été réalisé en 2015 par Nicolas Monette.
    Le film «Autrui» a été réalisé en 2015 par Micheline Lanctôt.
    Le film «Le bruit des arbres» a été réalisé en 2015 par François Péloquin.
    Le film «Ce qu'il ne faut pas dire» a été réalisé en 2015 par Marquise Lepage.
    Le film «Chorus» a été réalisé en 2015 par François Delisle.
    Le film «Le cœur de madame Sabali» a été réalisé en 2015 par Ryan McKenna.
    Le film «Corbo» a été réalisé en 2015 par Mathieu Denis.
    Le film «Les démons» a été réalisé en 2015 par Philippe Lesage.
    Le film «Le dep» a été réalisé en 2015 par Sonia Bonspille Boileau.
    Le film «Ego Trip» a été réalisé en 2015 par Benoit Pelletier.
    Le film «Elephant Song» a été réalisé en 2015 par Charles Binamé.
    Le film «L'énergie sombre» a été réalisé en 2015 par Leonardo Fuica.
    Le film «Les Êtres chers» a été réalisé en 2015 par Anne Émond.
    Le film «Félix et Meira» a été réalisé en 2015 par Maxime Giroux.
    Le film «The Forbidden Room» a été réalisé en 2015 par Guy Maddin, Evan Johnson.
    Le film «Le garagiste» a été réalisé en 2015 par Renée Beaulieu.
    Le film «La Guerre des tuques 3D» a été réalisé en 2015 par François Brisson, Jean-François Pouliot.
    Le film «Guibord s'en va-t-en guerre» a été réalisé en 2015 par Philippe Falardeau.
    Le film «Gurov & Anna» a été réalisé en 2015 par Rafaël Ouellet.
    Le film «Je suis à toi» a été réalisé en 2015 par David Lambert.
    Le film «Le journal d'un vieil homme» a été réalisé en 2015 par Bernard Émond.
    Le film «Les Loups» a été réalisé en 2015 par Sophie Deraspe.
    Le film «Le Mirage» a été réalisé en 2015 par Ricardo Trogi.
    Le film «Noir (NWA)» a été réalisé en 2015 par Yves-Christian Fournier.
    Le film «Nouvelles, nouvelles» a été réalisé en 2015 par Olivier Godin.
    Le film «La Passion d'Augustine» a été réalisé en 2015 par Léa Pool.
    Le film «Paul à Québec» a été réalisé en 2015 par François Bouvier.
    Le film «Preggoland» a été réalisé en 2015 par Jacob Tierney.
    Le film «Le scaphandrier» a été réalisé en 2015 par Alain Vézina.
    Le film «Scratch» a été réalisé en 2015 par Sébastien Godron.
    Le film «Standstill» a été réalisé en 2015 par Majdi El-Omari.
    Le film «Turbo Kid» a été réalisé en 2015 par François Simard, Anouk et Yoann-Karl Whissell.
    Le film «Ville-Marie» a été réalisé en 2015 par Guy Édoin.
    Le film «01:54» a été réalisé en 2016 par Yan England.
    Le film «Les 3 p'tits cochons 2» a été réalisé en 2016 par Jean-François Pouliot.
    Le film «9, le film» a été réalisé en 2016 par Un collectif de réalisateurs et de réalisatrices.
    Le film «Avant les rues» a été réalisé en 2016 par Chloé Leriche.
    Le film «Boris sans Béatrice» a été réalisé en 2016 par Denis Côté.
    Le film «Ceux qui font les révolutions à moitié n'ont fait que se creuser un tombeau» a été réalisé en 2016 par Simon Lavoie, Mathieu Denis.
    Le film «La Chasse au collet» a été réalisé en 2016 par Steve Kerr.
    Le film «Chasse-Galerie : La légende» a été réalisé en 2016 par Jean-Philippe Duval.
    Le film «Déserts» a été réalisé en 2016 par Charles-André Coderre, Yann-Manuel Hernandez.
    Le film «D’encre et de sang» a été réalisé en 2016 par Alexis Fortier Gauthier, Maxim Rheault, Francis Fortin.
    Le film «Early Winter» a été réalisé en 2016 par Michael Rowe.
    Le film «Ecartée» a été réalisé en 2016 par Lawrence Côté-Collins.
    Le film «Embrasse-moi comme tu m'aimes» a été réalisé en 2016 par André Forcier.
    Le film «Endorphine» a été réalisé en 2016 par André Turpin.
    Le film «Feuilles mortes» a été réalisé en 2016 par Thierry Bouffard, Steve Landry, Édouard A. Tremblay.
    Le film «Generation Wolf» a été réalisé en 2016 par Christian de la Cortina.
    Le film «Harry : Portrait d’un détective privé» a été réalisé en 2016 par Maxime Desruisseaux.
    Le film «Juste la fin du monde» a été réalisé en 2016 par Xavier Dolan.
    Le film «King Dave» a été réalisé en 2016 par Podz.
    Le film «Là où Attila passe» a été réalisé en 2016 par Onur Karaman.
    Le film «Les Mauvaises herbes» a été réalisé en 2016 par Louis Bélanger.
    Le film «Mes ennemis» a été réalisé en 2016 par Stéphane Géhami.
    Le film «Mon ami Dino» a été réalisé en 2016 par Jimmy Larouche.
    Le film «Montréal la blanche» a été réalisé en 2016 par Bachir Bensadekk.
    Le film «Nitro Rush» a été réalisé en 2016 par Alain Desrochers.
    Le film «L'Origine des espèces» a été réalisé en 2016 par Dominic Goyer.
    Le film «Le Pacte des anges» a été réalisé en 2016 par Richard Angers.
    Le film «Pays» a été réalisé en 2016 par Chloé Robichaud.
    Le film «Prank» a été réalisé en 2016 par Vincent Biron.
    Le film «Le Rang du lion» a été réalisé en 2016 par Stéphane Beaudoin.
    Le film «The Saver» a été réalisé en 2016 par Wiebke von Carolsfeld.
    Le film «Toujours encore» a été réalisé en 2016 par Jean-François Boisvenue.
    Le film «Two Lovers and a Bear» a été réalisé en 2016 par Kim Nguyen.
    Le film «Un paradis pour tous» a été réalisé en 2016 par Robert Morin.
    Le film «Vortex» a été réalisé en 2016 par Jephté Bastien.
    Le film «Votez Bougon» a été réalisé en 2016 par Jean-François Pouliot.
    Le film «Les Affamés» a été réalisé en 2017 par Robin Aubert.
    Le film «Après coup» a été réalisé en 2017 par Noël Mitrani.
    Le film «Les arts de la parole» a été réalisé en 2017 par Olivier Godin.
    Le film «L'Autre côté de novembre» a été réalisé en 2017 par Maryanne Zéhil.
    Le film «Ballerina» a été réalisé en 2017 par Éric Warrin, Éric Summer.
    Le film «Bon Cop Bad Cop 2» a été réalisé en 2017 par Alain Desrochers.
    Le film «Boost» a été réalisé en 2017 par Darren Curtis.
    Le film «Ça sent la coupe» a été réalisé en 2017 par Patrice Sauvé.
    Le film «C'est le coeur qui meurt en dernier» a été réalisé en 2017 par Alexis Durand-Brault.
    Le film «Le Cyclotron» a été réalisé en 2017 par Olivier Asselin.
    Le film «De père en flic 2» a été réalisé en 2017 par Émile Gaudreault.
    Le film «Et au pire, on se mariera» a été réalisé en 2017 par Léa Pool.
    Le film «Innocent» a été réalisé en 2017 par Marc-André Lavoie.
    Le film «Iqaluit» a été réalisé en 2017 par Benoît Pilon.
    Le film «Junior majeur» a été réalisé en 2017 par Éric Tessier.
    Le film «Maudite Poutine» a été réalisé en 2017 par Karl Lemieux.
    Le film «Mes nuits feront écho» a été réalisé en 2017 par Sophie Goyette.
    Le film «Nelly» a été réalisé en 2017 par Anne Émond.
    Le film «Nous sommes les autres» a été réalisé en 2017 par Jean-François Asselin.
    Le film «La Petite Fille qui aimait trop les allumettes» a été réalisé en 2017 par Simon Lavoie.
    Le film «Pieds nus dans l'aube» a été réalisé en 2017 par Francis Leclerc.
    Le film «Le problème d'infiltration» a été réalisé en 2017 par Robert Morin.
    Le film «Les Rois mongols» a été réalisé en 2017 par Luc Picard.
    Le film «Tadoussac» a été réalisé en 2017 par Martin Laroche.
    Le film «Ta peau si lisse» a été réalisé en 2017 par Denis Côté.
    Le film «Le trip à trois» a été réalisé en 2017 par Nicolas Monette.
    Le film «Tuktuq» a été réalisé en 2017 par Robin Aubert.
    Le film «1991» a été réalisé en 2018 par Ricardo Trogi.
    Le film «À tous ceux qui ne me lisent pas» a été réalisé en 2018 par Yan Giroux.
    Le film «Ailleurs» a été réalisé en 2018 par Samuel Matteau.
    Le film «Allure» a été réalisé en 2018 par Jason Sachez, Carlos Sanchez.
    Le film «All You Can Eat Buddha» a été réalisé en 2018 par Ian Lagarde.
    Le film «L’amour» a été réalisé en 2018 par Marc Bisaillon.
    Le film «Another Kind of Wedding» a été réalisé en 2018 par Pat Kiely.
    Le film «Birthmarked» a été réalisé en 2018 par Emanuel Hoss-Desmarais.
    Le film «La Bolduc» a été réalisé en 2018 par François Bouvier.
    Le film «Burn Out ou la servitude volontaire» a été réalisé en 2018 par Michel Jetté.
    Le film «Charlotte a du fun» a été réalisé en 2018 par Sophie Lorain.
    Le film «Chien de garde» a été réalisé en 2018 par Sophie Dupuis.
    Le film «La Chute de l'empire américain» a été réalisé en 2018 par Denys Arcand.
    Le film «La chute de Sparte» a été réalisé en 2018 par Tristan Dubois.
    Le film «Claire l’Hiver» a été réalisé en 2018 par Sophie Bédard-Marcotte.
    Le film «La course des Tuques» a été réalisé en 2018 par François Brisson, Benoît Godbout.
    Le film «La Disparition des lucioles» a été réalisé en 2018 par Sébastien Pilote.
    Le film «Dans la brume» a été réalisé en 2018 par Daniel Roby.
    Le film «Eye on Juliet» a été réalisé en 2018 par Kim Nguyen.
    Le film «Les faux tatouages» a été réalisé en 2018 par Pascal Plante.
    Le film «Hochelaga, terre des âmes» a été réalisé en 2018 par François Girard.
    Le film «Identités» a été réalisé en 2018 par Samuel Thivierge.
    Le film «Isla Blanca» a été réalisé en 2018 par Jeanne Leblanc.
    Le film «Lemonade» a été réalisé en 2018 par Ioana Uricaru.
    Le film «Napoléon en apparte» a été réalisé en 2018 par Jeff Denis.
    Le film «Nelly et Simon : Mission Yéti» a été réalisé en 2018 par Pierre Greco, Nancy Florence Savard.
    Le film «Le nid» a été réalisé en 2018 par David Paradis.
    Le film «Origami» a été réalisé en 2018 par Patrick Demers.
    Le film «Oscillations» a été réalisé en 2018 par Ky Nam Leduc.
    Le film «Papa est devenu un lutin» a été réalisé en 2018 par Dominique Adams.
    Le film «Pour vivre ici» a été réalisé en 2018 par Bernard Émond.
    Le film «Quand l'amour se creuse un trou» a été réalisé en 2018 par Ara Ball.
    Le film «Les salopes ou le sucre naturel de la peau» a été réalisé en 2018 par Renée Beaulieu.
    Le film «Ma vie avec John F. Donovan» a été réalisé en 2018 par Xavier Dolan.
    Le film «Kristina Wagenbauer» a été réalisé en 2018 par Carla Turcotte, Natalia Dontcheva, Emmanuel Schwartz.
    Le film «Les scènes fortuites» a été réalisé en 2018 par Guillaume Lambert.
    Le film «Touched» a été réalisé en 2018 par Karl R. Hearne.
    Le film «Un printemps d’ailleurs» a été réalisé en 2018 par Xiaodan He.
    Le film «Vénus» a été réalisé en 2018 par Eisha Marjara.
    Le film «Wolfe» a été réalisé en 2018 par Francis Bordeleau.
    Le film «Yolanda» a été réalisé en 2018 par Jeannine Gagné.
    Le film «Une colonie» a été réalisé en 2019 par Geneviève Dulude-De Celles.
    Le film «La grande noirceur» a été réalisé en 2019 par Maxime Giroux.
    Le film «Malek» a été réalisé en 2019 par Guy Édoin.
    Le film «Impetus» a été réalisé en 2019 par Jennifer Alleyn.
    Le film «Mon ami Walid» a été réalisé en 2019 par Adib Alkhalidey.
    Le film «Antigone» a été réalisé en 2019 par Sophie Deraspe.
    Le film «Attaque d'un autre monde» a été réalisé en 2019 par Adrien Lorion.
    Le film «Jeune Juliette» a été réalisé en 2019 par Anne Émond.
    Le film «Journal de Bolivie - 50 ans après la mort du Che» a été réalisé en 2019 par Jules Falardeau et Jean-Philippe Nadeau Marcoux.
    Le film «Kuessipan» a été réalisé en 2019 par Myriam Verreault.
    Le film «Matthias et Maxime» a été réalisé en 2019 par Xavier Dolan.
    Le film «Répertoire des villes disparues» a été réalisé en 2019 par Denis Côté.
    Le film «Sur les traces de Bochart» a été réalisé en 2019 par Pierre Saint-Yves et Yannick Gendron.
    Le film «Menteur» a été réalisé en 2019 par Émile Gaudreault.
    Le film «Apapacho: une caresse pour l'âme» a été réalisé en 2019 par Marquise Lepage.
    Le film «La femme de mon frère» a été réalisé en 2019 par Monia Chokri.
    Le film «Les Fleurs oubliées» a été réalisé en 2019 par André Forcier.
    Le film «Jouliks» a été réalisé en 2019 par Mariloup Wolfe.
    Le film «Vivre à 100 milles à l'heure» a été réalisé en 2019 par Louis Bélanger.
    Le film «Nous sommes Gold» a été réalisé en 2019 par Éric Morin.
    Le film «Wilcox» a été réalisé en 2019 par Denis Côté.
    Le film «Je finirai en prison» a été réalisé en 2019 par Alexandre Dostie.
    Le film «Cassy» a été réalisé en 2019 par Noël Mitrani.
    Le film «Les Barbares de La Malbaie» a été réalisé en 2019 par Vincent Biron.
    Le film «Avant qu'on explose» a été réalisé en 2019 par Rémi St-Michel.
    Le film «Le rire» a été réalisé en 2020 par Martin Laroche.
    Le film «Mafia Inc.» a été réalisé en 2020 par Podz.
    Le film «14 jours, 12 nuits» a été réalisé en 2020 par Jean-Philippe Duval.
    Le film «Les nôtres» a été réalisé en 2020 par Jeanne Leblanc.
    Le film «Tu te souviendras de moi» a été réalisé en 2020 par Éric Tessier.
    Le film «Le club Vinland» a été réalisé en 2020 par Benoît Pilon.
    Le film «Target Number One» a été réalisé en 2020 par Daniel Roby.
    Le film «Le guide de la famille parfaite» a été réalisé en 2020 par Ricardo Trogi.
    Le film «Gallant : Confessions d’un tueur à gages» a été réalisé en 2020 par Luc Picard.
    Le film «La déesse des mouches à feu» a été réalisé en 2020 par Anaïs Barbeau-Lavalette.


<h5 style="background: beige; text-align: center; padding: 10px;">La notion de compteur</h5>

Quand je fais une boucle dans une liste, j'ai pris l'habitude de créer un compteur pour savoir où mon code est rendu dans son traitement.

On peut mettre un compteur dans n'importe quelle variable. Ci-dessous, j'utilise ```n```.

Il s'agit de créer ```n``` avant que la boucle ne démarre et de lui donner la valeur de 0.

Puis, à chaque itération de la boucle, il faut lui ajouter 1.

On peut le faire ainsi: ```n = n + 1```.

Mais j'en profite pour vous montrer un opérateur unique au monde de l'informatique: ```+=```. Ce «plus-égal» signifie «remplace la valeur de ma variable par sa valeur actuelle plus le nombre qui suit».

Voici quelle forme le compteur peut prendre dans le code qu'on vient tout juste d'écrire, ci-dessus.


```python
n = 0
for film in films:
    n += 1
    print("Le film «{}» (le {}e de la liste) a été réalisé en {} par {}.".format(film[0],n,film[2],film[1]))
```

    Le film «À la croisée des chemins» (le 1e de la liste) a été réalisé en 1943 par Paul Guèvremont.
    Le film «Le Père Chopin» (le 2e de la liste) a été réalisé en 1945 par Fedor Ozep.
    Le film «La Forteresse» (le 3e de la liste) a été réalisé en 1947 par Fedor Ozep.
    Le film «Whispering City» (le 4e de la liste) a été réalisé en 1947 par Fedor Ozep.
    Le film «On ne triche pas avec la vie» (le 5e de la liste) a été réalisé en 1949 par René Delacroix et Paul Vandenberghe.
    Le film «Le Curé de village» (le 6e de la liste) a été réalisé en 1949 par Paul Gury.
    Le film «Le Gros Bill» (le 7e de la liste) a été réalisé en 1949 par Jean-Yves Bigras et René Delacroix.
    Le film «Un homme et son péché» (le 8e de la liste) a été réalisé en 1949 par Paul Gury.
    Le film «Les Lumières de ma ville» (le 9e de la liste) a été réalisé en 1950 par Jean-Yves Bigras.
    Le film «L'Inconnue de Montréal» (le 10e de la liste) a été réalisé en 1950 par Jean Devaivre.
    Le film «Séraphin» (le 11e de la liste) a été réalisé en 1950 par Paul Gury.
    Le film «Étienne Brûlé gibier de potence» (le 12e de la liste) a été réalisé en 1952 par Melburn E. Turner.
    Le film «La Petite Aurore, l'enfant martyre» (le 13e de la liste) a été réalisé en 1952 par Jean-Yves Bigras.
    Le film «Le Rossignol et les Cloches» (le 14e de la liste) a été réalisé en 1952 par René Delacroix.
    Le film «Cœur de maman» (le 15e de la liste) a été réalisé en 1953 par René Delacroix.
    Le film «Tit-Coq» (le 16e de la liste) a été réalisé en 1953 par Gratien Gélinas et René Delacroix.
    Le film «L'Esprit du mal» (le 17e de la liste) a été réalisé en 1954 par Jean-Yves Bigras.
    Le film «Le Village enchanté» (le 18e de la liste) a été réalisé en 1956 par Marcel et Réal Racicot.
    Le film «Le Survenant» (le 19e de la liste) a été réalisé en 1957 par Un collectif de réalisateurs.
    Le film «Les Mains nettes» (le 20e de la liste) a été réalisé en 1958 par Claude Jutra.
    Le film «Les Brûlés» (le 21e de la liste) a été réalisé en 1959 par Bernard Devlin.
    Le film «Accade in Canada» (le 22e de la liste) a été réalisé en 1962 par Luigi Petrucci.
    Le film «Seul ou avec d'autres» (le 23e de la liste) a été réalisé en 1962 par Denys Arcand avec Denis Héroux et Stéphane Venne.
    Le film «À tout prendre» (le 24e de la liste) a été réalisé en 1963 par Claude Jutra.
    Le film «Amanita Pestilens» (le 25e de la liste) a été réalisé en 1963 par René Bonnière.
    Le film «Drylanders (Un autre pays)» (le 26e de la liste) a été réalisé en 1963 par Donald Haldane.
    Le film «Le Chat dans le sac» (le 27e de la liste) a été réalisé en 1964 par Gilles Groulx.
    Le film «La Fleur de l'âge, ou Les adolescentes» (le 28e de la liste) a été réalisé en 1964 par Un collectif de réalisateurs.
    Le film «Jusqu'au cou» (le 29e de la liste) a été réalisé en 1964 par Denis Héroux.
    Le film «Nobody Waved Good-Bye» (le 30e de la liste) a été réalisé en 1964 par Don Owen.
    Le film «La Terre à boire» (le 31e de la liste) a été réalisé en 1964 par Jean-Paul Bernier.
    Le film «Trouble-fête» (le 32e de la liste) a été réalisé en 1964 par Pierre Patry.
    Le film «Astataïon ou le Festin des morts» (le 33e de la liste) a été réalisé en 1965 par Fernand Dansereau.
    Le film «Caïn» (le 34e de la liste) a été réalisé en 1965 par Pierre Patry.
    Le film «La Corde au cou» (le 35e de la liste) a été réalisé en 1965 par Pierre Patry.
    Le film «Le Coup de grâce» (le 36e de la liste) a été réalisé en 1965 par Jean Cayrol et Claude Durand.
    Le film «Pas de vacances pour les idoles» (le 37e de la liste) a été réalisé en 1965 par Denis Héroux.
    Le film «Le Révolutionnaire» (le 38e de la liste) a été réalisé en 1965 par Jean Pierre Lefebvre.
    Le film «La Vie heureuse de Léopold Z.» (le 39e de la liste) a été réalisé en 1965 par Gilles Carle.
    Le film «Carnaval en chute libre» (le 40e de la liste) a été réalisé en 1966 par Guy Bouchard.
    Le film «La Douzième heure» (le 41e de la liste) a été réalisé en 1966 par Jean Martimbeau.
    Le film «Footsteps in the Snow (Des pas sur la neige)» (le 42e de la liste) a été réalisé en 1966 par Martin Green.
    Le film «Yul 871» (le 43e de la liste) a été réalisé en 1966 par Jacques Godbout.
    Le film «Entre la mer et l'eau douce» (le 44e de la liste) a été réalisé en 1967 par Michel Brault.
    Le film «Il ne faut pas mourir pour ça» (le 45e de la liste) a été réalisé en 1967 par Jean Pierre Lefebvre.
    Le film «Manette : la folle et les dieux de carton» (le 46e de la liste) a été réalisé en 1967 par Camil Adam.
    Le film «C'est pas la faute à Jacques Cartier» (le 47e de la liste) a été réalisé en 1968 par Georges Dufaux et Clément Perron.
    Le film «Façade» (le 48e de la liste) a été réalisé en 1968 par Larry Kent.
    Le film «Isabel» (le 49e de la liste) a été réalisé en 1968 par Paul Almond.
    Le film «Las Joyas del diablo (Le diable aime les bijoux)» (le 50e de la liste) a été réalisé en 1968 par José Maria Elorrieta.
    Le film «Kid Sentiment» (le 51e de la liste) a été réalisé en 1968 par Jacques Godbout.
    Le film «Patricia et Jean-Baptiste» (le 52e de la liste) a été réalisé en 1968 par Jean Pierre Lefebvre.
    Le film «Poussière sur la ville» (le 53e de la liste) a été réalisé en 1968 par Arthur Lamothe.
    Le film «T-bone Steak dans... les mangeuses d'hommes» (le 54e de la liste) a été réalisé en 1968 par Gilles Marchand et Hugues Tremblay.
    Le film «Le Viol d'une jeune fille douce» (le 55e de la liste) a été réalisé en 1968 par Gilles Carle.
    Le film «La Chambre blanche» (le 56e de la liste) a été réalisé en 1969 par Jean Pierre Lefebvre.
    Le film «Danger pour la société» (le 57e de la liste) a été réalisé en 1969 par Jean Martimbeau.
    Le film «Délivrez-nous du mal» (le 58e de la liste) a été réalisé en 1969 par Jean-Claude Lord.
    Le film «Don't Let the Angels Fall (Seuls les enfants étaient présents)» (le 59e de la liste) a été réalisé en 1969 par George Kaczender.
    Le film «Le Grand Rock» (le 60e de la liste) a été réalisé en 1969 par Raymond Garceau.
    Le film «Jusqu'au cœur» (le 61e de la liste) a été réalisé en 1969 par Jean Pierre Lefebvre.
    Le film «Mon amie Pierrette» (le 62e de la liste) a été réalisé en 1969 par Jean Pierre Lefebvre.
    Le film «Où êtes-vous donc ?» (le 63e de la liste) a été réalisé en 1969 par Gilles Groulx.
    Le film «Le Soleil des autres» (le 64e de la liste) a été réalisé en 1969 par Jean Faucher.
    Le film «Valérie» (le 65e de la liste) a été réalisé en 1969 par Denis Héroux.
    Le film «Act of the Heart» (le 66e de la liste) a été réalisé en 1970 par Paul Almond.
    Le film «Ainsi soient-ils» (le 67e de la liste) a été réalisé en 1970 par Yvan Patry.
    Le film «L'Amour humain» (le 68e de la liste) a été réalisé en 1970 par Denis Héroux.
    Le film «Deux femmes en or» (le 69e de la liste) a été réalisé en 1970 par Claude Fournier.
    Le film «Entre tu et vous» (le 70e de la liste) a été réalisé en 1970 par Gilles Groulx.
    Le film «L'Initiation» (le 71e de la liste) a été réalisé en 1970 par Denis Héroux.
    Le film «Love in a 4 Letter World» (le 72e de la liste) a été réalisé en 1970 par John Sone.
    Le film «On est loin du soleil» (le 73e de la liste) a été réalisé en 1970 par Jacques Leduc.
    Le film «Pile ou face» (le 74e de la liste) a été réalisé en 1970 par Roger Fournier.
    Le film «Q-bec my love» (le 75e de la liste) a été réalisé en 1970 par Jean Pierre Lefebvre.
    Le film «Quand hurlent les loups» (le 76e de la liste) a été réalisé en 1970 par André Lafferrere.
    Le film «Red» (le 77e de la liste) a été réalisé en 1970 par Gilles Carle.
    Le film «St-Denis dans le temps...» (le 78e de la liste) a été réalisé en 1970 par Marcel Carrière.
    Le film «Vive la France» (le 79e de la liste) a été réalisé en 1970 par Raymond Garceau.
    Le film «Wow» (le 80e de la liste) a été réalisé en 1970 par Claude Jutra.
    Le film «7 fois… (par jour)» (le 81e de la liste) a été réalisé en 1971 par Denis Héroux.
    Le film «Après-ski» (le 82e de la liste) a été réalisé en 1971 par Roger Cardinal.
    Le film «Les Chats bottés» (le 83e de la liste) a été réalisé en 1971 par Claude Fournier.
    Le film «Corps et âme» (le 84e de la liste) a été réalisé en 1971 par Michel Audy.
    Le film «L'Explosion» (le 85e de la liste) a été réalisé en 1971 par Marc Simenon.
    Le film «Finalement...» (le 86e de la liste) a été réalisé en 1971 par Richard Martin.
    Le film «Fleur bleue» (le 87e de la liste) a été réalisé en 1971 par Larry Kent.
    Le film «Le Grand film ordinaire» (le 88e de la liste) a été réalisé en 1971 par Roger Frappier.
    Le film «IXE-13» (le 89e de la liste) a été réalisé en 1971 par Jacques Godbout.
    Le film «Jean-François Xavier de...» (le 90e de la liste) a été réalisé en 1971 par Michel Audy.
    Le film «Les Mâles» (le 91e de la liste) a été réalisé en 1971 par Gilles Carle.
    Le film «Le Martien de Noël» (le 92e de la liste) a été réalisé en 1971 par Bernard Gosselin.
    Le film «Les Maudits sauvages» (le 93e de la liste) a été réalisé en 1971 par Jean Pierre Lefebvre.
    Le film «Mon enfance à Montréal» (le 94e de la liste) a été réalisé en 1971 par Jean Chabot.
    Le film «Mon œil» (le 95e de la liste) a été réalisé en 1971 par Jean Pierre Lefebvre.
    Le film «Mon oncle Antoine» (le 96e de la liste) a été réalisé en 1971 par Claude Jutra.
    Le film «Question de vie» (le 97e de la liste) a été réalisé en 1971 par André Théberge.
    Le film «Le Retour de l'Immaculée Conception» (le 98e de la liste) a été réalisé en 1971 par André Forcier.
    Le film «Stop» (le 99e de la liste) a été réalisé en 1971 par Jean Beaudin.
    Le film «Tiens-toi bien après les oreilles à Papa...» (le 100e de la liste) a été réalisé en 1971 par Jean Bissonnette.
    Le film «Ty-Peupe» (le 101e de la liste) a été réalisé en 1971 par Fernand Bélanger.
    Le film «L'Apparition» (le 102e de la liste) a été réalisé en 1972 par Roger Cardinal.
    Le film «Les Colombes» (le 103e de la liste) a été réalisé en 1972 par Jean-Claude Lord.
    Le film «Le diable est parmi nous» (le 104e de la liste) a été réalisé en 1972 par Jean Beaudin.
    Le film «Et du fils» (le 105e de la liste) a été réalisé en 1972 par Raymond Garceau.
    Le film «L'exil» (le 106e de la liste) a été réalisé en 1972 par Thomas Vamos.
    Le film «Le Grand Sabordage» (le 107e de la liste) a été réalisé en 1972 par Alain Périsson.
    Le film «Isis au 8» (le 108e de la liste) a été réalisé en 1972 par Alain Chartrand.
    Le film «Journey» (le 109e de la liste) a été réalisé en 1972 par Paul Almond.
    Le film «La Maudite Galette» (le 110e de la liste) a été réalisé en 1972 par Denys Arcand.
    Le film «Montréal blues» (le 111e de la liste) a été réalisé en 1972 par Pascal Gélinas.
    Le film «Le p'tit vient vite» (le 112e de la liste) a été réalisé en 1972 par Louis-Georges Carrier.
    Le film «Pas de jeu sans soleil» (le 113e de la liste) a été réalisé en 1972 par Claude Bérubé.
    Le film «Quelques arpents de neige» (le 114e de la liste) a été réalisé en 1972 par Denis Héroux.
    Le film «Les Smattes» (le 115e de la liste) a été réalisé en 1972 par Jean-Claude Labrecque.
    Le film «Le Temps d'une chasse» (le 116e de la liste) a été réalisé en 1972 par Francis Mankiewicz.
    Le film «La Tête au neutre» (le 117e de la liste) a été réalisé en 1972 par Jean Gagné.
    Le film «Un enfant comme les autres» (le 118e de la liste) a été réalisé en 1972 par Denis Héroux.
    Le film «La Vie rêvée» (le 119e de la liste) a été réalisé en 1972 par Mireille Dansereau.
    Le film «La Vraie nature de Bernadette» (le 120e de la liste) a été réalisé en 1972 par Gilles Carle.
    Le film «Ah ! Si mon moine voulait...» (le 121e de la liste) a été réalisé en 1973 par Claude Pierson.
    Le film «Les Allées de la terre» (le 122e de la liste) a été réalisé en 1973 par André Théberge.
    Le film «La Conquête» (le 123e de la liste) a été réalisé en 1973 par Jean Gagné.
    Le film «Les Corps célestes» (le 124e de la liste) a été réalisé en 1973 par Gilles Carle.
    Le film «Les Dernières fiançailles» (le 125e de la liste) a été réalisé en 1973 par Jean Pierre Lefebvre.
    Le film «Il était une fois dans l'Est» (le 126e de la liste) a été réalisé en 1973 par André Brassard.
    Le film «J'ai mon voyage!» (le 127e de la liste) a été réalisé en 1973 par Denis Héroux.
    Le film «Kamouraska» (le 128e de la liste) a été réalisé en 1973 par Claude Jutra.
    Le film «Keep it in the Family» (le 129e de la liste) a été réalisé en 1973 par Larry Kent.
    Le film «La Maîtresse» (le 130e de la liste) a été réalisé en 1973 par Anton Van de Water.
    Le film «La Mort d'un bûcheron» (le 131e de la liste) a été réalisé en 1973 par Gilles Carle.
    Le film «Mourir pour vivre» (le 132e de la liste) a été réalisé en 1973 par Franco Vigilante.
    Le film «The Neptune Factor: An Undersea Odyssey» (le 133e de la liste) a été réalisé en 1973 par Daniel Petrie.
    Le film «Noël et Juliette» (le 134e de la liste) a été réalisé en 1973 par Michel Bouchard.
    Le film «O.K.... Laliberté» (le 135e de la liste) a été réalisé en 1973 par Marcel Carrière.
    Le film «On n'engraisse pas les cochons à l'eau claire» (le 136e de la liste) a été réalisé en 1973 par Jean Pierre Lefebvre.
    Le film «The Rainbow Boys» (le 137e de la liste) a été réalisé en 1973 par Gerald Potterton.
    Le film «Réjeanne Padovani» (le 138e de la liste) a été réalisé en 1973 par Denys Arcand.
    Le film «Sensations» (le 139e de la liste) a été réalisé en 1973 par Robert Séguin.
    Le film «A Star is Lost!» (le 140e de la liste) a été réalisé en 1973 par John Howe.
    Le film «Taureau» (le 141e de la liste) a été réalisé en 1973 par Clément Perron.
    Le film «Tendresse ordinaire» (le 142e de la liste) a été réalisé en 1973 par Jacques Leduc.
    Le film «Tu brûles... tu brûles...» (le 143e de la liste) a été réalisé en 1973 par Jean-Guy Noël.
    Le film «U-Turn» (le 144e de la liste) a été réalisé en 1973 par George Kaczender.
    Le film «Ultimatum» (le 145e de la liste) a été réalisé en 1973 par Jean Pierre Lefebvre.
    Le film «Y a toujours moyen de moyenner!» (le 146e de la liste) a été réalisé en 1973 par Denis Héroux.
    Le film «Alien Thunder» (le 147e de la liste) a été réalisé en 1974 par Claude Fournier.
    Le film «The Apprenticeship of Duddy Kravitz» (le 148e de la liste) a été réalisé en 1974 par Ted Kotcheff.
    Le film «Au boutt'» (le 149e de la liste) a été réalisé en 1974 par Roger Laliberté.
    Le film «Les Aventures d'une jeune veuve» (le 150e de la liste) a été réalisé en 1974 par Roger Fournier.
    Le film «Bar Salon» (le 151e de la liste) a été réalisé en 1974 par André Forcier.
    Le film «Les Beaux dimanches» (le 152e de la liste) a été réalisé en 1974 par Richard Martin.
    Le film «Bingo» (le 153e de la liste) a été réalisé en 1974 par Jean-Claude Lord.
    Le film «Bulldozer» (le 154e de la liste) a été réalisé en 1974 par Pierre Harel.
    Le film «Les Deux pieds dans la même bottine» (le 155e de la liste) a été réalisé en 1974 par Pierre Rose.
    Le film «La Gammick» (le 156e de la liste) a été réalisé en 1974 par Jacques Godbout.
    Le film «Guitare» (le 157e de la liste) a été réalisé en 1974 par Richard Lavoie.
    Le film «Je t'aime» (le 158e de la liste) a été réalisé en 1974 par Pierre Duceppe.
    Le film «Montreal Main» (le 159e de la liste) a été réalisé en 1974 par Frank Vitale.
    Le film «Ô ou l'invisible enfant» (le 160e de la liste) a été réalisé en 1974 par Raôul Duguay.
    Le film «Les Ordres» (le 161e de la liste) a été réalisé en 1974 par Michel Brault.
    Le film «Le Plumard en folie» (le 162e de la liste) a été réalisé en 1974 par Jacques Lem.
    Le film «La Pomme, la queue... et les pépins!» (le 163e de la liste) a été réalisé en 1974 par Claude Fournier.
    Le film «Running Time» (le 164e de la liste) a été réalisé en 1974 par Mort Ransen.
    Le film «Why Rock the Boat?» (le 165e de la liste) a été réalisé en 1974 par John Howe.
    Le film «Y'a pas d'mal à se faire du bien» (le 166e de la liste) a été réalisé en 1974 par Claude Mulot.
    Le film «L'amour blessé» (le 167e de la liste) a été réalisé en 1975 par Jean Pierre Lefebvre.
    Le film «Candice Candy» (le 168e de la liste) a été réalisé en 1975 par Pierre Unia.
    Le film «Cold Journey» (le 169e de la liste) a été réalisé en 1975 par Martin Defalco.
    Le film «Eliza's Horoscope» (le 170e de la liste) a été réalisé en 1975 par Gordon Sheppard.
    Le film «Gina» (le 171e de la liste) a été réalisé en 1975 par Denys Arcand.
    Le film «The Heatwave Lasted Four Days» (le 172e de la liste) a été réalisé en 1975 par Douglas Jackson.
    Le film «L'île jaune» (le 173e de la liste) a été réalisé en 1975 par Jean Cousineau.
    Le film «Lies My Father Told Me» (le 174e de la liste) a été réalisé en 1975 par Ján Kádar.
    Le film «M'en revenant par les épinettes» (le 175e de la liste) a été réalisé en 1975 par François Brault.
    Le film «Mustang» (le 176e de la liste) a été réalisé en 1975 par Marcel Lefebvre.
    Le film «Partis pour la gloire» (le 177e de la liste) a été réalisé en 1975 par Clément Perron.
    Le film «Pour le meilleur et pour le pire» (le 178e de la liste) a été réalisé en 1975 par Claude Jutra.
    Le film «Pousse mais pousse égal» (le 179e de la liste) a été réalisé en 1975 par Denis Héroux.
    Le film «Shivers» (le 180e de la liste) a été réalisé en 1975 par David Cronenberg.
    Le film «Le temps de l'avant» (le 181e de la liste) a été réalisé en 1975 par Anne Claire Poirier.
    Le film «La tête de Normande St-Onge» (le 182e de la liste) a été réalisé en 1975 par Gilles Carle.
    Le film «Tout feu, tout femme» (le 183e de la liste) a été réalisé en 1975 par Gilles Richer.
    Le film «Une nuit en Amérique» (le 184e de la liste) a été réalisé en 1975 par Jean Chabot.
    Le film «Les vautours» (le 185e de la liste) a été réalisé en 1975 par Jean-Claude Labrecque.
    Le film «L'absence» (le 186e de la liste) a été réalisé en 1976 par Brigitte Sauriol.
    Le film «Beat» (le 187e de la liste) a été réalisé en 1976 par André Blanchard.
    Le film «Born for Hell» (le 188e de la liste) a été réalisé en 1976 par Denis Héroux.
    Le film «Chanson pour Julie» (le 189e de la liste) a été réalisé en 1976 par Jacques Vallée.
    Le film «Contebleu» (le 190e de la liste) a été réalisé en 1976 par Yves Angrignon.
    Le film «Death Weekend» (le 191e de la liste) a été réalisé en 1976 par William Fruet.
    Le film «La fleur aux dents» (le 192e de la liste) a été réalisé en 1976 par Thomas Vamos.
    Le film «J.A. Martin photographe» (le 193e de la liste) a été réalisé en 1976 par Jean Beaudin.
    Le film «Je suis loin de toi mignonne» (le 194e de la liste) a été réalisé en 1976 par Claude Fournier.
    Le film «Jos Carbone» (le 195e de la liste) a été réalisé en 1976 par Hugues Tremblay.
    Le film «The Little Girl Who Lives Down the Lane» (le 196e de la liste) a été réalisé en 1976 par Nicolas Gessner.
    Le film «Parlez-nous d'amour» (le 197e de la liste) a été réalisé en 1976 par Jean-Claude Lord.
    Le film «La piastre» (le 198e de la liste) a été réalisé en 1976 par Alain Chartrand.
    Le film «Simple histoire d'amours» (le 199e de la liste) a été réalisé en 1976 par Fernand Dansereau.
    Le film «Ti-Cul Tougas» (le 200e de la liste) a été réalisé en 1976 par Jean-Guy Noël.
    Le film «L'ange et la femme» (le 201e de la liste) a été réalisé en 1977 par Gilles Carle.
    Le film «Cathy's Curse» (le 202e de la liste) a été réalisé en 1977 par Eddy Matalon.
    Le film «L'eau chaude, l'eau frette» (le 203e de la liste) a été réalisé en 1977 par André Forcier.
    Le film «Full Circle» (le 204e de la liste) a été réalisé en 1977 par Richard Loncraine.
    Le film «Ilsa the Tigress of Siberia» (le 205e de la liste) a été réalisé en 1977 par Jean Lafleur.
    Le film «La menace» (le 206e de la liste) a été réalisé en 1977 par Alain Corneau.
    Le film «One Man» (le 207e de la liste) a été réalisé en 1977 par Robin Spry.
    Le film «La p'tite violence» (le 208e de la liste) a été réalisé en 1977 par Hélène Girard.
    Le film «Panique» (le 209e de la liste) a été réalisé en 1977 par Jean-Claude Lord.
    Le film «Rabid» (le 210e de la liste) a été réalisé en 1977 par David Cronenberg.
    Le film «Rituals» (le 211e de la liste) a été réalisé en 1977 par Peter Carter.
    Le film «Le soleil se lève en retard» (le 212e de la liste) a été réalisé en 1977 par André Brassard.
    Le film «Ti-Mine, Bernie pis la gang…» (le 213e de la liste) a été réalisé en 1977 par Marcel Carrière.
    Le film «Le vieux pays où Rimbaud est mort» (le 214e de la liste) a été réalisé en 1977 par Jean Pierre Lefebvre.
    Le film «L'ange gardien» (le 215e de la liste) a été réalisé en 1978 par Jacques Fournier.
    Le film «Blackout» (le 216e de la liste) a été réalisé en 1978 par Eddy Matalon.
    Le film «City on Fire» (le 217e de la liste) a été réalisé en 1978 par Alvin Rakoff.
    Le film «Comme les six doigts de la main» (le 218e de la liste) a été réalisé en 1978 par André Mélançon.
    Le film «Drying Up the Streets» (le 219e de la liste) a été réalisé en 1978 par Robin Spry.
    Le film «L'homme en colère» (le 220e de la liste) a été réalisé en 1978 par Claude Pinoteau.
    Le film «In Praise of Older Women» (le 221e de la liste) a été réalisé en 1978 par George Kaczender.
    Le film «Jacob Two-Two Meets the Hooded Fang» (le 222e de la liste) a été réalisé en 1978 par Theodore J. Flicker.
    Le film «Les liens de sang» (le 223e de la liste) a été réalisé en 1978 par Claude Chabrol.
    Le film «Tomorrow Never Comes» (le 224e de la liste) a été réalisé en 1978 par Peter Collinson.
    Le film «Un grand logement» (le 225e de la liste) a été réalisé en 1978 par Mario Bolduc et Marie-Ginette Guay.
    Le film «Une amie d'enfance» (le 226e de la liste) a été réalisé en 1978 par Francis Mankiewicz.
    Le film «Violette Nozière» (le 227e de la liste) a été réalisé en 1978 par Claude Chabrol.
    Le film «À nous deux» (le 228e de la liste) a été réalisé en 1979 par Claude Lelouch.
    Le film «L'affaire Coffin» (le 229e de la liste) a été réalisé en 1979 par Jean-Claude Labrecque.
    Le film «L'arrache-cœur» (le 230e de la liste) a été réalisé en 1979 par Mireille Dansereau.
    Le film «Au revoir… à lundi» (le 231e de la liste) a été réalisé en 1979 par Maurice Dugowson.
    Le film «Avoir 16 ans» (le 232e de la liste) a été réalisé en 1979 par Jean Pierre Lefebvre.
    Le film «La belle apparence» (le 233e de la liste) a été réalisé en 1979 par Denyse Benoît.
    Le film «The Brood» (le 234e de la liste) a été réalisé en 1979 par David Cronenberg.
    Le film «Caro papa» (le 235e de la liste) a été réalisé en 1979 par Dino Risi.
    Le film «Les célébrations» (le 236e de la liste) a été réalisé en 1979 par Yves Simoneau.
    Le film «Le château de cartes» (le 237e de la liste) a été réalisé en 1979 par François Labonté.
    Le film «Cordélia» (le 238e de la liste) a été réalisé en 1979 par Jean Beaudin.
    Le film «Éclair au chocolat» (le 239e de la liste) a été réalisé en 1979 par Jean-Claude Lord.
    Le film «Fuir» (le 240e de la liste) a été réalisé en 1979 par Hélène Girard.
    Le film «Girls» (le 241e de la liste) a été réalisé en 1979 par Just Jaeckin.
    Le film «L'hiver bleu (Abitibi 1978)» (le 242e de la liste) a été réalisé en 1979 par André Blanchard.
    Le film «La maladie c'est les compagnies» (le 243e de la liste) a été réalisé en 1979 par Richard Boutet.
    Le film «A Man Called Intrepid» (le 244e de la liste) a été réalisé en 1979 par Peter Carter.
    Le film «Mourir à tue-tête» (le 245e de la liste) a été réalisé en 1979 par Anne Claire Poirier.
    Le film «Vie d'ange» (le 246e de la liste) a été réalisé en 1979 par Pierre Harel.
    Le film «Agency» (le 247e de la liste) a été réalisé en 1980 par George Kaczender.
    Le film «Atlantic City, U.S.A.» (le 248e de la liste) a été réalisé en 1980 par Louis Malle.
    Le film «Les bons débarras» (le 249e de la liste) a été réalisé en 1980 par Francis Mankiewicz.
    Le film «Ça peut pas être l'hiver on n'a même pas eu d'été» (le 250e de la liste) a été réalisé en 1980 par Louise Carré.
    Le film «Le chef se déniaise» (le 251e de la liste) a été réalisé en 1980 par Jean Luret.
    Le film «Contrecœur» (le 252e de la liste) a été réalisé en 1980 par Jean-Guy Noël.
    Le film «La cuisine rouge» (le 253e de la liste) a été réalisé en 1980 par Paule Baillargeon.
    Le film «Fantastica» (le 254e de la liste) a été réalisé en 1980 par Gilles Carle.
    Le film «Final Assignment» (le 255e de la liste) a été réalisé en 1980 par Paul Almond.
    Le film «Les grands enfants» (le 256e de la liste) a été réalisé en 1980 par Paul Tana.
    Le film «Hog Wild» (le 257e de la liste) a été réalisé en 1980 par Les Rose.
    Le film «L'homme à tout faire» (le 258e de la liste) a été réalisé en 1980 par Micheline Lanctôt.
    Le film «Hot Dogs» (le 259e de la liste) a été réalisé en 1980 par Claude Fournier.
    Le film «The Lucky Star» (le 260e de la liste) a été réalisé en 1980 par Max Fischer.
    Le film «Pinball Summer» (le 261e de la liste) a été réalisé en 1980 par George Mihalka.
    Le film «Strass café» (le 262e de la liste) a été réalisé en 1980 par Léa Pool.
    Le film «Suzanne» (le 263e de la liste) a été réalisé en 1980 par Robin Spry.
    Le film «Terror Train» (le 264e de la liste) a été réalisé en 1980 par Roger Spottiswoode.
    Le film «Thetford au milieu de notre vie» (le 265e de la liste) a été réalisé en 1980 par Iolande Cadrin-Rossignol et Fernand Dansereau.
    Le film «Les beaux souvenirs» (le 266e de la liste) a été réalisé en 1981 par Francis Mankiewicz.
    Le film «Black Mirror» (le 267e de la liste) a été réalisé en 1981 par Pierre-Alain Jolivet.
    Le film «Deux super dingues» (le 268e de la liste) a été réalisé en 1981 par Claude Castravelli.
    Le film «The Funny Farm» (le 269e de la liste) a été réalisé en 1981 par Ron Clark.
    Le film «Gas» (le 270e de la liste) a été réalisé en 1981 par Les Rose.
    Le film «La guerre du feu» (le 271e de la liste) a été réalisé en 1981 par Jean-Jacques Annaud.
    Le film «Happy Birthday to Me» (le 272e de la liste) a été réalisé en 1981 par J. Lee Thompson.
    Le film «Hard Feelings» (le 273e de la liste) a été réalisé en 1981 par Daryl Duke.
    Le film «Heartaches» (le 274e de la liste) a été réalisé en 1981 par Donald Shebib.
    Le film «Hot Touch» (le 275e de la liste) a été réalisé en 1981 par Roger Vadim.
    Le film «My Bloody Valentine» (le 276e de la liste) a été réalisé en 1981 par George Mihalka.
    Le film «Les Plouffe» (le 277e de la liste) a été réalisé en 1981 par Gilles Carle.
    Le film «Salut! J.W.» (le 278e de la liste) a été réalisé en 1981 par Ian Ireland.
    Le film «Le shift de nuit» (le 279e de la liste) a été réalisé en 1981 par Mario Bolduc.
    Le film «Ups & Downs» (le 280e de la liste) a été réalisé en 1981 par Paul Almond.
    Le film «Yesterday» (le 281e de la liste) a été réalisé en 1981 par Larry Kent.
    Le film «Your Ticket is No Longer Valid» (le 282e de la liste) a été réalisé en 1981 par George Kaczender.
    Le film «Cross Country» (le 283e de la liste) a été réalisé en 1982 par Paul Lynch.
    Le film «Doux aveux» (le 284e de la liste) a été réalisé en 1982 par Fernand Dansereau.
    Le film «Les fleurs sauvages» (le 285e de la liste) a été réalisé en 1982 par Jean Pierre Lefebvre.
    Le film «Le futur intérieur» (le 286e de la liste) a été réalisé en 1982 par Jean Chabot.
    Le film «Killing 'em Softly» (le 287e de la liste) a été réalisé en 1982 par Max Fischer.
    Le film «Larose, Pierrot et la Luce» (le 288e de la liste) a été réalisé en 1982 par Claude Gagnon.
    Le film «Luc ou La part des choses» (le 289e de la liste) a été réalisé en 1982 par Michel Audy.
    Le film «Paradise» (le 290e de la liste) a été réalisé en 1982 par Stuart Gilliard.
    Le film «La quarantaine» (le 291e de la liste) a été réalisé en 1982 par Anne-Claire Poirier.
    Le film «Scandale» (le 292e de la liste) a été réalisé en 1982 par George Mihalka.
    Le film «Une journée en taxi» (le 293e de la liste) a été réalisé en 1982 par Robert Ménard.
    Le film «Visiting Hours» (le 294e de la liste) a été réalisé en 1982 par Jean-Claude Lord.
    Le film «Les yeux rouges ou Les vérités accidentelles» (le 295e de la liste) a été réalisé en 1982 par Yves Simoneau.
    Le film «Au clair de la lune» (le 296e de la liste) a été réalisé en 1983 par André Forcier.
    Le film «Au pays de Zom» (le 297e de la liste) a été réalisé en 1983 par Gilles Groulx.
    Le film «Bonheur d'occasion» (le 298e de la liste) a été réalisé en 1983 par Claude Fournier.
    Le film «Lucien Brouillard» (le 299e de la liste) a été réalisé en 1983 par Bruno Carrière.
    Le film «Maria Chapdelaine» (le 300e de la liste) a été réalisé en 1983 par Gilles Carle.
    Le film «Porky's 2: The Next Day» (le 301e de la liste) a été réalisé en 1983 par Bob Clark.
    Le film «Rien qu'un jeu» (le 302e de la liste) a été réalisé en 1983 par Brigitte Sauriol.
    Le film «Le ruffian» (le 303e de la liste) a été réalisé en 1983 par José Giovanni.
    Le film «Snapshot» (le 304e de la liste) a été réalisé en 1983 par Abderahmane Mazouz.
    Le film «Videodrome» (le 305e de la liste) a été réalisé en 1983 par David Cronenberg.
    Le film «The Wars» (le 306e de la liste) a été réalisé en 1983 par Robin Phillips.
    Le film «Les Années de rêves» (le 307e de la liste) a été réalisé en 1984 par Jean-Claude Labrecque.
    Le film «The Bay Boy» (le 308e de la liste) a été réalisé en 1984 par Daniel Petrie.
    Le film «La couleur encerclée» (le 309e de la liste) a été réalisé en 1984 par Jean Gagné, Serge Gagné.
    Le film «Le crime d'Ovide Plouffe» (le 310e de la liste) a été réalisé en 1984 par Denys Arcand.
    Le film «Evil Judgment» (le 311e de la liste) a été réalisé en 1984 par Claude Castravelli.
    Le film «La femme de l'hôtel» (le 312e de la liste) a été réalisé en 1984 par Léa Pool.
    Le film «La Guerre des tuques» (le 313e de la liste) a été réalisé en 1984 par André Melançon.
    Le film «Hey Babe!» (le 314e de la liste) a été réalisé en 1984 par Rafal Zielinski.
    Le film «The Hotel New Hampshire» (le 315e de la liste) a été réalisé en 1984 par Tony Richardson.
    Le film «Jacques et Novembre» (le 316e de la liste) a été réalisé en 1984 par Jean Beaudry.
    Le film «Le jour « S... »» (le 317e de la liste) a été réalisé en 1984 par Jean Pierre Lefebvre.
    Le film «Louisiana» (le 318e de la liste) a été réalisé en 1984 par Philippe de Broca.
    Le film «Mario» (le 319e de la liste) a été réalisé en 1984 par Jean Beaudin.
    Le film «The Masculine Mystique» (le 320e de la liste) a été réalisé en 1984 par John N. Smith, Giles Walker.
    Le film «Memoirs» (le 321e de la liste) a été réalisé en 1984 par Bashar Shbib.
    Le film «Mother's Meat Freuds Flesh» (le 322e de la liste) a été réalisé en 1984 par Demetri Demetrios.
    Le film «Paroles et musiques» (le 323e de la liste) a été réalisé en 1984 par Élie Chouraqui.
    Le film «Perfect Timing» (le 324e de la liste) a été réalisé en 1984 par René Bonnière.
    Le film «Le sang des autres» (le 325e de la liste) a été réalisé en 1984 par Claude Chabrol.
    Le film «Sonatine» (le 326e de la liste) a été réalisé en 1984 par Micheline Lanctôt.
    Le film «The Surrogate» (le 327e de la liste) a été réalisé en 1984 par Don Carmody.
    Le film «90 Days» (le 328e de la liste) a été réalisé en 1985 par Giles Walker.
    Le film «L'adolescente sucre d'amour» (le 329e de la liste) a été réalisé en 1985 par Jocelyne Saab.
    Le film «Adramélech» (le 330e de la liste) a été réalisé en 1985 par Pierre Grégoire.
    Le film «Bayo» (le 331e de la liste) a été réalisé en 1985 par Mort Ransen.
    Le film «The Blue Man» (le 332e de la liste) a été réalisé en 1985 par George Mihalka.
    Le film «Caffè Italia Montréal» (le 333e de la liste) a été réalisé en 1985 par Paul Tana.
    Le film «Celui qui voit les heures» (le 334e de la liste) a été réalisé en 1985 par Pierre Goupil.
    Le film «La dame en couleurs» (le 335e de la liste) a été réalisé en 1985 par Claude Jutra.
    Le film «Le dernier glacier» (le 336e de la liste) a été réalisé en 1985 par Roger Frappier, Jacques Leduc.
    Le film «Elvis Gratton : Le king des kings» (le 337e de la liste) a été réalisé en 1985 par Pierre Falardeau.
    Le film «The Gunrunner» (le 338e de la liste) a été réalisé en 1985 par Nardo Castillo.
    Le film «Instantanés» (le 339e de la liste) a été réalisé en 1985 par Michel Juliani.
    Le film «Joshua Then and Now» (le 340e de la liste) a été réalisé en 1985 par Ted Kotcheff.
    Le film «Junior» (le 341e de la liste) a été réalisé en 1985 par Jim Hanley.
    Le film «Lune de miel» (le 342e de la liste) a été réalisé en 1985 par Patrick Jamain.
    Le film «Le matou» (le 343e de la liste) a été réalisé en 1985 par Jean Beaudin.
    Le film «Medium blues» (le 344e de la liste) a été réalisé en 1985 par Michel Préfontaine.
    Le film «Night Magic» (le 345e de la liste) a été réalisé en 1985 par Lewis Furey.
    Le film «Opération beurre de pinottes» (le 346e de la liste) a été réalisé en 1985 par Michael Rubbo.
    Le film «Québec, opération Lambda» (le 347e de la liste) a été réalisé en 1985 par Marc Degryse.
    Le film «Visage pâle» (le 348e de la liste) a été réalisé en 1985 par Claude Gagnon.
    Le film «Anne Trister» (le 349e de la liste) a été réalisé en 1986 par Léa Pool.
    Le film «Bach et Bottine» (le 350e de la liste) a été réalisé en 1986 par André Melançon.
    Le film «The Boy in Blue» (le 351e de la liste) a été réalisé en 1986 par Charles Jarrott.
    Le film «Cat Squad» (le 352e de la liste) a été réalisé en 1986 par William Freidkin.
    Le film «Contes des mille et un jours» (le 353e de la liste) a été réalisé en 1986 par Iolande Cadrin-Rossignol.
    Le film «Deaf & Mute» (le 354e de la liste) a été réalisé en 1986 par Hunt Hoe.
    Le film «Le Déclin de l'empire américain» (le 355e de la liste) a été réalisé en 1986 par Denys Arcand.
    Le film «Le dernier havre» (le 356e de la liste) a été réalisé en 1986 par Denyse Benoit.
    Le film «Équinoxe» (le 357e de la liste) a été réalisé en 1986 par Arthur Lamothe.
    Le film «Evixion» (le 358e de la liste) a été réalisé en 1986 par Bashar Shbib.
    Le film «The First Killing Frost» (le 359e de la liste) a été réalisé en 1986 par Roland Hallé.
    Le film «Exit» (le 360e de la liste) a été réalisé en 1986 par Robert Ménard.
    Le film «Les fous de bassan» (le 361e de la liste) a été réalisé en 1986 par Yves Simoneau.
    Le film «La guêpe» (le 362e de la liste) a été réalisé en 1986 par Gilles Carle.
    Le film «Henri» (le 363e de la liste) a été réalisé en 1986 par François Labonté.
    Le film «L'homme renversé» (le 364e de la liste) a été réalisé en 1986 par Yves Dion.
    Le film «Meatballs 3» (le 365e de la liste) a été réalisé en 1986 par George Mendeluk.
    Le film «The Morning Man» (le 366e de la liste) a été réalisé en 1986 par Danièle J Suissa.
    Le film «Overnight» (le 367e de la liste) a été réalisé en 1986 par Jack Darcus.
    Le film «Pouvoir intime» (le 368e de la liste) a été réalisé en 1986 par Yves Simoneau.
    Le film «Qui a tiré sur nos histoires d'amour?» (le 369e de la liste) a été réalisé en 1986 par Louise Carré.
    Le film «Sauve-toi Lola» (le 370e de la liste) a été réalisé en 1986 par Michel Drach.
    Le film «Sitting in Limbo» (le 371e de la liste) a été réalisé en 1986 par John N. Smith.
    Le film «Toby McTeague» (le 372e de la liste) a été réalisé en 1986 par Jean-Claude Lord.
    Le film «À l'automne de la vie» (le 373e de la liste) a été réalisé en 1987 par Yvan Chouinard.
    Le film «The Great Land of Small» (le 374e de la liste) a été réalisé en 1987 par Vojtech Jasny.
    Le film «Le frère André» (le 375e de la liste) a été réalisé en 1987 par Jean-Claude Labrecque.
    Le film «Grelots rouges sanglots bleus» (le 376e de la liste) a été réalisé en 1987 par Pierre Harel.
    Le film «La Guerre oubliée» (le 377e de la liste) a été réalisé en 1987 par Richard Boutet.
    Le film «Hitting Home» (le 378e de la liste) a été réalisé en 1987 par Robin Spry.
    Le film «L'Île» (le 379e de la liste) a été réalisé en 1987 par François Leterrier.
    Le film «Le Jeune Magicien» (le 380e de la liste) a été réalisé en 1987 par Waldemar Dziki.
    Le film «Keeping Track» (le 381e de la liste) a été réalisé en 1987 par Robin Spry.
    Le film «The Kid Brother ou Kenny» (le 382e de la liste) a été réalisé en 1987 par Claude Gagnon.
    Le film «The Last Straw» (le 383e de la liste) a été réalisé en 1987 par Giles Walker.
    Le film «La ligne de chaleur» (le 384e de la liste) a été réalisé en 1987 par Hubert-Yves Rose.
    Le film «Marie s'en va-t-en ville» (le 385e de la liste) a été réalisé en 1987 par Marquise Lepage.
    Le film «Les roses de Matmata» (le 386e de la liste) a été réalisé en 1987 par José Pinheiro.
    Le film «Seductio» (le 387e de la liste) a été réalisé en 1987 par Bashar Shbib.
    Le film «Le sourd dans la ville» (le 388e de la liste) a été réalisé en 1987 par Mireille Dansereau.
    Le film «Tinamer» (le 389e de la liste) a été réalisé en 1987 par Jean-Guy Noël.
    Le film «Train of Dreams» (le 390e de la liste) a été réalisé en 1987 par John N. Smith.
    Le film «Tristesse, modèle réduit» (le 391e de la liste) a été réalisé en 1987 par Robert Morin.
    Le film «Un zoo la nuit» (le 392e de la liste) a été réalisé en 1987 par Jean-Claude Lauzon.
    Le film «À corps perdu» (le 393e de la liste) a été réalisé en 1988 par Léa Pool.
    Le film «La boîte à soleil» (le 394e de la liste) a été réalisé en 1988 par Jean Pierre Lefebvre.
    Le film «La bottega del orefice» (le 395e de la liste) a été réalisé en 1988 par Michael Anderson.
    Le film «Clair-obscur» (le 396e de la liste) a été réalisé en 1988 par Bashar Shbib.
    Le film «Eva Guerrillera» (le 397e de la liste) a été réalisé en 1988 par Jacqueline Levitin.
    Le film «Family Reunion» (le 398e de la liste) a été réalisé en 1988 par Dick Sarin.
    Le film «Gaspard et fils» (le 399e de la liste) a été réalisé en 1988 par François Labonté.
    Le film «La Grenouille et la Baleine» (le 400e de la liste) a été réalisé en 1988 par Jean-Claude Lord.
    Le film «Horses in Winter» (le 401e de la liste) a été réalisé en 1988 par Rick Raxlen.
    Le film «Kalamazoo» (le 402e de la liste) a été réalisé en 1988 par André Forcier.
    Le film «Moonlight Flight» (le 403e de la liste) a été réalisé en 1988 par Jim Kaufman.
    Le film «La nuit avec Hortense» (le 404e de la liste) a été réalisé en 1988 par Jean Chabot.
    Le film «Oh! Oh! Satan!» (le 405e de la liste) a été réalisé en 1988 par André Farwagi.
    Le film «La peau et les os» (le 406e de la liste) a été réalisé en 1988 par Johanne Prégent.
    Le film «Les Portes tournantes» (le 407e de la liste) a été réalisé en 1988 par Francis Mankiewicz.
    Le film «Salut Victor» (le 408e de la liste) a été réalisé en 1988 par Anne Claire Poirier.
    Le film «Something About Love» (le 409e de la liste) a été réalisé en 1988 par Tom Berry.
    Le film «Les Tisserands du pouvoir» (le 410e de la liste) a été réalisé en 1988 par Claude Fournier.
    Le film «Tommy Tricker and the Stamp Traveller» (le 411e de la liste) a été réalisé en 1988 par Michael Rubbo.
    Le film «L'Air de rien» (le 412e de la liste) a été réalisé en 1989 par Mary Jimenez.
    Le film «The Amityville Curse» (le 413e de la liste) a été réalisé en 1989 par Tom Berry.
    Le film «Bye bye Chaperon rouge» (le 414e de la liste) a été réalisé en 1989 par Márta Mészáros.
    Le film «Cher frangin» (le 415e de la liste) a été réalisé en 1989 par Gérard Mordillat.
    Le film «Comment faire l'amour avec un nègre sans se fatiguer» (le 416e de la liste) a été réalisé en 1989 par Jacques W. Benoît.
    Le film «Cruising Bar» (le 417e de la liste) a été réalisé en 1989 par Robert Ménard.
    Le film «Dans le ventre du dragon» (le 418e de la liste) a été réalisé en 1989 par Yves Simoneau.
    Le film «Fierro… l'été des secrets» (le 419e de la liste) a été réalisé en 1989 par André Melançon.
    Le film «Jésus de Montréal» (le 420e de la liste) a été réalisé en 1989 par Denys Arcand.
    Le film «Laura Laur» (le 421e de la liste) a été réalisé en 1989 par Brigitte Sauriol.
    Le film «Matinée» (le 422e de la liste) a été réalisé en 1989 par Richard Martin.
    Le film «Les matins infidèles» (le 423e de la liste) a été réalisé en 1989 par Jean Beaudry, François Bouvier.
    Le film «Mindfield» (le 424e de la liste) a été réalisé en 1989 par Jean-Claude Lord.
    Le film «Les Noces de papier» (le 425e de la liste) a été réalisé en 1989 par Michel Brault.
    Le film «Off Off Off» (le 426e de la liste) a été réalisé en 1989 par Jorge Fajardo.
    Le film «Portion d'éternité» (le 427e de la liste) a été réalisé en 1989 par Robert Favreau.
    Le film «La Réception» (le 428e de la liste) a été réalisé en 1989 par Robert Morin.
    Le film «La Révolution française : les années terribles» (le 429e de la liste) a été réalisé en 1989 par Richard T. Heffron.
    Le film «Short Change» (le 430e de la liste) a été réalisé en 1989 par Nicholas Kinsey.
    Le film «Sous les draps, les étoiles» (le 431e de la liste) a été réalisé en 1989 par Jean-Pierre Gariepy.
    Le film «T'es belle Jeanne» (le 432e de la liste) a été réalisé en 1989 par Robert Ménard.
    Le film «Trois pommes à côté du sommeil» (le 433e de la liste) a été réalisé en 1989 par Jacques Leduc.
    Le film «Vent de galerne» (le 434e de la liste) a été réalisé en 1989 par Bernard Favre.
    Le film «Welcome to Canada» (le 435e de la liste) a été réalisé en 1989 par John N. Smith.
    Le film «Babylone» (le 436e de la liste) a été réalisé en 1990 par Manu Bonmariage.
    Le film «Back Stab» (le 437e de la liste) a été réalisé en 1990 par Jim Kaufman.
    Le film «Bethune: The Making of a Hero» (le 438e de la liste) a été réalisé en 1990 par Phillip Borsos.
    Le film «Breadhead» (le 439e de la liste) a été réalisé en 1990 par Carlo Alacchi.
    Le film «A Bullet in the Head» (le 440e de la liste) a été réalisé en 1990 par Attila Bertalan.
    Le film «Cargo» (le 441e de la liste) a été réalisé en 1990 par François Girard.
    Le film «The Company of Strangers» (le 442e de la liste) a été réalisé en 1990 par Cynthia Scott.
    Le film «Cursed» (le 443e de la liste) a été réalisé en 1990 par Mychel Arsenault.
    Le film «Dames galantes» (le 444e de la liste) a été réalisé en 1990 par Jean-Charles Tacchella.
    Le film «Ding et Dong, le film» (le 445e de la liste) a été réalisé en 1990 par Alain Chartrand.
    Le film «Falling Over Backwards» (le 446e de la liste) a été réalisé en 1990 par Mort Ransen.
    Le film «La Fille du maquignon» (le 447e de la liste) a été réalisé en 1990 par Abderrahmane Mazouz.
    Le film «I'm Happy. You're Happy. We're All Happy. Happy, Happy, Happy.» (le 448e de la liste) a été réalisé en 1990 par Verlcrow Ripper.
    Le film «La Liberté d'une statue» (le 449e de la liste) a été réalisé en 1990 par Olivier Asselin.
    Le film «Lola Zipper» (le 450e de la liste) a été réalisé en 1990 par Ilan Duran Cohen.
    Le film «Manuel : le fils emprunté» (le 451e de la liste) a été réalisé en 1990 par François Labonté.
    Le film «Moody Beach» (le 452e de la liste) a été réalisé en 1990 par Richard Roy.
    Le film «New York doré» (le 453e de la liste) a été réalisé en 1990 par Suzanne Guy.
    Le film «Le Party» (le 454e de la liste) a été réalisé en 1990 par Pierre Falardeau.
    Le film «Pas de répit pour Mélanie» (le 455e de la liste) a été réalisé en 1990 par Jean Beaudry.
    Le film «Princes in Exile» (le 456e de la liste) a été réalisé en 1990 par Giles Walker.
    Le film «Rafales» (le 457e de la liste) a été réalisé en 1990 par André Melançon.
    Le film «Remous» (le 458e de la liste) a été réalisé en 1990 par Sylvie Van Brabant.
    Le film «Le Royaume ou l'asile» (le 459e de la liste) a été réalisé en 1990 par Jean Gagné.
    Le film «The Secret of Nandy» (le 460e de la liste) a été réalisé en 1990 par Danièle J Suissa.
    Le film «Simon les nuages» (le 461e de la liste) a été réalisé en 1990 par Roger Cantin.
    Le film «Un autre homme» (le 462e de la liste) a été réalisé en 1990 par Charles Binamé.
    Le film «Un été après l'autre» (le 463e de la liste) a été réalisé en 1990 par Anne-Marie Étienne.
    Le film «Une histoire inventée» (le 464e de la liste) a été réalisé en 1990 par André Forcier.
    Le film «Alisée» (le 465e de la liste) a été réalisé en 1991 par André Blanchard.
    Le film «Amoureux fou» (le 466e de la liste) a été réalisé en 1991 par Robert Ménard.
    Le film «L'annonce faite à Marie» (le 467e de la liste) a été réalisé en 1991 par Alain Cuny.
    Le film «L'assassin jouait du trombone» (le 468e de la liste) a été réalisé en 1991 par Roger Cantin.
    Le film «La Championne» (le 469e de la liste) a été réalisé en 1991 par Elisabeta Bostan.
    Le film «… comme un voleur» (le 470e de la liste) a été réalisé en 1991 par Michel Langlois.
    Le film «The Dance Goes On» (le 471e de la liste) a été réalisé en 1991 par Paul Almond.
    Le film «Les Danseurs du Mozambique» (le 472e de la liste) a été réalisé en 1991 par Philippe Lefebvre.
    Le film «Deadly Surveillance» (le 473e de la liste) a été réalisé en 1991 par Paul Ziller.
    Le film «La Demoiselle sauvage» (le 474e de la liste) a été réalisé en 1991 par Léa Pool.
    Le film «Le Fabuleux voyage de l'ange» (le 475e de la liste) a été réalisé en 1991 par Jean Pierre Lefebvre.
    Le film «The Final Heist» (le 476e de la liste) a été réalisé en 1991 par George Mihalka.
    Le film «Lana in Love» (le 477e de la liste) a été réalisé en 1991 par Bashar Shbib.
    Le film «Love-moi» (le 478e de la liste) a été réalisé en 1991 par Marcel Simard.
    Le film «Montréal vu par…» (le 479e de la liste) a été réalisé en 1991 par Un collectif de réalisateurs.
    Le film «Motyli Cas» (le 480e de la liste) a été réalisé en 1991 par Bretislav Pojar.
    Le film «Nelligan» (le 481e de la liste) a été réalisé en 1991 par Robert Favreau.
    Le film «Pablo qui court» (le 482e de la liste) a été réalisé en 1991 par Bernard Bergeron.
    Le film «The Pianist» (le 483e de la liste) a été réalisé en 1991 par Claude Gagnon.
    Le film «La Révolution française : les années lumières» (le 484e de la liste) a été réalisé en 1991 par Robert Enrico.
    Le film «Scanners 2: The New Order» (le 485e de la liste) a été réalisé en 1991 par Christian Duguay.
    Le film «Sous le signe du poisson» (le 486e de la liste) a été réalisé en 1991 par Serge Pénard.
    Le film «Un Léger vertige» (le 487e de la liste) a été réalisé en 1991 par Diane Poitras.
    Le film «Vincent et moi» (le 488e de la liste) a été réalisé en 1991 par Michael Rubbo.
    Le film «Aline» (le 489e de la liste) a été réalisé en 1992 par Carole Laganière.
    Le film «L'Automne sauvage» (le 490e de la liste) a été réalisé en 1992 par Gabriel Pelletier.
    Le film «Being at Home with Claude» (le 491e de la liste) a été réalisé en 1992 par Jean Beaudin.
    Le film «La Bête de foire» (le 492e de la liste) a été réalisé en 1992 par Isabelle Hayeur.
    Le film «Call of the Wild» (le 493e de la liste) a été réalisé en 1992 par Bjorn Kristen.
    Le film «Canvas» (le 494e de la liste) a été réalisé en 1992 par Alain Zaloum.
    Le film «Coyote» (le 495e de la liste) a été réalisé en 1992 par Richard Ciupka.
    Le film «A Cry in the Night» (le 496e de la liste) a été réalisé en 1992 par Robin Spry.
    Le film «Deadbolt» (le 497e de la liste) a été réalisé en 1992 par Douglas Jackson.
    Le film «La Fenêtre» (le 498e de la liste) a été réalisé en 1992 par Monique Champagne.
    Le film «Francœur: exit pour nomades» (le 499e de la liste) a été réalisé en 1992 par Pierre Bastien.
    Le film «L'Homme de ma vie» (le 500e de la liste) a été réalisé en 1992 par Jean-Charles Tacchella.
    Le film «Killer Image» (le 501e de la liste) a été réalisé en 1992 par David Winning.
    Le film «El lado oscuro del corazón» (le 502e de la liste) a été réalisé en 1992 par Eliseo Subiela.
    Le film «Léolo» (le 503e de la liste) a été réalisé en 1992 par Jean-Claude Lauzon.
    Le film «Le Mirage» (le 504e de la liste) a été réalisé en 1992 par Jean-Claude Guiguet.
    Le film «La Postière» (le 505e de la liste) a été réalisé en 1992 par Gilles Carle.
    Le film «Psychic» (le 506e de la liste) a été réalisé en 1992 par George Mihalka.
    Le film «Requiem pour un beau sans-cœur» (le 507e de la liste) a été réalisé en 1992 par Robert Morin.
    Le film «La Sarrasine» (le 508e de la liste) a été réalisé en 1992 par Paul Tana.
    Le film «Scanners 3: The Takeover» (le 509e de la liste) a été réalisé en 1992 par Christian Duguay.
    Le film «Shadow of the Wolf» (le 510e de la liste) a été réalisé en 1992 par Jacques Dorfmann, Pierre Magny.
    Le film «Tirelire Combines & Cie» (le 511e de la liste) a été réalisé en 1992 par Jean Beaudry.
    Le film «La Vie fantôme» (le 512e de la liste) a été réalisé en 1992 par Jacques Leduc.
    Le film «Le Voleur de caméra» (le 513e de la liste) a été réalisé en 1992 par Claude Fortin.
    Le film «Les Amoureuses» (le 514e de la liste) a été réalisé en 1993 par Johanne Prégent.
    Le film «Armen et Bullik» (le 515e de la liste) a été réalisé en 1993 par Allan Cook.
    Le film «Because Why» (le 516e de la liste) a été réalisé en 1993 par Arto Paragamian.
    Le film «Cap Tourmente» (le 517e de la liste) a été réalisé en 1993 par Michel Langlois.
    Le film «Deux actrices» (le 518e de la liste) a été réalisé en 1993 par Micheline Lanctôt.
    Le film «Doublures» (le 519e de la liste) a été réalisé en 1993 par Michel Murray.
    Le film «La Florida» (le 520e de la liste) a été réalisé en 1993 par George Mihalka.
    Le film «L'Homme sur les quais» (le 521e de la liste) a été réalisé en 1993 par Raoul Peck.
    Le film «Love & Human Remains» (le 522e de la liste) a été réalisé en 1993 par Denys Arcand.
    Le film «Matusalem» (le 523e de la liste) a été réalisé en 1993 par Roger Cantin.
    Le film «Les Mots perdus : un film en quatre saisons» (le 524e de la liste) a été réalisé en 1993 par Marcel Simard.
    Le film «The Myth of the Male Orgasm» (le 525e de la liste) a été réalisé en 1993 par John Hamilton.
    Le film «The Neighbor» (le 526e de la liste) a été réalisé en 1993 par Rodney Gibbons.
    Le film «Pamietnik znaleziony w garbie» (le 527e de la liste) a été réalisé en 1993 par Jan Kidawa-Blonski.
    Le film «Les Pots cassés» (le 528e de la liste) a été réalisé en 1993 par François Bouvier.
    Le film «Saint-Fortunat, les blues» (le 529e de la liste) a été réalisé en 1993 par Daniel St-Laurent.
    Le film «Le Sexe des étoiles» (le 530e de la liste) a été réalisé en 1993 par Paule Baillargeon.
    Le film «Tendre guerre» (le 531e de la liste) a été réalisé en 1993 par Daniel Morin.
    Le film «Thirty Two Short Films About Glenn Gould» (le 532e de la liste) a été réalisé en 1993 par François Girard.
    Le film «La Visite» (le 533e de la liste) a été réalisé en 1993 par Jorge Fajardo.
    Le film «C'était le 12 du 12 et Chili avait les blues» (le 534e de la liste) a été réalisé en 1994 par Charles Binamé.
    Le film «The Child» (le 535e de la liste) a été réalisé en 1994 par George Mihalka.
    Le film «Crack Me Up» (le 536e de la liste) a été réalisé en 1994 par Bashar Shbib.
    Le film «Craque la vie !» (le 537e de la liste) a été réalisé en 1994 par Jean Beaudin.
    Le film «Draghoula» (le 538e de la liste) a été réalisé en 1994 par Basha Shbib.
    Le film «La Fête des rois» (le 539e de la liste) a été réalisé en 1994 par Marquise Lepage.
    Le film «Gaïa» (le 540e de la liste) a été réalisé en 1994 par Pierre Lang.
    Le film «El jardín del edén» (le 541e de la liste) a été réalisé en 1994 par Maria Novaro.
    Le film «Kabloonak» (le 542e de la liste) a été réalisé en 1994 par Claude Massot.
    Le film «Le Lac de la lune» (le 543e de la liste) a été réalisé en 1994 par Michel Jetté.
    Le film «Louis 19, le roi des ondes» (le 544e de la liste) a été réalisé en 1994 par Michel Poulette.
    Le film «Ma sœur, mon amour» (le 545e de la liste) a été réalisé en 1994 par Suzy Cohen.
    Le film «Mon amie Max» (le 546e de la liste) a été réalisé en 1994 par Michel Brault.
    Le film «Mouvements du désir» (le 547e de la liste) a été réalisé en 1994 par Léa Pool.
    Le film «Los Naufragos» (le 548e de la liste) a été réalisé en 1994 par Miguel Littin.
    Le film «Octobre» (le 549e de la liste) a été réalisé en 1994 par Pierre Falardeau.
    Le film «The Paperboy» (le 550e de la liste) a été réalisé en 1994 par Douglas Jackson.
    Le film «Le Retour des aventuriers du timbre perdu» (le 551e de la liste) a été réalisé en 1994 par Michael Rubbo.
    Le film «Rêve aveugle» (le 552e de la liste) a été réalisé en 1994 par Diane Beaudry.
    Le film «Ride Me» (le 553e de la liste) a été réalisé en 1994 par Bashar Shbib.
    Le film «Ruth» (le 554e de la liste) a été réalisé en 1994 par François Delisle.
    Le film «Le Secret de Jérôme» (le 555e de la liste) a été réalisé en 1994 par Phil Comeau.
    Le film «Stalked» (le 556e de la liste) a été réalisé en 1994 par Douglas Jackson.
    Le film «V'la l'cinéma ou le roman de Charles Pathé» (le 557e de la liste) a été réalisé en 1994 par Jacques Rouffio.
    Le film «Le Vent du Wyoming» (le 558e de la liste) a été réalisé en 1994 par André Forcier.
    Le film «La Vie d'un héros» (le 559e de la liste) a été réalisé en 1994 par Micheline Lanctôt.
    Le film «Warriors» (le 560e de la liste) a été réalisé en 1994 par Shimon Dotan.
    Le film «Windigo» (le 561e de la liste) a été réalisé en 1994 par Robert Morin.
    Le film «Yes sir! Madame…» (le 562e de la liste) a été réalisé en 1994 par Robert Morin.
    Le film «Angelo, Frédo et Roméo» (le 563e de la liste) a été réalisé en 1995 par Pierre Plante.
    Le film «Le Confessionnal» (le 564e de la liste) a été réalisé en 1995 par Robert Lepage.
    Le film «Eldorado» (le 565e de la liste) a été réalisé en 1995 par Charles Binamé.
    Le film «L'Enfant d'eau» (le 566e de la liste) a été réalisé en 1995 par Robert Ménard.
    Le film «Erreur sur la personne» (le 567e de la liste) a été réalisé en 1995 par Gilles Noël.
    Le film «La folie des crinolines» (le 568e de la liste) a été réalisé en 1995 par Jean Gagné, Serge Gagné.
    Le film «Highlander 3: The Sorcerer» (le 569e de la liste) a été réalisé en 1995 par Andy Morahan.
    Le film «Hollow Point» (le 570e de la liste) a été réalisé en 1995 par Sidney J. Furie.
    Le film «J'aime j'aime pas» (le 571e de la liste) a été réalisé en 1995 par Sylvie Groulx.
    Le film «Le jour des cris» (le 572e de la liste) a été réalisé en 1995 par Éric Gignac.
    Le film «Kids of the Round Table» (le 573e de la liste) a été réalisé en 1995 par Robert Tinnell.
    Le film «Liste noire» (le 574e de la liste) a été réalisé en 1995 par Jean-Marc Vallée.
    Le film «Motel» (le 575e de la liste) a été réalisé en 1995 par Pascal Maeder (en).
    Le film «La Mule et les Émeraudes» (le 576e de la liste) a été réalisé en 1995 par Bashar Shbib.
    Le film «Screamers» (le 577e de la liste) a été réalisé en 1995 par Christian Duguay.
    Le film «Silent Hunter» (le 578e de la liste) a été réalisé en 1995 par Fred Williamson.
    Le film «Le Sphinx» (le 579e de la liste) a été réalisé en 1995 par Louis Saïa.
    Le film «Suburban Legend» (le 580e de la liste) a été réalisé en 1995 par Jay Ferguson.
    Le film «Witchboard 3» (le 581e de la liste) a été réalisé en 1995 par Peter Svatek.
    Le film «Wrong Woman» (le 582e de la liste) a été réalisé en 1995 par Douglas Jackson.
    Le film «Zigrail» (le 583e de la liste) a été réalisé en 1995 par André Turpin.
    Le film «Aire libre» (le 584e de la liste) a été réalisé en 1996 par Luis Armando Roche.
    Le film «La Ballade de Titus : une fable tout en couleur» (le 585e de la liste) a été réalisé en 1996 par Vincent De Brus.
    Le film «La Beauté, fatale et féroce» (le 586e de la liste) a été réalisé en 1996 par Roger Boire.
    Le film «Bullet to Beijing» (le 587e de la liste) a été réalisé en 1996 par George Mihalka.
    Le film «Caboose» (le 588e de la liste) a été réalisé en 1996 par Richard Roy.
    Le film «Cosmos» (le 589e de la liste) a été réalisé en 1996 par Un collectif de réalisateurs et de réalisatrices.
    Le film «Le Cri de la nuit» (le 590e de la liste) a été réalisé en 1996 par Jean Beaudry.
    Le film «L'Escorte» (le 591e de la liste) a été réalisé en 1996 par Denis Langlois.
    Le film «La Fabrication d'un meurtrier» (le 592e de la liste) a été réalisé en 1996 par Isabelle Poissant.
    Le film «L'Homme idéal» (le 593e de la liste) a été réalisé en 1996 par George Mihalka.
    Le film «L'Homme perché» (le 594e de la liste) a été réalisé en 1996 par Stefan Pleszczynski.
    Le film «Hot Sauce» (le 595e de la liste) a été réalisé en 1996 par Bashar Shbib.
    Le film «J'en suis !» (le 596e de la liste) a été réalisé en 1996 par Claude Fournier.
    Le film «Joyeux Calvaire» (le 597e de la liste) a été réalisé en 1996 par Denys Arcand.
    Le film «Karmina» (le 598e de la liste) a été réalisé en 1996 par Gabriel Pelletier.
    Le film «Lilies» (le 599e de la liste) a été réalisé en 1996 par John Greyson.
    Le film «La Marche à l'amour» (le 600e de la liste) a été réalisé en 1996 par Jean Gagné, Serge Gagné.
    Le film «Never Too Late» (le 601e de la liste) a été réalisé en 1996 par Giles Walker.
    Le film «L'Oreille d'un sourd» (le 602e de la liste) a été réalisé en 1996 par Mario Bolduc.
    Le film «Panic» (le 603e de la liste) a été réalisé en 1996 par Bashar Shbib.
    Le film «Le Polygraphe» (le 604e de la liste) a été réalisé en 1996 par Robert Lepage.
    Le film «Pudding chômeur» (le 605e de la liste) a été réalisé en 1996 par Gilles Carle.
    Le film «Rowing Through» (le 606e de la liste) a été réalisé en 1996 par Masato Harada.
    Le film «Le Silence des fusils» (le 607e de la liste) a été réalisé en 1996 par Arthur Lamothe.
    Le film «Sous-sol» (le 608e de la liste) a été réalisé en 1996 par Pierre Gang.
    Le film «The Strange Blues of Cowboy Red» (le 609e de la liste) a été réalisé en 1996 par Rick Raxlen.
    Le film «La Vengeance de la femme en noir» (le 610e de la liste) a été réalisé en 1996 par Roger Cantin.
    Le film «The Windsor Protocol» (le 611e de la liste) a été réalisé en 1996 par George Mihalka.
    Le film «L'absent» (le 612e de la liste) a été réalisé en 1997 par Céline Baril.
    Le film «The Assignment» (le 613e de la liste) a été réalisé en 1997 par Christian Duguay.
    Le film «Les Boys» (le 614e de la liste) a été réalisé en 1997 par Louis Saïa.
    Le film «Burnt Eden» (le 615e de la liste) a été réalisé en 1997 par Eugene Garcia.
    Le film «Cabaret neiges noires» (le 616e de la liste) a été réalisé en 1997 par Raymond Saint-Jean.
    Le film «The Call of the Wild» (le 617e de la liste) a été réalisé en 1997 par Peter Svatek.
    Le film «Le Ciel est à nous» (le 618e de la liste) a été réalisé en 1997 par Graham Guit.
    Le film «Clandestins» (le 619e de la liste) a été réalisé en 1997 par Denis Chouinard, Nicolas Wadimoff.
    Le film «La Comtesse de Bâton Rouge» (le 620e de la liste) a été réalisé en 1997 par André Forcier.
    Le film «La Conciergerie» (le 621e de la liste) a été réalisé en 1997 par Michel Poulette.
    Le film «De l'autre côté du cœur» (le 622e de la liste) a été réalisé en 1997 par Suzy Cohen.
    Le film «Habitat» (le 623e de la liste) a été réalisé en 1997 par René Daalder.
    Le film «Hemoglobin» (le 624e de la liste) a été réalisé en 1997 par Peter Svatek.
    Le film «Kayla» (le 625e de la liste) a été réalisé en 1997 par Nicholas Kendall.
    Le film «Maîtres anciens» (le 626e de la liste) a été réalisé en 1997 par Olivier Asselin.
    Le film «Matusalem II : le dernier des Beauchesne» (le 627e de la liste) a été réalisé en 1997 par Roger Cantin.
    Le film «Les Mille merveilles de l'univers» (le 628e de la liste) a été réalisé en 1997 par Jean-Michel Roux.
    Le film «Moustaches» (le 629e de la liste) a été réalisé en 1997 par Jim Kaufman.
    Le film «Reaper» (le 630e de la liste) a été réalisé en 1997 par John Bradshaw.
    Le film «Sinon, oui» (le 631e de la liste) a été réalisé en 1997 par Claire Simon.
    Le film «Le Siège de l'âme» (le 632e de la liste) a été réalisé en 1997 par Olivier Asselin.
    Le film «Stranger in the House» (le 633e de la liste) a été réalisé en 1997 par Rodney Gibbons.
    Le film «Strip Search» (le 634e de la liste) a été réalisé en 1997 par Rod Hewitt.
    Le film «Treasure Island» (le 635e de la liste) a été réalisé en 1997 par Peter Rowe.
    Le film «2 secondes» (le 636e de la liste) a été réalisé en 1998 par Manon Briand.
    Le film «L'âge de braise» (le 637e de la liste) a été réalisé en 1998 par Jacques Leduc.
    Le film «Aujourd'hui ou jamais» (le 638e de la liste) a été réalisé en 1998 par Jean Pierre Lefebvre.
    Le film «Babel» (le 639e de la liste) a été réalisé en 1998 par Gérard Pullicino.
    Le film «Les Boys 2» (le 640e de la liste) a été réalisé en 1998 par Louis Saïa.
    Le film «C't'à ton tour, Laura Cadieux» (le 641e de la liste) a été réalisé en 1998 par Denise Filiatrault.
    Le film «Captive» (le 642e de la liste) a été réalisé en 1998 par Matt Dorff.
    Le film «Le Cœur au poing» (le 643e de la liste) a été réalisé en 1998 par Charles Binamé.
    Le film «Dancing on the Moon» (le 644e de la liste) a été réalisé en 1998 par Kit Hood.
    Le film «Dead End» (le 645e de la liste) a été réalisé en 1998 par Douglas Jackson.
    Le film «La Déroute» (le 646e de la liste) a été réalisé en 1998 par Paul Tana.
    Le film «Fatal Affair» (le 647e de la liste) a été réalisé en 1998 par Marc S. Grenier.
    Le film «Foreign Ghosts» (le 648e de la liste) a été réalisé en 1998 par Hunt Hoe.
    Le film «Going to Kansas City» (le 649e de la liste) a été réalisé en 1998 par Pekka Mandart.
    Le film «Le grand serpent du monde» (le 650e de la liste) a été réalisé en 1998 par Yves Dion.
    Le film «Hasards ou Coïncidences» (le 651e de la liste) a été réalisé en 1998 par Claude Lelouch.
    Le film «Hathi» (le 652e de la liste) a été réalisé en 1998 par Philippe Gautier.
    Le film «In Her Defense» (le 653e de la liste) a été réalisé en 1998 par Sidney J. Furie.
    Le film «N.I.P.» (le 654e de la liste) a été réalisé en 1998 par François Raymond.
    Le film «Nguol thùa» (le 655e de la liste) a été réalisé en 1998 par Dai Sijie.
    Le film «Nico the Unicorn» (le 656e de la liste) a été réalisé en 1998 par Graeme Campbell.
    Le film «Nô» (le 657e de la liste) a été réalisé en 1998 par Robert Lepage.
    Le film «La Position de l'escargot» (le 658e de la liste) a été réalisé en 1998 par Michka Saäl.
    Le film «The Press Run» (le 659e de la liste) a été réalisé en 1998 par Robert Ditchburn.
    Le film «Quelque chose d'organique» (le 660e de la liste) a été réalisé en 1998 par Bertrand Bonello.
    Le film «Quiconque meurt, meurt à douleur» (le 661e de la liste) a été réalisé en 1998 par Robert Morin.
    Le film «Random Encounter» (le 662e de la liste) a été réalisé en 1998 par Douglas Jackson.
    Le film «Requiem for Murder» (le 663e de la liste) a été réalisé en 1998 par Douglas Jackson.
    Le film «Revoir Julie» (le 664e de la liste) a été réalisé en 1998 par Jeanne Crépeau.
    Le film «Running Home» (le 665e de la liste) a été réalisé en 1998 par Marc F. Voizard.
    Le film «Snitch» (le 666e de la liste) a été réalisé en 1998 par Ted Demme.
    Le film «Sublet» (le 667e de la liste) a été réalisé en 1998 par John Hamilton.
    Le film «Thunder Point» (le 668e de la liste) a été réalisé en 1998 par George Mihalka.
    Le film «Un 32 août sur Terre» (le 669e de la liste) a été réalisé en 1998 par Denis Villeneuve.
    Le film «Une aventure de Papyrus : La vengeance de Seth» (le 670e de la liste) a été réalisé en 1998 par Michel Gauthier.
    Le film «Le Violon rouge» (le 671e de la liste) a été réalisé en 1998 par François Girard.
    Le film «Winter Lily» (le 672e de la liste) a été réalisé en 1998 par Roshell Bissett.
    Le film «You Can Thank Me Later» (le 673e de la liste) a été réalisé en 1998 par Shimon Dotan.
    Le film «4 Days» (le 674e de la liste) a été réalisé en 1999 par Curtis Wehrfritz.
    Le film «Artificial Lies» (le 675e de la liste) a été réalisé en 1999 par Rodney Gibbons.
    Le film «L'Autobiographe amateur» (le 676e de la liste) a été réalisé en 1999 par Claude Fortin.
    Le film «Autour de la maison rose» (le 677e de la liste) a été réalisé en 1999 par Joana Hadjithomas.
    Le film «Le Dernier Souffle» (le 678e de la liste) a été réalisé en 1999 par Richard Ciupka.
    Le film «Elvis Gratton 2 : Miracle à Memphis» (le 679e de la liste) a été réalisé en 1999 par Pierre Falardeau, Julien Poulin.
    Le film «Emporte-moi» (le 680e de la liste) a été réalisé en 1999 par Léa Pool.
    Le film «La Fabrication des mâles» (le 681e de la liste) a été réalisé en 1999 par Un collectif de réalisateurs et de réalisatrices.
    Le film «Fish Out of Water» (le 682e de la liste) a été réalisé en 1999 par Geoffrey Edwards.
    Le film «Full Blast» (le 683e de la liste) a été réalisé en 1999 par Rodrigue Jean.
    Le film «Grey Owl» (le 684e de la liste) a été réalisé en 1999 par Richard Attenborough.
    Le film «Histoires d'hiver» (le 685e de la liste) a été réalisé en 1999 par François Bouvier.
    Le film «Home Team» (le 686e de la liste) a été réalisé en 1999 par Allan A. Goldstein.
    Le film «Laura Cadieux… la suite» (le 687e de la liste) a été réalisé en 1999 par Denise Filiatrault.
    Le film «Matroni et moi» (le 688e de la liste) a été réalisé en 1999 par Jean-Philippe Duval.
    Le film «Le Petit ciel» (le 689e de la liste) a été réalisé en 1999 par Jean-Sébastien Lord.
    Le film «Pin-Pon, le film» (le 690e de la liste) a été réalisé en 1999 par Ghyslaine Côté.
    Le film «Post mortem» (le 691e de la liste) a été réalisé en 1999 par Louis Bélanger.
    Le film «Quand je serai parti… vous vivrez encore» (le 692e de la liste) a été réalisé en 1999 par Michel Brault.
    Le film «Les Siamoises» (le 693e de la liste) a été réalisé en 1999 par Isabelle Hayeur.
    Le film «Souffle d'ailleurs» (le 694e de la liste) a été réalisé en 1999 par Martin Leclerc, Thomas Schneider.
    Le film «Souvenirs intimes» (le 695e de la liste) a été réalisé en 1999 par Jean Beaudin.
    Le film «Task Force» (le 696e de la liste) a été réalisé en 1999 par Richard Ciupka.
    Le film «Taxman» (le 697e de la liste) a été réalisé en 1999 par Alain Zaloum.
    Le film «Who Gets the House?» (le 698e de la liste) a été réalisé en 1999 par Timothy J. Nelson.
    Le film «The Witness Files» (le 699e de la liste) a été réalisé en 1999 par Douglas Jackson.
    Le film «Alegria» (le 700e de la liste) a été réalisé en 2000 par Franco Dragone.
    Le film «The Art of War» (le 701e de la liste) a été réalisé en 2000 par Christian Duguay.
    Le film «La beauté de Pandore» (le 702e de la liste) a été réalisé en 2000 par Charles Binamé.
    Le film «Blind Terror» (le 703e de la liste) a été réalisé en 2000 par Giles Walker.
    Le film «La bouteille» (le 704e de la liste) a été réalisé en 2000 par Alain DesRochers.
    Le film «Café Olé» (le 705e de la liste) a été réalisé en 2000 par Richard Roy.
    Le film «Deception» (le 706e de la liste) a été réalisé en 2000 par Max Fischer.
    Le film «Desire» (le 707e de la liste) a été réalisé en 2000 par Colleen Murphy.
    Le film «Les fantômes des trois Madeleine» (le 708e de la liste) a été réalisé en 2000 par Guylaine Dionne.
    Le film «Heavy Metal 2000» (le 709e de la liste) a été réalisé en 2000 par Michael Coldewey, Michel Lemire.
    Le film «Hochelaga» (le 710e de la liste) a été réalisé en 2000 par Michel Jetté.
    Le film «L'île de sable» (le 711e de la liste) a été réalisé en 2000 par Johanne Prégent.
    Le film «L'invention de l'amour» (le 712e de la liste) a été réalisé en 2000 par Claude Demers.
    Le film «Island of the Dead» (le 713e de la liste) a été réalisé en 2000 par Tim Southam.
    Le film «Jack and Ella» (le 714e de la liste) a été réalisé en 2000 par Brenda Keesal.
    Le film «La journée avant» (le 715e de la liste) a été réalisé en 2000 par David Lussier.
    Le film «The List» (le 716e de la liste) a été réalisé en 2000 par Sylvain Guy.
    Le film «Maelström» (le 717e de la liste) a été réalisé en 2000 par Denis Villeneuve.
    Le film «Méchant party» (le 718e de la liste) a été réalisé en 2000 par Mario Chabot.
    Le film «La moitié gauche du frigo» (le 719e de la liste) a été réalisé en 2000 par Philippe Falardeau.
    Le film «Les muses orphelines» (le 720e de la liste) a été réalisé en 2000 par Robert Favreau.
    Le film «My Little Devil» (le 721e de la liste) a été réalisé en 2000 par Gopi Desai.
    Le film «Ne dis rien» (le 722e de la liste) a été réalisé en 2000 par Simon Lacombe.
    Le film «Possible Worlds» (le 723e de la liste) a été réalisé en 2000 par Robert Lepage.
    Le film «Rats and Rabbits» (le 724e de la liste) a été réalisé en 2000 par Lewis Furey.
    Le film «La répétition» (le 725e de la liste) a été réalisé en 2000 par Catherine Corsini.
    Le film «Saint Jude» (le 726e de la liste) a été réalisé en 2000 par John L'Écuyer.
    Le film «Stardom» (le 727e de la liste) a été réalisé en 2000 par Denys Arcand.
    Le film «Subconscious Cruelty» (le 728e de la liste) a été réalisé en 2000 par Karim Hussain.
    Le film «The Tracker» (le 729e de la liste) a été réalisé en 2000 par Jeff Schechter.
    Le film «Trick or Treat» (le 730e de la liste) a été réalisé en 2000 par Marc Cayer.
    Le film «Two Thousand and None» (le 731e de la liste) a été réalisé en 2000 par Arto Paragamian.
    Le film «Un petit vent de panique» (le 732e de la liste) a été réalisé en 2000 par Pierre Greco.
    Le film «Varian's War» (le 733e de la liste) a été réalisé en 2000 par Lionel Chetwynd.
    Le film «La veuve de Saint-Pierre» (le 734e de la liste) a été réalisé en 2000 par Patrice Leconte.
    Le film «La vie après l'amour» (le 735e de la liste) a été réalisé en 2000 par Gabriel Pelletier.
    Le film «Xchange» (le 736e de la liste) a été réalisé en 2000 par Allan Moyle.
    Le film «15 février 1839» (le 737e de la liste) a été réalisé en 2001 par Pierre Falardeau.
    Le film «L'ange de goudron» (le 738e de la liste) a été réalisé en 2001 par Denis Chouinard.
    Le film «Les Boys 3» (le 739e de la liste) a été réalisé en 2001 par Louis Saïa.
    Le film «Des chiens dans la neige» (le 740e de la liste) a été réalisé en 2001 par Michel Welterlin.
    Le film «Le ciel sur la tête» (le 741e de la liste) a été réalisé en 2001 par Geneviève Lefebvre, André Melançon.
    Le film «Crème glacée, chocolat et autres consolations» (le 742e de la liste) a été réalisé en 2001 par Julie Hivon.
    Le film «Danny in the Sky» (le 743e de la liste) a été réalisé en 2001 par Denis Langlois.
    Le film «Dead Awake» (le 744e de la liste) a été réalisé en 2001 par Marc S. Grenier.
    Le film «Du pic au cœur» (le 745e de la liste) a été réalisé en 2001 par Céline Baril.
    Le film «La femme qui boit» (le 746e de la liste) a été réalisé en 2001 par Bernard Émond.
    Le film «La forteresse suspendue» (le 747e de la liste) a été réalisé en 2001 par Roger Cantin.
    Le film «Hidden Agenda» (le 748e de la liste) a été réalisé en 2001 par Marc S. Grenier.
    Le film «Hôtel des horizons» (le 749e de la liste) a été réalisé en 2001 par Marc Cayer.
    Le film «Karmen Geï» (le 750e de la liste) a été réalisé en 2001 par Joseph Gaï Ramaka.
    Le film «Karmina 2» (le 751e de la liste) a été réalisé en 2001 par Gabriel Pelletier.
    Le film «La loi du cochon» (le 752e de la liste) a été réalisé en 2001 par Érik Canuel.
    Le film «Lost and Delirious» (le 753e de la liste) a été réalisé en 2001 par Léa Pool.
    Le film «Mariages» (le 754e de la liste) a été réalisé en 2001 par Catherine Martin.
    Le film «Nuit de noces» (le 755e de la liste) a été réalisé en 2001 par Émile Gaudreault.
    Le film «Opération Cobra» (le 756e de la liste) a été réalisé en 2001 par Dominic Gagnon, Richard Jutras, Robert Morin.
    Le film «Le pornographe» (le 757e de la liste) a été réalisé en 2001 par Bertrand Bonello.
    Le film «Protection» (le 758e de la liste) a été réalisé en 2001 par John Flynn.
    Le film «So Faraway and Blue» (le 759e de la liste) a été réalisé en 2001 par Roy Cross.
    Le film «Stéphanie, Nathalie, Caroline et Vincent» (le 760e de la liste) a été réalisé en 2001 par Carl Ulrich.
    Le film «Un crabe dans la tête» (le 761e de la liste) a été réalisé en 2001 par André Turpin.
    Le film «Une jeune fille à la fenêtre» (le 762e de la liste) a été réalisé en 2001 par Francis Leclerc.
    Le film «La vérité est un mensonge» (le 763e de la liste) a été réalisé en 2001 par Pierre Goupil.
    Le film «Au fil de l'eau» (le 764e de la liste) a été réalisé en 2002 par Jeannine Gagné.
    Le film «Barbaloune» (le 765e de la liste) a été réalisé en 2002 par Jean Gagné, Serge Gagné.
    Le film «The Baroness and the Pig» (le 766e de la liste) a été réalisé en 2002 par Michael MacKenzie.
    Le film «Between Strangers» (le 767e de la liste) a été réalisé en 2002 par Edoardo Ponti.
    Le film «Le collectionneur» (le 768e de la liste) a été réalisé en 2002 par Jean Beaudin.
    Le film «Les Dangereux» (le 769e de la liste) a été réalisé en 2002 par Louis Saïa.
    Le film «Les Fils de Marie» (le 770e de la liste) a été réalisé en 2002 par Carole Laure.
    Le film «Le gambit du fou» (le 771e de la liste) a été réalisé en 2002 par Bruno Dubuc.
    Le film «Histoire de pen» (le 772e de la liste) a été réalisé en 2002 par Michel Jetté.
    Le film «Home» (le 773e de la liste) a été réalisé en 2002 par Phyllis Katrapani.
    Le film «Iso» (le 774e de la liste) a été réalisé en 2002 par Dominic Gagnon.
    Le film «Katryn's Place» (le 775e de la liste) a été réalisé en 2002 par Bénédicte Ronfard.
    Le film «Leaving Metropolis» (le 776e de la liste) a été réalisé en 2002 par Brad Fraser.
    Le film «Madame Brouette» (le 777e de la liste) a été réalisé en 2002 par Moussa Sene Absa.
    Le film «Le marais» (le 778e de la liste) a été réalisé en 2002 par Kim Nguyen.
    Le film «La Mystérieuse Mademoiselle C.» (le 779e de la liste) a été réalisé en 2002 par Richard Ciupka.
    Le film «Le Nèg'» (le 780e de la liste) a été réalisé en 2002 par Robert Morin.
    Le film «L'Odyssée d'Alice Tremblay» (le 781e de la liste) a été réalisé en 2002 par Denise Filiatrault.
    Le film «One Way Out» (le 782e de la liste) a été réalisé en 2002 par Allan A. Goldstein.
    Le film «Québec-Montréal» (le 783e de la liste) a été réalisé en 2002 par Ricardo Trogi.
    Le film «Régina!» (le 784e de la liste) a été réalisé en 2002 par Maria Sigurdardottir.
    Le film «Royal Bonbon» (le 785e de la liste) a été réalisé en 2002 par Charles Najman.
    Le film «Savage Messiah» (le 786e de la liste) a été réalisé en 2002 par Mario Azzopardi.
    Le film «Secret de banlieue» (le 787e de la liste) a été réalisé en 2002 par Louis Choquette.
    Le film «Séraphin : un homme et son péché» (le 788e de la liste) a été réalisé en 2002 par Charles Binamé.
    Le film «Sex at the End of the Millenium» (le 789e de la liste) a été réalisé en 2002 par Sheldon Neuberger.
    Le film «Station Nord» (le 790e de la liste) a été réalisé en 2002 par Jean-Claude Lord.
    Le film «Summer» (le 791e de la liste) a été réalisé en 2002 par Phil Price.
    Le film «La turbulence des fluides» (le 792e de la liste) a été réalisé en 2002 par Manon Briand.
    Le film «Women Without Wings» (le 793e de la liste) a été réalisé en 2002 par Nicholas Kinsey.
    Le film «Yellowknife» (le 794e de la liste) a été réalisé en 2002 par Rodrigue Jean.
    Le film «100 % bio» (le 795e de la liste) a été réalisé en 2003 par Claude Fortin.
    Le film «20h17, rue Darling» (le 796e de la liste) a été réalisé en 2003 par Bernard Émond.
    Le film «Annie Brocoli dans les fonds marins» (le 797e de la liste) a été réalisé en 2003 par Claude Brie.
    Le film «The Book of Eve» (le 798e de la liste) a été réalisé en 2003 par Claude Fournier.
    Le film «Comment ma mère accoucha de moi durant sa ménopause» (le 799e de la liste) a été réalisé en 2003 par Sébastien Rose.
    Le film «La face cachée de la lune» (le 800e de la liste) a été réalisé en 2003 par Robert Lepage.
    Le film «The Favourite Game» (le 801e de la liste) a été réalisé en 2003 par Bernar Hébert.
    Le film «Gaz Bar Blues» (le 802e de la liste) a été réalisé en 2003 par Louis Bélanger.
    Le film «La grande séduction» (le 803e de la liste) a été réalisé en 2003 par Jean-François Pouliot.
    Le film «L'homme trop pressé prend son thé à la fourchette» (le 804e de la liste) a été réalisé en 2003 par Sylvie Groulx.
    Le film «Les immortels» (le 805e de la liste) a été réalisé en 2003 par Paul Thinel.
    Le film «Les invasions barbares» (le 806e de la liste) a été réalisé en 2003 par Denys Arcand.
    Le film «Jack Paradise» (le 807e de la liste) a été réalisé en 2003 par Gilles Noël.
    Le film «Ma voisine danse le ska» (le 808e de la liste) a été réalisé en 2003 par Nathalie St-Pierre.
    Le film «Mambo Italiano» (le 809e de la liste) a été réalisé en 2003 par Émile Gaudreault.
    Le film «Le manuscrit érotique» (le 810e de la liste) a été réalisé en 2003 par Jean Pierre Lefebvre.
    Le film «Nez rouge» (le 811e de la liste) a été réalisé en 2003 par Érik Canuel.
    Le film «Père et fils» (le 812e de la liste) a été réalisé en 2003 par Michel Boujenah.
    Le film «La petite Lili» (le 813e de la liste) a été réalisé en 2003 par Claude Miller.
    Le film «Le piège d'Issoudun» (le 814e de la liste) a été réalisé en 2003 par Micheline Lanctôt.
    Le film «Revival Blues» (le 815e de la liste) a été réalisé en 2003 par Claude Gagnon.
    Le film «Saved by the Belles» (le 816e de la liste) a été réalisé en 2003 par Ziad Touma.
    Le film «Le secret de Cyndia» (le 817e de la liste) a été réalisé en 2003 par Denyse Benoît.
    Le film «A Silent Love» (le 818e de la liste) a été réalisé en 2003 par Federico Hidalgo.
    Le film «Sur le seuil» (le 819e de la liste) a été réalisé en 2003 par Éric Tessier.
    Le film «Les triplettes de Belleville» (le 820e de la liste) a été réalisé en 2003 par Sylvain Chomet.
    Le film «Twist» (le 821e de la liste) a été réalisé en 2003 par Jacob Tierney.
    Le film «Acapulco Gold» (le 822e de la liste) a été réalisé en 2004 par André Forcier.
    Le film «Les aimants» (le 823e de la liste) a été réalisé en 2004 par Yves P. Pelletier.
    Le film «The Blue Butterfly» (le 824e de la liste) a été réalisé en 2004 par Léa Pool.
    Le film «Le bonheur c'est une chanson triste» (le 825e de la liste) a été réalisé en 2004 par François Delisle.
    Le film «Bonzaïon» (le 826e de la liste) a été réalisé en 2004 par Danny Gilmore, Clermont Jolicoeur.
    Le film «C'est pas moi… c'est l'autre» (le 827e de la liste) a été réalisé en 2004 par Alain Zaloum.
    Le film «Camping sauvage» (le 828e de la liste) a été réalisé en 2004 par Guy A. Lepage, Sylvain Roy.
    Le film «Comment conquérir l'Amérique en une nuit?» (le 829e de la liste) a été réalisé en 2004 par Dany Laferrière.
    Le film «Comment devenir un trou de cul et enfin plaire aux femmes» (le 830e de la liste) a été réalisé en 2004 par Roger Boire.
    Le film «CQ2 (Seek You Too)» (le 831e de la liste) a été réalisé en 2004 par Carole Laure.
    Le film «Dans l'œil du chat» (le 832e de la liste) a été réalisé en 2004 par Rudy Barichello.
    Le film «Dans une galaxie près de chez vous» (le 833e de la liste) a été réalisé en 2004 par Claude Desrosiers.
    Le film «Le dernier tunnel» (le 834e de la liste) a été réalisé en 2004 par Érik Canuel.
    Le film «Dorian» (le 835e de la liste) a été réalisé en 2004 par Allan A. Goldstein.
    Le film «Elles étaient cinq» (le 836e de la liste) a été réalisé en 2004 par Ghyslaine Côté.
    Le film «Elvis Gratton XXX : la vengeance d'Elvis Wong» (le 837e de la liste) a été réalisé en 2004 par Pierre Falardeau.
    Le film «L'espérance» (le 838e de la liste) a été réalisé en 2004 par Stefan Pleszczynski.
    Le film «Éternelle» (le 839e de la liste) a été réalisé en 2004 par Wilhelm Liebenberg.
    Le film «Folle embellie» (le 840e de la liste) a été réalisé en 2004 par Dominique Cabrera.
    Le film «Goldirocks» (le 841e de la liste) a été réalisé en 2004 par Paula Tiberius.
    Le film «Le golem de Montréal» (le 842e de la liste) a été réalisé en 2004 par Isabelle Hayeur.
    Le film «Le goût des jeunes filles» (le 843e de la liste) a été réalisé en 2004 par John L'Écuyer.
    Le film «L'incomparable mademoiselle C.» (le 844e de la liste) a été réalisé en 2004 par Richard Ciupka.
    Le film «Je n'aime que toi» (le 845e de la liste) a été réalisé en 2004 par Claude Fournier.
    Le film «Jeunesse dans l'ombre» (le 846e de la liste) a été réalisé en 2004 par Gervais Germain.
    Le film «Jimmywork» (le 847e de la liste) a été réalisé en 2004 par Simon Sauvé.
    Le film «Littoral» (le 848e de la liste) a été réalisé en 2004 par Wajdi Mouawad.
    Le film «La lune viendra d'elle-même» (le 849e de la liste) a été réalisé en 2004 par Marie-Jan Seille.
    Le film «Ma vie en cinémascope» (le 850e de la liste) a été réalisé en 2004 par Denise Filiatrault.
    Le film «Mémoires affectives» (le 851e de la liste) a été réalisé en 2004 par Francis Leclerc.
    Le film «Monica la mitraille» (le 852e de la liste) a été réalisé en 2004 par Pierre Houle.
    Le film «Les moscovites» (le 853e de la liste) a été réalisé en 2004 par Charles Barabé.
    Le film «Nouvelle-France» (le 854e de la liste) a été réalisé en 2004 par Jean Beaudin.
    Le film «Ordo» (le 855e de la liste) a été réalisé en 2004 par Laurence Ferreira Barbosa.
    Le film «La peau blanche» (le 856e de la liste) a été réalisé en 2004 par Daniel Roby.
    Le film «La pension des étranges» (le 857e de la liste) a été réalisé en 2004 par Stella Goulet.
    Le film «Pinocchio 3000» (le 858e de la liste) a été réalisé en 2004 par Daniel Robichaud.
    Le film «La planque» (le 859e de la liste) a été réalisé en 2004 par Alexandre Chartrand.
    Le film «Premier juillet - le film» (le 860e de la liste) a été réalisé en 2004 par Philippe Gagnon.
    Le film «Pure» (le 861e de la liste) a été réalisé en 2004 par Jim Donovan.
    Le film «Vendus» (le 862e de la liste) a été réalisé en 2004 par Éric Tessier.
    Le film «Amnésie - L'Énigme James Brighton» (le 863e de la liste) a été réalisé en 2005 par Denis Langlois.
    Le film «L'audition» (le 864e de la liste) a été réalisé en 2005 par Luc Picard.
    Le film «Aurore» (le 865e de la liste) a été réalisé en 2005 par Luc Dionne.
    Le film «Barmaids» (le 866e de la liste) a été réalisé en 2005 par Simon Boisvert.
    Le film «Les Boys 4» (le 867e de la liste) a été réalisé en 2005 par George Mihalka.
    Le film «C.R.A.Z.Y.» (le 868e de la liste) a été réalisé en 2005 par Jean-Marc Vallée.
    Le film «Le chalet» (le 869e de la liste) a été réalisé en 2005 par Jarrett Mann.
    Le film «Daniel et les Superdogs» (le 870e de la liste) a été réalisé en 2005 par André Melançon.
    Le film «La dernière incarnation» (le 871e de la liste) a été réalisé en 2005 par Demian Fuica.
    Le film «Les états nordiques» (le 872e de la liste) a été réalisé en 2005 par Denis Côté.
    Le film «Les États-Unis d'Albert» (le 873e de la liste) a été réalisé en 2005 par André Forcier.
    Le film «Familia» (le 874e de la liste) a été réalisé en 2005 par Louise Archambault.
    Le film «Greg & Gentillon» (le 875e de la liste) a été réalisé en 2005 par Matthiew Klinck.
    Le film «Horloge biologique» (le 876e de la liste) a été réalisé en 2005 par Ricardo Trogi.
    Le film «Idole instantanée» (le 877e de la liste) a été réalisé en 2005 par Yves Desgagnés.
    Le film «Kamataki» (le 878e de la liste) a été réalisé en 2005 par Claude Gagnon.
    Le film «Latex» (le 879e de la liste) a été réalisé en 2005 par Pierre Lapointe.
    Le film «Maman Last Call» (le 880e de la liste) a été réalisé en 2005 par François Bouvier.
    Le film «Manners of Dying» (le 881e de la liste) a été réalisé en 2005 par Jeremy Peter Allen.
    Le film «Maurice Richard» (le 882e de la liste) a été réalisé en 2005 par Charles Binamé.
    Le film «Les moutons de Jacob» (le 883e de la liste) a été réalisé en 2005 par Jean-François Pothier.
    Le film «La neuvaine» (le 884e de la liste) a été réalisé en 2005 par Bernard Émond.
    Le film «Niagara Motel» (le 885e de la liste) a été réalisé en 2005 par Gary Yates.
    Le film «Petit pow! pow! Noël» (le 886e de la liste) a été réalisé en 2005 par Robert Morin.
    Le film «La pharmacie de l'espoir» (le 887e de la liste) a été réalisé en 2005 par François Gourd.
    Le film «Saint-Ralph» (le 888e de la liste) a été réalisé en 2005 par Michael McGowan.
    Le film «Saints-Martyrs-des-Damnés» (le 889e de la liste) a été réalisé en 2005 par Robin Aubert.
    Le film «Stories of a Gravedigger» (le 890e de la liste) a été réalisé en 2005 par David Aubin.
    Le film «Summer with the Ghosts» (le 891e de la liste) a été réalisé en 2005 par Bernd Neuburger.
    Le film «Le survenant» (le 892e de la liste) a été réalisé en 2005 par Érik Canuel.
    Le film «Vers le sud» (le 893e de la liste) a été réalisé en 2005 par Laurent Cantet.
    Le film «La vie avec mon père» (le 894e de la liste) a été réalisé en 2005 par Sébastien Rose.
    Le film «1st bite» (le 895e de la liste) a été réalisé en 2006 par Hunt Hoe.
    Le film «The 4th Life» (le 896e de la liste) a été réalisé en 2006 par François Miron.
    Le film «L'anus Horribilis» (le 897e de la liste) a été réalisé en 2006 par Bruno Dubuc.
    Le film «La belle bête» (le 898e de la liste) a été réalisé en 2006 par Karim Hussain.
    Le film «Black Eyed Dog» (le 899e de la liste) a été réalisé en 2006 par Pierre Gang.
    Le film «Bluff sou bluff» (le 900e de la liste) a été réalisé en 2006 par Wilfort Estimable.
    Le film «Bobby» (le 901e de la liste) a été réalisé en 2006 par François Blouin.
    Le film «Bon cop bad cop» (le 902e de la liste) a été réalisé en 2006 par Érik Canuel.
    Le film «C'est beau une ville la nuit» (le 903e de la liste) a été réalisé en 2006 par Richard Bohringer.
    Le film «Cadavre exquis première édition» (le 904e de la liste) a été réalisé en 2006 par Un collectif de réalisateurs et de réalisatrices.
    Le film «Les cavaliers de la canette» (le 905e de la liste) a été réalisé en 2006 par Louis Champagne.
    Le film «Cercle vicieux» (le 906e de la liste) a été réalisé en 2006 par Patrick Hébert.
    Le film «Cheech» (le 907e de la liste) a été réalisé en 2006 par Patrice Sauvé.
    Le film «Comme tout le monde» (le 908e de la liste) a été réalisé en 2006 par Pierre-Paul Renders.
    Le film «Congorama» (le 909e de la liste) a été réalisé en 2006 par Philippe Falardeau.
    Le film «Convoitises» (le 910e de la liste) a été réalisé en 2006 par Jean Alix Holmand.
    Le film «La coupure» (le 911e de la liste) a été réalisé en 2006 par Jean Châteauvert.
    Le film «De ma fenêtre, sans maison…» (le 912e de la liste) a été réalisé en 2006 par Maryanne Zéhil.
    Le film «Délivrez-moi» (le 913e de la liste) a été réalisé en 2006 par Denis Chouinard.
    Le film «The Descendant» (le 914e de la liste) a été réalisé en 2006 par Philippe Spurrell.
    Le film «Duo» (le 915e de la liste) a été réalisé en 2006 par Richard Ciupka.
    Le film «End of the Line» (le 916e de la liste) a été réalisé en 2006 par Maurice Devereaux.
    Le film «Les filles du botaniste» (le 917e de la liste) a été réalisé en 2006 par Dai Sijie.
    Le film «Gason makoklen 1» (le 918e de la liste) a été réalisé en 2006 par Wilfort Estimable.
    Le film «Gason makoklen 2» (le 919e de la liste) a été réalisé en 2006 par Wilfort Estimable.
    Le film «Le génie du crime» (le 920e de la liste) a été réalisé en 2006 par Louis Bélanger.
    Le film «Le guide de la petite vengeance» (le 921e de la liste) a été réalisé en 2006 par Jean-François Pouliot.
    Le film «Heads of Control: The Gorul Baheu Brain Expedition» (le 922e de la liste) a été réalisé en 2006 par Pat Tremblay.
    Le film «Histoire de famille» (le 923e de la liste) a été réalisé en 2006 par Michel Poulette.
    Le film «Hiver» (le 924e de la liste) a été réalisé en 2006 par Charles Barabé.
    Le film «The Journals of Knud Rasmussen» (le 925e de la liste) a été réalisé en 2006 par Norman Cohn, Zacharias Kunuk.
    Le film «Lézaureil kilyve» (le 926e de la liste) a été réalisé en 2006 par Charles Barabé.
    Le film «Malléables journées» (le 927e de la liste) a été réalisé en 2006 par Charles Barabé.
    Le film «Manga Latina: Killer on the Loose» (le 928e de la liste) a été réalisé en 2006 par Henrique Vera-Villaneuva.
    Le film «Martin Clear: The Whole Story» (le 929e de la liste) a été réalisé en 2006 par Martin Leclerc.
    Le film «The Passenger» (le 930e de la liste) a été réalisé en 2006 par François Rotger.
    Le film «The Point» (le 931e de la liste) a été réalisé en 2006 par Joshua Dorsey.
    Le film «La porte de Fersein» (le 932e de la liste) a été réalisé en 2006 par Éric Campeau, Simon Tardif.
    Le film «Pourquoi le dire?» (le 933e de la liste) a été réalisé en 2006 par Hélène Duchesneau.
    Le film «Prenez vos places» (le 934e de la liste) a été réalisé en 2006 par Marc Thomas-Dupuis.
    Le film «Que Dieu bénisse l'Amérique» (le 935e de la liste) a été réalisé en 2006 par Robert Morin.
    Le film «La rage de l'ange» (le 936e de la liste) a été réalisé en 2006 par Dan Bigras.
    Le film «Rechercher Victor Pellerin» (le 937e de la liste) a été réalisé en 2006 par Sophie Deraspe.
    Le film «Roméo et Juliette» (le 938e de la liste) a été réalisé en 2006 par Yves Desgagnés.
    Le film «Sans elle» (le 939e de la liste) a été réalisé en 2006 par Jean Beaudin.
    Le film «Le secret de ma mère» (le 940e de la liste) a été réalisé en 2006 par Ghyslaine Côté.
    Le film «Steel Toes» (le 941e de la liste) a été réalisé en 2006 par Mark Adam, David Gow.
    Le film «Sur la trace d'Igor Rizzi» (le 942e de la liste) a été réalisé en 2006 par Noël Mitrani.
    Le film «These Girls» (le 943e de la liste) a été réalisé en 2006 par John Hazlett.
    Le film «Touche pas à mon homme» (le 944e de la liste) a été réalisé en 2006 par Jean Rony Lubin.
    Le film «Tous les autres, sauf moi» (le 945e de la liste) a été réalisé en 2006 par Ann Arson.
    Le film «Un dimanche à Kigali» (le 946e de la liste) a été réalisé en 2006 par Robert Favreau.
    Le film «Une rame à l'eau» (le 947e de la liste) a été réalisé en 2006 par Jean-François Boudreault.
    Le film «La vie secrète des gens heureux» (le 948e de la liste) a été réalisé en 2006 par Stéphane Lapointe.
    Le film «A Year in the Death of Jack Richards» (le 949e de la liste) a été réalisé en 2006 par Benjamin P. Paquette.
    Le film «Les 3 p'tits cochons» (le 950e de la liste) a été réalisé en 2007 par Patrick Huard.
    Le film «À vos marques… party!» (le 951e de la liste) a été réalisé en 2007 par Frédérik D'Amours.
    Le film «Adam's Wall» (le 952e de la liste) a été réalisé en 2007 par Michael MacKenzie.
    Le film «L'âge des ténèbres» (le 953e de la liste) a été réalisé en 2007 par Denys Arcand.
    Le film «Amour, mensonges et conséquences» (le 954e de la liste) a été réalisé en 2007 par Jean Alix Holmand.
    Le film «The Backup Man» (le 955e de la liste) a été réalisé en 2007 par Doug Sutherland.
    Le film «La belle empoisonneuse» (le 956e de la liste) a été réalisé en 2007 par Richard Jutras.
    Le film «Bluff» (le 957e de la liste) a été réalisé en 2007 par Simon-Olivier Fecteau, Marc-André Lavoie.
    Le film «Borderline» (le 958e de la liste) a été réalisé en 2007 par Lyne Charlebois.
    Le film «La brunante» (le 959e de la liste) a été réalisé en 2007 par Fernand Dansereau.
    Le film «La capture» (le 960e de la liste) a été réalisé en 2007 par Carole Laure.
    Le film «Le cèdre penché» (le 961e de la liste) a été réalisé en 2007 par Rafaël Ouellet.
    Le film «Continental, un film sans fusil» (le 962e de la liste) a été réalisé en 2007 par Stéphane Lafleur.
    Le film «Contre toute espérance» (le 963e de la liste) a été réalisé en 2007 par Bernard Émond.
    Le film «Dans les villes» (le 964e de la liste) a été réalisé en 2007 par Catherine Martin.
    Le film «De l'autre côté» (le 965e de la liste) a été réalisé en 2007 par Sean Marckos.
    Le film «Échangistes» (le 966e de la liste) a été réalisé en 2007 par Simon Boisvert.
    Le film «Emotional Arithmetic» (le 967e de la liste) a été réalisé en 2007 par Paolo Barzman.
    Le film «Imitation» (le 968e de la liste) a été réalisé en 2007 par Federico Hidalgo.
    Le film «La lâcheté» (le 969e de la liste) a été réalisé en 2007 par Marc Bisaillon.
    Le film «La logique du remords» (le 970e de la liste) a été réalisé en 2007 par Martin Laroche.
    Le film «Ma fille mon ange» (le 971e de la liste) a été réalisé en 2007 par Alexis Durand-Brault.
    Le film «Ma tante Aline» (le 972e de la liste) a été réalisé en 2007 par Gabriel Pelletier.
    Le film «Minushi» (le 973e de la liste) a été réalisé en 2007 par Tyler Gibb.
    Le film «Nitro» (le 974e de la liste) a été réalisé en 2007 par Alain DesRochers.
    Le film «Nos vies privées» (le 975e de la liste) a été réalisé en 2007 par Denis Côté.
    Le film «Nos voisins Dhantsu» (le 976e de la liste) a été réalisé en 2007 par Alain Chicoine.
    Le film «Où vas-tu, Moshé?» (le 977e de la liste) a été réalisé en 2007 par Hassan Benjelloun.
    Le film «Puffball» (le 978e de la liste) a été réalisé en 2007 par Nicolas Roeg.
    Le film «Rêves de poussière» (le 979e de la liste) a été réalisé en 2007 par Laurent Salgues.
    Le film «Le ring» (le 980e de la liste) a été réalisé en 2007 par Anaïs Barbeau-Lavalette.
    Le film «La rivière aux castors» (le 981e de la liste) a été réalisé en 2007 par Philippe Calderon.
    Le film «Shake Hands with the Devil» (le 982e de la liste) a été réalisé en 2007 par Roger Spottiswoode.
    Le film «Silk» (le 983e de la liste) a été réalisé en 2007 par François Girard.
    Le film «Steak» (le 984e de la liste) a été réalisé en 2007 par Quentin Dupieux.
    Le film «Surviving My Mother» (le 985e de la liste) a été réalisé en 2007 par Émile Gaudreault.
    Le film «Toi» (le 986e de la liste) a été réalisé en 2007 par François Delisle.
    Le film «Un cri au bonheur» (le 987e de la liste) a été réalisé en 2007 par Un collectif de réalisateurs et de réalisatrices.
    Le film «Voleurs de chevaux» (le 988e de la liste) a été réalisé en 2007 par Micha Wald.
    Le film «Young Triffie» (le 989e de la liste) a été réalisé en 2007 par Mary Walsh.
    Le film «24 mesures» (le 990e de la liste) a été réalisé en 2008 par Jalil Lespert.
    Le film «À l'ouest de Pluton» (le 991e de la liste) a été réalisé en 2008 par Henry Bernadet, Myriam Verreault.
    Le film «Adagio pour un gars de bicycle» (le 992e de la liste) a été réalisé en 2008 par Pascale Ferland.
    Le film «Amour, destin et rock'n'roll» (le 993e de la liste) a été réalisé en 2008 par Dario Gasbarro.
    Le film «Babine» (le 994e de la liste) a été réalisé en 2008 par Luc Picard.
    Le film «Le banquet» (le 995e de la liste) a été réalisé en 2008 par Sébastien Rose.
    Le film «Before Tomorrow» (le 996e de la liste) a été réalisé en 2008 par Marie-Hélène Cousineau, Madeline Ivalu.
    Le film «C'est pas moi, je le jure!» (le 997e de la liste) a été réalisé en 2008 par Philippe Falardeau.
    Le film «Le cas Roberge» (le 998e de la liste) a été réalisé en 2008 par Raphaël Malo.
    Le film «Ce qu'il faut pour vivre» (le 999e de la liste) a été réalisé en 2008 par Benoît Pilon.
    Le film «Cruising Bar 2» (le 1000e de la liste) a été réalisé en 2008 par Michel Côté, Robert Ménard.
    Le film «Dans une galaxie près de chez vous 2» (le 1001e de la liste) a été réalisé en 2008 par Philippe Gagnon.
    Le film «Demain» (le 1002e de la liste) a été réalisé en 2008 par Maxime Giroux.
    Le film «Derrière moi» (le 1003e de la liste) a été réalisé en 2008 par Rafaël Ouellet.
    Le film «Le déserteur» (le 1004e de la liste) a été réalisé en 2008 par Simon Lavoie.
    Le film «Elle veut le chaos» (le 1005e de la liste) a été réalisé en 2008 par Denis Côté.
    Le film «En plein cœur» (le 1006e de la liste) a été réalisé en 2008 par Stéphane Géhami.
    Le film «Le grand départ» (le 1007e de la liste) a été réalisé en 2008 par Claude Meunier.
    Le film «La ligne brisée» (le 1008e de la liste) a été réalisé en 2008 par Louis Choquette.
    Le film «Magique!» (le 1009e de la liste) a été réalisé en 2008 par Philippe Muyl.
    Le film «Maman est chez le coiffeur» (le 1010e de la liste) a été réalisé en 2008 par Léa Pool.
    Le film «Papa à la chasse aux lagopèdes» (le 1011e de la liste) a été réalisé en 2008 par Robert Morin.
    Le film «Le piège américain» (le 1012e de la liste) a été réalisé en 2008 par Charles Binamé.
    Le film «Les plus beaux yeux du monde» (le 1013e de la liste) a été réalisé en 2008 par Pascal Courchesne, Charlotte Laurier.
    Le film «Restless» (le 1014e de la liste) a été réalisé en 2008 par Amos Kollek.
    Le film «Suivre Catherine» (le 1015e de la liste) a été réalisé en 2008 par Jeanne Crépeau.
    Le film «Tout est parfait» (le 1016e de la liste) a été réalisé en 2008 par Yves-Christian Fournier.
    Le film «Truffe» (le 1017e de la liste) a été réalisé en 2008 par Kim Nguyen.
    Le film «Un capitalisme sentimental» (le 1018e de la liste) a été réalisé en 2008 par Olivier Asselin.
    Le film «Un été sans point ni coup sûr» (le 1019e de la liste) a été réalisé en 2008 par Francis Leclerc.
    Le film «Wushu Warrior» (le 1020e de la liste) a été réalisé en 2008 par Alain DesRochers.
    Le film «1981» (le 1021e de la liste) a été réalisé en 2009 par Ricardo Trogi.
    Le film «3 saisons» (le 1022e de la liste) a été réalisé en 2009 par Jim Donovan.
    Le film «40 is the new 20» (le 1023e de la liste) a été réalisé en 2009 par Simon Boisvert.
    Le film «5150, rue des Ormes» (le 1024e de la liste) a été réalisé en 2009 par Éric Tessier.
    Le film «À quelle heure le train pour nulle part» (le 1025e de la liste) a été réalisé en 2009 par Robin Aubert.
    Le film «À vos marques… party! 2» (le 1026e de la liste) a été réalisé en 2009 par Frédérik D'Amours.
    Le film «Le bonheur de Pierre» (le 1027e de la liste) a été réalisé en 2009 par Robert Ménard.
    Le film «Cadavres» (le 1028e de la liste) a été réalisé en 2009 par Érik Canuel.
    Le film «Carcasses» (le 1029e de la liste) a été réalisé en 2009 par Denis Côté.
    Le film «La chambre noire» (le 1030e de la liste) a été réalisé en 2009 par François Aubry.
    Le film «De père en flic» (le 1031e de la liste) a été réalisé en 2009 par Émile Gaudreault.
    Le film «Dédé à travers les brumes» (le 1032e de la liste) a été réalisé en 2009 par Jean-Philippe Duval.
    Le film «Détour» (le 1033e de la liste) a été réalisé en 2009 par Sylvain Guy.
    Le film «Les doigts croches» (le 1034e de la liste) a été réalisé en 2009 par Ken Scott.
    Le film «La donation» (le 1035e de la liste) a été réalisé en 2009 par Bernard Émond.
    Le film «Grande Ourse - La clé des possibles» (le 1036e de la liste) a été réalisé en 2009 par Patrice Sauvé.
    Le film «Les grandes chaleurs» (le 1037e de la liste) a été réalisé en 2009 par Sophie Lorain.
    Le film «Impasse» (le 1038e de la liste) a été réalisé en 2009 par Joël Gauthier.
    Le film «J'ai tué ma mère» (le 1039e de la liste) a été réalisé en 2009 par Xavier Dolan.
    Le film «Je me souviens» (le 1040e de la liste) a été réalisé en 2009 par André Forcier.
    Le film «Lost Song» (le 1041e de la liste) a été réalisé en 2009 par Rodrigue Jean.
    Le film «Love and Savagery» (le 1042e de la liste) a été réalisé en 2009 par John N. Smith.
    Le film «Martyrs» (le 1043e de la liste) a été réalisé en 2009 par Pascal Laugier.
    Le film «Modern Love» (le 1044e de la liste) a été réalisé en 2009 par Stéphane Kazandjian.
    Le film «Noémie le secret» (le 1045e de la liste) a été réalisé en 2009 par Frédérik D'Amours.
    Le film «Nuages sur la ville» (le 1046e de la liste) a été réalisé en 2009 par Simon Galiero.
    Le film «Les pieds dans le vide» (le 1047e de la liste) a été réalisé en 2009 par Mariloup Wolfe.
    Le film «Polytechnique» (le 1048e de la liste) a été réalisé en 2009 par Denis Villeneuve.
    Le film «Pour toujours les Canadiens» (le 1049e de la liste) a été réalisé en 2009 par Sylvain Archambault.
    Le film «Refrain» (le 1050e de la liste) a été réalisé en 2009 par Tyler Gibb.
    Le film «Sans dessein» (le 1051e de la liste) a été réalisé en 2009 par Caroline Labrèche, Steeve Léonard.
    Le film «Serveuses demandées» (le 1052e de la liste) a été réalisé en 2009 par Guylaine Dionne.
    Le film «Suzie» (le 1053e de la liste) a été réalisé en 2009 par Micheline Lanctôt.
    Le film «The Timekeeper» (le 1054e de la liste) a été réalisé en 2009 par Louis Bélanger.
    Le film «Transit» (le 1055e de la liste) a été réalisé en 2009 par Christian de la Cortina.
    Le film «Turbid» (le 1056e de la liste) a été réalisé en 2009 par George Fok.
    Le film «Un ange à la mer» (le 1057e de la liste) a été réalisé en 2009 par Frédéric Dumont.
    Le film «Un cargo pour l'Afrique» (le 1058e de la liste) a été réalisé en 2009 par Roger Cantin.
    Le film «10 1/2» (le 1059e de la liste) a été réalisé en 2010 par Podz.
    Le film «2 fois une femme» (le 1060e de la liste) a été réalisé en 2010 par François Delisle.
    Le film «2 frogs dans l'Ouest» (le 1061e de la liste) a été réalisé en 2010 par Dany Papineau.
    Le film «À l'origine d'un cri» (le 1062e de la liste) a été réalisé en 2010 par Robin Aubert.
    Le film «Les amours imaginaires» (le 1063e de la liste) a été réalisé en 2010 par Xavier Dolan.
    Le film «L'appât» (le 1064e de la liste) a été réalisé en 2010 par Yves Simoneau.
    Le film «Le baiser du barbu» (le 1065e de la liste) a été réalisé en 2010 par Yves P. Pelletier.
    Le film «Cabotins» (le 1066e de la liste) a été réalisé en 2010 par Alain DesRochers.
    Le film «La cité» (le 1067e de la liste) a été réalisé en 2010 par Kim Nguyen.
    Le film «Curling» (le 1068e de la liste) a été réalisé en 2010 par Denis Côté.
    Le film «La dernière fugue» (le 1069e de la liste) a été réalisé en 2010 par Léa Pool.
    Le film «L'enfant prodige» (le 1070e de la liste) a été réalisé en 2010 par Luc Dionne.
    Le film «Everywhere» (le 1071e de la liste) a été réalisé en 2010 par Alexis Durand-Brault.
    Le film «Filière 13» (le 1072e de la liste) a été réalisé en 2010 par Patrick Huard.
    Le film «Incendies» (le 1073e de la liste) a été réalisé en 2010 par Denis Villeneuve.
    Le film «Le journal d'Aurélie Laflamme» (le 1074e de la liste) a été réalisé en 2010 par Christian Laurence.
    Le film «Journal d'un coopérant» (le 1075e de la liste) a été réalisé en 2010 par Robert Morin.
    Le film «Lance et compte» (le 1076e de la liste) a été réalisé en 2010 par Frédérik D'Amours.
    Le film «Lucidité passagère» (le 1077e de la liste) a été réalisé en 2010 par Fabrice Barrilliet.
    Le film «Mesrine - L'instinct de mort» (le 1078e de la liste) a été réalisé en 2010 par Jean-François Richet.
    Le film «Les mots gelés» (le 1079e de la liste) a été réalisé en 2010 par Isabelle D'Amours.
    Le film «New Denmark» (le 1080e de la liste) a été réalisé en 2010 par Rafaël Ouellet.
    Le film «Oscar et la dame rose» (le 1081e de la liste) a été réalisé en 2010 par Éric-Emmanuel Schmitt.
    Le film «Piché : entre ciel et terre» (le 1082e de la liste) a été réalisé en 2010 par Sylvain Archambault.
    Le film «Le poil de la bête» (le 1083e de la liste) a été réalisé en 2010 par Philippe Gagnon.
    Le film «Reste avec moi» (le 1084e de la liste) a été réalisé en 2010 par Robert Ménard.
    Le film «Romaine par moins 30» (le 1085e de la liste) a été réalisé en 2010 par Agnès Obadia.
    Le film «Route 132» (le 1086e de la liste) a été réalisé en 2010 par Louis Bélanger.
    Le film «Les sept jours du talion» (le 1087e de la liste) a été réalisé en 2010 par Podz.
    Le film «Les signes vitaux» (le 1088e de la liste) a été réalisé en 2010 par Sophie Deraspe.
    Le film «Snow & Ashes» (le 1089e de la liste) a été réalisé en 2010 par Charles-Olivier Michaud.
    Le film «Sortie 67» (le 1090e de la liste) a été réalisé en 2010 par Jephté Bastien.
    Le film «Trois temps après la mort d'Anna» (le 1091e de la liste) a été réalisé en 2010 par Catherine Martin.
    Le film «Tromper le silence» (le 1092e de la liste) a été réalisé en 2010 par Julie Hivon.
    Le film «The Trotsky» (le 1093e de la liste) a été réalisé en 2010 par Jacob Tierney.
    Le film «The Wild Hunt» (le 1094e de la liste) a été réalisé en 2010 par Alexandre Franchi.
    Le film «Y'en aura pas de facile» (le 1095e de la liste) a été réalisé en 2010 par Marc-André Lavoie.
    Le film «À trois, Marie s'en va» (le 1096e de la liste) a été réalisé en 2011 par Anne-Marie Ngô.
    Le film «Angle mort» (le 1097e de la liste) a été réalisé en 2011 par Dominic James.
    Le film «Le bonheur des autres» (le 1098e de la liste) a été réalisé en 2011 par Jean-Philippe Pearson.
    Le film «BumRush» (le 1099e de la liste) a été réalisé en 2011 par Michel Jetté.
    Le film «Café de Flore» (le 1100e de la liste) a été réalisé en 2011 par Jean-Marc Vallée.
    Le film «Le colis» (le 1101e de la liste) a été réalisé en 2011 par Gaël D'Ynglemare.
    Le film «Coteau Rouge» (le 1102e de la liste) a été réalisé en 2011 par André Forcier.
    Le film «Décharge» (le 1103e de la liste) a été réalisé en 2011 par Benoît Pilon.
    Le film «Die» (le 1104e de la liste) a été réalisé en 2011 par Dominic James.
    Le film «Le divan du monde» (le 1105e de la liste) a été réalisé en 2011 par Dominic Desjardins.
    Le film «En terrains connus» (le 1106e de la liste) a été réalisé en 2011 par Stéphane Lafleur.
    Le film «La fille de Montréal» (le 1107e de la liste) a été réalisé en 2011 par Jeanne Crépeau.
    Le film «French Immersion» (le 1108e de la liste) a été réalisé en 2011 par Kevin Tierney.
    Le film «French Kiss» (le 1109e de la liste) a été réalisé en 2011 par Sylvain Archambault.
    Le film «Frisson des collines» (le 1110e de la liste) a été réalisé en 2011 par Richard Roy.
    Le film «Funkytown» (le 1111e de la liste) a été réalisé en 2011 par Daniel Roby.
    Le film «Gerry» (le 1112e de la liste) a été réalisé en 2011 par Alain DesRochers.
    Le film «Good Neighbours» (le 1113e de la liste) a été réalisé en 2011 par Jacob Tierney.
    Le film «The High Cost of Living» (le 1114e de la liste) a été réalisé en 2011 par Deborah Chow.
    Le film «Jaloux» (le 1115e de la liste) a été réalisé en 2011 par Patrick Demers.
    Le film «Jo pour Jonathan» (le 1116e de la liste) a été réalisé en 2011 par Maxime Giroux.
    Le film «The Kate Logan Affair» (le 1117e de la liste) a été réalisé en 2011 par Noël Mitrani.
    Le film «Laurentie» (le 1118e de la liste) a été réalisé en 2011 par Mathieu Denis, Simon Lavoie.
    Le film «Marécages» (le 1119e de la liste) a été réalisé en 2011 par Guy Édoin.
    Le film «Monsieur Lazhar» (le 1120e de la liste) a été réalisé en 2011 par Philippe Falardeau.
    Le film «Nuit #1» (le 1121e de la liste) a été réalisé en 2011 par Anne Émond.
    Le film «Pour l'amour de Dieu» (le 1122e de la liste) a été réalisé en 2011 par Micheline Lanctôt.
    Le film «La run» (le 1123e de la liste) a été réalisé en 2011 par Demian Fuica.
    Le film «Le sens de l'humour» (le 1124e de la liste) a été réalisé en 2011 par Émile Gaudreault.
    Le film «Shadowboxing» (le 1125e de la liste) a été réalisé en 2011 par Jesse Klein.
    Le film «Starbuck» (le 1126e de la liste) a été réalisé en 2011 par Ken Scott.
    Le film «Sur le rythme» (le 1127e de la liste) a été réalisé en 2011 par Charles-Olivier Michaud.
    Le film «Une vie qui commence» (le 1128e de la liste) a été réalisé en 2011 par Michel Monty.
    Le film «Le vendeur» (le 1129e de la liste) a été réalisé en 2011 par Sébastien Pilote.
    Le film «La vérité» (le 1130e de la liste) a été réalisé en 2011 par Marc Bisaillon.
    Le film «The Year Dolly Parton Was My Mom» (le 1131e de la liste) a été réalisé en 2011 par Tara Johns.
    Le film «L'affaire Dumont» (le 1132e de la liste) a été réalisé en 2012 par Podz.
    Le film «Après la neige» (le 1133e de la liste) a été réalisé en 2012 par Paul Barbeau.
    Le film «Avant que mon cœur bascule» (le 1134e de la liste) a été réalisé en 2012 par Sébastien Rose.
    Le film «Bestiaire» (le 1135e de la liste) a été réalisé en 2012 par Denis Côté.
    Le film «Camion» (le 1136e de la liste) a été réalisé en 2012 par Rafaël Ouellet.
    Le film «Columbarium» (le 1137e de la liste) a été réalisé en 2012 par Steve Kerr.
    Le film «L'empire Bo$$é» (le 1138e de la liste) a été réalisé en 2012 par Claude Desrosiers.
    Le film «Ésimésac» (le 1139e de la liste) a été réalisé en 2012 par Luc Picard.
    Le film «The Girl in the White Coat» (le 1140e de la liste) a été réalisé en 2012 par Darrell Wasyk.
    Le film «The Hat Goes Wild» (le 1141e de la liste) a été réalisé en 2012 par Guy Sprung.
    Le film «Hors les murs» (le 1142e de la liste) a été réalisé en 2012 par David Lambert.
    Le film «Inch'Allah» (le 1143e de la liste) a été réalisé en 2012 par Anaïs Barbeau-Lavalette.
    Le film «L'incrédule» (le 1144e de la liste) a été réalisé en 2012 par Federico Hidalgo.
    Le film «J'espère que tu vas bien» (le 1145e de la liste) a été réalisé en 2012 par David La Haye.
    Le film «Karakara» (le 1146e de la liste) a été réalisé en 2012 par Claude Gagnon.
    Le film «Laurence Anyways» (le 1147e de la liste) a été réalisé en 2012 par Xavier Dolan.
    Le film «Liverpool» (le 1148e de la liste) a été réalisé en 2012 par Manon Briand.
    Le film «Mars et Avril» (le 1149e de la liste) a été réalisé en 2012 par Martin Villeneuve.
    Le film «Mesnak» (le 1150e de la liste) a été réalisé en 2012 par Yves Sioui Durand.
    Le film «La mise à l'aveugle» (le 1151e de la liste) a été réalisé en 2012 par Simon Galiero.
    Le film «Omertà» (le 1152e de la liste) a été réalisé en 2012 par Luc Dionne.
    Le film «Les pee-wee 3D» (le 1153e de la liste) a été réalisé en 2012 par Éric Tessier.
    Le film «La peur de l'eau» (le 1154e de la liste) a été réalisé en 2012 par Gabriel Pelletier.
    Le film «Rebelle» (le 1155e de la liste) a été réalisé en 2012 par Kim Nguyen.
    Le film «Roméo Onze» (le 1156e de la liste) a été réalisé en 2012 par Ivan Grbovic.
    Le film «Thanatomorphose» (le 1157e de la liste) a été réalisé en 2012 par Éric Falardeau.
    Le film «Le torrent» (le 1158e de la liste) a été réalisé en 2012 par Simon Lavoie.
    Le film «Tout ce que tu possèdes» (le 1159e de la liste) a été réalisé en 2012 par Bernard Émond.
    Le film «Une bouteille dans la mer de Gaza» (le 1160e de la liste) a été réalisé en 2012 par Thierry Binisti.
    Le film «La vallée des larmes» (le 1161e de la liste) a été réalisé en 2012 par Maryanne Zéhil.
    Le film «1er amour» (le 1162e de la liste) a été réalisé en 2013 par Guillaume Sylvestre.
    Le film «Les 4 soldats» (le 1163e de la liste) a été réalisé en 2013 par Robert Morin.
    Le film «Amsterdam» (le 1164e de la liste) a été réalisé en 2013 par Stefan Miljevic.
    Le film «L'autre maison» (le 1165e de la liste) a été réalisé en 2013 par Mathieu Roy.
    Le film «Catimini» (le 1166e de la liste) a été réalisé en 2013 par Nathalie Saint-Pierre.
    Le film «Chasse au Godard d'Abbittibbi» (le 1167e de la liste) a été réalisé en 2013 par Éric Morin.
    Le film «La cicatrice» (le 1168e de la liste) a été réalisé en 2013 par Jimmy Larouche.
    Le film «Cyanure» (le 1169e de la liste) a été réalisé en 2013 par Séverine Cornamusaz.
    Le film «Le démantèlement» (le 1170e de la liste) a été réalisé en 2013 par Sébastien Pilote.
    Le film «Diego Star» (le 1171e de la liste) a été réalisé en 2013 par Frédérick Pelletier.
    Le film «Émilie» (le 1172e de la liste) a été réalisé en 2013 par Guillaume Lonergan.
    Le film «Finissant(e)s» (le 1173e de la liste) a été réalisé en 2013 par Rafaël Ouellet.
    Le film «Gabrielle» (le 1174e de la liste) a été réalisé en 2013 par Louise Archambault.
    Le film «The Good Lie» (le 1175e de la liste) a été réalisé en 2013 par Shawn Linden.
    Le film «Home Again» (le 1176e de la liste) a été réalisé en 2013 par Sudz Sutherland.
    Le film «Hot Dog» (le 1177e de la liste) a été réalisé en 2013 par Marc-André Lavoie.
    Le film «Il était une fois les Boys» (le 1178e de la liste) a été réalisé en 2013 par Richard Goudreau.
    Le film «Jappeloup» (le 1179e de la liste) a été réalisé en 2013 par Christian Duguay.
    Le film «Lac Mystère» (le 1180e de la liste) a été réalisé en 2013 par Érik Canuel.
    Le film «La légende de Sarila» (le 1181e de la liste) a été réalisé en 2013 par Nancy Florence Savard.
    Le film «Louis Cyr» (le 1182e de la liste) a été réalisé en 2013 par Daniel Roby.
    Le film «La maison du pêcheur» (le 1183e de la liste) a été réalisé en 2013 par Alain Chartrand.
    Le film «Les manèges humains» (le 1184e de la liste) a été réalisé en 2013 par Martin Laroche.
    Le film «Memories Corner» (le 1185e de la liste) a été réalisé en 2013 par Audrey Fouché.
    Le film «Le météore» (le 1186e de la liste) a été réalisé en 2013 par François Delisle.
    Le film «Misogyny/Misandry» (le 1187e de la liste) a été réalisé en 2013 par Erik Anderson.
    Le film «Moroccan Gigolos» (le 1188e de la liste) a été réalisé en 2013 par Ismaël Saidi.
    Le film «Ressac» (le 1189e de la liste) a été réalisé en 2013 par Pascale Ferland.
    Le film «Roche papier ciseaux» (le 1190e de la liste) a été réalisé en 2013 par Yan Lanouette Turgeon.
    Le film «Rouge sang» (le 1191e de la liste) a été réalisé en 2013 par Martin Doepner.
    Le film «Sarah préfère la course» (le 1192e de la liste) a été réalisé en 2013 par Chloé Robichaud.
    Le film «Triptyque» (le 1193e de la liste) a été réalisé en 2013 par Robert Lepage.
    Le film «Une jeune fille» (le 1194e de la liste) a été réalisé en 2013 par Catherine Martin.
    Le film «Vic+Flo ont vu un ours» (le 1195e de la liste) a été réalisé en 2013 par Denis Côté.
    Le film «1987» (le 1196e de la liste) a été réalisé en 2014 par Ricardo Trogi.
    Le film «2 temps 3 mouvements» (le 1197e de la liste) a été réalisé en 2014 par Christophe Cousin.
    Le film «3 histoires d'Indiens» (le 1198e de la liste) a été réalisé en 2014 par Robert Morin.
    Le film «L'ange gardien» (le 1199e de la liste) a été réalisé en 2014 par Jean-Sébastien Lord.
    Le film «Arwad» (le 1200e de la liste) a été réalisé en 2014 par Dominique Chila.
    Le film «Bunker» (le 1201e de la liste) a été réalisé en 2014 par Patrick Boivin.
    Le film «Ceci n'est pas un polar» (le 1202e de la liste) a été réalisé en 2014 par Patrick Gazé.
    Le film «Le coq de St-Victor» (le 1203e de la liste) a été réalisé en 2014 par Pierre Greco.
    Le film «Discopathe» (le 1204e de la liste) a été réalisé en 2014 par Renaud Gauthier.
    Le film «Dr. Cabbie» (le 1205e de la liste) a été réalisé en 2014 par Jean-François Pouliot.
    Le film «Exil» (le 1206e de la liste) a été réalisé en 2014 par Charles-Olivier Michaud.
    Le film «La ferme des humains» (le 1207e de la liste) a été réalisé en 2014 par Onur Karaman.
    Le film «Le fille du Martin» (le 1208e de la liste) a été réalisé en 2014 par Samuel Thivierge.
    Le film «La gang des hors-la-loi» (le 1209e de la liste) a été réalisé en 2014 par Jean Beaudry.
    Le film «La garde» (le 1210e de la liste) a été réalisé en 2014 par Sylvain Archambault.
    Le film «Gerontophilia» (le 1211e de la liste) a été réalisé en 2014 par Bruce LaBruce.
    Le film «The Grand Seduction» (le 1212e de la liste) a été réalisé en 2014 par Don McKellar.
    Le film «Henri Henri» (le 1213e de la liste) a été réalisé en 2014 par Martin Talbot.
    Le film «J'espère que tu vas bien 2» (le 1214e de la liste) a été réalisé en 2014 par David La Haye.
    Le film «Limoilou» (le 1215e de la liste) a été réalisé en 2014 par Edgar Fritz.
    Le film «Love Projet» (le 1216e de la liste) a été réalisé en 2014 par Carole Laure.
    Le film «Maïna» (le 1217e de la liste) a été réalisé en 2014 par Michel Poulette.
    Le film «Les maîtres du suspense» (le 1218e de la liste) a été réalisé en 2014 par Stéphane Lapointe.
    Le film «Meetings with a Young Poet» (le 1219e de la liste) a été réalisé en 2014 par Rudy Barichello.
    Le film «Miraculum» (le 1220e de la liste) a été réalisé en 2014 par Podz.
    Le film «Mommy» (le 1221e de la liste) a été réalisé en 2014 par Xavier Dolan.
    Le film «My Guys» (le 1222e de la liste) a été réalisé en 2014 par Joseph Antaki.
    Le film «La petite reine» (le 1223e de la liste) a été réalisé en 2014 par Alexis Durand-Brault.
    Le film «Pour que plus jamais» (le 1224e de la liste) a été réalisé en 2014 par Marie Ange Barbancourt.
    Le film «Qu'est-ce qu'on fait ici?» (le 1225e de la liste) a été réalisé en 2014 par Julie Hivon.
    Le film «Que ta joie demeure» (le 1226e de la liste) a été réalisé en 2014 par Denis Côté.
    Le film «Rédemption» (le 1227e de la liste) a été réalisé en 2014 par Joël Gauthier.
    Le film «Le règne de la beauté» (le 1228e de la liste) a été réalisé en 2014 par Denys Arcand.
    Le film «Rhymes for Young Ghouls» (le 1229e de la liste) a été réalisé en 2014 par Jeff Barnaby.
    Le film «Tom à la ferme» (le 1230e de la liste) a été réalisé en 2014 par Xavier Dolan.
    Le film «Tu dors Nicole» (le 1231e de la liste) a été réalisé en 2014 par Stéphane Lafleur.
    Le film «Un parallèle plus tard» (le 1232e de la liste) a été réalisé en 2014 par Sébastien Landry.
    Le film «Une vie pour deux» (le 1233e de la liste) a été réalisé en 2014 par Luc Bourdon.
    Le film «Uvanga» (le 1234e de la liste) a été réalisé en 2014 par Marie-Hélène Cousineau, Madeline Piujuq Ivalu.
    Le film «La voix de l'ombre» (le 1235e de la liste) a été réalisé en 2014 par Annie Molin Vasseur.
    Le film «Le vrai du faux» (le 1236e de la liste) a été réalisé en 2014 par Émile Gaudreault.
    Le film «Whitewash» (le 1237e de la liste) a été réalisé en 2014 par Emanuel Hoss-Desmarais.
    Le film «L'amour au temps de la guerre civile» (le 1238e de la liste) a été réalisé en 2015 par Rodrigue Jean.
    Le film «Anna» (le 1239e de la liste) a été réalisé en 2015 par Charles-Olivier Michaud.
    Le film «Antoine et Marie» (le 1240e de la liste) a été réalisé en 2015 par Jimmy Larouche.
    Le film «Aurélie Laflamme - Les pieds sur terre» (le 1241e de la liste) a été réalisé en 2015 par Nicolas Monette.
    Le film «Autrui» (le 1242e de la liste) a été réalisé en 2015 par Micheline Lanctôt.
    Le film «Le bruit des arbres» (le 1243e de la liste) a été réalisé en 2015 par François Péloquin.
    Le film «Ce qu'il ne faut pas dire» (le 1244e de la liste) a été réalisé en 2015 par Marquise Lepage.
    Le film «Chorus» (le 1245e de la liste) a été réalisé en 2015 par François Delisle.
    Le film «Le cœur de madame Sabali» (le 1246e de la liste) a été réalisé en 2015 par Ryan McKenna.
    Le film «Corbo» (le 1247e de la liste) a été réalisé en 2015 par Mathieu Denis.
    Le film «Les démons» (le 1248e de la liste) a été réalisé en 2015 par Philippe Lesage.
    Le film «Le dep» (le 1249e de la liste) a été réalisé en 2015 par Sonia Bonspille Boileau.
    Le film «Ego Trip» (le 1250e de la liste) a été réalisé en 2015 par Benoit Pelletier.
    Le film «Elephant Song» (le 1251e de la liste) a été réalisé en 2015 par Charles Binamé.
    Le film «L'énergie sombre» (le 1252e de la liste) a été réalisé en 2015 par Leonardo Fuica.
    Le film «Les Êtres chers» (le 1253e de la liste) a été réalisé en 2015 par Anne Émond.
    Le film «Félix et Meira» (le 1254e de la liste) a été réalisé en 2015 par Maxime Giroux.
    Le film «The Forbidden Room» (le 1255e de la liste) a été réalisé en 2015 par Guy Maddin, Evan Johnson.
    Le film «Le garagiste» (le 1256e de la liste) a été réalisé en 2015 par Renée Beaulieu.
    Le film «La Guerre des tuques 3D» (le 1257e de la liste) a été réalisé en 2015 par François Brisson, Jean-François Pouliot.
    Le film «Guibord s'en va-t-en guerre» (le 1258e de la liste) a été réalisé en 2015 par Philippe Falardeau.
    Le film «Gurov & Anna» (le 1259e de la liste) a été réalisé en 2015 par Rafaël Ouellet.
    Le film «Je suis à toi» (le 1260e de la liste) a été réalisé en 2015 par David Lambert.
    Le film «Le journal d'un vieil homme» (le 1261e de la liste) a été réalisé en 2015 par Bernard Émond.
    Le film «Les Loups» (le 1262e de la liste) a été réalisé en 2015 par Sophie Deraspe.
    Le film «Le Mirage» (le 1263e de la liste) a été réalisé en 2015 par Ricardo Trogi.
    Le film «Noir (NWA)» (le 1264e de la liste) a été réalisé en 2015 par Yves-Christian Fournier.
    Le film «Nouvelles, nouvelles» (le 1265e de la liste) a été réalisé en 2015 par Olivier Godin.
    Le film «La Passion d'Augustine» (le 1266e de la liste) a été réalisé en 2015 par Léa Pool.
    Le film «Paul à Québec» (le 1267e de la liste) a été réalisé en 2015 par François Bouvier.
    Le film «Preggoland» (le 1268e de la liste) a été réalisé en 2015 par Jacob Tierney.
    Le film «Le scaphandrier» (le 1269e de la liste) a été réalisé en 2015 par Alain Vézina.
    Le film «Scratch» (le 1270e de la liste) a été réalisé en 2015 par Sébastien Godron.
    Le film «Standstill» (le 1271e de la liste) a été réalisé en 2015 par Majdi El-Omari.
    Le film «Turbo Kid» (le 1272e de la liste) a été réalisé en 2015 par François Simard, Anouk et Yoann-Karl Whissell.
    Le film «Ville-Marie» (le 1273e de la liste) a été réalisé en 2015 par Guy Édoin.
    Le film «01:54» (le 1274e de la liste) a été réalisé en 2016 par Yan England.
    Le film «Les 3 p'tits cochons 2» (le 1275e de la liste) a été réalisé en 2016 par Jean-François Pouliot.
    Le film «9, le film» (le 1276e de la liste) a été réalisé en 2016 par Un collectif de réalisateurs et de réalisatrices.
    Le film «Avant les rues» (le 1277e de la liste) a été réalisé en 2016 par Chloé Leriche.
    Le film «Boris sans Béatrice» (le 1278e de la liste) a été réalisé en 2016 par Denis Côté.
    Le film «Ceux qui font les révolutions à moitié n'ont fait que se creuser un tombeau» (le 1279e de la liste) a été réalisé en 2016 par Simon Lavoie, Mathieu Denis.
    Le film «La Chasse au collet» (le 1280e de la liste) a été réalisé en 2016 par Steve Kerr.
    Le film «Chasse-Galerie : La légende» (le 1281e de la liste) a été réalisé en 2016 par Jean-Philippe Duval.
    Le film «Déserts» (le 1282e de la liste) a été réalisé en 2016 par Charles-André Coderre, Yann-Manuel Hernandez.
    Le film «D’encre et de sang» (le 1283e de la liste) a été réalisé en 2016 par Alexis Fortier Gauthier, Maxim Rheault, Francis Fortin.
    Le film «Early Winter» (le 1284e de la liste) a été réalisé en 2016 par Michael Rowe.
    Le film «Ecartée» (le 1285e de la liste) a été réalisé en 2016 par Lawrence Côté-Collins.
    Le film «Embrasse-moi comme tu m'aimes» (le 1286e de la liste) a été réalisé en 2016 par André Forcier.
    Le film «Endorphine» (le 1287e de la liste) a été réalisé en 2016 par André Turpin.
    Le film «Feuilles mortes» (le 1288e de la liste) a été réalisé en 2016 par Thierry Bouffard, Steve Landry, Édouard A. Tremblay.
    Le film «Generation Wolf» (le 1289e de la liste) a été réalisé en 2016 par Christian de la Cortina.
    Le film «Harry : Portrait d’un détective privé» (le 1290e de la liste) a été réalisé en 2016 par Maxime Desruisseaux.
    Le film «Juste la fin du monde» (le 1291e de la liste) a été réalisé en 2016 par Xavier Dolan.
    Le film «King Dave» (le 1292e de la liste) a été réalisé en 2016 par Podz.
    Le film «Là où Attila passe» (le 1293e de la liste) a été réalisé en 2016 par Onur Karaman.
    Le film «Les Mauvaises herbes» (le 1294e de la liste) a été réalisé en 2016 par Louis Bélanger.
    Le film «Mes ennemis» (le 1295e de la liste) a été réalisé en 2016 par Stéphane Géhami.
    Le film «Mon ami Dino» (le 1296e de la liste) a été réalisé en 2016 par Jimmy Larouche.
    Le film «Montréal la blanche» (le 1297e de la liste) a été réalisé en 2016 par Bachir Bensadekk.
    Le film «Nitro Rush» (le 1298e de la liste) a été réalisé en 2016 par Alain Desrochers.
    Le film «L'Origine des espèces» (le 1299e de la liste) a été réalisé en 2016 par Dominic Goyer.
    Le film «Le Pacte des anges» (le 1300e de la liste) a été réalisé en 2016 par Richard Angers.
    Le film «Pays» (le 1301e de la liste) a été réalisé en 2016 par Chloé Robichaud.
    Le film «Prank» (le 1302e de la liste) a été réalisé en 2016 par Vincent Biron.
    Le film «Le Rang du lion» (le 1303e de la liste) a été réalisé en 2016 par Stéphane Beaudoin.
    Le film «The Saver» (le 1304e de la liste) a été réalisé en 2016 par Wiebke von Carolsfeld.
    Le film «Toujours encore» (le 1305e de la liste) a été réalisé en 2016 par Jean-François Boisvenue.
    Le film «Two Lovers and a Bear» (le 1306e de la liste) a été réalisé en 2016 par Kim Nguyen.
    Le film «Un paradis pour tous» (le 1307e de la liste) a été réalisé en 2016 par Robert Morin.
    Le film «Vortex» (le 1308e de la liste) a été réalisé en 2016 par Jephté Bastien.
    Le film «Votez Bougon» (le 1309e de la liste) a été réalisé en 2016 par Jean-François Pouliot.
    Le film «Les Affamés» (le 1310e de la liste) a été réalisé en 2017 par Robin Aubert.
    Le film «Après coup» (le 1311e de la liste) a été réalisé en 2017 par Noël Mitrani.
    Le film «Les arts de la parole» (le 1312e de la liste) a été réalisé en 2017 par Olivier Godin.
    Le film «L'Autre côté de novembre» (le 1313e de la liste) a été réalisé en 2017 par Maryanne Zéhil.
    Le film «Ballerina» (le 1314e de la liste) a été réalisé en 2017 par Éric Warrin, Éric Summer.
    Le film «Bon Cop Bad Cop 2» (le 1315e de la liste) a été réalisé en 2017 par Alain Desrochers.
    Le film «Boost» (le 1316e de la liste) a été réalisé en 2017 par Darren Curtis.
    Le film «Ça sent la coupe» (le 1317e de la liste) a été réalisé en 2017 par Patrice Sauvé.
    Le film «C'est le coeur qui meurt en dernier» (le 1318e de la liste) a été réalisé en 2017 par Alexis Durand-Brault.
    Le film «Le Cyclotron» (le 1319e de la liste) a été réalisé en 2017 par Olivier Asselin.
    Le film «De père en flic 2» (le 1320e de la liste) a été réalisé en 2017 par Émile Gaudreault.
    Le film «Et au pire, on se mariera» (le 1321e de la liste) a été réalisé en 2017 par Léa Pool.
    Le film «Innocent» (le 1322e de la liste) a été réalisé en 2017 par Marc-André Lavoie.
    Le film «Iqaluit» (le 1323e de la liste) a été réalisé en 2017 par Benoît Pilon.
    Le film «Junior majeur» (le 1324e de la liste) a été réalisé en 2017 par Éric Tessier.
    Le film «Maudite Poutine» (le 1325e de la liste) a été réalisé en 2017 par Karl Lemieux.
    Le film «Mes nuits feront écho» (le 1326e de la liste) a été réalisé en 2017 par Sophie Goyette.
    Le film «Nelly» (le 1327e de la liste) a été réalisé en 2017 par Anne Émond.
    Le film «Nous sommes les autres» (le 1328e de la liste) a été réalisé en 2017 par Jean-François Asselin.
    Le film «La Petite Fille qui aimait trop les allumettes» (le 1329e de la liste) a été réalisé en 2017 par Simon Lavoie.
    Le film «Pieds nus dans l'aube» (le 1330e de la liste) a été réalisé en 2017 par Francis Leclerc.
    Le film «Le problème d'infiltration» (le 1331e de la liste) a été réalisé en 2017 par Robert Morin.
    Le film «Les Rois mongols» (le 1332e de la liste) a été réalisé en 2017 par Luc Picard.
    Le film «Tadoussac» (le 1333e de la liste) a été réalisé en 2017 par Martin Laroche.
    Le film «Ta peau si lisse» (le 1334e de la liste) a été réalisé en 2017 par Denis Côté.
    Le film «Le trip à trois» (le 1335e de la liste) a été réalisé en 2017 par Nicolas Monette.
    Le film «Tuktuq» (le 1336e de la liste) a été réalisé en 2017 par Robin Aubert.
    Le film «1991» (le 1337e de la liste) a été réalisé en 2018 par Ricardo Trogi.
    Le film «À tous ceux qui ne me lisent pas» (le 1338e de la liste) a été réalisé en 2018 par Yan Giroux.
    Le film «Ailleurs» (le 1339e de la liste) a été réalisé en 2018 par Samuel Matteau.
    Le film «Allure» (le 1340e de la liste) a été réalisé en 2018 par Jason Sachez, Carlos Sanchez.
    Le film «All You Can Eat Buddha» (le 1341e de la liste) a été réalisé en 2018 par Ian Lagarde.
    Le film «L’amour» (le 1342e de la liste) a été réalisé en 2018 par Marc Bisaillon.
    Le film «Another Kind of Wedding» (le 1343e de la liste) a été réalisé en 2018 par Pat Kiely.
    Le film «Birthmarked» (le 1344e de la liste) a été réalisé en 2018 par Emanuel Hoss-Desmarais.
    Le film «La Bolduc» (le 1345e de la liste) a été réalisé en 2018 par François Bouvier.
    Le film «Burn Out ou la servitude volontaire» (le 1346e de la liste) a été réalisé en 2018 par Michel Jetté.
    Le film «Charlotte a du fun» (le 1347e de la liste) a été réalisé en 2018 par Sophie Lorain.
    Le film «Chien de garde» (le 1348e de la liste) a été réalisé en 2018 par Sophie Dupuis.
    Le film «La Chute de l'empire américain» (le 1349e de la liste) a été réalisé en 2018 par Denys Arcand.
    Le film «La chute de Sparte» (le 1350e de la liste) a été réalisé en 2018 par Tristan Dubois.
    Le film «Claire l’Hiver» (le 1351e de la liste) a été réalisé en 2018 par Sophie Bédard-Marcotte.
    Le film «La course des Tuques» (le 1352e de la liste) a été réalisé en 2018 par François Brisson, Benoît Godbout.
    Le film «La Disparition des lucioles» (le 1353e de la liste) a été réalisé en 2018 par Sébastien Pilote.
    Le film «Dans la brume» (le 1354e de la liste) a été réalisé en 2018 par Daniel Roby.
    Le film «Eye on Juliet» (le 1355e de la liste) a été réalisé en 2018 par Kim Nguyen.
    Le film «Les faux tatouages» (le 1356e de la liste) a été réalisé en 2018 par Pascal Plante.
    Le film «Hochelaga, terre des âmes» (le 1357e de la liste) a été réalisé en 2018 par François Girard.
    Le film «Identités» (le 1358e de la liste) a été réalisé en 2018 par Samuel Thivierge.
    Le film «Isla Blanca» (le 1359e de la liste) a été réalisé en 2018 par Jeanne Leblanc.
    Le film «Lemonade» (le 1360e de la liste) a été réalisé en 2018 par Ioana Uricaru.
    Le film «Napoléon en apparte» (le 1361e de la liste) a été réalisé en 2018 par Jeff Denis.
    Le film «Nelly et Simon : Mission Yéti» (le 1362e de la liste) a été réalisé en 2018 par Pierre Greco, Nancy Florence Savard.
    Le film «Le nid» (le 1363e de la liste) a été réalisé en 2018 par David Paradis.
    Le film «Origami» (le 1364e de la liste) a été réalisé en 2018 par Patrick Demers.
    Le film «Oscillations» (le 1365e de la liste) a été réalisé en 2018 par Ky Nam Leduc.
    Le film «Papa est devenu un lutin» (le 1366e de la liste) a été réalisé en 2018 par Dominique Adams.
    Le film «Pour vivre ici» (le 1367e de la liste) a été réalisé en 2018 par Bernard Émond.
    Le film «Quand l'amour se creuse un trou» (le 1368e de la liste) a été réalisé en 2018 par Ara Ball.
    Le film «Les salopes ou le sucre naturel de la peau» (le 1369e de la liste) a été réalisé en 2018 par Renée Beaulieu.
    Le film «Ma vie avec John F. Donovan» (le 1370e de la liste) a été réalisé en 2018 par Xavier Dolan.
    Le film «Kristina Wagenbauer» (le 1371e de la liste) a été réalisé en 2018 par Carla Turcotte, Natalia Dontcheva, Emmanuel Schwartz.
    Le film «Les scènes fortuites» (le 1372e de la liste) a été réalisé en 2018 par Guillaume Lambert.
    Le film «Touched» (le 1373e de la liste) a été réalisé en 2018 par Karl R. Hearne.
    Le film «Un printemps d’ailleurs» (le 1374e de la liste) a été réalisé en 2018 par Xiaodan He.
    Le film «Vénus» (le 1375e de la liste) a été réalisé en 2018 par Eisha Marjara.
    Le film «Wolfe» (le 1376e de la liste) a été réalisé en 2018 par Francis Bordeleau.
    Le film «Yolanda» (le 1377e de la liste) a été réalisé en 2018 par Jeannine Gagné.
    Le film «Une colonie» (le 1378e de la liste) a été réalisé en 2019 par Geneviève Dulude-De Celles.
    Le film «La grande noirceur» (le 1379e de la liste) a été réalisé en 2019 par Maxime Giroux.
    Le film «Malek» (le 1380e de la liste) a été réalisé en 2019 par Guy Édoin.
    Le film «Impetus» (le 1381e de la liste) a été réalisé en 2019 par Jennifer Alleyn.
    Le film «Mon ami Walid» (le 1382e de la liste) a été réalisé en 2019 par Adib Alkhalidey.
    Le film «Antigone» (le 1383e de la liste) a été réalisé en 2019 par Sophie Deraspe.
    Le film «Attaque d'un autre monde» (le 1384e de la liste) a été réalisé en 2019 par Adrien Lorion.
    Le film «Jeune Juliette» (le 1385e de la liste) a été réalisé en 2019 par Anne Émond.
    Le film «Journal de Bolivie - 50 ans après la mort du Che» (le 1386e de la liste) a été réalisé en 2019 par Jules Falardeau et Jean-Philippe Nadeau Marcoux.
    Le film «Kuessipan» (le 1387e de la liste) a été réalisé en 2019 par Myriam Verreault.
    Le film «Matthias et Maxime» (le 1388e de la liste) a été réalisé en 2019 par Xavier Dolan.
    Le film «Répertoire des villes disparues» (le 1389e de la liste) a été réalisé en 2019 par Denis Côté.
    Le film «Sur les traces de Bochart» (le 1390e de la liste) a été réalisé en 2019 par Pierre Saint-Yves et Yannick Gendron.
    Le film «Menteur» (le 1391e de la liste) a été réalisé en 2019 par Émile Gaudreault.
    Le film «Apapacho: une caresse pour l'âme» (le 1392e de la liste) a été réalisé en 2019 par Marquise Lepage.
    Le film «La femme de mon frère» (le 1393e de la liste) a été réalisé en 2019 par Monia Chokri.
    Le film «Les Fleurs oubliées» (le 1394e de la liste) a été réalisé en 2019 par André Forcier.
    Le film «Jouliks» (le 1395e de la liste) a été réalisé en 2019 par Mariloup Wolfe.
    Le film «Vivre à 100 milles à l'heure» (le 1396e de la liste) a été réalisé en 2019 par Louis Bélanger.
    Le film «Nous sommes Gold» (le 1397e de la liste) a été réalisé en 2019 par Éric Morin.
    Le film «Wilcox» (le 1398e de la liste) a été réalisé en 2019 par Denis Côté.
    Le film «Je finirai en prison» (le 1399e de la liste) a été réalisé en 2019 par Alexandre Dostie.
    Le film «Cassy» (le 1400e de la liste) a été réalisé en 2019 par Noël Mitrani.
    Le film «Les Barbares de La Malbaie» (le 1401e de la liste) a été réalisé en 2019 par Vincent Biron.
    Le film «Avant qu'on explose» (le 1402e de la liste) a été réalisé en 2019 par Rémi St-Michel.
    Le film «Le rire» (le 1403e de la liste) a été réalisé en 2020 par Martin Laroche.
    Le film «Mafia Inc.» (le 1404e de la liste) a été réalisé en 2020 par Podz.
    Le film «14 jours, 12 nuits» (le 1405e de la liste) a été réalisé en 2020 par Jean-Philippe Duval.
    Le film «Les nôtres» (le 1406e de la liste) a été réalisé en 2020 par Jeanne Leblanc.
    Le film «Tu te souviendras de moi» (le 1407e de la liste) a été réalisé en 2020 par Éric Tessier.
    Le film «Le club Vinland» (le 1408e de la liste) a été réalisé en 2020 par Benoît Pilon.
    Le film «Target Number One» (le 1409e de la liste) a été réalisé en 2020 par Daniel Roby.
    Le film «Le guide de la famille parfaite» (le 1410e de la liste) a été réalisé en 2020 par Ricardo Trogi.
    Le film «Gallant : Confessions d’un tueur à gages» (le 1411e de la liste) a été réalisé en 2020 par Luc Picard.
    Le film «La déesse des mouches à feu» (le 1412e de la liste) a été réalisé en 2020 par Anaïs Barbeau-Lavalette.


<img src="https://journalisme.uqam.ca/wp-content/uploads/sites/77/2020/05/bonus.png" style="text-align: center;">

<h4 style="background: cyan; text-align: center; padding: 10px;">Boucles ```while```</h4>

Il existe un autre type de boucle, dont je me sers moins souvent.

Les boucles ```for``` sont des boucles «finies», dont on connaît la taille.

Il existe aussi des boucles «infinies» qu'on peut créer avec la commande ```while```.

Comme elles peuvent rouler à l'infini, il faut simplement prévoir une condition qui les fait arrêter. Voici un exemple.


```python
n = 0
while n < 100:
    n += 1
    print(n,n**2)
```

    1 1
    2 4
    3 9
    4 16
    5 25
    6 36
    7 49
    8 64
    9 81
    10 100
    11 121
    12 144
    13 169
    14 196
    15 225
    16 256
    17 289
    18 324
    19 361
    20 400
    21 441
    22 484
    23 529
    24 576
    25 625
    26 676
    27 729
    28 784
    29 841
    30 900
    31 961
    32 1024
    33 1089
    34 1156
    35 1225
    36 1296
    37 1369
    38 1444
    39 1521
    40 1600
    41 1681
    42 1764
    43 1849
    44 1936
    45 2025
    46 2116
    47 2209
    48 2304
    49 2401
    50 2500
    51 2601
    52 2704
    53 2809
    54 2916
    55 3025
    56 3136
    57 3249
    58 3364
    59 3481
    60 3600
    61 3721
    62 3844
    63 3969
    64 4096
    65 4225
    66 4356
    67 4489
    68 4624
    69 4761
    70 4900
    71 5041
    72 5184
    73 5329
    74 5476
    75 5625
    76 5776
    77 5929
    78 6084
    79 6241
    80 6400
    81 6561
    82 6724
    83 6889
    84 7056
    85 7225
    86 7396
    87 7569
    88 7744
    89 7921
    90 8100
    91 8281
    92 8464
    93 8649
    94 8836
    95 9025
    96 9216
    97 9409
    98 9604
    99 9801
    100 10000


Ce que fait la boucle ci-dessus est simple. À chaque itération, elle ajoute 1 à la valeur de ```n```, puis en affiche la valeur, ainsi que son carré. Et la boucle va faire cela tant et aussi longtemps que ```n``` sera plus petit que 100. Comme cette condition est vérifiée **avant** qu'on ajoute 1 à ```n```, c'est pour cela que les valeurs «100 et 10000» sont affichées.

Ah, tiens! Je viens tout juste d'aborder avec vous l'idée des conditions. Plongeons alors tout de suite dans cette partie de la matière.

<h3 style="background: pink; text-align: center; padding: 10px;">Conditions</h3>

On travaille beaucoup avec des conditions en informatique pour trier ou classer des données.

Leur syntaxe générale est la suivante:
    
<code>**if** &lt;condition&gt;:
     *&lt;On fait quelque chose&gt;*
**elif** &lt;condition alternative&gt;:
     *&lt;On fait autre chose si la première condition n'est pas respectée&gt;*
**else**:
     *&lt;On fait encore autre chose si aucune des conditions précédentes ne sont respectées&gt;*
</code>

Vous voyez qu'il y a trois commandes possibles. Ça prend minimalement un ```if``` pour tester au moins une condition.

Ça peut être suivi par un ```elif```, qui est une contraction de *else if* («alors si»), dans les cas où on veut vérifier une autre condition.

Ça peut aussi se terminer par un ```else``` («alors») si aucune des conditions ne se vérifie.

Vous remarquez aussi que l'**indentation** est présente, comme dans les boucles. Seul le code qui se trouve *sous* la condition (après les deux points (```:```)) et qui est indenté sera exécuté par la condition.

Commençons par un premier exemple. Dans notre grande variable ```films```, si vous souhaitez n'afficher que les films réalisés l'année de votre naissance, par exemple, vous pourriez inclure dans votre boucle ```for``` une condition exprimée de la façon suivante:


```python
for film in films:
    if film[2] == "1966":
        print(film)
```

    ['Carnaval en chute libre', 'Guy Bouchard', '1966']
    ['La Douzième heure', 'Jean Martimbeau', '1966']
    ['Footsteps in the Snow (Des pas sur la neige)', 'Martin Green', '1966']
    ['Yul 871', 'Jacques Godbout', '1966']


Vous avez peut-être remarqué qu'il y a deux symboles «égal» (```==```). C'est qu'en informatique, un seul symbole «égal» (```=```) ne signifie pas «égale». Cela signifie «je déclare que telle valeur sera placée dans telle variable». La véritable égalité est testée avec le double égal.

Voici les autres opérateurs que vous pouvez utiliser dans les conditions.

<table>
<thead>
  <tr style="text-align: center;">
    <th>Opérateur</th>
    <th>Description</th>
    <th>Exemple</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td style="text-align: left;">**==**</td>
    <td style="text-align: left;">Égal</td>
    <td style="text-align: right;">```if a == b:```</td>
  </tr>
  <tr>
    <td style="text-align: left;">**!=**</td>
    <td style="text-align: left;">Pas égal</td>
    <td style="text-align: right;">```if a != b:```</td>
  </tr>
  <tr>
    <td style="text-align: left;">**&gt;**</td>
    <td style="text-align: left;">Plus grand</td>
    <td style="text-align: right;">```if a &gt; b:```</td>
  </tr>
  <tr>
    <td style="text-align: left;">**&gt;=**</td>
    <td style="text-align: left;">Plus grand ou égal</td>
    <td style="text-align: right;">```if a &gt;= b:```</td>
  </tr>
  <tr>
    <td style="text-align: left;">**&lt;**</td>
    <td style="text-align: left;">Plus petit</td>
    <td style="text-align: right;">```if a &lt; b:```</td>
  </tr>
  <tr>
    <td style="text-align: left;">**&lt;=**</td>
    <td style="text-align: left;">Plus petit ou égal</td>
    <td style="text-align: right;">```if a &lt;= b:```</td>
  </tr>
  <tr>
    <td style="text-align: left;">**and**</td>
    <td style="text-align: left;">et<br>*Fonctionne si tous les tests sont vrais*</td>
    <td style="text-align: right;">```if x &gt; 1000 and x &lt; 2000:```</td>
  </tr>
  <tr>
    <td style="text-align: left;">**or**</td>
    <td style="text-align: left;">ou<br>*Fonctionne si l'un des tests est vrai*</td>
    <td style="text-align: right;">```if x &lt; 300 or y &gt; 300:```</td>
  </tr>
  <tr>
    <td style="text-align: left;">**not**</td>
    <td style="text-align: left;">non<br>*Fonctionne si le test est faux*</td>
    <td style="text-align: right;">```if not(x &gt; 5 and y &lt; 10):```</td>
  </tr>
  <tr>
    <td style="text-align: left;">**in**</td>
    <td style="text-align: left;">Est présent dans</td>
    <td style="text-align: right;">```if "texte" in z:```</td>
  </tr>
  <tr>
    <td style="text-align: left;">**not in**</td>
    <td style="text-align: left;">N'est pas présent dans</td>
    <td style="text-align: right;">```if "texte" not in z:```</td>
  </tr>
</tbody>
</table>

Par exemple, si vous cherchez les films réalisés par Guy A. Lepage, vous pourriez être tenté d'écrire:


```python
for film in films:
    if film[1] == "Guy A. Lepage":
        print(film)
```

Mais ça ne donne aucun résultat. C'est que M. Lepage a signé un film avec un autre réalisateur. Ainsi, il n'arrive jamais que le 2e élément de la variable ```film``` contienne très exactement et uniquement «Guy A. Lepage». Le code suivant est donc préférable:


```python
for film in films:
    if "Guy A. Lepage" in film[1]:
        print(film)
```

    ['Camping sauvage', 'Guy A. Lepage, Sylvain Roy', '2004']


Je vous ai aussi montré comment trier les films réalisés en 2000 ou après. Il s'agit de se servir du 3e élément de la variable ```film``` (```film[2]```) qui contient l'année où chaque film a été réalisé. Cette variable est de type texte. Ainsi, on peut en extraire les deux premiers caractères, comme on l'a fait ci-dessus avec du texte. Il suffit d'ajouter ```[:2]``` à ```film[2]```.

Les films contenus dans la liste ont été réalisés entre 1943 et 2020. Les deux premiers caractères de ces années ne peuvent être que «19» ou «20». Ainsi, il suffit de faire la condition ci-dessous pour ne faire afficher que les films réalisés en 2000 ou après:


```python
for film in films:
    if film[2][:2] == "20":
        print("Le film {} a été réalisé en {}.".format(film[0],film[2]))
```

    Le film Alegria a été réalisé en 2000.
    Le film The Art of War a été réalisé en 2000.
    Le film La beauté de Pandore a été réalisé en 2000.
    Le film Blind Terror a été réalisé en 2000.
    Le film La bouteille a été réalisé en 2000.
    Le film Café Olé a été réalisé en 2000.
    Le film Deception a été réalisé en 2000.
    Le film Desire a été réalisé en 2000.
    Le film Les fantômes des trois Madeleine a été réalisé en 2000.
    Le film Heavy Metal 2000 a été réalisé en 2000.
    Le film Hochelaga a été réalisé en 2000.
    Le film L'île de sable a été réalisé en 2000.
    Le film L'invention de l'amour a été réalisé en 2000.
    Le film Island of the Dead a été réalisé en 2000.
    Le film Jack and Ella a été réalisé en 2000.
    Le film La journée avant a été réalisé en 2000.
    Le film The List a été réalisé en 2000.
    Le film Maelström a été réalisé en 2000.
    Le film Méchant party a été réalisé en 2000.
    Le film La moitié gauche du frigo a été réalisé en 2000.
    Le film Les muses orphelines a été réalisé en 2000.
    Le film My Little Devil a été réalisé en 2000.
    Le film Ne dis rien a été réalisé en 2000.
    Le film Possible Worlds a été réalisé en 2000.
    Le film Rats and Rabbits a été réalisé en 2000.
    Le film La répétition a été réalisé en 2000.
    Le film Saint Jude a été réalisé en 2000.
    Le film Stardom a été réalisé en 2000.
    Le film Subconscious Cruelty a été réalisé en 2000.
    Le film The Tracker a été réalisé en 2000.
    Le film Trick or Treat a été réalisé en 2000.
    Le film Two Thousand and None a été réalisé en 2000.
    Le film Un petit vent de panique a été réalisé en 2000.
    Le film Varian's War a été réalisé en 2000.
    Le film La veuve de Saint-Pierre a été réalisé en 2000.
    Le film La vie après l'amour a été réalisé en 2000.
    Le film Xchange a été réalisé en 2000.
    Le film 15 février 1839 a été réalisé en 2001.
    Le film L'ange de goudron a été réalisé en 2001.
    Le film Les Boys 3 a été réalisé en 2001.
    Le film Des chiens dans la neige a été réalisé en 2001.
    Le film Le ciel sur la tête a été réalisé en 2001.
    Le film Crème glacée, chocolat et autres consolations a été réalisé en 2001.
    Le film Danny in the Sky a été réalisé en 2001.
    Le film Dead Awake a été réalisé en 2001.
    Le film Du pic au cœur a été réalisé en 2001.
    Le film La femme qui boit a été réalisé en 2001.
    Le film La forteresse suspendue a été réalisé en 2001.
    Le film Hidden Agenda a été réalisé en 2001.
    Le film Hôtel des horizons a été réalisé en 2001.
    Le film Karmen Geï a été réalisé en 2001.
    Le film Karmina 2 a été réalisé en 2001.
    Le film La loi du cochon a été réalisé en 2001.
    Le film Lost and Delirious a été réalisé en 2001.
    Le film Mariages a été réalisé en 2001.
    Le film Nuit de noces a été réalisé en 2001.
    Le film Opération Cobra a été réalisé en 2001.
    Le film Le pornographe a été réalisé en 2001.
    Le film Protection a été réalisé en 2001.
    Le film So Faraway and Blue a été réalisé en 2001.
    Le film Stéphanie, Nathalie, Caroline et Vincent a été réalisé en 2001.
    Le film Un crabe dans la tête a été réalisé en 2001.
    Le film Une jeune fille à la fenêtre a été réalisé en 2001.
    Le film La vérité est un mensonge a été réalisé en 2001.
    Le film Au fil de l'eau a été réalisé en 2002.
    Le film Barbaloune a été réalisé en 2002.
    Le film The Baroness and the Pig a été réalisé en 2002.
    Le film Between Strangers a été réalisé en 2002.
    Le film Le collectionneur a été réalisé en 2002.
    Le film Les Dangereux a été réalisé en 2002.
    Le film Les Fils de Marie a été réalisé en 2002.
    Le film Le gambit du fou a été réalisé en 2002.
    Le film Histoire de pen a été réalisé en 2002.
    Le film Home a été réalisé en 2002.
    Le film Iso a été réalisé en 2002.
    Le film Katryn's Place a été réalisé en 2002.
    Le film Leaving Metropolis a été réalisé en 2002.
    Le film Madame Brouette a été réalisé en 2002.
    Le film Le marais a été réalisé en 2002.
    Le film La Mystérieuse Mademoiselle C. a été réalisé en 2002.
    Le film Le Nèg' a été réalisé en 2002.
    Le film L'Odyssée d'Alice Tremblay a été réalisé en 2002.
    Le film One Way Out a été réalisé en 2002.
    Le film Québec-Montréal a été réalisé en 2002.
    Le film Régina! a été réalisé en 2002.
    Le film Royal Bonbon a été réalisé en 2002.
    Le film Savage Messiah a été réalisé en 2002.
    Le film Secret de banlieue a été réalisé en 2002.
    Le film Séraphin : un homme et son péché a été réalisé en 2002.
    Le film Sex at the End of the Millenium a été réalisé en 2002.
    Le film Station Nord a été réalisé en 2002.
    Le film Summer a été réalisé en 2002.
    Le film La turbulence des fluides a été réalisé en 2002.
    Le film Women Without Wings a été réalisé en 2002.
    Le film Yellowknife a été réalisé en 2002.
    Le film 100 % bio a été réalisé en 2003.
    Le film 20h17, rue Darling a été réalisé en 2003.
    Le film Annie Brocoli dans les fonds marins a été réalisé en 2003.
    Le film The Book of Eve a été réalisé en 2003.
    Le film Comment ma mère accoucha de moi durant sa ménopause a été réalisé en 2003.
    Le film La face cachée de la lune a été réalisé en 2003.
    Le film The Favourite Game a été réalisé en 2003.
    Le film Gaz Bar Blues a été réalisé en 2003.
    Le film La grande séduction a été réalisé en 2003.
    Le film L'homme trop pressé prend son thé à la fourchette a été réalisé en 2003.
    Le film Les immortels a été réalisé en 2003.
    Le film Les invasions barbares a été réalisé en 2003.
    Le film Jack Paradise a été réalisé en 2003.
    Le film Ma voisine danse le ska a été réalisé en 2003.
    Le film Mambo Italiano a été réalisé en 2003.
    Le film Le manuscrit érotique a été réalisé en 2003.
    Le film Nez rouge a été réalisé en 2003.
    Le film Père et fils a été réalisé en 2003.
    Le film La petite Lili a été réalisé en 2003.
    Le film Le piège d'Issoudun a été réalisé en 2003.
    Le film Revival Blues a été réalisé en 2003.
    Le film Saved by the Belles a été réalisé en 2003.
    Le film Le secret de Cyndia a été réalisé en 2003.
    Le film A Silent Love a été réalisé en 2003.
    Le film Sur le seuil a été réalisé en 2003.
    Le film Les triplettes de Belleville a été réalisé en 2003.
    Le film Twist a été réalisé en 2003.
    Le film Acapulco Gold a été réalisé en 2004.
    Le film Les aimants a été réalisé en 2004.
    Le film The Blue Butterfly a été réalisé en 2004.
    Le film Le bonheur c'est une chanson triste a été réalisé en 2004.
    Le film Bonzaïon a été réalisé en 2004.
    Le film C'est pas moi… c'est l'autre a été réalisé en 2004.
    Le film Camping sauvage a été réalisé en 2004.
    Le film Comment conquérir l'Amérique en une nuit? a été réalisé en 2004.
    Le film Comment devenir un trou de cul et enfin plaire aux femmes a été réalisé en 2004.
    Le film CQ2 (Seek You Too) a été réalisé en 2004.
    Le film Dans l'œil du chat a été réalisé en 2004.
    Le film Dans une galaxie près de chez vous a été réalisé en 2004.
    Le film Le dernier tunnel a été réalisé en 2004.
    Le film Dorian a été réalisé en 2004.
    Le film Elles étaient cinq a été réalisé en 2004.
    Le film Elvis Gratton XXX : la vengeance d'Elvis Wong a été réalisé en 2004.
    Le film L'espérance a été réalisé en 2004.
    Le film Éternelle a été réalisé en 2004.
    Le film Folle embellie a été réalisé en 2004.
    Le film Goldirocks a été réalisé en 2004.
    Le film Le golem de Montréal a été réalisé en 2004.
    Le film Le goût des jeunes filles a été réalisé en 2004.
    Le film L'incomparable mademoiselle C. a été réalisé en 2004.
    Le film Je n'aime que toi a été réalisé en 2004.
    Le film Jeunesse dans l'ombre a été réalisé en 2004.
    Le film Jimmywork a été réalisé en 2004.
    Le film Littoral a été réalisé en 2004.
    Le film La lune viendra d'elle-même a été réalisé en 2004.
    Le film Ma vie en cinémascope a été réalisé en 2004.
    Le film Mémoires affectives a été réalisé en 2004.
    Le film Monica la mitraille a été réalisé en 2004.
    Le film Les moscovites a été réalisé en 2004.
    Le film Nouvelle-France a été réalisé en 2004.
    Le film Ordo a été réalisé en 2004.
    Le film La peau blanche a été réalisé en 2004.
    Le film La pension des étranges a été réalisé en 2004.
    Le film Pinocchio 3000 a été réalisé en 2004.
    Le film La planque a été réalisé en 2004.
    Le film Premier juillet - le film a été réalisé en 2004.
    Le film Pure a été réalisé en 2004.
    Le film Vendus a été réalisé en 2004.
    Le film Amnésie - L'Énigme James Brighton a été réalisé en 2005.
    Le film L'audition a été réalisé en 2005.
    Le film Aurore a été réalisé en 2005.
    Le film Barmaids a été réalisé en 2005.
    Le film Les Boys 4 a été réalisé en 2005.
    Le film C.R.A.Z.Y. a été réalisé en 2005.
    Le film Le chalet a été réalisé en 2005.
    Le film Daniel et les Superdogs a été réalisé en 2005.
    Le film La dernière incarnation a été réalisé en 2005.
    Le film Les états nordiques a été réalisé en 2005.
    Le film Les États-Unis d'Albert a été réalisé en 2005.
    Le film Familia a été réalisé en 2005.
    Le film Greg & Gentillon a été réalisé en 2005.
    Le film Horloge biologique a été réalisé en 2005.
    Le film Idole instantanée a été réalisé en 2005.
    Le film Kamataki a été réalisé en 2005.
    Le film Latex a été réalisé en 2005.
    Le film Maman Last Call a été réalisé en 2005.
    Le film Manners of Dying a été réalisé en 2005.
    Le film Maurice Richard a été réalisé en 2005.
    Le film Les moutons de Jacob a été réalisé en 2005.
    Le film La neuvaine a été réalisé en 2005.
    Le film Niagara Motel a été réalisé en 2005.
    Le film Petit pow! pow! Noël a été réalisé en 2005.
    Le film La pharmacie de l'espoir a été réalisé en 2005.
    Le film Saint-Ralph a été réalisé en 2005.
    Le film Saints-Martyrs-des-Damnés a été réalisé en 2005.
    Le film Stories of a Gravedigger a été réalisé en 2005.
    Le film Summer with the Ghosts a été réalisé en 2005.
    Le film Le survenant a été réalisé en 2005.
    Le film Vers le sud a été réalisé en 2005.
    Le film La vie avec mon père a été réalisé en 2005.
    Le film 1st bite a été réalisé en 2006.
    Le film The 4th Life a été réalisé en 2006.
    Le film L'anus Horribilis a été réalisé en 2006.
    Le film La belle bête a été réalisé en 2006.
    Le film Black Eyed Dog a été réalisé en 2006.
    Le film Bluff sou bluff a été réalisé en 2006.
    Le film Bobby a été réalisé en 2006.
    Le film Bon cop bad cop a été réalisé en 2006.
    Le film C'est beau une ville la nuit a été réalisé en 2006.
    Le film Cadavre exquis première édition a été réalisé en 2006.
    Le film Les cavaliers de la canette a été réalisé en 2006.
    Le film Cercle vicieux a été réalisé en 2006.
    Le film Cheech a été réalisé en 2006.
    Le film Comme tout le monde a été réalisé en 2006.
    Le film Congorama a été réalisé en 2006.
    Le film Convoitises a été réalisé en 2006.
    Le film La coupure a été réalisé en 2006.
    Le film De ma fenêtre, sans maison… a été réalisé en 2006.
    Le film Délivrez-moi a été réalisé en 2006.
    Le film The Descendant a été réalisé en 2006.
    Le film Duo a été réalisé en 2006.
    Le film End of the Line a été réalisé en 2006.
    Le film Les filles du botaniste a été réalisé en 2006.
    Le film Gason makoklen 1 a été réalisé en 2006.
    Le film Gason makoklen 2 a été réalisé en 2006.
    Le film Le génie du crime a été réalisé en 2006.
    Le film Le guide de la petite vengeance a été réalisé en 2006.
    Le film Heads of Control: The Gorul Baheu Brain Expedition a été réalisé en 2006.
    Le film Histoire de famille a été réalisé en 2006.
    Le film Hiver a été réalisé en 2006.
    Le film The Journals of Knud Rasmussen a été réalisé en 2006.
    Le film Lézaureil kilyve a été réalisé en 2006.
    Le film Malléables journées a été réalisé en 2006.
    Le film Manga Latina: Killer on the Loose a été réalisé en 2006.
    Le film Martin Clear: The Whole Story a été réalisé en 2006.
    Le film The Passenger a été réalisé en 2006.
    Le film The Point a été réalisé en 2006.
    Le film La porte de Fersein a été réalisé en 2006.
    Le film Pourquoi le dire? a été réalisé en 2006.
    Le film Prenez vos places a été réalisé en 2006.
    Le film Que Dieu bénisse l'Amérique a été réalisé en 2006.
    Le film La rage de l'ange a été réalisé en 2006.
    Le film Rechercher Victor Pellerin a été réalisé en 2006.
    Le film Roméo et Juliette a été réalisé en 2006.
    Le film Sans elle a été réalisé en 2006.
    Le film Le secret de ma mère a été réalisé en 2006.
    Le film Steel Toes a été réalisé en 2006.
    Le film Sur la trace d'Igor Rizzi a été réalisé en 2006.
    Le film These Girls a été réalisé en 2006.
    Le film Touche pas à mon homme a été réalisé en 2006.
    Le film Tous les autres, sauf moi a été réalisé en 2006.
    Le film Un dimanche à Kigali a été réalisé en 2006.
    Le film Une rame à l'eau a été réalisé en 2006.
    Le film La vie secrète des gens heureux a été réalisé en 2006.
    Le film A Year in the Death of Jack Richards a été réalisé en 2006.
    Le film Les 3 p'tits cochons a été réalisé en 2007.
    Le film À vos marques… party! a été réalisé en 2007.
    Le film Adam's Wall a été réalisé en 2007.
    Le film L'âge des ténèbres a été réalisé en 2007.
    Le film Amour, mensonges et conséquences a été réalisé en 2007.
    Le film The Backup Man a été réalisé en 2007.
    Le film La belle empoisonneuse a été réalisé en 2007.
    Le film Bluff a été réalisé en 2007.
    Le film Borderline a été réalisé en 2007.
    Le film La brunante a été réalisé en 2007.
    Le film La capture a été réalisé en 2007.
    Le film Le cèdre penché a été réalisé en 2007.
    Le film Continental, un film sans fusil a été réalisé en 2007.
    Le film Contre toute espérance a été réalisé en 2007.
    Le film Dans les villes a été réalisé en 2007.
    Le film De l'autre côté a été réalisé en 2007.
    Le film Échangistes a été réalisé en 2007.
    Le film Emotional Arithmetic a été réalisé en 2007.
    Le film Imitation a été réalisé en 2007.
    Le film La lâcheté a été réalisé en 2007.
    Le film La logique du remords a été réalisé en 2007.
    Le film Ma fille mon ange a été réalisé en 2007.
    Le film Ma tante Aline a été réalisé en 2007.
    Le film Minushi a été réalisé en 2007.
    Le film Nitro a été réalisé en 2007.
    Le film Nos vies privées a été réalisé en 2007.
    Le film Nos voisins Dhantsu a été réalisé en 2007.
    Le film Où vas-tu, Moshé? a été réalisé en 2007.
    Le film Puffball a été réalisé en 2007.
    Le film Rêves de poussière a été réalisé en 2007.
    Le film Le ring a été réalisé en 2007.
    Le film La rivière aux castors a été réalisé en 2007.
    Le film Shake Hands with the Devil a été réalisé en 2007.
    Le film Silk a été réalisé en 2007.
    Le film Steak a été réalisé en 2007.
    Le film Surviving My Mother a été réalisé en 2007.
    Le film Toi a été réalisé en 2007.
    Le film Un cri au bonheur a été réalisé en 2007.
    Le film Voleurs de chevaux a été réalisé en 2007.
    Le film Young Triffie a été réalisé en 2007.
    Le film 24 mesures a été réalisé en 2008.
    Le film À l'ouest de Pluton a été réalisé en 2008.
    Le film Adagio pour un gars de bicycle a été réalisé en 2008.
    Le film Amour, destin et rock'n'roll a été réalisé en 2008.
    Le film Babine a été réalisé en 2008.
    Le film Le banquet a été réalisé en 2008.
    Le film Before Tomorrow a été réalisé en 2008.
    Le film C'est pas moi, je le jure! a été réalisé en 2008.
    Le film Le cas Roberge a été réalisé en 2008.
    Le film Ce qu'il faut pour vivre a été réalisé en 2008.
    Le film Cruising Bar 2 a été réalisé en 2008.
    Le film Dans une galaxie près de chez vous 2 a été réalisé en 2008.
    Le film Demain a été réalisé en 2008.
    Le film Derrière moi a été réalisé en 2008.
    Le film Le déserteur a été réalisé en 2008.
    Le film Elle veut le chaos a été réalisé en 2008.
    Le film En plein cœur a été réalisé en 2008.
    Le film Le grand départ a été réalisé en 2008.
    Le film La ligne brisée a été réalisé en 2008.
    Le film Magique! a été réalisé en 2008.
    Le film Maman est chez le coiffeur a été réalisé en 2008.
    Le film Papa à la chasse aux lagopèdes a été réalisé en 2008.
    Le film Le piège américain a été réalisé en 2008.
    Le film Les plus beaux yeux du monde a été réalisé en 2008.
    Le film Restless a été réalisé en 2008.
    Le film Suivre Catherine a été réalisé en 2008.
    Le film Tout est parfait a été réalisé en 2008.
    Le film Truffe a été réalisé en 2008.
    Le film Un capitalisme sentimental a été réalisé en 2008.
    Le film Un été sans point ni coup sûr a été réalisé en 2008.
    Le film Wushu Warrior a été réalisé en 2008.
    Le film 1981 a été réalisé en 2009.
    Le film 3 saisons a été réalisé en 2009.
    Le film 40 is the new 20 a été réalisé en 2009.
    Le film 5150, rue des Ormes a été réalisé en 2009.
    Le film À quelle heure le train pour nulle part a été réalisé en 2009.
    Le film À vos marques… party! 2 a été réalisé en 2009.
    Le film Le bonheur de Pierre a été réalisé en 2009.
    Le film Cadavres a été réalisé en 2009.
    Le film Carcasses a été réalisé en 2009.
    Le film La chambre noire a été réalisé en 2009.
    Le film De père en flic a été réalisé en 2009.
    Le film Dédé à travers les brumes a été réalisé en 2009.
    Le film Détour a été réalisé en 2009.
    Le film Les doigts croches a été réalisé en 2009.
    Le film La donation a été réalisé en 2009.
    Le film Grande Ourse - La clé des possibles a été réalisé en 2009.
    Le film Les grandes chaleurs a été réalisé en 2009.
    Le film Impasse a été réalisé en 2009.
    Le film J'ai tué ma mère a été réalisé en 2009.
    Le film Je me souviens a été réalisé en 2009.
    Le film Lost Song a été réalisé en 2009.
    Le film Love and Savagery a été réalisé en 2009.
    Le film Martyrs a été réalisé en 2009.
    Le film Modern Love a été réalisé en 2009.
    Le film Noémie le secret a été réalisé en 2009.
    Le film Nuages sur la ville a été réalisé en 2009.
    Le film Les pieds dans le vide a été réalisé en 2009.
    Le film Polytechnique a été réalisé en 2009.
    Le film Pour toujours les Canadiens a été réalisé en 2009.
    Le film Refrain a été réalisé en 2009.
    Le film Sans dessein a été réalisé en 2009.
    Le film Serveuses demandées a été réalisé en 2009.
    Le film Suzie a été réalisé en 2009.
    Le film The Timekeeper a été réalisé en 2009.
    Le film Transit a été réalisé en 2009.
    Le film Turbid a été réalisé en 2009.
    Le film Un ange à la mer a été réalisé en 2009.
    Le film Un cargo pour l'Afrique a été réalisé en 2009.
    Le film 10 1/2 a été réalisé en 2010.
    Le film 2 fois une femme a été réalisé en 2010.
    Le film 2 frogs dans l'Ouest a été réalisé en 2010.
    Le film À l'origine d'un cri a été réalisé en 2010.
    Le film Les amours imaginaires a été réalisé en 2010.
    Le film L'appât a été réalisé en 2010.
    Le film Le baiser du barbu a été réalisé en 2010.
    Le film Cabotins a été réalisé en 2010.
    Le film La cité a été réalisé en 2010.
    Le film Curling a été réalisé en 2010.
    Le film La dernière fugue a été réalisé en 2010.
    Le film L'enfant prodige a été réalisé en 2010.
    Le film Everywhere a été réalisé en 2010.
    Le film Filière 13 a été réalisé en 2010.
    Le film Incendies a été réalisé en 2010.
    Le film Le journal d'Aurélie Laflamme a été réalisé en 2010.
    Le film Journal d'un coopérant a été réalisé en 2010.
    Le film Lance et compte a été réalisé en 2010.
    Le film Lucidité passagère a été réalisé en 2010.
    Le film Mesrine - L'instinct de mort a été réalisé en 2010.
    Le film Les mots gelés a été réalisé en 2010.
    Le film New Denmark a été réalisé en 2010.
    Le film Oscar et la dame rose a été réalisé en 2010.
    Le film Piché : entre ciel et terre a été réalisé en 2010.
    Le film Le poil de la bête a été réalisé en 2010.
    Le film Reste avec moi a été réalisé en 2010.
    Le film Romaine par moins 30 a été réalisé en 2010.
    Le film Route 132 a été réalisé en 2010.
    Le film Les sept jours du talion a été réalisé en 2010.
    Le film Les signes vitaux a été réalisé en 2010.
    Le film Snow & Ashes a été réalisé en 2010.
    Le film Sortie 67 a été réalisé en 2010.
    Le film Trois temps après la mort d'Anna a été réalisé en 2010.
    Le film Tromper le silence a été réalisé en 2010.
    Le film The Trotsky a été réalisé en 2010.
    Le film The Wild Hunt a été réalisé en 2010.
    Le film Y'en aura pas de facile a été réalisé en 2010.
    Le film À trois, Marie s'en va a été réalisé en 2011.
    Le film Angle mort a été réalisé en 2011.
    Le film Le bonheur des autres a été réalisé en 2011.
    Le film BumRush a été réalisé en 2011.
    Le film Café de Flore a été réalisé en 2011.
    Le film Le colis a été réalisé en 2011.
    Le film Coteau Rouge a été réalisé en 2011.
    Le film Décharge a été réalisé en 2011.
    Le film Die a été réalisé en 2011.
    Le film Le divan du monde a été réalisé en 2011.
    Le film En terrains connus a été réalisé en 2011.
    Le film La fille de Montréal a été réalisé en 2011.
    Le film French Immersion a été réalisé en 2011.
    Le film French Kiss a été réalisé en 2011.
    Le film Frisson des collines a été réalisé en 2011.
    Le film Funkytown a été réalisé en 2011.
    Le film Gerry a été réalisé en 2011.
    Le film Good Neighbours a été réalisé en 2011.
    Le film The High Cost of Living a été réalisé en 2011.
    Le film Jaloux a été réalisé en 2011.
    Le film Jo pour Jonathan a été réalisé en 2011.
    Le film The Kate Logan Affair a été réalisé en 2011.
    Le film Laurentie a été réalisé en 2011.
    Le film Marécages a été réalisé en 2011.
    Le film Monsieur Lazhar a été réalisé en 2011.
    Le film Nuit #1 a été réalisé en 2011.
    Le film Pour l'amour de Dieu a été réalisé en 2011.
    Le film La run a été réalisé en 2011.
    Le film Le sens de l'humour a été réalisé en 2011.
    Le film Shadowboxing a été réalisé en 2011.
    Le film Starbuck a été réalisé en 2011.
    Le film Sur le rythme a été réalisé en 2011.
    Le film Une vie qui commence a été réalisé en 2011.
    Le film Le vendeur a été réalisé en 2011.
    Le film La vérité a été réalisé en 2011.
    Le film The Year Dolly Parton Was My Mom a été réalisé en 2011.
    Le film L'affaire Dumont a été réalisé en 2012.
    Le film Après la neige a été réalisé en 2012.
    Le film Avant que mon cœur bascule a été réalisé en 2012.
    Le film Bestiaire a été réalisé en 2012.
    Le film Camion a été réalisé en 2012.
    Le film Columbarium a été réalisé en 2012.
    Le film L'empire Bo$$é a été réalisé en 2012.
    Le film Ésimésac a été réalisé en 2012.
    Le film The Girl in the White Coat a été réalisé en 2012.
    Le film The Hat Goes Wild a été réalisé en 2012.
    Le film Hors les murs a été réalisé en 2012.
    Le film Inch'Allah a été réalisé en 2012.
    Le film L'incrédule a été réalisé en 2012.
    Le film J'espère que tu vas bien a été réalisé en 2012.
    Le film Karakara a été réalisé en 2012.
    Le film Laurence Anyways a été réalisé en 2012.
    Le film Liverpool a été réalisé en 2012.
    Le film Mars et Avril a été réalisé en 2012.
    Le film Mesnak a été réalisé en 2012.
    Le film La mise à l'aveugle a été réalisé en 2012.
    Le film Omertà a été réalisé en 2012.
    Le film Les pee-wee 3D a été réalisé en 2012.
    Le film La peur de l'eau a été réalisé en 2012.
    Le film Rebelle a été réalisé en 2012.
    Le film Roméo Onze a été réalisé en 2012.
    Le film Thanatomorphose a été réalisé en 2012.
    Le film Le torrent a été réalisé en 2012.
    Le film Tout ce que tu possèdes a été réalisé en 2012.
    Le film Une bouteille dans la mer de Gaza a été réalisé en 2012.
    Le film La vallée des larmes a été réalisé en 2012.
    Le film 1er amour a été réalisé en 2013.
    Le film Les 4 soldats a été réalisé en 2013.
    Le film Amsterdam a été réalisé en 2013.
    Le film L'autre maison a été réalisé en 2013.
    Le film Catimini a été réalisé en 2013.
    Le film Chasse au Godard d'Abbittibbi a été réalisé en 2013.
    Le film La cicatrice a été réalisé en 2013.
    Le film Cyanure a été réalisé en 2013.
    Le film Le démantèlement a été réalisé en 2013.
    Le film Diego Star a été réalisé en 2013.
    Le film Émilie a été réalisé en 2013.
    Le film Finissant(e)s a été réalisé en 2013.
    Le film Gabrielle a été réalisé en 2013.
    Le film The Good Lie a été réalisé en 2013.
    Le film Home Again a été réalisé en 2013.
    Le film Hot Dog a été réalisé en 2013.
    Le film Il était une fois les Boys a été réalisé en 2013.
    Le film Jappeloup a été réalisé en 2013.
    Le film Lac Mystère a été réalisé en 2013.
    Le film La légende de Sarila a été réalisé en 2013.
    Le film Louis Cyr a été réalisé en 2013.
    Le film La maison du pêcheur a été réalisé en 2013.
    Le film Les manèges humains a été réalisé en 2013.
    Le film Memories Corner a été réalisé en 2013.
    Le film Le météore a été réalisé en 2013.
    Le film Misogyny/Misandry a été réalisé en 2013.
    Le film Moroccan Gigolos a été réalisé en 2013.
    Le film Ressac a été réalisé en 2013.
    Le film Roche papier ciseaux a été réalisé en 2013.
    Le film Rouge sang a été réalisé en 2013.
    Le film Sarah préfère la course a été réalisé en 2013.
    Le film Triptyque a été réalisé en 2013.
    Le film Une jeune fille a été réalisé en 2013.
    Le film Vic+Flo ont vu un ours a été réalisé en 2013.
    Le film 1987 a été réalisé en 2014.
    Le film 2 temps 3 mouvements a été réalisé en 2014.
    Le film 3 histoires d'Indiens a été réalisé en 2014.
    Le film L'ange gardien a été réalisé en 2014.
    Le film Arwad a été réalisé en 2014.
    Le film Bunker a été réalisé en 2014.
    Le film Ceci n'est pas un polar a été réalisé en 2014.
    Le film Le coq de St-Victor a été réalisé en 2014.
    Le film Discopathe a été réalisé en 2014.
    Le film Dr. Cabbie a été réalisé en 2014.
    Le film Exil a été réalisé en 2014.
    Le film La ferme des humains a été réalisé en 2014.
    Le film Le fille du Martin a été réalisé en 2014.
    Le film La gang des hors-la-loi a été réalisé en 2014.
    Le film La garde a été réalisé en 2014.
    Le film Gerontophilia a été réalisé en 2014.
    Le film The Grand Seduction a été réalisé en 2014.
    Le film Henri Henri a été réalisé en 2014.
    Le film J'espère que tu vas bien 2 a été réalisé en 2014.
    Le film Limoilou a été réalisé en 2014.
    Le film Love Projet a été réalisé en 2014.
    Le film Maïna a été réalisé en 2014.
    Le film Les maîtres du suspense a été réalisé en 2014.
    Le film Meetings with a Young Poet a été réalisé en 2014.
    Le film Miraculum a été réalisé en 2014.
    Le film Mommy a été réalisé en 2014.
    Le film My Guys a été réalisé en 2014.
    Le film La petite reine a été réalisé en 2014.
    Le film Pour que plus jamais a été réalisé en 2014.
    Le film Qu'est-ce qu'on fait ici? a été réalisé en 2014.
    Le film Que ta joie demeure a été réalisé en 2014.
    Le film Rédemption a été réalisé en 2014.
    Le film Le règne de la beauté a été réalisé en 2014.
    Le film Rhymes for Young Ghouls a été réalisé en 2014.
    Le film Tom à la ferme a été réalisé en 2014.
    Le film Tu dors Nicole a été réalisé en 2014.
    Le film Un parallèle plus tard a été réalisé en 2014.
    Le film Une vie pour deux a été réalisé en 2014.
    Le film Uvanga a été réalisé en 2014.
    Le film La voix de l'ombre a été réalisé en 2014.
    Le film Le vrai du faux a été réalisé en 2014.
    Le film Whitewash a été réalisé en 2014.
    Le film L'amour au temps de la guerre civile a été réalisé en 2015.
    Le film Anna a été réalisé en 2015.
    Le film Antoine et Marie a été réalisé en 2015.
    Le film Aurélie Laflamme - Les pieds sur terre a été réalisé en 2015.
    Le film Autrui a été réalisé en 2015.
    Le film Le bruit des arbres a été réalisé en 2015.
    Le film Ce qu'il ne faut pas dire a été réalisé en 2015.
    Le film Chorus a été réalisé en 2015.
    Le film Le cœur de madame Sabali a été réalisé en 2015.
    Le film Corbo a été réalisé en 2015.
    Le film Les démons a été réalisé en 2015.
    Le film Le dep a été réalisé en 2015.
    Le film Ego Trip a été réalisé en 2015.
    Le film Elephant Song a été réalisé en 2015.
    Le film L'énergie sombre a été réalisé en 2015.
    Le film Les Êtres chers a été réalisé en 2015.
    Le film Félix et Meira a été réalisé en 2015.
    Le film The Forbidden Room a été réalisé en 2015.
    Le film Le garagiste a été réalisé en 2015.
    Le film La Guerre des tuques 3D a été réalisé en 2015.
    Le film Guibord s'en va-t-en guerre a été réalisé en 2015.
    Le film Gurov & Anna a été réalisé en 2015.
    Le film Je suis à toi a été réalisé en 2015.
    Le film Le journal d'un vieil homme a été réalisé en 2015.
    Le film Les Loups a été réalisé en 2015.
    Le film Le Mirage a été réalisé en 2015.
    Le film Noir (NWA) a été réalisé en 2015.
    Le film Nouvelles, nouvelles a été réalisé en 2015.
    Le film La Passion d'Augustine a été réalisé en 2015.
    Le film Paul à Québec a été réalisé en 2015.
    Le film Preggoland a été réalisé en 2015.
    Le film Le scaphandrier a été réalisé en 2015.
    Le film Scratch a été réalisé en 2015.
    Le film Standstill a été réalisé en 2015.
    Le film Turbo Kid a été réalisé en 2015.
    Le film Ville-Marie a été réalisé en 2015.
    Le film 01:54 a été réalisé en 2016.
    Le film Les 3 p'tits cochons 2 a été réalisé en 2016.
    Le film 9, le film a été réalisé en 2016.
    Le film Avant les rues a été réalisé en 2016.
    Le film Boris sans Béatrice a été réalisé en 2016.
    Le film Ceux qui font les révolutions à moitié n'ont fait que se creuser un tombeau a été réalisé en 2016.
    Le film La Chasse au collet a été réalisé en 2016.
    Le film Chasse-Galerie : La légende a été réalisé en 2016.
    Le film Déserts a été réalisé en 2016.
    Le film D’encre et de sang a été réalisé en 2016.
    Le film Early Winter a été réalisé en 2016.
    Le film Ecartée a été réalisé en 2016.
    Le film Embrasse-moi comme tu m'aimes a été réalisé en 2016.
    Le film Endorphine a été réalisé en 2016.
    Le film Feuilles mortes a été réalisé en 2016.
    Le film Generation Wolf a été réalisé en 2016.
    Le film Harry : Portrait d’un détective privé a été réalisé en 2016.
    Le film Juste la fin du monde a été réalisé en 2016.
    Le film King Dave a été réalisé en 2016.
    Le film Là où Attila passe a été réalisé en 2016.
    Le film Les Mauvaises herbes a été réalisé en 2016.
    Le film Mes ennemis a été réalisé en 2016.
    Le film Mon ami Dino a été réalisé en 2016.
    Le film Montréal la blanche a été réalisé en 2016.
    Le film Nitro Rush a été réalisé en 2016.
    Le film L'Origine des espèces a été réalisé en 2016.
    Le film Le Pacte des anges a été réalisé en 2016.
    Le film Pays a été réalisé en 2016.
    Le film Prank a été réalisé en 2016.
    Le film Le Rang du lion a été réalisé en 2016.
    Le film The Saver a été réalisé en 2016.
    Le film Toujours encore a été réalisé en 2016.
    Le film Two Lovers and a Bear a été réalisé en 2016.
    Le film Un paradis pour tous a été réalisé en 2016.
    Le film Vortex a été réalisé en 2016.
    Le film Votez Bougon a été réalisé en 2016.
    Le film Les Affamés a été réalisé en 2017.
    Le film Après coup a été réalisé en 2017.
    Le film Les arts de la parole a été réalisé en 2017.
    Le film L'Autre côté de novembre a été réalisé en 2017.
    Le film Ballerina a été réalisé en 2017.
    Le film Bon Cop Bad Cop 2 a été réalisé en 2017.
    Le film Boost a été réalisé en 2017.
    Le film Ça sent la coupe a été réalisé en 2017.
    Le film C'est le coeur qui meurt en dernier a été réalisé en 2017.
    Le film Le Cyclotron a été réalisé en 2017.
    Le film De père en flic 2 a été réalisé en 2017.
    Le film Et au pire, on se mariera a été réalisé en 2017.
    Le film Innocent a été réalisé en 2017.
    Le film Iqaluit a été réalisé en 2017.
    Le film Junior majeur a été réalisé en 2017.
    Le film Maudite Poutine a été réalisé en 2017.
    Le film Mes nuits feront écho a été réalisé en 2017.
    Le film Nelly a été réalisé en 2017.
    Le film Nous sommes les autres a été réalisé en 2017.
    Le film La Petite Fille qui aimait trop les allumettes a été réalisé en 2017.
    Le film Pieds nus dans l'aube a été réalisé en 2017.
    Le film Le problème d'infiltration a été réalisé en 2017.
    Le film Les Rois mongols a été réalisé en 2017.
    Le film Tadoussac a été réalisé en 2017.
    Le film Ta peau si lisse a été réalisé en 2017.
    Le film Le trip à trois a été réalisé en 2017.
    Le film Tuktuq a été réalisé en 2017.
    Le film 1991 a été réalisé en 2018.
    Le film À tous ceux qui ne me lisent pas a été réalisé en 2018.
    Le film Ailleurs a été réalisé en 2018.
    Le film Allure a été réalisé en 2018.
    Le film All You Can Eat Buddha a été réalisé en 2018.
    Le film L’amour a été réalisé en 2018.
    Le film Another Kind of Wedding a été réalisé en 2018.
    Le film Birthmarked a été réalisé en 2018.
    Le film La Bolduc a été réalisé en 2018.
    Le film Burn Out ou la servitude volontaire a été réalisé en 2018.
    Le film Charlotte a du fun a été réalisé en 2018.
    Le film Chien de garde a été réalisé en 2018.
    Le film La Chute de l'empire américain a été réalisé en 2018.
    Le film La chute de Sparte a été réalisé en 2018.
    Le film Claire l’Hiver a été réalisé en 2018.
    Le film La course des Tuques a été réalisé en 2018.
    Le film La Disparition des lucioles a été réalisé en 2018.
    Le film Dans la brume a été réalisé en 2018.
    Le film Eye on Juliet a été réalisé en 2018.
    Le film Les faux tatouages a été réalisé en 2018.
    Le film Hochelaga, terre des âmes a été réalisé en 2018.
    Le film Identités a été réalisé en 2018.
    Le film Isla Blanca a été réalisé en 2018.
    Le film Lemonade a été réalisé en 2018.
    Le film Napoléon en apparte a été réalisé en 2018.
    Le film Nelly et Simon : Mission Yéti a été réalisé en 2018.
    Le film Le nid a été réalisé en 2018.
    Le film Origami a été réalisé en 2018.
    Le film Oscillations a été réalisé en 2018.
    Le film Papa est devenu un lutin a été réalisé en 2018.
    Le film Pour vivre ici a été réalisé en 2018.
    Le film Quand l'amour se creuse un trou a été réalisé en 2018.
    Le film Les salopes ou le sucre naturel de la peau a été réalisé en 2018.
    Le film Ma vie avec John F. Donovan a été réalisé en 2018.
    Le film Kristina Wagenbauer a été réalisé en 2018.
    Le film Les scènes fortuites a été réalisé en 2018.
    Le film Touched a été réalisé en 2018.
    Le film Un printemps d’ailleurs a été réalisé en 2018.
    Le film Vénus a été réalisé en 2018.
    Le film Wolfe a été réalisé en 2018.
    Le film Yolanda a été réalisé en 2018.
    Le film Une colonie a été réalisé en 2019.
    Le film La grande noirceur a été réalisé en 2019.
    Le film Malek a été réalisé en 2019.
    Le film Impetus a été réalisé en 2019.
    Le film Mon ami Walid a été réalisé en 2019.
    Le film Antigone a été réalisé en 2019.
    Le film Attaque d'un autre monde a été réalisé en 2019.
    Le film Jeune Juliette a été réalisé en 2019.
    Le film Journal de Bolivie - 50 ans après la mort du Che a été réalisé en 2019.
    Le film Kuessipan a été réalisé en 2019.
    Le film Matthias et Maxime a été réalisé en 2019.
    Le film Répertoire des villes disparues a été réalisé en 2019.
    Le film Sur les traces de Bochart a été réalisé en 2019.
    Le film Menteur a été réalisé en 2019.
    Le film Apapacho: une caresse pour l'âme a été réalisé en 2019.
    Le film La femme de mon frère a été réalisé en 2019.
    Le film Les Fleurs oubliées a été réalisé en 2019.
    Le film Jouliks a été réalisé en 2019.
    Le film Vivre à 100 milles à l'heure a été réalisé en 2019.
    Le film Nous sommes Gold a été réalisé en 2019.
    Le film Wilcox a été réalisé en 2019.
    Le film Je finirai en prison a été réalisé en 2019.
    Le film Cassy a été réalisé en 2019.
    Le film Les Barbares de La Malbaie a été réalisé en 2019.
    Le film Avant qu'on explose a été réalisé en 2019.
    Le film Le rire a été réalisé en 2020.
    Le film Mafia Inc. a été réalisé en 2020.
    Le film 14 jours, 12 nuits a été réalisé en 2020.
    Le film Les nôtres a été réalisé en 2020.
    Le film Tu te souviendras de moi a été réalisé en 2020.
    Le film Le club Vinland a été réalisé en 2020.
    Le film Target Number One a été réalisé en 2020.
    Le film Le guide de la famille parfaite a été réalisé en 2020.
    Le film Gallant : Confessions d’un tueur à gages a été réalisé en 2020.
    Le film La déesse des mouches à feu a été réalisé en 2020.


Je vous ai ensuite montré une technique semblable pour trier les films réalisés avant les années 1960. Essayons d'appliquer la même chose, mais aux films réalisés durant les années 1980 et 1990.

Cela implique l'utilisation de la fonction ```int()``` pour transformer en nombres les trois premiers caractères des années.


```python
for film in films:
    if int(film[2][:3]) > 197 and int(film[2][:3]) <= 199:
        print("Le film {} a été réalisé en {}.".format(film[0],film[2]))
```

    Le film Agency a été réalisé en 1980.
    Le film Atlantic City, U.S.A. a été réalisé en 1980.
    Le film Les bons débarras a été réalisé en 1980.
    Le film Ça peut pas être l'hiver on n'a même pas eu d'été a été réalisé en 1980.
    Le film Le chef se déniaise a été réalisé en 1980.
    Le film Contrecœur a été réalisé en 1980.
    Le film La cuisine rouge a été réalisé en 1980.
    Le film Fantastica a été réalisé en 1980.
    Le film Final Assignment a été réalisé en 1980.
    Le film Les grands enfants a été réalisé en 1980.
    Le film Hog Wild a été réalisé en 1980.
    Le film L'homme à tout faire a été réalisé en 1980.
    Le film Hot Dogs a été réalisé en 1980.
    Le film The Lucky Star a été réalisé en 1980.
    Le film Pinball Summer a été réalisé en 1980.
    Le film Strass café a été réalisé en 1980.
    Le film Suzanne a été réalisé en 1980.
    Le film Terror Train a été réalisé en 1980.
    Le film Thetford au milieu de notre vie a été réalisé en 1980.
    Le film Les beaux souvenirs a été réalisé en 1981.
    Le film Black Mirror a été réalisé en 1981.
    Le film Deux super dingues a été réalisé en 1981.
    Le film The Funny Farm a été réalisé en 1981.
    Le film Gas a été réalisé en 1981.
    Le film La guerre du feu a été réalisé en 1981.
    Le film Happy Birthday to Me a été réalisé en 1981.
    Le film Hard Feelings a été réalisé en 1981.
    Le film Heartaches a été réalisé en 1981.
    Le film Hot Touch a été réalisé en 1981.
    Le film My Bloody Valentine a été réalisé en 1981.
    Le film Les Plouffe a été réalisé en 1981.
    Le film Salut! J.W. a été réalisé en 1981.
    Le film Le shift de nuit a été réalisé en 1981.
    Le film Ups & Downs a été réalisé en 1981.
    Le film Yesterday a été réalisé en 1981.
    Le film Your Ticket is No Longer Valid a été réalisé en 1981.
    Le film Cross Country a été réalisé en 1982.
    Le film Doux aveux a été réalisé en 1982.
    Le film Les fleurs sauvages a été réalisé en 1982.
    Le film Le futur intérieur a été réalisé en 1982.
    Le film Killing 'em Softly a été réalisé en 1982.
    Le film Larose, Pierrot et la Luce a été réalisé en 1982.
    Le film Luc ou La part des choses a été réalisé en 1982.
    Le film Paradise a été réalisé en 1982.
    Le film La quarantaine a été réalisé en 1982.
    Le film Scandale a été réalisé en 1982.
    Le film Une journée en taxi a été réalisé en 1982.
    Le film Visiting Hours a été réalisé en 1982.
    Le film Les yeux rouges ou Les vérités accidentelles a été réalisé en 1982.
    Le film Au clair de la lune a été réalisé en 1983.
    Le film Au pays de Zom a été réalisé en 1983.
    Le film Bonheur d'occasion a été réalisé en 1983.
    Le film Lucien Brouillard a été réalisé en 1983.
    Le film Maria Chapdelaine a été réalisé en 1983.
    Le film Porky's 2: The Next Day a été réalisé en 1983.
    Le film Rien qu'un jeu a été réalisé en 1983.
    Le film Le ruffian a été réalisé en 1983.
    Le film Snapshot a été réalisé en 1983.
    Le film Videodrome a été réalisé en 1983.
    Le film The Wars a été réalisé en 1983.
    Le film Les Années de rêves a été réalisé en 1984.
    Le film The Bay Boy a été réalisé en 1984.
    Le film La couleur encerclée a été réalisé en 1984.
    Le film Le crime d'Ovide Plouffe a été réalisé en 1984.
    Le film Evil Judgment a été réalisé en 1984.
    Le film La femme de l'hôtel a été réalisé en 1984.
    Le film La Guerre des tuques a été réalisé en 1984.
    Le film Hey Babe! a été réalisé en 1984.
    Le film The Hotel New Hampshire a été réalisé en 1984.
    Le film Jacques et Novembre a été réalisé en 1984.
    Le film Le jour « S... » a été réalisé en 1984.
    Le film Louisiana a été réalisé en 1984.
    Le film Mario a été réalisé en 1984.
    Le film The Masculine Mystique a été réalisé en 1984.
    Le film Memoirs a été réalisé en 1984.
    Le film Mother's Meat Freuds Flesh a été réalisé en 1984.
    Le film Paroles et musiques a été réalisé en 1984.
    Le film Perfect Timing a été réalisé en 1984.
    Le film Le sang des autres a été réalisé en 1984.
    Le film Sonatine a été réalisé en 1984.
    Le film The Surrogate a été réalisé en 1984.
    Le film 90 Days a été réalisé en 1985.
    Le film L'adolescente sucre d'amour a été réalisé en 1985.
    Le film Adramélech a été réalisé en 1985.
    Le film Bayo a été réalisé en 1985.
    Le film The Blue Man a été réalisé en 1985.
    Le film Caffè Italia Montréal a été réalisé en 1985.
    Le film Celui qui voit les heures a été réalisé en 1985.
    Le film La dame en couleurs a été réalisé en 1985.
    Le film Le dernier glacier a été réalisé en 1985.
    Le film Elvis Gratton : Le king des kings a été réalisé en 1985.
    Le film The Gunrunner a été réalisé en 1985.
    Le film Instantanés a été réalisé en 1985.
    Le film Joshua Then and Now a été réalisé en 1985.
    Le film Junior a été réalisé en 1985.
    Le film Lune de miel a été réalisé en 1985.
    Le film Le matou a été réalisé en 1985.
    Le film Medium blues a été réalisé en 1985.
    Le film Night Magic a été réalisé en 1985.
    Le film Opération beurre de pinottes a été réalisé en 1985.
    Le film Québec, opération Lambda a été réalisé en 1985.
    Le film Visage pâle a été réalisé en 1985.
    Le film Anne Trister a été réalisé en 1986.
    Le film Bach et Bottine a été réalisé en 1986.
    Le film The Boy in Blue a été réalisé en 1986.
    Le film Cat Squad a été réalisé en 1986.
    Le film Contes des mille et un jours a été réalisé en 1986.
    Le film Deaf & Mute a été réalisé en 1986.
    Le film Le Déclin de l'empire américain a été réalisé en 1986.
    Le film Le dernier havre a été réalisé en 1986.
    Le film Équinoxe a été réalisé en 1986.
    Le film Evixion a été réalisé en 1986.
    Le film The First Killing Frost a été réalisé en 1986.
    Le film Exit a été réalisé en 1986.
    Le film Les fous de bassan a été réalisé en 1986.
    Le film La guêpe a été réalisé en 1986.
    Le film Henri a été réalisé en 1986.
    Le film L'homme renversé a été réalisé en 1986.
    Le film Meatballs 3 a été réalisé en 1986.
    Le film The Morning Man a été réalisé en 1986.
    Le film Overnight a été réalisé en 1986.
    Le film Pouvoir intime a été réalisé en 1986.
    Le film Qui a tiré sur nos histoires d'amour? a été réalisé en 1986.
    Le film Sauve-toi Lola a été réalisé en 1986.
    Le film Sitting in Limbo a été réalisé en 1986.
    Le film Toby McTeague a été réalisé en 1986.
    Le film À l'automne de la vie a été réalisé en 1987.
    Le film The Great Land of Small a été réalisé en 1987.
    Le film Le frère André a été réalisé en 1987.
    Le film Grelots rouges sanglots bleus a été réalisé en 1987.
    Le film La Guerre oubliée a été réalisé en 1987.
    Le film Hitting Home a été réalisé en 1987.
    Le film L'Île a été réalisé en 1987.
    Le film Le Jeune Magicien a été réalisé en 1987.
    Le film Keeping Track a été réalisé en 1987.
    Le film The Kid Brother ou Kenny a été réalisé en 1987.
    Le film The Last Straw a été réalisé en 1987.
    Le film La ligne de chaleur a été réalisé en 1987.
    Le film Marie s'en va-t-en ville a été réalisé en 1987.
    Le film Les roses de Matmata a été réalisé en 1987.
    Le film Seductio a été réalisé en 1987.
    Le film Le sourd dans la ville a été réalisé en 1987.
    Le film Tinamer a été réalisé en 1987.
    Le film Train of Dreams a été réalisé en 1987.
    Le film Tristesse, modèle réduit a été réalisé en 1987.
    Le film Un zoo la nuit a été réalisé en 1987.
    Le film À corps perdu a été réalisé en 1988.
    Le film La boîte à soleil a été réalisé en 1988.
    Le film La bottega del orefice a été réalisé en 1988.
    Le film Clair-obscur a été réalisé en 1988.
    Le film Eva Guerrillera a été réalisé en 1988.
    Le film Family Reunion a été réalisé en 1988.
    Le film Gaspard et fils a été réalisé en 1988.
    Le film La Grenouille et la Baleine a été réalisé en 1988.
    Le film Horses in Winter a été réalisé en 1988.
    Le film Kalamazoo a été réalisé en 1988.
    Le film Moonlight Flight a été réalisé en 1988.
    Le film La nuit avec Hortense a été réalisé en 1988.
    Le film Oh! Oh! Satan! a été réalisé en 1988.
    Le film La peau et les os a été réalisé en 1988.
    Le film Les Portes tournantes a été réalisé en 1988.
    Le film Salut Victor a été réalisé en 1988.
    Le film Something About Love a été réalisé en 1988.
    Le film Les Tisserands du pouvoir a été réalisé en 1988.
    Le film Tommy Tricker and the Stamp Traveller a été réalisé en 1988.
    Le film L'Air de rien a été réalisé en 1989.
    Le film The Amityville Curse a été réalisé en 1989.
    Le film Bye bye Chaperon rouge a été réalisé en 1989.
    Le film Cher frangin a été réalisé en 1989.
    Le film Comment faire l'amour avec un nègre sans se fatiguer a été réalisé en 1989.
    Le film Cruising Bar a été réalisé en 1989.
    Le film Dans le ventre du dragon a été réalisé en 1989.
    Le film Fierro… l'été des secrets a été réalisé en 1989.
    Le film Jésus de Montréal a été réalisé en 1989.
    Le film Laura Laur a été réalisé en 1989.
    Le film Matinée a été réalisé en 1989.
    Le film Les matins infidèles a été réalisé en 1989.
    Le film Mindfield a été réalisé en 1989.
    Le film Les Noces de papier a été réalisé en 1989.
    Le film Off Off Off a été réalisé en 1989.
    Le film Portion d'éternité a été réalisé en 1989.
    Le film La Réception a été réalisé en 1989.
    Le film La Révolution française : les années terribles a été réalisé en 1989.
    Le film Short Change a été réalisé en 1989.
    Le film Sous les draps, les étoiles a été réalisé en 1989.
    Le film T'es belle Jeanne a été réalisé en 1989.
    Le film Trois pommes à côté du sommeil a été réalisé en 1989.
    Le film Vent de galerne a été réalisé en 1989.
    Le film Welcome to Canada a été réalisé en 1989.
    Le film Babylone a été réalisé en 1990.
    Le film Back Stab a été réalisé en 1990.
    Le film Bethune: The Making of a Hero a été réalisé en 1990.
    Le film Breadhead a été réalisé en 1990.
    Le film A Bullet in the Head a été réalisé en 1990.
    Le film Cargo a été réalisé en 1990.
    Le film The Company of Strangers a été réalisé en 1990.
    Le film Cursed a été réalisé en 1990.
    Le film Dames galantes a été réalisé en 1990.
    Le film Ding et Dong, le film a été réalisé en 1990.
    Le film Falling Over Backwards a été réalisé en 1990.
    Le film La Fille du maquignon a été réalisé en 1990.
    Le film I'm Happy. You're Happy. We're All Happy. Happy, Happy, Happy. a été réalisé en 1990.
    Le film La Liberté d'une statue a été réalisé en 1990.
    Le film Lola Zipper a été réalisé en 1990.
    Le film Manuel : le fils emprunté a été réalisé en 1990.
    Le film Moody Beach a été réalisé en 1990.
    Le film New York doré a été réalisé en 1990.
    Le film Le Party a été réalisé en 1990.
    Le film Pas de répit pour Mélanie a été réalisé en 1990.
    Le film Princes in Exile a été réalisé en 1990.
    Le film Rafales a été réalisé en 1990.
    Le film Remous a été réalisé en 1990.
    Le film Le Royaume ou l'asile a été réalisé en 1990.
    Le film The Secret of Nandy a été réalisé en 1990.
    Le film Simon les nuages a été réalisé en 1990.
    Le film Un autre homme a été réalisé en 1990.
    Le film Un été après l'autre a été réalisé en 1990.
    Le film Une histoire inventée a été réalisé en 1990.
    Le film Alisée a été réalisé en 1991.
    Le film Amoureux fou a été réalisé en 1991.
    Le film L'annonce faite à Marie a été réalisé en 1991.
    Le film L'assassin jouait du trombone a été réalisé en 1991.
    Le film La Championne a été réalisé en 1991.
    Le film … comme un voleur a été réalisé en 1991.
    Le film The Dance Goes On a été réalisé en 1991.
    Le film Les Danseurs du Mozambique a été réalisé en 1991.
    Le film Deadly Surveillance a été réalisé en 1991.
    Le film La Demoiselle sauvage a été réalisé en 1991.
    Le film Le Fabuleux voyage de l'ange a été réalisé en 1991.
    Le film The Final Heist a été réalisé en 1991.
    Le film Lana in Love a été réalisé en 1991.
    Le film Love-moi a été réalisé en 1991.
    Le film Montréal vu par… a été réalisé en 1991.
    Le film Motyli Cas a été réalisé en 1991.
    Le film Nelligan a été réalisé en 1991.
    Le film Pablo qui court a été réalisé en 1991.
    Le film The Pianist a été réalisé en 1991.
    Le film La Révolution française : les années lumières a été réalisé en 1991.
    Le film Scanners 2: The New Order a été réalisé en 1991.
    Le film Sous le signe du poisson a été réalisé en 1991.
    Le film Un Léger vertige a été réalisé en 1991.
    Le film Vincent et moi a été réalisé en 1991.
    Le film Aline a été réalisé en 1992.
    Le film L'Automne sauvage a été réalisé en 1992.
    Le film Being at Home with Claude a été réalisé en 1992.
    Le film La Bête de foire a été réalisé en 1992.
    Le film Call of the Wild a été réalisé en 1992.
    Le film Canvas a été réalisé en 1992.
    Le film Coyote a été réalisé en 1992.
    Le film A Cry in the Night a été réalisé en 1992.
    Le film Deadbolt a été réalisé en 1992.
    Le film La Fenêtre a été réalisé en 1992.
    Le film Francœur: exit pour nomades a été réalisé en 1992.
    Le film L'Homme de ma vie a été réalisé en 1992.
    Le film Killer Image a été réalisé en 1992.
    Le film El lado oscuro del corazón a été réalisé en 1992.
    Le film Léolo a été réalisé en 1992.
    Le film Le Mirage a été réalisé en 1992.
    Le film La Postière a été réalisé en 1992.
    Le film Psychic a été réalisé en 1992.
    Le film Requiem pour un beau sans-cœur a été réalisé en 1992.
    Le film La Sarrasine a été réalisé en 1992.
    Le film Scanners 3: The Takeover a été réalisé en 1992.
    Le film Shadow of the Wolf a été réalisé en 1992.
    Le film Tirelire Combines & Cie a été réalisé en 1992.
    Le film La Vie fantôme a été réalisé en 1992.
    Le film Le Voleur de caméra a été réalisé en 1992.
    Le film Les Amoureuses a été réalisé en 1993.
    Le film Armen et Bullik a été réalisé en 1993.
    Le film Because Why a été réalisé en 1993.
    Le film Cap Tourmente a été réalisé en 1993.
    Le film Deux actrices a été réalisé en 1993.
    Le film Doublures a été réalisé en 1993.
    Le film La Florida a été réalisé en 1993.
    Le film L'Homme sur les quais a été réalisé en 1993.
    Le film Love & Human Remains a été réalisé en 1993.
    Le film Matusalem a été réalisé en 1993.
    Le film Les Mots perdus : un film en quatre saisons a été réalisé en 1993.
    Le film The Myth of the Male Orgasm a été réalisé en 1993.
    Le film The Neighbor a été réalisé en 1993.
    Le film Pamietnik znaleziony w garbie a été réalisé en 1993.
    Le film Les Pots cassés a été réalisé en 1993.
    Le film Saint-Fortunat, les blues a été réalisé en 1993.
    Le film Le Sexe des étoiles a été réalisé en 1993.
    Le film Tendre guerre a été réalisé en 1993.
    Le film Thirty Two Short Films About Glenn Gould a été réalisé en 1993.
    Le film La Visite a été réalisé en 1993.
    Le film C'était le 12 du 12 et Chili avait les blues a été réalisé en 1994.
    Le film The Child a été réalisé en 1994.
    Le film Crack Me Up a été réalisé en 1994.
    Le film Craque la vie ! a été réalisé en 1994.
    Le film Draghoula a été réalisé en 1994.
    Le film La Fête des rois a été réalisé en 1994.
    Le film Gaïa a été réalisé en 1994.
    Le film El jardín del edén a été réalisé en 1994.
    Le film Kabloonak a été réalisé en 1994.
    Le film Le Lac de la lune a été réalisé en 1994.
    Le film Louis 19, le roi des ondes a été réalisé en 1994.
    Le film Ma sœur, mon amour a été réalisé en 1994.
    Le film Mon amie Max a été réalisé en 1994.
    Le film Mouvements du désir a été réalisé en 1994.
    Le film Los Naufragos a été réalisé en 1994.
    Le film Octobre a été réalisé en 1994.
    Le film The Paperboy a été réalisé en 1994.
    Le film Le Retour des aventuriers du timbre perdu a été réalisé en 1994.
    Le film Rêve aveugle a été réalisé en 1994.
    Le film Ride Me a été réalisé en 1994.
    Le film Ruth a été réalisé en 1994.
    Le film Le Secret de Jérôme a été réalisé en 1994.
    Le film Stalked a été réalisé en 1994.
    Le film V'la l'cinéma ou le roman de Charles Pathé a été réalisé en 1994.
    Le film Le Vent du Wyoming a été réalisé en 1994.
    Le film La Vie d'un héros a été réalisé en 1994.
    Le film Warriors a été réalisé en 1994.
    Le film Windigo a été réalisé en 1994.
    Le film Yes sir! Madame… a été réalisé en 1994.
    Le film Angelo, Frédo et Roméo a été réalisé en 1995.
    Le film Le Confessionnal a été réalisé en 1995.
    Le film Eldorado a été réalisé en 1995.
    Le film L'Enfant d'eau a été réalisé en 1995.
    Le film Erreur sur la personne a été réalisé en 1995.
    Le film La folie des crinolines a été réalisé en 1995.
    Le film Highlander 3: The Sorcerer a été réalisé en 1995.
    Le film Hollow Point a été réalisé en 1995.
    Le film J'aime j'aime pas a été réalisé en 1995.
    Le film Le jour des cris a été réalisé en 1995.
    Le film Kids of the Round Table a été réalisé en 1995.
    Le film Liste noire a été réalisé en 1995.
    Le film Motel a été réalisé en 1995.
    Le film La Mule et les Émeraudes a été réalisé en 1995.
    Le film Screamers a été réalisé en 1995.
    Le film Silent Hunter a été réalisé en 1995.
    Le film Le Sphinx a été réalisé en 1995.
    Le film Suburban Legend a été réalisé en 1995.
    Le film Witchboard 3 a été réalisé en 1995.
    Le film Wrong Woman a été réalisé en 1995.
    Le film Zigrail a été réalisé en 1995.
    Le film Aire libre a été réalisé en 1996.
    Le film La Ballade de Titus : une fable tout en couleur a été réalisé en 1996.
    Le film La Beauté, fatale et féroce a été réalisé en 1996.
    Le film Bullet to Beijing a été réalisé en 1996.
    Le film Caboose a été réalisé en 1996.
    Le film Cosmos a été réalisé en 1996.
    Le film Le Cri de la nuit a été réalisé en 1996.
    Le film L'Escorte a été réalisé en 1996.
    Le film La Fabrication d'un meurtrier a été réalisé en 1996.
    Le film L'Homme idéal a été réalisé en 1996.
    Le film L'Homme perché a été réalisé en 1996.
    Le film Hot Sauce a été réalisé en 1996.
    Le film J'en suis ! a été réalisé en 1996.
    Le film Joyeux Calvaire a été réalisé en 1996.
    Le film Karmina a été réalisé en 1996.
    Le film Lilies a été réalisé en 1996.
    Le film La Marche à l'amour a été réalisé en 1996.
    Le film Never Too Late a été réalisé en 1996.
    Le film L'Oreille d'un sourd a été réalisé en 1996.
    Le film Panic a été réalisé en 1996.
    Le film Le Polygraphe a été réalisé en 1996.
    Le film Pudding chômeur a été réalisé en 1996.
    Le film Rowing Through a été réalisé en 1996.
    Le film Le Silence des fusils a été réalisé en 1996.
    Le film Sous-sol a été réalisé en 1996.
    Le film The Strange Blues of Cowboy Red a été réalisé en 1996.
    Le film La Vengeance de la femme en noir a été réalisé en 1996.
    Le film The Windsor Protocol a été réalisé en 1996.
    Le film L'absent a été réalisé en 1997.
    Le film The Assignment a été réalisé en 1997.
    Le film Les Boys a été réalisé en 1997.
    Le film Burnt Eden a été réalisé en 1997.
    Le film Cabaret neiges noires a été réalisé en 1997.
    Le film The Call of the Wild a été réalisé en 1997.
    Le film Le Ciel est à nous a été réalisé en 1997.
    Le film Clandestins a été réalisé en 1997.
    Le film La Comtesse de Bâton Rouge a été réalisé en 1997.
    Le film La Conciergerie a été réalisé en 1997.
    Le film De l'autre côté du cœur a été réalisé en 1997.
    Le film Habitat a été réalisé en 1997.
    Le film Hemoglobin a été réalisé en 1997.
    Le film Kayla a été réalisé en 1997.
    Le film Maîtres anciens a été réalisé en 1997.
    Le film Matusalem II : le dernier des Beauchesne a été réalisé en 1997.
    Le film Les Mille merveilles de l'univers a été réalisé en 1997.
    Le film Moustaches a été réalisé en 1997.
    Le film Reaper a été réalisé en 1997.
    Le film Sinon, oui a été réalisé en 1997.
    Le film Le Siège de l'âme a été réalisé en 1997.
    Le film Stranger in the House a été réalisé en 1997.
    Le film Strip Search a été réalisé en 1997.
    Le film Treasure Island a été réalisé en 1997.
    Le film 2 secondes a été réalisé en 1998.
    Le film L'âge de braise a été réalisé en 1998.
    Le film Aujourd'hui ou jamais a été réalisé en 1998.
    Le film Babel a été réalisé en 1998.
    Le film Les Boys 2 a été réalisé en 1998.
    Le film C't'à ton tour, Laura Cadieux a été réalisé en 1998.
    Le film Captive a été réalisé en 1998.
    Le film Le Cœur au poing a été réalisé en 1998.
    Le film Dancing on the Moon a été réalisé en 1998.
    Le film Dead End a été réalisé en 1998.
    Le film La Déroute a été réalisé en 1998.
    Le film Fatal Affair a été réalisé en 1998.
    Le film Foreign Ghosts a été réalisé en 1998.
    Le film Going to Kansas City a été réalisé en 1998.
    Le film Le grand serpent du monde a été réalisé en 1998.
    Le film Hasards ou Coïncidences a été réalisé en 1998.
    Le film Hathi a été réalisé en 1998.
    Le film In Her Defense a été réalisé en 1998.
    Le film N.I.P. a été réalisé en 1998.
    Le film Nguol thùa a été réalisé en 1998.
    Le film Nico the Unicorn a été réalisé en 1998.
    Le film Nô a été réalisé en 1998.
    Le film La Position de l'escargot a été réalisé en 1998.
    Le film The Press Run a été réalisé en 1998.
    Le film Quelque chose d'organique a été réalisé en 1998.
    Le film Quiconque meurt, meurt à douleur a été réalisé en 1998.
    Le film Random Encounter a été réalisé en 1998.
    Le film Requiem for Murder a été réalisé en 1998.
    Le film Revoir Julie a été réalisé en 1998.
    Le film Running Home a été réalisé en 1998.
    Le film Snitch a été réalisé en 1998.
    Le film Sublet a été réalisé en 1998.
    Le film Thunder Point a été réalisé en 1998.
    Le film Un 32 août sur Terre a été réalisé en 1998.
    Le film Une aventure de Papyrus : La vengeance de Seth a été réalisé en 1998.
    Le film Le Violon rouge a été réalisé en 1998.
    Le film Winter Lily a été réalisé en 1998.
    Le film You Can Thank Me Later a été réalisé en 1998.
    Le film 4 Days a été réalisé en 1999.
    Le film Artificial Lies a été réalisé en 1999.
    Le film L'Autobiographe amateur a été réalisé en 1999.
    Le film Autour de la maison rose a été réalisé en 1999.
    Le film Le Dernier Souffle a été réalisé en 1999.
    Le film Elvis Gratton 2 : Miracle à Memphis a été réalisé en 1999.
    Le film Emporte-moi a été réalisé en 1999.
    Le film La Fabrication des mâles a été réalisé en 1999.
    Le film Fish Out of Water a été réalisé en 1999.
    Le film Full Blast a été réalisé en 1999.
    Le film Grey Owl a été réalisé en 1999.
    Le film Histoires d'hiver a été réalisé en 1999.
    Le film Home Team a été réalisé en 1999.
    Le film Laura Cadieux… la suite a été réalisé en 1999.
    Le film Matroni et moi a été réalisé en 1999.
    Le film Le Petit ciel a été réalisé en 1999.
    Le film Pin-Pon, le film a été réalisé en 1999.
    Le film Post mortem a été réalisé en 1999.
    Le film Quand je serai parti… vous vivrez encore a été réalisé en 1999.
    Le film Les Siamoises a été réalisé en 1999.
    Le film Souffle d'ailleurs a été réalisé en 1999.
    Le film Souvenirs intimes a été réalisé en 1999.
    Le film Task Force a été réalisé en 1999.
    Le film Taxman a été réalisé en 1999.
    Le film Who Gets the House? a été réalisé en 1999.
    Le film The Witness Files a été réalisé en 1999.


Enfin, pour vous donner un exemple de l'utilisation de ```if```, de ```elif``` et de ```else```, je vais me servir de la variable ```olympiques```.

Le code ci-dessous vérifie trois conditions différentes, pour produire quatre résultats différents.


```python
for annee in olympiques:
    if annee == 1916:
        print("Les jeux de {} ont été annulés en raison de la Première Guerre mondiale".format(annee))
    elif annee == 1940 or annee == 1944:
        print("Les jeux de {} ont été annulés en raison de la Seconde Guerre mondiale".format(annee))
    elif annee == 2020:
        print("Les jeux de {} ont été reportés en raison de la pandémie de COVID-19".format(annee))
    else:
        print("Les jeux de {} ont bel et bien eu lieu".format(annee))
```

    Les jeux de 1896 ont bel et bien eu lieu
    Les jeux de 1900 ont bel et bien eu lieu
    Les jeux de 1904 ont bel et bien eu lieu
    Les jeux de 1908 ont bel et bien eu lieu
    Les jeux de 1912 ont bel et bien eu lieu
    Les jeux de 1916 ont été annulés en raison de la Première Guerre mondiale
    Les jeux de 1920 ont bel et bien eu lieu
    Les jeux de 1924 ont bel et bien eu lieu
    Les jeux de 1928 ont bel et bien eu lieu
    Les jeux de 1932 ont bel et bien eu lieu
    Les jeux de 1936 ont bel et bien eu lieu
    Les jeux de 1940 ont été annulés en raison de la Seconde Guerre mondiale
    Les jeux de 1944 ont été annulés en raison de la Seconde Guerre mondiale
    Les jeux de 1948 ont bel et bien eu lieu
    Les jeux de 1952 ont bel et bien eu lieu
    Les jeux de 1956 ont bel et bien eu lieu
    Les jeux de 1960 ont bel et bien eu lieu
    Les jeux de 1964 ont bel et bien eu lieu
    Les jeux de 1968 ont bel et bien eu lieu
    Les jeux de 1972 ont bel et bien eu lieu
    Les jeux de 1976 ont bel et bien eu lieu
    Les jeux de 1980 ont bel et bien eu lieu
    Les jeux de 1984 ont bel et bien eu lieu
    Les jeux de 1988 ont bel et bien eu lieu
    Les jeux de 1992 ont bel et bien eu lieu
    Les jeux de 1996 ont bel et bien eu lieu
    Les jeux de 2000 ont bel et bien eu lieu
    Les jeux de 2004 ont bel et bien eu lieu
    Les jeux de 2008 ont bel et bien eu lieu
    Les jeux de 2012 ont bel et bien eu lieu
    Les jeux de 2016 ont bel et bien eu lieu
    Les jeux de 2020 ont été reportés en raison de la pandémie de COVID-19

